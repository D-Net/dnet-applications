package eu.dnetlib.broker.common.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;

public class MapValueTest {

	@Test
	public void test_null_value() {
		final MapValue v = new MapValue(MapValueType.STRING, null);
		assertEquals(v.asObject(), null);
	}

	@Test
	public void test_null_type() {
		final MapValue v = new MapValue(null, "XXX");
		assertEquals(v.asObject(), null);
	}

	@Test
	public void test_string() {
		final MapValue v = new MapValue(MapValueType.STRING, "pippo");
		assertEquals(v.asObject(), "pippo");
	}

	@Test
	public void test_int() {
		final MapValue v = new MapValue(MapValueType.INTEGER, 10);
		assertEquals(v.asObject(), 10L);
	}

	@Test
	public void test_int_s() {
		final MapValue v = new MapValue(MapValueType.INTEGER, "10");
		assertEquals(v.asObject(), 10L);
	}

	@Test
	public void test_float() {
		final MapValue v = new MapValue(MapValueType.FLOAT, -10.22);
		assertEquals(v.asObject(), -10.22d);
	}

	@Test
	public void test_float_s() {
		final MapValue v = new MapValue(MapValueType.FLOAT, "-10.22");
		assertEquals(v.asObject(), -10.22d);
	}

	@Test
	public void test_boolean_true() {
		final MapValue v = new MapValue(MapValueType.BOOLEAN, "true");
		final Boolean b = (Boolean) v.asObject();
		assertTrue(b);
	}

	@Test
	public void test_boolean_false() {
		final MapValue v = new MapValue(MapValueType.BOOLEAN, "false");
		final Boolean b = (Boolean) v.asObject();
		assertFalse(b);
	}

	@Test
	public void test_boolean_invalid() {
		final MapValue v = new MapValue(MapValueType.BOOLEAN, "csscx");
		final Boolean b = (Boolean) v.asObject();
		assertFalse(b);
	}

	@Test
	public void test_date_1() {
		final MapValue v = new MapValue(MapValueType.DATE, "2012-11-23");
		final Date date = (Date) v.asObject();

		assertNotNull(date);
		assertTrue(date.getTime() > 0);
	}

	@Test
	public void test_date_2() {
		final MapValue v = new MapValue(MapValueType.DATE, "XXXX");
		final Date date = (Date) v.asObject();
		assertNull(date);
	}

	@Test
	public void test_list_string() {
		final MapValue v = new MapValue(MapValueType.LIST_STRING, Arrays.asList("AAA", "BBB", "CCC"));
		final List<?> list = (List<?>) v.asObject();
		assertNotNull(list);
		assertEquals(list.size(), 3);
		assertEquals(list.get(0), "AAA");
		assertEquals(list.get(1), "BBB");
		assertEquals(list.get(2), "CCC");
	}

	@Test
	public void test_list_int() {
		final MapValue v = new MapValue(MapValueType.LIST_INTEGER, Arrays.asList("1", "2", 3, 4));
		final List<?> list = (List<?>) v.asObject();
		assertNotNull(list);
		assertEquals(list.size(), 4);
		assertEquals(list.get(0), 1L);
		assertEquals(list.get(1), 2L);
		assertEquals(list.get(2), 3L);
		assertEquals(list.get(3), 4L);
	}

	@Test
	public void test_list_float() {
		final MapValue v = new MapValue(MapValueType.LIST_FLOAT, Arrays.asList("1.02", "-2.3", 3.2, -4.3));
		final List<?> list = (List<?>) v.asObject();
		assertNotNull(list);
		assertEquals(list.size(), 4);
		assertEquals(list.get(0), 1.02d);
		assertEquals(list.get(1), -2.3d);
		assertEquals(list.get(2), 3.2d);
		assertEquals(list.get(3), -4.3d);
	}

	@Test
	public void test_list_boolean() {
		final MapValue v = new MapValue(MapValueType.LIST_BOOLEAN, Arrays.asList("true", "false", true, false));
		final List<?> list = (List<?>) v.asObject();
		assertNotNull(list);
		assertEquals(list.size(), 4);
		assertTrue((Boolean) list.get(0));
		assertFalse((Boolean) list.get(1));
		assertTrue((Boolean) list.get(2));
		assertFalse((Boolean) list.get(3));
	}

	@Test
	public void test_list_date() {
		final MapValue v = new MapValue(MapValueType.LIST_DATE, Arrays.asList("2012-05-01", "2012-05-02"));
		final List<?> list = (List<?>) v.asObject();
		assertNotNull(list);
		assertEquals(list.size(), 2);
		assertTrue(list.get(0) instanceof Date);
		assertTrue(list.get(1) instanceof Date);
	}

}
