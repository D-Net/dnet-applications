#!/bin/bash

INDEXBASEURL=http://localhost:9200
EVENTINDEXNAME=oa_events_beta
NOTIFICATIONINDEXNAME=oa_notifications_beta


echo
echo "Update settings of  events index..."
curl -XPOST "$INDEXBASEURL/$EVENTINDEXNAME/_close"
echo
curl -XPUT -H 'Content-Type: application/json' "$INDEXBASEURL/$EVENTINDEXNAME/_settings" -d '
{
	"index": {
		"translog": {
			"sync_interval": "15s",
			"durability": "ASYNC" 
		},
		"analysis": {
			"analyzer": {
				"analyzer_keyword": {
					"filter": "lowercase",
					"tokenizer": "keyword" 
				}
			}
		}
	}
}'
echo
curl -XPOST "$INDEXBASEURL/$EVENTINDEXNAME/_open"

echo
echo
echo "Update settings of  notifications index..."
curl -XPOST "$INDEXBASEURL/$NOTIFICATIONINDEXNAME/_close"
echo
curl -XPUT -H 'Content-Type: application/json' "$INDEXBASEURL/$NOTIFICATIONINDEXNAME/_settings" -d '
{
	"index": {
		"translog": {
			"sync_interval": "15s",
			"durability": "ASYNC" 
		},
		"analysis": {
			"analyzer": {
				"analyzer_keyword": {
					"filter": "lowercase",
					"tokenizer": "keyword" 
				}
			}
		}
	}
}'
echo
curl -XPOST "$INDEXBASEURL/$NOTIFICATIONINDEXNAME/_open"

echo
echo
echo "Fixing events index..."
curl -XPUT -H 'Content-Type: application/json' "$INDEXBASEURL/$EVENTINDEXNAME/_mapping" -d '
{
	"properties" : {
		"map": {
			"type": "nested",
			"properties": {
				"targetDatasourceId": {
					"type": "keyword",
					"index": true
				},
				"targetDatasourceName": {
					"type": "keyword",
					"index": true
				},
				"targetDatasourceType": {
					"type": "keyword",
					"index": true
				}
			}
		}
	}
}'

echo
echo
echo "Fixing notifications index..."
curl -XPUT -H 'Content-Type: application/json' "$INDEXBASEURL/$NOTIFICATIONINDEXNAME/_mapping" -d '
{
	"properties" : {
		"map": {
			"type": "nested",
			"properties": {
				"targetDatasourceId": {
					"type": "keyword",
					"index": true
				},
				"targetDatasourceName": {
					"type": "keyword",
					"index": true
				},
				"targetDatasourceType": {
					"type": "keyword",
					"index": true
				}
			}
		}
	}
}'

echo
echo