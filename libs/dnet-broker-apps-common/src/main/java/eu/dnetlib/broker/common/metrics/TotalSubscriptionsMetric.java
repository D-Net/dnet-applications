package eu.dnetlib.broker.common.metrics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.dnetlib.common.metrics.MetricInfo;

@Component("provide_broker_subscriptions")
public class TotalSubscriptionsMetric implements MetricInfo {

	@Autowired
	private MetricsCacheUtils metricsCacheUtils;

	@Override
	public double obtainValue() {
		return metricsCacheUtils.countSubscriptions();
	}

}
