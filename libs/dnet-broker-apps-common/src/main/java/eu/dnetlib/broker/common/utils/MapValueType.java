package eu.dnetlib.broker.common.utils;

public enum MapValueType {
	STRING, INTEGER, FLOAT, DATE, BOOLEAN, LIST_STRING, LIST_INTEGER, LIST_FLOAT, LIST_DATE, LIST_BOOLEAN;
}
