package eu.dnetlib.broker.common.feedbacks;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity(name = "feedbacks")
@Table(name = "feedbacks")
public class DbEventFeedback implements Serializable {

	private static final long serialVersionUID = -1252332235554824748L;

	@Id
	@Column(name = "eventid")
	private String eventId;

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private FeedbackStatus status;

	@CreatedDate
	@Column(name = "creation_date", nullable = false, updatable = false, columnDefinition = "timestamp without time zone default now()'")
	private Date creationDate = new Date();

	@LastModifiedDate
	@Column(name = "modification_date", nullable = false, columnDefinition = "timestamp without time zone default now()")
	private Date modificationDate = new Date();

	public String getEventId() {
		return eventId;
	}

	public void setEventId(final String eventId) {
		this.eventId = eventId;
	}

	public FeedbackStatus getStatus() {
		return status;
	}

	public void setStatus(final FeedbackStatus status) {
		this.status = status;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(final Date modificationDate) {
		this.modificationDate = modificationDate;
	}
}
