package eu.dnetlib.broker.common.topics;

import org.springframework.data.repository.CrudRepository;

public interface TopicTypeRepository extends CrudRepository<TopicType, String> {

}
