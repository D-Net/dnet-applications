package eu.dnetlib.broker.common.utils;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DateParser {

	private static final Log log = LogFactory.getLog(DateParser.class);

	public static Date parse(final String s) {
		if (s == null) {
			log.warn("Date is NULL");
			return null;
		}
		try {
			return DateUtils.parseDate(s, "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss");
		} catch (final ParseException e) {
			log.warn("Invalid Date: " + s);
			return null;
		}
	}
}
