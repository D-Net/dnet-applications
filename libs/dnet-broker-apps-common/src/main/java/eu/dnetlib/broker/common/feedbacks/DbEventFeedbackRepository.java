package eu.dnetlib.broker.common.feedbacks;

import org.springframework.data.repository.CrudRepository;

public interface DbEventFeedbackRepository extends CrudRepository<DbEventFeedback, String> {

}
