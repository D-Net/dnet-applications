package eu.dnetlib.broker.common.stats;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface OpenaireDsStatRepository extends CrudRepository<OpenaireDsStat, OpenaireDsStatPK> {

	@Query(value = "select sum(size) from oa_datasource_stats", nativeQuery = true)
	long totalEvents();

	@Query(value = "select count(distinct name) from oa_datasource_stats where size > 0", nativeQuery = true)
	long countDatasourcesWithEvents();
}
