package eu.dnetlib.broker.common.feedbacks;

public enum FeedbackStatus {
	DISCARDED,	// the event was not processable by the system. OpenAIRE should not interpret such status in a negative or positive sense
				// with regard to the accuracy of the notification
	REJECTED,	// a human takes the decision to reject the suggestion as it was wrong
	ACCEPTED	// a human takes the decision to apply the suggested enrichment to the local record

}
