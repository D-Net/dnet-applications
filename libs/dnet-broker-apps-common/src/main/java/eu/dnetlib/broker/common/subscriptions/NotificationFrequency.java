package eu.dnetlib.broker.common.subscriptions;

public enum NotificationFrequency {
	never, realtime, daily, weekly, monthly
}
