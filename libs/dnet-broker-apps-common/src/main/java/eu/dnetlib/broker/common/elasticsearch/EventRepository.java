package eu.dnetlib.broker.common.elasticsearch;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends ElasticsearchRepository<Event, String> {
	// TODO: use the @Query annotation if necessary
	// See: http://docs.spring.io/spring-data/elasticsearch/docs/current/reference/html/

	Iterable<Event> findByTopic(String topic);

	Page<Event> findByTopic(String topic, Pageable pageable);

	long deleteByExpiryDateBetween(long from, long to);

	long deleteByCreationDateBetween(long from, long to);

}
