package eu.dnetlib.broker.common.subscriptions;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriptionRepository extends CrudRepository<Subscription, String> {

	Iterable<Subscription> findByTopic(String topic);

	Iterable<Subscription> findBySubscriber(String email);

	@Query(value = "select count(distinct subscriber) from subscriptions", nativeQuery = true)
	long countSubscribers();

}
