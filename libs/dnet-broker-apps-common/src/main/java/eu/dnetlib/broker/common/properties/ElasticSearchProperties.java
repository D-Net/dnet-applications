package eu.dnetlib.broker.common.properties;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component("elasticSearchProperties")
@ConfigurationProperties(prefix = "lbs.elastic")
public class ElasticSearchProperties {

	@NotNull
	private String clusterNodes;
	@NotNull
	private String homepage;
	@NotNull
	private String eventsIndexName;
	@NotNull
	private String notificationsIndexName;
	@NotNull
	private long connectionTimeout;
	@NotNull
	private long socketTimeout;

	public String getClusterNodes() {
		return this.clusterNodes;
	}

	public void setClusterNodes(final String clusterNodes) {
		this.clusterNodes = clusterNodes;
	}

	public String getHomepage() {
		return this.homepage;
	}

	public void setHomepage(final String homepage) {
		this.homepage = homepage;
	}

	public String getEventsIndexName() {
		return this.eventsIndexName;
	}

	public void setEventsIndexName(final String eventsIndexName) {
		this.eventsIndexName = eventsIndexName;
	}

	public String getNotificationsIndexName() {
		return this.notificationsIndexName;
	}

	public void setNotificationsIndexName(final String notificationsIndexName) {
		this.notificationsIndexName = notificationsIndexName;
	}

	public long getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(final long connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public long getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(final long socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

}
