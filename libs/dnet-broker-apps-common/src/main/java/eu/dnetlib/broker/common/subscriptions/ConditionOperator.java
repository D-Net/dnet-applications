package eu.dnetlib.broker.common.subscriptions;

public enum ConditionOperator {
	EXACT, MATCH_ANY, MATCH_ALL, RANGE
}
