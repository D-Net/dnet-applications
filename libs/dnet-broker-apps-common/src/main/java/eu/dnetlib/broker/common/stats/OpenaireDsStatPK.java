package eu.dnetlib.broker.common.stats;

import java.io.Serializable;
import java.util.Objects;

public class OpenaireDsStatPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3061558721019854932L;

	private String id;

	private String topic;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(final String topic) {
		this.topic = topic;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, topic);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof OpenaireDsStatPK)) { return false; }
		final OpenaireDsStatPK other = (OpenaireDsStatPK) obj;
		return Objects.equals(id, other.id) && Objects.equals(topic, other.topic);
	}

	@Override
	public String toString() {
		return String.format("OpenaireDsStatPK [id=%s, topic=%s]", id, topic);
	}
}
