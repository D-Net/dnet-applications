package eu.dnetlib.broker.common.elasticsearch;

import java.util.Date;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import eu.dnetlib.broker.common.subscriptions.Subscription;

@Document(indexName = "#{@elasticSearchProperties.getNotificationsIndexName()}")
public class Notification {

	@Id
	private String notificationId;

	@Field(type = FieldType.Keyword)
	private String subscriptionId;

	@Field(type = FieldType.Keyword)
	private String producerId;

	@Field(type = FieldType.Keyword)
	private String eventId;

	@Field(type = FieldType.Keyword)
	private String topic;

	@Field(type = FieldType.Text)
	private String payload;

	@Field(type = FieldType.Nested)
	private Map<String, Object> map;

	@Field(type = FieldType.Long)
	private Long date;

	public Notification() {}

	public Notification(final String notificationId, final String subscriptionId, final String producerId, final String eventId, final String topic,
		final String payload,
		final Map<String, Object> map, final Long date) {
		super();
		this.notificationId = notificationId;
		this.subscriptionId = subscriptionId;
		this.producerId = producerId;
		this.eventId = eventId;
		this.topic = topic;
		this.payload = payload;
		this.map = map;
		this.date = date;
	}

	public Notification(final Subscription s, final Event e) {
		this(Notification.calculateId(s, e),
			s.getSubscriptionId(),
			e.getProducerId(),
			e.getEventId(),
			e.getTopic(),
			e.getPayload(),
			e.getMap(),
			new Date().getTime());
	}

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(final String notificationId) {
		this.notificationId = notificationId;
	}

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(final String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getProducerId() {
		return producerId;
	}

	public void setProducerId(final String producerId) {
		this.producerId = producerId;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(final String eventId) {
		this.eventId = eventId;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(final Long date) {
		this.date = date;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(final String topic) {
		this.topic = topic;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(final String payload) {
		this.payload = payload;
	}

	public Map<String, Object> getMap() {
		return map;
	}

	public void setMap(final Map<String, Object> map) {
		this.map = map;
	}

	public static String calculateId(final Subscription s, final Event e) {
		return "ntf-" + DigestUtils.md5Hex(s.getSubscriptionId() + "@@@" + e.getEventId());
	}

}
