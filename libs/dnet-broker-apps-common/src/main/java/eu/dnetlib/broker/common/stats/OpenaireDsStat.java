package eu.dnetlib.broker.common.stats;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "oa_datasource_stats")
@IdClass(OpenaireDsStatPK.class)
public class OpenaireDsStat implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6498718759303687338L;

	@Id
	@Column(name = "id", nullable = false)
	private String id;

	@Id
	@Column(name = "topic", nullable = false)
	private String topic;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "type", nullable = false)
	private String type;

	@Column(name = "size", nullable = false)
	private final Long size = 0l;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(final String topic) {
		this.topic = topic;
	}

	public Long getSize() {
		return size;
	}

}
