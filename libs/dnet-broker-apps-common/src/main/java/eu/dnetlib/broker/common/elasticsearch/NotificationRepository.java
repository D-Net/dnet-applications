package eu.dnetlib.broker.common.elasticsearch;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface NotificationRepository extends ElasticsearchRepository<Notification, String> {
	// TODO: use the @Query annotation if necessary
	// See: http://docs.spring.io/spring-data/elasticsearch/docs/current/reference/html/

	Iterable<Notification> findBySubscriptionId(String subscriptionId);

	@Override
	Page<Notification> findAll(Pageable pageable);

	Page<Notification> findByEventId(String topic, Pageable pageable);

	long deleteByDateBefore(long date);

	long countBySubscriptionId(String subscriptionId);

	long countBySubscriptionIdAndDateAfter(String subscriptionId, long from);

	void deleteBySubscriptionId(String subscriptionId);

	long deleteByDateBetween(long from, long to);

}
