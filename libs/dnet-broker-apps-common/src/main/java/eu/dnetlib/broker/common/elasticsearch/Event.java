package eu.dnetlib.broker.common.elasticsearch;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "#{@elasticSearchProperties.getEventsIndexName()}")
public class Event {

	@Id
	private String eventId;

	@Field(type = FieldType.Keyword)
	private String producerId;

	@Field(type = FieldType.Keyword)
	private String topic;

	@Field(type = FieldType.Text)
	private String payload;

	@Field(type = FieldType.Long)
	private Long creationDate;

	@Field(type = FieldType.Long)
	private Long expiryDate;

	@Field(type = FieldType.Boolean)
	private boolean instantMessage;

	@Field(type = FieldType.Nested)
	private Map<String, Object> map;

	public Event() {}

	public Event(final String producerId, final String eventId, final String topic, final String payload, final Long creationDate, final Long expiryDate,
		final boolean instantMessage,
		final Map<String, Object> map) {
		this.producerId = producerId;
		this.eventId = eventId;
		this.topic = topic;
		this.payload = payload;
		this.creationDate = creationDate;
		this.expiryDate = expiryDate;
		this.instantMessage = instantMessage;
		this.map = map;
	}

	public String getProducerId() {
		return producerId;
	}

	public void setProducerId(final String producerId) {
		this.producerId = producerId;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(final String eventId) {
		this.eventId = eventId;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(final String topic) {
		this.topic = topic;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(final String payload) {
		this.payload = payload;
	}

	public Long getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(final Long creationDate) {
		this.creationDate = creationDate;
	}

	public Long getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(final Long expiryDate) {
		this.expiryDate = expiryDate;
	}

	public boolean isInstantMessage() {
		return instantMessage;
	}

	public void setInstantMessage(final boolean instantMessage) {
		this.instantMessage = instantMessage;
	}

	public Map<String, Object> getMap() {
		return map;
	}

	public void setMap(final Map<String, Object> map) {
		this.map = map;
	}

}
