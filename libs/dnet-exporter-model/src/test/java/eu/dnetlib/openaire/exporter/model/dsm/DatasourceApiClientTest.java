package eu.dnetlib.openaire.exporter.model.dsm;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class DatasourceApiClientTest {

	@Test
	public void testaAgregationHistoryV2_local() throws Exception {

		final ObjectMapper mapper = new ObjectMapper();

		final AggregationHistoryResponseV2 res =
				mapper.readValue(IOUtils.toString(getClass().getResourceAsStream("aggregation-info-v2.json"), "UTF-8"), AggregationHistoryResponseV2.class);

		System.out.println(mapper.writeValueAsString(res));

		assertTrue(res.getAggregationInfo().size() > 0);
	}

}
