package eu.dnetlib.openaire.exporter.model.dsm;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Created by claudio on 12/09/16.
 */
@JsonAutoDetect
public enum CollectionMode {
	REFRESH,
	INCREMENTAL
}
