package eu.dnetlib.openaire.exporter.model.funders;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class FunderDetails {

	private String id;

	private String name;

	private String shortname;

	private String jurisdiction;

	private Date registrationDate;

	private Date lastUpdateDate;

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getShortname() {
		return shortname;
	}

	public String getJurisdiction() {
		return jurisdiction;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public FunderDetails setId(final String id) {
		this.id = id;
		return this;
	}

	public FunderDetails setName(final String name) {
		this.name = name;
		return this;
	}

	public FunderDetails setShortname(final String shortname) {
		this.shortname = shortname;
		return this;
	}

	public FunderDetails setJurisdiction(final String jurisdiction) {
		this.jurisdiction = jurisdiction;
		return this;
	}

	public FunderDetails setRegistrationDate(final Date registrationDate) {
		this.registrationDate = registrationDate;
		return this;
	}

	public FunderDetails setLastUpdateDate(final Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
		return this;
	}

}
