package eu.dnetlib.openaire.exporter.model.funders;

@Deprecated
public class FundingStream {

	private String id;

	private String name;

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public FundingStream setId(final String id) {
		this.id = id;
		return this;
	}

	public FundingStream setName(final String name) {
		this.name = name;
		return this;
	}

}
