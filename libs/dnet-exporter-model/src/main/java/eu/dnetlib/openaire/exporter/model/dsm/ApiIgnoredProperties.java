package eu.dnetlib.openaire.exporter.model.dsm;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class ApiIgnoredProperties {

	@JsonIgnore
	protected Boolean active = false;

	@JsonIgnore
	protected String lastCollectionMdid;

	@JsonIgnore
	protected String lastAggregationMdid;

	@JsonIgnore
	protected String lastDownloadObjid;

	@JsonIgnore
	protected String lastValidationJob;

	@JsonIgnore
	protected boolean compatibilityOverrided;

	public Boolean getActive() {
		return active;
	}

	public void setActive(final Boolean active) {
		this.active = active;
	}

	public String getLastCollectionMdid() {
		return lastCollectionMdid;
	}

	public void setLastCollectionMdid(final String lastCollectionMdid) {
		this.lastCollectionMdid = lastCollectionMdid;
	}

	public String getLastAggregationMdid() {
		return lastAggregationMdid;
	}

	public void setLastAggregationMdid(final String lastAggregationMdid) {
		this.lastAggregationMdid = lastAggregationMdid;
	}

	public String getLastDownloadObjid() {
		return lastDownloadObjid;
	}

	public void setLastDownloadObjid(final String lastDownloadObjid) {
		this.lastDownloadObjid = lastDownloadObjid;
	}

	public String getLastValidationJob() {
		return lastValidationJob;
	}

	public void setLastValidationJob(final String lastValidationJob) {
		this.lastValidationJob = lastValidationJob;
	}

	public boolean isCompatibilityOverrided() {
		return compatibilityOverrided;
	}

	public void setCompatibilityOverrided(final boolean compatibilityOverrided) {
		this.compatibilityOverrided = compatibilityOverrided;
	}
}
