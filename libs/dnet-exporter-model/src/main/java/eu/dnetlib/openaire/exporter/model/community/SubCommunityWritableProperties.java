package eu.dnetlib.openaire.exporter.model.community;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import eu.dnetlib.openaire.exporter.model.community.selectioncriteria.SelectionCriteria;
import eu.dnetlib.openaire.exporter.model.context.Param;
import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
public class SubCommunityWritableProperties {

	@Schema(description = "the label of the subCommunity", required = true)
	private String label;

	@Schema(description = "the category of the subCommunity", required = true)
	private String category;

	@Schema(description = "the parameters of the subCommunity", required = true)
	private List<Param> params = new ArrayList<>();

	@Schema(description = "it supports the claims", required = true)
	private Boolean claim = null;

	@Schema(description = "it is browsable", required = true)
	private Boolean browsable = null;

	@Schema(description = "list of subjects (keywords) that characterise this sub-community")
	private List<String> subjects;

	@Schema(description = "list of fos that characterise this sub-community")
	private List<String> fos;

	@Schema(description = "list of sdg that characterise this sub-community")
	private List<String> sdg;

	@Schema(description = "list of advanced criteria to associate results to this sub-community")
	private SelectionCriteria advancedConstraints;

	@Schema(description = "list of the remove criteria for this sub-community")
	private SelectionCriteria removeConstraints;

	@Schema(description = "Zenodo community associated to this sub-community")
	protected String zenodoCommunity;

	@Schema(description = "other zenodo communities for this sub-community")
	private List<String> otherZenodoCommunities;

	@Schema(description = "Suggested Acknowledgements for this sub-community")
	private List<String> suggestedAcknowledgements;

	public String getLabel() {
		return label;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(final String category) {
		this.category = category;
	}

	public List<Param> getParams() {
		return params;
	}

	public void setParams(final List<Param> params) {
		this.params = params;
	}

	public Boolean getClaim() {
		return claim;
	}

	public void setClaim(final Boolean claim) {
		this.claim = claim;
	}

	public Boolean getBrowsable() {
		return browsable;
	}

	public void setBrowsable(final Boolean browsable) {
		this.browsable = browsable;
	}

	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(final List<String> subjects) {
		this.subjects = subjects;
	}

	public List<String> getFos() {
		return fos;
	}

	public void setFos(final List<String> fos) {
		this.fos = fos;
	}

	public List<String> getSdg() {
		return sdg;
	}

	public void setSdg(final List<String> sdg) {
		this.sdg = sdg;
	}

	public SelectionCriteria getAdvancedConstraints() {
		return advancedConstraints;
	}

	public void setAdvancedConstraints(final SelectionCriteria advancedConstraints) {
		this.advancedConstraints = advancedConstraints;
	}

	public SelectionCriteria getRemoveConstraints() {
		return removeConstraints;
	}

	public void setRemoveConstraints(final SelectionCriteria removeConstraints) {
		this.removeConstraints = removeConstraints;
	}

	public String getZenodoCommunity() {
		return zenodoCommunity;
	}

	public void setZenodoCommunity(final String zenodoCommunity) {
		this.zenodoCommunity = zenodoCommunity;
	}

	public List<String> getOtherZenodoCommunities() {
		return otherZenodoCommunities;
	}

	public void setOtherZenodoCommunities(final List<String> otherZenodoCommunities) {
		this.otherZenodoCommunities = otherZenodoCommunities;
	}

	public List<String> getSuggestedAcknowledgements() {
		return suggestedAcknowledgements;
	}

	public void setSuggestedAcknowledgements(final List<String> suggestedAcknowledgements) {
		this.suggestedAcknowledgements = suggestedAcknowledgements;
	}

}
