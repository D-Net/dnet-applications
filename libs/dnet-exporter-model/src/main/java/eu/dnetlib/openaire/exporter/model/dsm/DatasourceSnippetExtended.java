package eu.dnetlib.openaire.exporter.model.dsm;

import java.util.Date;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
@Schema(name = "Datasource model", description = "provides extended information about the datasource")
public class DatasourceSnippetExtended {

	@NotBlank
	private String id;

	@NotBlank
	private String officialname;

	@NotBlank
	private String englishname;

	private String websiteurl;

	@Email
	private String registeredby;

	private Date registrationdate;

	private String eoscDatasourceType;

	private String logoUrl;

	private String description;

	private Boolean consentTermsOfUse;

	private Date consentTermsOfUseDate;

	private Date lastConsentTermsOfUseDate;

	private Boolean fullTextDownload;

	private Set<OrganizationDetails> organizations;

	@Deprecated
	private String typology;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getOfficialname() {
		return officialname;
	}

	public void setOfficialname(final String officialname) {
		this.officialname = officialname;
	}

	public String getEnglishname() {
		return englishname;
	}

	public void setEnglishname(final String englishname) {
		this.englishname = englishname;
	}

	public String getWebsiteurl() {
		return websiteurl;
	}

	public void setWebsiteurl(final String websiteurl) {
		this.websiteurl = websiteurl;
	}

	public String getRegisteredby() {
		return registeredby;
	}

	public void setRegisteredby(final String registeredby) {
		this.registeredby = registeredby;
	}

	public Date getRegistrationdate() {
		return registrationdate;
	}

	public void setRegistrationdate(final Date registrationdate) {
		this.registrationdate = registrationdate;
	}

	public String getEoscDatasourceType() {
		return eoscDatasourceType;
	}

	public void setEoscDatasourceType(final String eoscDatasourceType) {
		this.eoscDatasourceType = eoscDatasourceType;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public Boolean getConsentTermsOfUse() {
		return consentTermsOfUse;
	}

	public DatasourceSnippetExtended setConsentTermsOfUse(final Boolean consentTermsOfUse) {
		this.consentTermsOfUse = consentTermsOfUse;
		return this;
	}

	public Date getConsentTermsOfUseDate() {
		return consentTermsOfUseDate;
	}

	public DatasourceSnippetExtended setConsentTermsOfUseDate(final Date consentTermsOfUseDate) {
		this.consentTermsOfUseDate = consentTermsOfUseDate;
		return this;
	}

	public Boolean getFullTextDownload() {
		return fullTextDownload;
	}

	public DatasourceSnippetExtended setFullTextDownload(final Boolean fullTextDownload) {
		this.fullTextDownload = fullTextDownload;
		return this;
	}

	public DatasourceSnippetExtended setLogoUrl(final String logoUrl) {
		this.logoUrl = logoUrl;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public DatasourceSnippetExtended setDescription(final String description) {
		this.description = description;
		return this;
	}

	public Set<OrganizationDetails> getOrganizations() {
		return organizations;
	}

	public DatasourceSnippetExtended setOrganizations(final Set<OrganizationDetails> organizations) {
		this.organizations = organizations;
		return this;
	}

	@Deprecated
	public String getTypology() {
		return typology;
	}

	@Deprecated
	public DatasourceSnippetExtended setTypology(final String typology) {
		this.typology = typology;
		return this;
	}

	public Date getLastConsentTermsOfUseDate() {
		return lastConsentTermsOfUseDate;
	}

	public DatasourceSnippetExtended setLastConsentTermsOfUseDate(final Date lastConsentTermsOfUseDate) {
		this.lastConsentTermsOfUseDate = lastConsentTermsOfUseDate;
		return this;
	}

}
