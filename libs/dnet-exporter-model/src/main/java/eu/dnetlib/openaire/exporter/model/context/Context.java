package eu.dnetlib.openaire.exporter.model.context;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class Context {

	private String id;

	private String label;

	private String type;

	private Date creationDate;

	private Date lastUpdateDate;

	private List<Param> params;

	private Map<String, Category> categories;

	public String getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public String getType() {
		return type;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public List<Param> getParams() {
		return params;
	}

	public Map<String, Category> getCategories() {
		return categories;
	}

	public Context setId(final String id) {
		this.id = id;
		return this;
	}

	public Context setLabel(final String label) {
		this.label = label;
		return this;
	}

	public Context setType(final String type) {
		this.type = type;
		return this;
	}

	public Context setCreationDate(final Date dateofcreation) {
		this.creationDate = dateofcreation;
		return this;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public Context setLastUpdateDate(final Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
		return this;
	}

	public Context setParams(final List<Param> params) {
		this.params = params;
		return this;
	}

	public Context setCategories(final Map<String, Category> categories) {
		this.categories = categories;
		return this;
	}

}
