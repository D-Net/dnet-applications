package eu.dnetlib.openaire.exporter.model.vocabularies;

/**
 * Created by claudio on 15/09/2017.
 */
public class Term {
	private String englishName;
	private String code;

	public Term() {
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(final String englishName) {
		this.englishName = englishName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

}
