package eu.dnetlib.openaire.exporter.model.dsm;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
@Schema(name = "Api response model", description = "Api response model, provides a response header")
public class Response {

	private Header header;

	public Response() {
		this.header = new Header();
	}

	public Header getHeader() {
		return header;
	}

	public Response setHeader(final Header header) {
		this.header = header;
		return this;
	}

}
