package eu.dnetlib.openaire.exporter.model.dsm;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonAutoDetect
@JsonInclude(Include.NON_NULL)
public class AggregationInfoV1 implements Serializable {

	private static final long serialVersionUID = 7333873121568523946L;

	private int numberOfRecords;

	private String date;

	private AggregationStage aggregationStage;

	private boolean indexedVersion = false;

	private CollectionMode collectionMode;

	public AggregationInfoV1() {}

	public int getNumberOfRecords() {
		return numberOfRecords;
	}

	public void setNumberOfRecords(final int numberOfRecords) {
		this.numberOfRecords = numberOfRecords;
	}

	public String getDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	public AggregationStage getAggregationStage() {
		return aggregationStage;
	}

	public void setAggregationStage(final AggregationStage aggregationStage) {
		this.aggregationStage = aggregationStage;
	}

	public boolean isIndexedVersion() {
		return indexedVersion;
	}

	public void setIndexedVersion(final boolean indexedVersion) {
		this.indexedVersion = indexedVersion;
	}

	public CollectionMode getCollectionMode() {
		return collectionMode;
	}

	public void setCollectionMode(final CollectionMode collectionMode) {
		this.collectionMode = collectionMode;
	}

}
