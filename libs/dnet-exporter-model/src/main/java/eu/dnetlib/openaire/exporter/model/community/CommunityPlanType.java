package eu.dnetlib.openaire.exporter.model.community;

public enum CommunityPlanType {
	Standard, Advanced, Premium, National, Default
}
