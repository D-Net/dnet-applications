package eu.dnetlib.openaire.exporter.exceptions;

public class DsmApiException extends Exception {

	private static final long serialVersionUID = -8173126561260106405L;

	private int code;

	public DsmApiException(final int code, final String msg) {
		super(msg);
		this.code = code;
	}

	public DsmApiException(final int code, final Throwable e) {
		super(e);
		this.code = code;
	}

	public DsmApiException(final int code, final String msg, final Throwable e) {
		super(msg, e);
		this.code = code;
	}

	public DsmApiException(final String msg) {
		this(500, msg);
	}

	public int getCode() {
		return code;
	}

	public void setCode(final int code) {
		this.code = code;
	}

}
