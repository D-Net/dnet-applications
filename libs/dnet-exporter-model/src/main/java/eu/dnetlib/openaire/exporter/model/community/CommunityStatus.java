package eu.dnetlib.openaire.exporter.model.community;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
public enum CommunityStatus {

	@Schema(description = "hidden visibility")
	hidden,

	@Schema(description = "visible only to RCD managers")
	manager,

	@Schema(description = "visible to RCD managers and to the community users")
	all,

	@Schema(description = "restricted visibility")
	RESTRICTED,

	@Schema(description = "public visibility")
	PUBLIC

}
