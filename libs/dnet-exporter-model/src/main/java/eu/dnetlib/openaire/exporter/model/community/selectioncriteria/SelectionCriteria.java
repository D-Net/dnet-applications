package eu.dnetlib.openaire.exporter.model.community.selectioncriteria;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonAutoDetect
public class SelectionCriteria implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4303936216579280542L;
	private static final Log log = LogFactory.getLog(SelectionCriteria.class);
	private List<Constraints> criteria;

	public SelectionCriteria() {}

	public List<Constraints> getCriteria() {
		return criteria;
	}

	public void setCriteria(final List<Constraints> criteria) {
		this.criteria = criteria;
	}

	public static SelectionCriteria fromJson(final String json) {
		if (StringUtils.isEmpty(json)) { return null; }
		try {
			return new ObjectMapper().readValue(json, SelectionCriteria.class);
		} catch (final JsonProcessingException e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	public String toJson() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (final JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

}
