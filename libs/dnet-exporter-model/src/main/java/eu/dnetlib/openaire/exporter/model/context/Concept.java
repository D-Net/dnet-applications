package eu.dnetlib.openaire.exporter.model.context;

import java.util.List;

public class Concept {

	private String id;

	private String label;

	private boolean claim;

	private List<Param> params;

	private List<Concept> concepts;

	public boolean hasSubConcepts() {
		return getConcepts() != null && !getConcepts().isEmpty();
	}

	public String getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public boolean isClaim() {
		return claim;
	}

	public List<Param> getParams() {
		return params;
	}

	public List<Concept> getConcepts() {
		return concepts;
	}

	public Concept setId(final String id) {
		this.id = id;
		return this;
	}

	public Concept setLabel(final String label) {
		this.label = label;
		return this;
	}

	public Concept setClaim(final boolean claim) {
		this.claim = claim;
		return this;
	}

	public Concept setParams(final List<Param> params) {
		this.params = params;
		return this;
	}

	public Concept setConcepts(final List<Concept> concepts) {
		this.concepts = concepts;
		return this;
	}

}
