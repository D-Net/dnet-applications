package eu.dnetlib.openaire.exporter.model.community;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
public class CommunityProject {

	@Schema(description = "OpenAIRE identifier for this project, if available", required = false)
	private String openaireId;

	@Schema(description = "the community identifier this project belongs to", required = true)
	private String communityId;

	@Schema(description = "project name", required = true)
	private String name;

	@Schema(description = "project acronym", required = false)
	private String acronym;

	@Schema(description = "project funder", required = true)
	private String funder;

	@Schema(description = "project grant id", required = true)
	private String grantId;

	@Schema(description = "available since", required = false)
	private LocalDate availableSince;

	public String getOpenaireId() {
		return openaireId;
	}

	public void setOpenaireId(final String openaireId) {
		this.openaireId = openaireId;
	}

	public String getCommunityId() {
		return communityId;
	}

	public void setCommunityId(final String communityId) {
		this.communityId = communityId;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(final String acronym) {
		this.acronym = acronym;
	}

	public String getFunder() {
		return funder;
	}

	public void setFunder(final String funder) {
		this.funder = funder;
	}

	public String getGrantId() {
		return grantId;
	}

	public void setGrantId(final String grantId) {
		this.grantId = grantId;
	}

	public LocalDate getAvailableSince() {
		return availableSince;
	}

	public void setAvailableSince(final LocalDate availableSince) {
		this.availableSince = availableSince;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("CommunityProject [\n\topenaireId = ")
			.append(openaireId)
			.append(",\n\tcommunityId = ")
			.append(communityId)
			.append(",\n\tname = ")
			.append(name)
			.append(",\n\tacronym = ")
			.append(acronym)
			.append(",\n\tfunder = ")
			.append(funder)
			.append(",\n\tgrantId = ")
			.append(funder)
			.append(",\n\tavailableSince = ")
			.append(availableSince)
			.append("\n]");
		return builder.toString();
	}

}
