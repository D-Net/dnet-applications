package eu.dnetlib.openaire.exporter.model.dsm;

public enum FilterType {
	exact,
	search,
	searchOrgs,
	multiSearch
}
