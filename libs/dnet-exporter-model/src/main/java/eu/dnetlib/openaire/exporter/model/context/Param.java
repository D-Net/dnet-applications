package eu.dnetlib.openaire.exporter.model.context;

import java.io.Serializable;

public class Param implements Serializable {

	private static final long serialVersionUID = -1534376651378828800L;

	private String name;

	private String value;

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public Param setName(final String name) {
		this.name = name;
		return this;
	}

	public Param setValue(final String value) {
		this.value = value;
		return this;
	}

}
