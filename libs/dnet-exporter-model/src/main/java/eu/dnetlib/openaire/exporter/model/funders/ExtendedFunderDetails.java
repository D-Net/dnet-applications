package eu.dnetlib.openaire.exporter.model.funders;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@Deprecated
@JsonAutoDetect
public class ExtendedFunderDetails extends FunderDetails {

	private List<FundingStream> fundingStreams;

	public ExtendedFunderDetails() {}

	public ExtendedFunderDetails(final FunderDetails fd) {
		super();
		setId(fd.getId());
		setName(fd.getName());
		setShortname(fd.getShortname());
		setJurisdiction(fd.getJurisdiction());
		setRegistrationDate(fd.getRegistrationDate());
		setLastUpdateDate(fd.getLastUpdateDate());
	}

	public List<FundingStream> getFundingStreams() {
		return fundingStreams;
	}

	public ExtendedFunderDetails setFundingStreams(final List<FundingStream> fundingStreams) {
		this.fundingStreams = fundingStreams;
		return this;
	}

}
