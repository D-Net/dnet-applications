package eu.dnetlib.openaire.exporter.model.community.selectioncriteria;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class Constraint implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5996232267609464747L;

	private String verb;
	private String field;
	private String value;

	public Constraint() {}

	public String getVerb() {
		return verb;
	}

	public void setVerb(final String verb) {
		this.verb = verb;
	}

	public String getField() {
		return field;
	}

	public void setField(final String field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

}
