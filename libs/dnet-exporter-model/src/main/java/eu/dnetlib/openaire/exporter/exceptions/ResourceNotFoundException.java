package eu.dnetlib.openaire.exporter.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = -5605421323034135778L;

	public ResourceNotFoundException(final String msg) {
		super(msg);
	}

	public ResourceNotFoundException(final Exception e) {
		super(e);
	}

	public ResourceNotFoundException(){super();}

}
