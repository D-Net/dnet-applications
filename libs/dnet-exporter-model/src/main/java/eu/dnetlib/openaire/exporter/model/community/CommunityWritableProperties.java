package eu.dnetlib.openaire.exporter.model.community;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import eu.dnetlib.openaire.exporter.model.community.selectioncriteria.SelectionCriteria;
import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
public class CommunityWritableProperties {

	@Schema(description = "community name")
	private String name;

	@Schema(description = "community short name")
	private String shortName;

	@Schema(description = "community name for display")
	private String displayName;

	@Schema(description = "community short name for display")
	private String displayShortName;

	@Schema(description = "community description")
	private String description;

	@Schema(description = "http url for the community logo")
	private String logoUrl;

	@Schema(description = "list of subjects (keywords) that characterise this community")
	private List<String> subjects;

	@Schema(description = "list of fos that characterise this community")
	private List<String> fos;

	@Schema(description = "list of sdg that characterise this community")
	private List<String> sdg;

	@Schema(description = "Advanced constraint for the association of results to the community")
	private SelectionCriteria advancedConstraints;

	@Schema(description = "Constraint for removing")
	private SelectionCriteria removeConstraints;

	@Schema(description = "status of the community, drives its visibility")
	private CommunityStatus status;

	@Schema(description = "id of the main Zenodo community")
	private String mainZenodoCommunity;

	@Schema(description = "identifiers of the other Zenodo community")
	private List<String> otherZenodoCommunities;

	@Schema(description = "membership of the community")
	private CommunityMembershipType membership;

	@Schema(description = "type of the community")
	private CommunityType type;

	@Schema(description = "type of supported claim")
	private CommunityClaimType claim;

	@Schema(description = "community plan")
	private CommunityPlanType plan;

	@Schema(description = "featured")
	private Boolean featured;

	public List<String> getFos() {
		return fos;
	}

	public void setFos(final List<String> fos) {
		this.fos = fos;
	}

	public List<String> getSdg() {
		return sdg;
	}

	public void setSdg(final List<String> sdg) {
		this.sdg = sdg;
	}

	public SelectionCriteria getAdvancedConstraints() {
		return advancedConstraints;
	}

	public void setAdvancedConstraints(final SelectionCriteria advancedConstraints) {
		this.advancedConstraints = advancedConstraints;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayShortName() {
		return displayShortName;
	}

	public void setDisplayShortName(final String displayShortName) {
		this.displayShortName = displayShortName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(final String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(final List<String> subjects) {
		this.subjects = subjects;
	}

	public CommunityStatus getStatus() {
		return status;
	}

	public void setStatus(final CommunityStatus status) {
		this.status = status;
	}

	public String getMainZenodoCommunity() {
		return mainZenodoCommunity;
	}

	public void setMainZenodoCommunity(final String mainZenodoCommunity) {
		this.mainZenodoCommunity = mainZenodoCommunity;
	}

	public CommunityMembershipType getMembership() {
		return membership;
	}

	public void setMembership(final CommunityMembershipType membership) {
		this.membership = membership;
	}

	public CommunityType getType() {
		return type;
	}

	public void setType(final CommunityType type) {
		this.type = type;
	}

	public CommunityClaimType getClaim() {
		return claim;
	}

	public void setClaim(final CommunityClaimType claim) {
		this.claim = claim;
	}

	public SelectionCriteria getRemoveConstraints() {
		return removeConstraints;
	}

	public void setRemoveConstraints(final SelectionCriteria removeConstraints) {
		this.removeConstraints = removeConstraints;
	}

	public List<String> getOtherZenodoCommunities() {
		return otherZenodoCommunities;
	}

	public void setOtherZenodoCommunities(final List<String> otherZenodoCommunities) {
		this.otherZenodoCommunities = otherZenodoCommunities;
	}

	public CommunityPlanType getPlan() {
		return plan;
	}

	public void setPlan(final CommunityPlanType plan) {
		this.plan = plan;
	}

	public Boolean getFeatured() {
		return featured;
	}

	public void setFeatured(final Boolean featured) {
		this.featured = featured;
	}

}
