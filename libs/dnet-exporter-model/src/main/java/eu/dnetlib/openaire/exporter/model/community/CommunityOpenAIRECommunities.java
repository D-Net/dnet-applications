package eu.dnetlib.openaire.exporter.model.community;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;

public class CommunityOpenAIRECommunities {

	@NotNull
	@Schema(description = "the zenodo community identifier", required = true)
	private String zenodoid;

	@NotNull
	@Schema(description = "identifies this zenodo community within the context it belongs to", required = true)
	private List<String> openAirecommunitylist;

	public CommunityOpenAIRECommunities() {
		this.zenodoid = "";
		openAirecommunitylist = new ArrayList<>();
	}

	public List<String> getOpenAirecommunitylist() {
		return openAirecommunitylist;
	}

	public CommunityOpenAIRECommunities setOpenAirecommunitylist(final List<String> openAirecommunitylist) {
		this.openAirecommunitylist = openAirecommunitylist;
		return this;
	}

	public String getZenodoid() {
		return zenodoid;
	}

	public CommunityOpenAIRECommunities setZenodoid(final String zenodoid) {
		this.zenodoid = zenodoid;
		return this;
	}

}
