package eu.dnetlib.openaire.exporter.exceptions;

public class CommunityException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = -4961233580574761346L;

	public CommunityException(final String message) {
		super(message);
	}

	public CommunityException(final Throwable e) {
		super(e);
	}
}
