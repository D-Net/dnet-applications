package eu.dnetlib.openaire.exporter.model.vocabularies;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by claudio on 15/09/2017.
 */
public class Vocabulary {


	private static final Log log = LogFactory.getLog(Vocabulary.class);

	private List<Term> terms;

	public Vocabulary() {
	}

	private static final Map<String, Term> byCode = Maps.newConcurrentMap();

	public String getEnglishName(final String code) {
		if (byCode.isEmpty()) {
			hashByCode();
		}
		final Term term = byCode.get(code);
		return term != null ? term.getEnglishName() : null;
	}

	public boolean hasCode(final String code) {
		return getEnglishName(code) != null;
	}

	private void hashByCode() {
		log.info("hashing vocabulary by code ...");
		getTerms().forEach(term -> byCode.put(term.getCode(), term));
		log.info("hashing vocabulary by code ... done!");
	}

	public List<Term> getTerms() {
		return terms;
	}

	public void setTerms(final List<Term> terms) {
		this.terms = terms;
	}
}
