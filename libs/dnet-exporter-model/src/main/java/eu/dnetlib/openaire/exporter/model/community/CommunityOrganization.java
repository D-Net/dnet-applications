package eu.dnetlib.openaire.exporter.model.community;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
public class CommunityOrganization {

	@NotNull
	@Schema(description = "the community identifier this organization belongs to", required = true)
	private String communityId;

	@NotNull
	@Schema(description = "name of the organization", required = true)
	private String name;

	@NotNull
	@Schema(description = "url of the logo for this organization", required = true)
	private String logo_url;

	@NotNull
	@Schema(description = "website url for this organization", required = true)
	private String website_url;

	public String getCommunityId() {
		return communityId;
	}

	public CommunityOrganization setCommunityId(final String communityId) {
		this.communityId = communityId;
		return this;
	}

	public String getName() {
		return name;
	}

	public CommunityOrganization setName(final String name) {
		this.name = name;
		return this;
	}

	public String getLogo_url() {
		return logo_url;
	}

	public CommunityOrganization setLogo_url(final String logo_url) {
		this.logo_url = logo_url;
		return this;
	}

	public String getWebsite_url() {
		return website_url;
	}

	public CommunityOrganization setWebsite_url(final String website_url) {
		this.website_url = website_url;
		return this;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("CommunityOrganization [\n\tcommunityId = ")
			.append(communityId)
			.append(",\n\tname = ")
			.append(name)
			.append(",\n\tlogo_url = ")
			.append(logo_url)
			.append(",\n\twebsite_url = ")
			.append(website_url)
			.append("\n]");
		return builder.toString();
	}
}
