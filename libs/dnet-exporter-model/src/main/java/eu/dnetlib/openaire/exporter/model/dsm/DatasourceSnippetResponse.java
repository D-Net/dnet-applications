package eu.dnetlib.openaire.exporter.model.dsm;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class DatasourceSnippetResponse extends Response {

	private List<DatasourceSnippetExtended> datasourceInfo;

	public DatasourceSnippetResponse(final List<DatasourceSnippetExtended> datasourceInfo) {
		super();
		this.datasourceInfo = datasourceInfo;
	}

	public List<DatasourceSnippetExtended> getDatasourceInfo() {
		return datasourceInfo;
	}

	public void setDatasourceInfo(final List<DatasourceSnippetExtended> datasourceInfo) {
		this.datasourceInfo = datasourceInfo;
	}
}
