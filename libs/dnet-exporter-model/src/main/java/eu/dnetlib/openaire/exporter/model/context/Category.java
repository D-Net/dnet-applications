package eu.dnetlib.openaire.exporter.model.context;

import java.util.List;

public class Category {

	private String id;

	private String label;

	private boolean claim;

	private List<Param> params;

	private List<Concept> concepts;

	public String getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public boolean isClaim() {
		return claim;
	}

	public boolean hasConcepts() {
		return getConcepts() != null && !getConcepts().isEmpty();
	}

	public List<Param> getParams() {
		return params;
	}

	public List<Concept> getConcepts() {
		return concepts;
	}

	public Category setId(final String id) {
		this.id = id;
		return this;
	}

	public Category setLabel(final String label) {
		this.label = label;
		return this;
	}

	public Category setClaim(final boolean claim) {
		this.claim = claim;
		return this;
	}

	public Category setParams(final List<Param> params) {
		this.params = params;
		return this;
	}

	public Category setConcepts(final List<Concept> concepts) {
		this.concepts = concepts;
		return this;
	}

}
