package eu.dnetlib.openaire.exporter.model.dsm;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
@Deprecated
public class AggregationHistoryResponseV1 extends Response {

	private List<AggregationInfoV1> aggregationInfo;

	public AggregationHistoryResponseV1(final List<AggregationInfoV1> aggregationInfo) {
		super();
		this.aggregationInfo = aggregationInfo;
	}

	public List<AggregationInfoV1> getAggregationInfo() {
		return aggregationInfo;
	}

	public void setAggregationInfo(final List<AggregationInfoV1> aggregationInfo) {
		this.aggregationInfo = aggregationInfo;
	}
}
