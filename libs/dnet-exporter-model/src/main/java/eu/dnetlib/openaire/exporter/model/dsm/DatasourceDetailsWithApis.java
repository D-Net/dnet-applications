package eu.dnetlib.openaire.exporter.model.dsm;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Created by claudio on 12/09/16.
 */
@JsonAutoDetect
@Schema(name = "Datasource model with apis", description = "provides information about the datasource and its apis")
public class DatasourceDetailsWithApis {

	private DatasourceDetails datasource;

	private List<ApiDetails> apis = new ArrayList<>();

	public DatasourceDetails getDatasource() {
		return datasource;
	}

	public void setDatasource(final DatasourceDetails datasource) {
		this.datasource = datasource;
	}

	public List<ApiDetails> getApis() {
		return apis;
	}

	public void setApis(final List<ApiDetails> apis) {
		this.apis = apis;
	}

}
