package eu.dnetlib.openaire.exporter.model.context;

import java.util.Objects;

public class CategorySummary {

	private String id;

	private String label;

	private boolean hasConcept;

	public String getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public boolean isHasConcept() {
		return hasConcept;
	}

	public CategorySummary setId(final String id) {
		this.id = id;
		return this;
	}

	public CategorySummary setLabel(final String label) {
		this.label = label;
		return this;
	}

	public CategorySummary setHasConcept(final boolean hasConcept) {
		this.hasConcept = hasConcept;
		return this;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof CategorySummary)) { return false; }
		final CategorySummary other = (CategorySummary) obj;
		return Objects.equals(id, other.id);
	}

}
