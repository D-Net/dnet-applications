package eu.dnetlib.openaire.exporter.model.community.selectioncriteria;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class Constraints implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2694950017620361195L;

	private List<Constraint> constraint;

	public Constraints() {}

	public List<Constraint> getConstraint() {
		return constraint;
	}

	public void setConstraint(final List<Constraint> constraint) {
		this.constraint = constraint;
	}
}
