package eu.dnetlib.openaire.exporter.model.vocabularies;

/**
 * Created by claudio on 15/09/2017.
 */
public class Country {

	private String code;

	private String name;

	public Country() {
	}

	public Country(final String code, final String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}
}
