package eu.dnetlib.openaire.exporter.model.community;

public enum CommunityMembershipType {

	open("open"),
	byInvitation("by-invitation");

	private final String description;

	private CommunityMembershipType(final String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public static CommunityMembershipType fromDescription(final String dbData) {
		for (final CommunityMembershipType t : CommunityMembershipType.values()) {
			if (t.description.equalsIgnoreCase(dbData)) { return t; }
		}
		return null;
	}
}
