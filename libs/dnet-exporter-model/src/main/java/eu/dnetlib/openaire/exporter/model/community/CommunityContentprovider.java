package eu.dnetlib.openaire.exporter.model.community;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.openaire.exporter.model.community.selectioncriteria.SelectionCriteria;
import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
public class CommunityContentprovider {

	@Schema(description = "OpenAIRE identifier for this content provider, if available", required = false)
	private String openaireId;

	@NotNull
	@Schema(description = "the community identifier this content provider belongs to", required = true)
	private String communityId;

	@Schema(description = "content provider name", required = false)
	private String name;

	@NotNull
	@Schema(description = "content provider official name", required = true)
	private String officialname;

	// @NotNull
	@Schema(description = "content provider enabled for content inclusion", required = false)
	private boolean enabled;

	// @NotNull
	@Schema(description = "content provider selection criteria", required = false)
	private SelectionCriteria selectioncriteria;

	@Schema(description = "suggested for deposition", required = false)
	private Boolean deposit;

	@Schema(description = "message for the deposition page", required = false)
	private String message;

	public String getOpenaireId() {
		return openaireId;
	}

	public void setOpenaireId(final String openaireId) {
		this.openaireId = openaireId;
	}

	public String getCommunityId() {
		return communityId;
	}

	public void setCommunityId(final String communityId) {
		this.communityId = communityId;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getOfficialname() {
		return officialname;
	}

	public void setOfficialname(final String officialname) {
		this.officialname = officialname;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(final boolean enabled) {
		this.enabled = enabled;
	}

	public SelectionCriteria getSelectioncriteria() {
		return selectioncriteria;
	}

	public void setSelectioncriteria(final SelectionCriteria selectioncriteria) {
		this.selectioncriteria = selectioncriteria;

	}

	public String toJson() {
		if (selectioncriteria == null) { return ""; }
		try {
			return new ObjectMapper().writeValueAsString(selectioncriteria);
		} catch (final JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	public String toXML() {
		if (selectioncriteria == null) { return ""; }
		return "<![CDATA[" + toJson() + "]]>";
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("CommunityContentprovider [\n\topenaireId = ")
				.append(openaireId)
				.append(",\n\tcommunityId = ")
				.append(communityId)
				.append(",\n\tname = ")
				.append(name)
				.append(",\n\tofficialname = ")
				.append(officialname)
				.append(",\n\tselectioncriteria = ")
				.append(selectioncriteria)
				.append("\n]");
		return builder.toString();
	}

	public Boolean getDeposit() {
		return deposit;
	}

	public void setDeposit(final Boolean deposit) {
		this.deposit = deposit;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

}
