package eu.dnetlib.openaire.exporter.model.community;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
public class CommunitySummary {

	@Schema(description = "identifies the community")
	protected String id;

	@Schema(description = "values for this field reflect the index field _community_ in the index, e.g. 'egi||EGI Federation'")
	protected String queryId;

	@Schema(description = "community type")
	protected CommunityType type;

	@Schema(description = "community name")
	protected String name;

	@Schema(description = "community short name")
	protected String shortName;

	@Schema(description = "community name for display")
	protected String displayName;

	@Schema(description = "community short name for display")
	protected String displayShortName;

	@Schema(description = "community creation date")
	protected LocalDateTime creationDate;

	@Schema(description = "community last update date")
	protected LocalDateTime lastUpdateDate;

	@Schema(description = "community description")
	protected String description;

	@Schema(description = "http url for the community logo")
	protected String logoUrl;

	@Schema(description = "status of the community, drives its visibility")
	protected CommunityStatus status;

	@Schema(description = "type of claim")
	private CommunityClaimType claim;

	@Schema(description = "type of membership")
	private CommunityMembershipType membership;

	@Schema(description = "Zenodo community associated to this community")
	protected String zenodoCommunity;

	@Schema(description = "community plan")
	protected CommunityPlanType plan;

	@Schema(description = "featured")
	protected Boolean featured;

	public CommunitySummary() {}

	public CommunitySummary(
			final String id,
			final String queryId,
			final CommunityType type,
			final String name,
			final String shortName,
			final String displayName,
			final String displayShortName,
			final LocalDateTime creationDate,
			final LocalDateTime lastUpdateDate,
			final String description,
			final String logoUrl,
			final CommunityStatus status,
			final String zenodoCommunity,
			final CommunityPlanType plan,
			final Boolean featured) {
		this.id = id;
		this.queryId = queryId;
		this.type = type;
		this.name = name;
		this.shortName = shortName;
		this.displayName = displayName;
		this.displayShortName = displayShortName;
		this.creationDate = creationDate;
		this.lastUpdateDate = lastUpdateDate;
		this.description = description;
		this.logoUrl = logoUrl;
		this.status = status;
		this.zenodoCommunity = zenodoCommunity;
		this.plan = plan;
		this.featured = featured;
	}

	public CommunitySummary(final CommunitySummary summary) {
		this(summary.getId(),
				summary.getQueryId(),
				summary.getType(),
				summary.getName(),
				summary.getShortName(),
				summary.getDisplayName(),
				summary.getDisplayShortName(),
				summary.getCreationDate(),
				summary.getLastUpdateDate(),
				summary.getDescription(),
				summary.getLogoUrl(),
				summary.getStatus(),
				summary.getZenodoCommunity(),
				summary.getPlan(),
				summary.getFeatured());
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(final String queryId) {
		this.queryId = queryId;
	}

	public CommunityType getType() {
		return type;
	}

	public void setType(final CommunityType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayShortName() {
		return displayShortName;
	}

	public void setDisplayShortName(final String displayShortName) {
		this.displayShortName = displayShortName;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(final LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(final LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(final String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public CommunityStatus getStatus() {
		return status;
	}

	public void setStatus(final CommunityStatus status) {
		this.status = status;
	}

	public CommunityClaimType getClaim() {
		return claim;
	}

	public void setClaim(final CommunityClaimType claim) {
		this.claim = claim;
	}

	public CommunityMembershipType getMembership() {
		return membership;
	}

	public void setMembership(final CommunityMembershipType membership) {
		this.membership = membership;
	}

	public String getZenodoCommunity() {
		return zenodoCommunity;
	}

	public void setZenodoCommunity(final String zenodoCommunity) {
		this.zenodoCommunity = zenodoCommunity;
	}

	public CommunityPlanType getPlan() {
		return plan;
	}

	public void setPlan(final CommunityPlanType plan) {
		this.plan = plan;
	}

	public Boolean getFeatured() {
		return featured;
	}

	public void setFeatured(final Boolean featured) {
		this.featured = featured;
	}

}
