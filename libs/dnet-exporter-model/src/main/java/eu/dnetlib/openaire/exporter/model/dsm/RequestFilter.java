package eu.dnetlib.openaire.exporter.model.dsm;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
@Schema(name = "Request filter", description = "field name and value pairs")
public class RequestFilter extends HashMap<FilterName, Object> {

	/**
	 *
	 */
	private static final long serialVersionUID = 5501969842482508379L;

	public RequestFilter() {}

}
