package eu.dnetlib.openaire.exporter.model.dsm;

public class RegisteredDatasourceInfo {

	private String id;
	private String officialName;
	private String englishName;
	private String organization;
	private String eoscDatasourceType;
	private String registeredBy;
	private String registrationDate;
	private String compatibility;
	private String lastCollectionDate;
	private long lastCollectionTotal;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getOfficialName() {
		return officialName;
	}

	public void setOfficialName(final String officialName) {
		this.officialName = officialName;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(final String englishName) {
		this.englishName = englishName;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(final String organization) {
		this.organization = organization;
	}

	public String getEoscDatasourceType() {
		return eoscDatasourceType;
	}

	public void setEoscDatasourceType(final String eoscDatasourceType) {
		this.eoscDatasourceType = eoscDatasourceType;
	}

	public String getRegisteredBy() {
		return registeredBy;
	}

	public void setRegisteredBy(final String registeredBy) {
		this.registeredBy = registeredBy;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(final String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getCompatibility() {
		return compatibility;
	}

	public void setCompatibility(final String compatibility) {
		this.compatibility = compatibility;
	}

	public String getLastCollectionDate() {
		return lastCollectionDate;
	}

	public void setLastCollectionDate(final String lastCollectionDate) {
		this.lastCollectionDate = lastCollectionDate;
	}

	public long getLastCollectionTotal() {
		return lastCollectionTotal;
	}

	public void setLastCollectionTotal(final long lastCollectionTotal) {
		this.lastCollectionTotal = lastCollectionTotal;
	}

}
