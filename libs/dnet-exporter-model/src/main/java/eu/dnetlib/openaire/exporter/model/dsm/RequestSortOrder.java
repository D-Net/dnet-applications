package eu.dnetlib.openaire.exporter.model.dsm;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public enum RequestSortOrder {
	ASCENDING, DESCENDING
}
