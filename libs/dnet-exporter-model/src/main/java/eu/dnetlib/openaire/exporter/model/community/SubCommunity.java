package eu.dnetlib.openaire.exporter.model.community;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import eu.dnetlib.openaire.exporter.model.community.selectioncriteria.SelectionCriteria;
import eu.dnetlib.openaire.exporter.model.context.Param;
import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
public class SubCommunity {

	@Schema(description = "the id of the subCommunity", required = true)
	private String subCommunityId;

	@Schema(description = "the community identifier this sub community belongs to", required = true)
	private String communityId;

	@Schema(description = "the parent of the subCommunity, if available (it should the id of another subCommunity)", required = false)
	private String parent;

	@Schema(description = "the label of the subCommunity", required = true)
	private String label;

	@Schema(description = "the category of the subCommunity", required = true)
	private String category;

	@Schema(description = "the parameters of the subCommunity", required = true)
	private List<Param> params = new ArrayList<>();

	@Schema(description = "it supports the claims", required = true)
	private boolean claim = false;

	@Schema(description = "it is browsable", required = true)
	private boolean browsable = false;

	@Schema(description = "list of subjects (keywords) that characterise this sub-community")
	private List<String> subjects;

	@Schema(description = "list of fos that characterise this sub-community")
	private List<String> fos;

	@Schema(description = "list of sdg that characterise this sub-community")
	private List<String> sdg;

	@Schema(description = "list of advanced criteria to associate results to this sub-community")
	private SelectionCriteria advancedConstraints;

	@Schema(description = "list of the remove criteria for this sub-community")
	private SelectionCriteria removeConstraints;

	@Schema(description = "Zenodo community associated to this sub-community")
	protected String zenodoCommunity;

	@Schema(description = "other zenodo communities for this sub-community")
	private List<String> otherZenodoCommunities;

	@Schema(description = "Suggested Acknowledgements for this sub-community")
	private List<String> suggestedAcknowledgements;

	public String getSubCommunityId() {
		return subCommunityId;
	}

	public void setSubCommunityId(final String subCommunityId) {
		this.subCommunityId = subCommunityId;
	}

	public String getCommunityId() {
		return communityId;
	}

	public void setCommunityId(final String communityId) {
		this.communityId = communityId;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(final String parent) {
		this.parent = parent;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(final String category) {
		this.category = category;
	}

	public List<Param> getParams() {
		return params;
	}

	public void setParams(final List<Param> map) {
		params = map;
	}

	public boolean isClaim() {
		return claim;
	}

	public void setClaim(final boolean claim) {
		this.claim = claim;
	}

	public boolean isBrowsable() {
		return browsable;
	}

	public void setBrowsable(final boolean browsable) {
		this.browsable = browsable;
	}

	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(final List<String> subjects) {
		this.subjects = subjects;
	}

	public List<String> getFos() {
		return fos;
	}

	public void setFos(final List<String> fos) {
		this.fos = fos;
	}

	public List<String> getSdg() {
		return sdg;
	}

	public void setSdg(final List<String> sdg) {
		this.sdg = sdg;
	}

	public SelectionCriteria getAdvancedConstraints() {
		return advancedConstraints;
	}

	public void setAdvancedConstraints(final SelectionCriteria advancedConstraints) {
		this.advancedConstraints = advancedConstraints;
	}

	public SelectionCriteria getRemoveConstraints() {
		return removeConstraints;
	}

	public void setRemoveConstraints(final SelectionCriteria removeConstraints) {
		this.removeConstraints = removeConstraints;
	}

	public String getZenodoCommunity() {
		return zenodoCommunity;
	}

	public void setZenodoCommunity(final String zenodoCommunity) {
		this.zenodoCommunity = zenodoCommunity;
	}

	public List<String> getOtherZenodoCommunities() {
		return otherZenodoCommunities;
	}

	public void setOtherZenodoCommunities(final List<String> otherZenodoCommunities) {
		this.otherZenodoCommunities = otherZenodoCommunities;
	}

	public List<String> getSuggestedAcknowledgements() {
		return suggestedAcknowledgements;
	}

	public void setSuggestedAcknowledgements(final List<String> suggestedAcknowledgements) {
		this.suggestedAcknowledgements = suggestedAcknowledgements;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("SubCommunity [subCommunityId=");
		builder.append(subCommunityId);
		builder.append(", communityId=");
		builder.append(communityId);
		builder.append(", parent=");
		builder.append(parent);
		builder.append(", label=");
		builder.append(label);
		builder.append(", category=");
		builder.append(category);
		builder.append(", params=");
		builder.append(params);
		builder.append(", claim=");
		builder.append(claim);
		builder.append(", browsable=");
		builder.append(browsable);
		builder.append(", subjects=");
		builder.append(subjects);
		builder.append(", fos=");
		builder.append(fos);
		builder.append(", sdg=");
		builder.append(sdg);
		builder.append(", advancedConstraints=");
		builder.append(advancedConstraints);
		builder.append(", removeConstraints=");
		builder.append(removeConstraints);
		builder.append(", zenodoCommunity=");
		builder.append(zenodoCommunity);
		builder.append(", otherZenodoCommunities=");
		builder.append(otherZenodoCommunities);
		builder.append(", suggestedAcknowledgements=");
		builder.append(suggestedAcknowledgements);
		builder.append("]");
		return builder.toString();
	}

}
