package eu.dnetlib.openaire.exporter.model.community;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import eu.dnetlib.openaire.exporter.model.community.selectioncriteria.SelectionCriteria;
import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
public class CommunityDetails extends CommunitySummary {

	@Schema(description = "list of subjects (keywords) that characterise this community")
	private List<String> subjects;

	@Schema(description = "list of fos that characterise this community")
	private List<String> fos;

	@Schema(description = "list of sdg that characterise this community")
	private List<String> sdg;

	@Schema(description = "list of advanced criteria to associate results to this community")
	private SelectionCriteria advancedConstraints;

	@Schema(description = "list of the remove criteria")
	private SelectionCriteria removeConstraints;

	@Schema(description = "other zenodo communities")
	private List<String> otherZenodoCommunities;

	@Schema(description = "Suggested Acknowledgements")
	private List<String> suggestedAcknowledgements;

	public CommunityDetails() {}

	public CommunityDetails(final CommunitySummary summary) {
		super(summary);
	}

	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(final List<String> subjects) {
		this.subjects = subjects;
	}

	public List<String> getFos() {
		return fos;
	}

	public void setFos(final List<String> fos) {
		this.fos = fos;
	}

	public List<String> getSdg() {
		return sdg;
	}

	public void setSdg(final List<String> sdg) {
		this.sdg = sdg;
	}

	public SelectionCriteria getAdvancedConstraints() {
		return advancedConstraints;
	}

	public void setAdvancedConstraints(final SelectionCriteria advancedConstraints) {
		this.advancedConstraints = advancedConstraints;
	}

	public SelectionCriteria getRemoveConstraints() {
		return removeConstraints;
	}

	public void setRemoveConstraints(final SelectionCriteria removeConstraints) {
		this.removeConstraints = removeConstraints;
	}

	public List<String> getOtherZenodoCommunities() {
		return otherZenodoCommunities;
	}

	public void setOtherZenodoCommunities(final List<String> otherZenodoCommunities) {
		this.otherZenodoCommunities = otherZenodoCommunities;
	}

	public List<String> getSuggestedAcknowledgements() {
		return suggestedAcknowledgements;
	}

	public void setSuggestedAcknowledgements(final List<String> suggestedAcknowledgements) {
		this.suggestedAcknowledgements = suggestedAcknowledgements;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("CommunityDetails [\n\tcreationDate = ")
				.append(creationDate)
				.append(",\n\tlastUpdateDate = ")
				.append(lastUpdateDate)
				.append(",\n\tsubjects = ")
				.append(subjects)
				.append(",\n\tfos = ")
				.append(fos)
				.append(",\n\tsdg = ")
				.append(sdg)
				.append(",\n\tadvancedConstraints = ")
				.append(advancedConstraints)
				.append(",\n\tremoveConstraints = ")
				.append(removeConstraints)
				.append(",\n\totherZenodoCommunities = ")
				.append(otherZenodoCommunities)
				.append(",\n\tid = ")
				.append(id)
				.append(",\n\tqueryId = ")
				.append(queryId)
				.append(",\n\ttype = ")
				.append(type)
				.append(",\n\tname = ")
				.append(name)
				.append(",\n\tshortName = ")
				.append(shortName)
				.append(",\n\tdisplayName = ")
				.append(displayName)
				.append(",\n\tdisplayShortName = ")
				.append(displayShortName)
				.append(",\n\tdescription = ")
				.append(description)
				.append(",\n\tlogoUrl = ")
				.append(logoUrl)
				.append(",\n\tstatus = ")
				.append(status)
				.append(",\n\tzenodoCommunity = ")
				.append(zenodoCommunity)
				.append(",\n\tsuggestedAcknowledgements = ")
				.append(suggestedAcknowledgements)
				.append(",\n\tplan = ")
				.append(plan)
				.append("\n]");
		return builder.toString();
	}

}
