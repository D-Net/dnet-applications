package eu.dnetlib.openaire.exporter.model.community;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
@Deprecated
public class CommunityZenodoCommunity {

	@NotNull
	@Schema(description = "the community identifier this zenodo Community belongs to", required = true)
	private String communityId;

	@NotNull
	@Schema(description = "Zenodo identifier for this community", required = true)
	private String zenodoid;

	@NotNull
	@Schema(description = "identifies this zenodo community within the context it belongs to", required = true)
	private String id;

	public String getZenodoid() {
		return zenodoid;
	}

	public void setZenodoid(final String zenodoid) {
		this.zenodoid = zenodoid;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getCommunityId() {
		return communityId;
	}

	public void setCommunityId(final String communityId) {
		this.communityId = communityId;
	}

}
