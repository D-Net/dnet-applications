package eu.dnetlib.openaire.exporter.model.dsm;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class AggregationHistoryResponseV2 extends Response {

	private List<AggregationInfo> aggregationInfo;

	public AggregationHistoryResponseV2() {
		super();
	}

	public AggregationHistoryResponseV2(final List<AggregationInfo> aggregationInfo) {
		super();
		this.aggregationInfo = aggregationInfo;
	}

	public List<AggregationInfo> getAggregationInfo() {
		return aggregationInfo;
	}

	public void setAggregationInfo(final List<AggregationInfo> aggregationInfo) {
		this.aggregationInfo = aggregationInfo;
	}
}
