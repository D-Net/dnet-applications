package eu.dnetlib.openaire.exporter.model.context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class IISConfigurationEntry implements Serializable {

	private static final long serialVersionUID = -1470248262314248937L;

	private String id;
	private String label;
	private List<Param> params = new ArrayList<>();

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public List<Param> getParams() {
		return params;
	}

	public void setParams(final List<Param> params) {
		this.params = params;
	}

	public void addParams(final String name, final String... values) {
		if (StringUtils.isBlank(name)) { return; }

		if (values == null) {
			params.add(new Param().setName(name).setValue(""));
		}

		for (final String v : values) {
			params.add(new Param().setName(name).setValue(v != null ? v : ""));
		}
	}

}
