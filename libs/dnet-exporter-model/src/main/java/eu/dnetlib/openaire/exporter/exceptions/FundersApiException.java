package eu.dnetlib.openaire.exporter.exceptions;

public class FundersApiException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 842353818131133522L;

	public FundersApiException() {}

	public FundersApiException(final String message) {
		super(message);
	}

	public FundersApiException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public FundersApiException(final Throwable cause) {
		super(cause);
	}
}
