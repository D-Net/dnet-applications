package eu.dnetlib.openaire.exporter.model.community;

public enum CommunityClaimType {

	managersOnly("managers-only"),
	membersOnly("members-only"),
	all("all");

	private final String description;

	private CommunityClaimType(final String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public static CommunityClaimType fromDescription(final String dbData) {
		for (final CommunityClaimType t : CommunityClaimType.values()) {
			if (t.description.equalsIgnoreCase(dbData)) { return t; }
		}
		return null;
	}

}
