package eu.dnetlib.openaire.exporter.model.dsm;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class DatasourceDetailResponse extends Response {

	private List<DatasourceDetails> datasourceInfo;

	public DatasourceDetailResponse(final List<DatasourceDetails> datasourceInfo) {
		super();
		this.datasourceInfo = datasourceInfo;
	}

	public List<DatasourceDetails> getDatasourceInfo() {
		return datasourceInfo;
	}

	public void setDatasourceInfo(final List<DatasourceDetails> datasourceInfo) {
		this.datasourceInfo = datasourceInfo;
	}
}
