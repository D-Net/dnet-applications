package eu.dnetlib.openaire.exporter.model.dsm;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class IdentitiesDetails {

	private String pid;

	private String issuertype;

	public String getPid() {
		return pid;
	}

	public String getIssuertype() {
		return issuertype;
	}

	public IdentitiesDetails setPid(final String pid) {
		this.pid = pid;
		return this;
	}

	public IdentitiesDetails setIssuertype(final String issuertype) {
		this.issuertype = issuertype;
		return this;
	}

}
