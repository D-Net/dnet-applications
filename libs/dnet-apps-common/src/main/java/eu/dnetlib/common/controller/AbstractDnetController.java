package eu.dnetlib.common.controller;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public abstract class AbstractDnetController {

	private static final Logger log = LoggerFactory.getLogger(AbstractDnetController.class);

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ErrorMessage handleException(final Exception e) {
		log.error("Error processing http method", e);
		return new ErrorMessage(e);
	}

	public class ErrorMessage {

		private final String message;
		private final String stacktrace;

		public ErrorMessage(final Exception e) {
			this(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}

		public ErrorMessage(final String message, final String stacktrace) {
			this.message = message;
			this.stacktrace = stacktrace;
		}

		public String getMessage() {
			return this.message;
		}

		public String getStacktrace() {
			return this.stacktrace;
		}

	}
}
