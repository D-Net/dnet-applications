package eu.dnetlib.common.exceptions;

public class DnetException extends Exception {

	private static final long serialVersionUID = 1318223089824261627L;

	public DnetException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public DnetException(final String message) {
		super(message);
	}

}
