package eu.dnetlib.common.metrics;

@Deprecated
public abstract interface MetricInfo {

	double obtainValue();

}
