package eu.dnetlib.common.metrics;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.prometheus.client.exporter.common.TextFormat;

@Controller
public class KpiMetricsController {

	@Autowired
	private MetricUtils metricUtils;

	@ResponseBody
	@RequestMapping(value = "/kpis", method = RequestMethod.GET, produces = "*/*")
	public ResponseEntity<String> kpiMetrics(@RequestHeader(value = "Accept", required = false, defaultValue = "") final String accept) {

		final String contentType = TextFormat.chooseContentType(accept);
		final String result = metricUtils.output(contentType);

		return ResponseEntity.ok()
			.header(CONTENT_TYPE, contentType)
			.body(result);
	}
}
