package eu.dnetlib.common.app;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

import io.micrometer.core.instrument.ImmutableTag;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.util.StringUtils;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.tags.Tag;

public abstract class AbstractDnetApp {

	private static final String DEFAULT_VERSION = "1.1";

	private static final String DEFAULT_DESC = "APIs documentation";

	protected static final License APACHE_2_LICENSE = new License().name("Apache 2.0").url("http://www.apache.org/licenses/LICENSE-2.0");

	protected static final License AGPL_3_LICENSE =
		new License().name("GNU Affero General Public License v3.0 or later").url("https://www.gnu.org/licenses/agpl-3.0.txt");

	@Value("${maven.pom.path}")
	private ClassPathResource pom;

	@Value("${server.public_url}")
	private String serverPublicUrl;

	@Value("${server.public_desc}")
	private String serverPublicDesc;

	private static final Logger log = LoggerFactory.getLogger(AbstractDnetApp.class);

	@PostConstruct
	public void init() {
		final MavenXpp3Reader reader = new MavenXpp3Reader();
		try {
			final Model model = reader.read(new InputStreamReader(pom.getInputStream()));

			log.info(String.format("registering metric for %s", model.getArtifactId()));

			final ImmutableTag tag1 = new ImmutableTag("component", model.getGroupId() + ":" + model.getArtifactId());
			final ImmutableTag tag2 = new ImmutableTag("version", model.getVersion());
			final ImmutableTag tag3 = new ImmutableTag("scmtag", model.getScm().getTag());

			Metrics.gauge("micrometer_info", Arrays.asList(tag1, tag2, tag3), 1);
		} catch (IOException | XmlPullParserException e) {
			log.error("Error registering metric", e);
		}
	}

	@Bean
	public OpenAPI newSwaggerDocket() {
		final List<Server> servers = new ArrayList<>();
		if (StringUtils.isNotBlank(serverPublicUrl)) {
			final Server server = new Server();
			server.setUrl(serverPublicUrl);
			server.setDescription(serverPublicDesc);
			servers.add(server);
		}

		return new OpenAPI()
			.servers(servers)
			.info(getSwaggerInfo())
			.tags(swaggerTags());
	}

	private Info getSwaggerInfo() {
		return new Info()
			.title(swaggerTitle())
			.description(swaggerDesc())
			.version(swaggerVersion())
			.license(swaggerLicense());
	}

	protected abstract String swaggerTitle();

	protected String swaggerDesc() {
		return DEFAULT_DESC;
	}

	protected String swaggerVersion() {
		return DEFAULT_VERSION;
	}

	protected License swaggerLicense() {
		return AGPL_3_LICENSE;
	}

	protected List<Tag> swaggerTags() {
		return new ArrayList<>();
	}

}
