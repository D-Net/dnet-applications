package eu.dnetlib.common.metrics;

import java.util.List;

import io.prometheus.client.Collector.MetricFamilySamples;

public abstract interface MetricsCalculator {

	List<MetricFamilySamples> getMetrics();

}
