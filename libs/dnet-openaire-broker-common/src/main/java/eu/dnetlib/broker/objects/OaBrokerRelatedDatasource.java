package eu.dnetlib.broker.objects;

import java.io.Serializable;

public class OaBrokerRelatedDatasource implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5528390911729639973L;

	private String openaireId;
	private String name;
	private String type;
	private String relType;

	public String getOpenaireId() {
		return openaireId;
	}

	public void setOpenaireId(final String openaireId) {
		this.openaireId = openaireId;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getRelType() {
		return relType;
	}

	public void setRelType(final String relType) {
		this.relType = relType;
	}

}
