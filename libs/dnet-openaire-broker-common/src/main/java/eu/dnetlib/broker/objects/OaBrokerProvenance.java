package eu.dnetlib.broker.objects;

import java.io.Serializable;

/**
 * Created by claudio on 26/07/16.
 */
public class OaBrokerProvenance implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2370917119345695230L;

	private String id;

	private String repositoryName;

	private String repositoryType;

	private String url;

	public OaBrokerProvenance() {}

	public OaBrokerProvenance(final String id, final String repositoryName, final String repositoryType, final String url) {
		this.id = id;
		this.repositoryName = repositoryName;
		this.repositoryType = repositoryType;
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getRepositoryName() {
		return repositoryName;
	}

	public void setRepositoryName(final String repositoryName) {
		this.repositoryName = repositoryName;
	}

	public String getRepositoryType() {
		return repositoryType;
	}

	public void setRepositoryType(final String repositoryType) {
		this.repositoryType = repositoryType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

}
