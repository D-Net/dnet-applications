package eu.dnetlib.broker.objects;

import java.io.Serializable;

public class OaBrokerRelatedSoftware implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 176600509589408338L;

	private String openaireId;

	private String name;

	private String description;

	private String landingPage;

	private String repository;

	public OaBrokerRelatedSoftware() {}

	public OaBrokerRelatedSoftware(final String openaireId, final String name, final String description, final String landingPage, final String repository) {
		this.openaireId = openaireId;
		this.name = name;
		this.description = description;
		this.landingPage = landingPage;
		this.repository = repository;
	}

	public String getOpenaireId() {
		return openaireId;
	}

	public void setOpenaireId(final String openaireId) {
		this.openaireId = openaireId;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getLandingPage() {
		return landingPage;
	}

	public void setLandingPage(final String landingPage) {
		this.landingPage = landingPage;
	}

	public String getRepository() {
		return repository;
	}

	public void setRepository(final String repository) {
		this.repository = repository;
	}

}
