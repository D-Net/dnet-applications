package eu.dnetlib.broker.objects;

import java.io.Serializable;

/**
 * Created by claudio on 22/07/16.
 */
public class OaBrokerExternalReference implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5528787961232948079L;

	private String url;

	private String sitename;

	private String type;

	private String refidentifier;

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getSitename() {
		return sitename;
	}

	public void setSitename(final String sitename) {
		this.sitename = sitename;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getRefidentifier() {
		return refidentifier;
	}

	public void setRefidentifier(final String refidentifier) {
		this.refidentifier = refidentifier;
	}

}
