package eu.dnetlib.broker.objects;

import java.io.Serializable;

/**
 * Created by claudio on 22/07/16.
 */
public class OaBrokerJournal implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 941929788063513440L;

	private String name;

	private String issn;

	private String eissn;

	private String lissn;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getIssn() {
		return issn;
	}

	public void setIssn(final String issn) {
		this.issn = issn;
	}

	public String getEissn() {
		return eissn;
	}

	public void setEissn(final String eissn) {
		this.eissn = eissn;
	}

	public String getLissn() {
		return lissn;
	}

	public void setLissn(final String lissn) {
		this.lissn = lissn;
	}

}
