package eu.dnetlib.broker.objects;

import java.io.Serializable;

public class OaBrokerProject implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2080495225075576873L;

	private String openaireId;
	private String code;
	private String acronym;
	private String title;
	private String funder;
	private String fundingProgram;
	private String jurisdiction;

	public OaBrokerProject() {}

	public OaBrokerProject(final String openaireId, final String code, final String acronym, final String title, final String funder,
			final String fundingProgram, final String jurisdiction) {
		this.openaireId = openaireId;
		this.code = code;
		this.acronym = acronym;
		this.title = title;
		this.funder = funder;
		this.fundingProgram = fundingProgram;
		this.jurisdiction = jurisdiction;
	}

	public String getOpenaireId() {
		return openaireId;
	}

	public void setOpenaireId(final String openaireId) {
		this.openaireId = openaireId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(final String acronym) {
		this.acronym = acronym;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getFunder() {
		return funder;
	}

	public void setFunder(final String funder) {
		this.funder = funder;
	}

	public String getFundingProgram() {
		return fundingProgram;
	}

	public void setFundingProgram(final String fundingProgram) {
		this.fundingProgram = fundingProgram;
	}

	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(final String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String asOpenaireProjectIdentifier() {
		if (verifyManadatoryFields()) {
			return String.format("info:eu-repo/grantAgreement/%s/%s/%s/%s/%s/%s",
					funder,
					fundingProgram,
					code,
					jurisdiction != null ? jurisdiction : "",
					title != null ? title : "",
					acronym != null ? acronym : "");
		} else {
			return "";
		}
	}

	public boolean verifyManadatoryFields() {
		return funder != null && fundingProgram != null && code != null
				&& !funder.isEmpty() && !fundingProgram.isEmpty() && !code.isEmpty();
	}

}
