package eu.dnetlib.broker.objects;

import java.io.Serializable;

/**
 * Created by claudio on 22/07/16.
 */
public class OaBrokerTypedValue implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4797440344311785195L;

	private String type;

	private String value;

	public OaBrokerTypedValue() {}

	public OaBrokerTypedValue(final String type, final String value) {
		this.type = type;
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

}
