package eu.dnetlib.broker.objects;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by claudio on 22/07/16.
 */
public class OaBrokerInstance implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4064188385743605220L;

	private String url;

	private String license;

	private String hostedby;

	private String instancetype;

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(final String license) {
		this.license = license;
	}

	public String getHostedby() {
		return hostedby;
	}

	public void setHostedby(final String hostedby) {
		this.hostedby = hostedby;
	}

	public String getInstancetype() {
		return instancetype;
	}

	public void setInstancetype(final String instancetype) {
		this.instancetype = instancetype;
	}

	@Override
	public int hashCode() {
		return Objects.hash(hostedby, instancetype, license, url);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof OaBrokerInstance)) { return false; }
		final OaBrokerInstance other = (OaBrokerInstance) obj;
		return Objects.equals(hostedby, other.hostedby) && Objects.equals(instancetype, other.instancetype) && Objects.equals(license, other.license)
			&& Objects.equals(url, other.url);
	}

}
