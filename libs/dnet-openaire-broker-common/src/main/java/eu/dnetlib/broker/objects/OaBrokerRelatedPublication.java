package eu.dnetlib.broker.objects;

import java.io.Serializable;
import java.util.List;

public class OaBrokerRelatedPublication implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6845435777531929885L;

	private String openaireId;
	private String originalId;
	private String title;
	private String collectedFrom;
	private List<OaBrokerTypedValue> pids;
	private List<OaBrokerInstance> instances;
	private String relType;

	public String getOpenaireId() {
		return openaireId;
	}

	public void setOpenaireId(final String openaireId) {
		this.openaireId = openaireId;
	}

	public String getOriginalId() {
		return originalId;
	}

	public void setOriginalId(final String originalId) {
		this.originalId = originalId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getCollectedFrom() {
		return collectedFrom;
	}

	public void setCollectedFrom(final String collectedFrom) {
		this.collectedFrom = collectedFrom;
	}

	public List<OaBrokerTypedValue> getPids() {
		return pids;
	}

	public void setPids(final List<OaBrokerTypedValue> pids) {
		this.pids = pids;
	}

	public List<OaBrokerInstance> getInstances() {
		return instances;
	}

	public void setInstances(final List<OaBrokerInstance> instances) {
		this.instances = instances;
	}

	public String getRelType() {
		return relType;
	}

	public void setRelType(final String relType) {
		this.relType = relType;
	}

}
