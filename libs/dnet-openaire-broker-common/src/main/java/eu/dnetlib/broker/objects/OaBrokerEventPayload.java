package eu.dnetlib.broker.objects;

import java.io.Serializable;

import com.google.gson.GsonBuilder;

/**
 * Created by claudio on 11/07/16.
 */
public class OaBrokerEventPayload implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5910606923372311229L;

	private OaBrokerMainEntity result;

	private OaBrokerMainEntity highlight;

	private OaBrokerProvenance provenance;

	private float trust;

	public OaBrokerMainEntity getResult() {
		return result;
	}

	public void setResult(final OaBrokerMainEntity result) {
		this.result = result;
	}

	public OaBrokerMainEntity getHighlight() {
		return highlight;
	}

	public void setHighlight(final OaBrokerMainEntity highlight) {
		this.highlight = highlight;
	}

	public OaBrokerProvenance getProvenance() {
		return provenance;
	}

	public void setProvenance(final OaBrokerProvenance provenance) {
		this.provenance = provenance;
	}

	public float getTrust() {
		return trust;
	}

	public void setTrust(final float trust) {
		this.trust = trust;
	}

	public static OaBrokerEventPayload fromJSON(final String json) {
		final GsonBuilder b = new GsonBuilder();
		return b.create().fromJson(json, OaBrokerEventPayload.class);
	}

	public String toJSON() {
		final GsonBuilder b = new GsonBuilder();
		return b.create().toJson(this);
	}

}
