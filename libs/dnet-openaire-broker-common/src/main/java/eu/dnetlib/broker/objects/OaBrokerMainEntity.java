package eu.dnetlib.broker.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by claudio on 22/07/16.
 */
public class OaBrokerMainEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -944960541904447441L;

	private String openaireId;

	private String originalId;

	private String typology;

	private List<String> titles = new ArrayList<>();

	private List<String> abstracts = new ArrayList<>();

	private String language;

	private List<OaBrokerTypedValue> subjects = new ArrayList<>();

	private List<OaBrokerAuthor> creators = new ArrayList<>();

	private String publicationdate;

	private String publisher;

	private String embargoenddate;

	private List<String> contributor = new ArrayList<>();

	private OaBrokerJournal journal;

	private List<OaBrokerTypedValue> pids = new ArrayList<>();

	private List<OaBrokerInstance> instances = new ArrayList<>();

	private List<OaBrokerExternalReference> externalReferences = new ArrayList<>();

	private List<OaBrokerProject> projects = new ArrayList<>();

	private List<OaBrokerRelatedDataset> datasets = new ArrayList<>();

	private List<OaBrokerRelatedPublication> publications = new ArrayList<>();

	private List<OaBrokerRelatedSoftware> softwares = new ArrayList<>();

	private List<OaBrokerRelatedDatasource> datasources = new ArrayList<>();

	public String getOpenaireId() {
		return openaireId;
	}

	public void setOpenaireId(final String openaireId) {
		this.openaireId = openaireId;
	}

	public String getOriginalId() {
		return originalId;
	}

	public void setOriginalId(final String originalId) {
		this.originalId = originalId;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(final String typology) {
		this.typology = typology;
	}

	public List<String> getTitles() {
		return titles;
	}

	public void setTitles(final List<String> titles) {
		this.titles = titles;
	}

	public List<String> getAbstracts() {
		return abstracts;
	}

	public void setAbstracts(final List<String> abstracts) {
		this.abstracts = abstracts;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(final String language) {
		this.language = language;
	}

	public List<OaBrokerTypedValue> getSubjects() {
		return subjects;
	}

	public void setSubjects(final List<OaBrokerTypedValue> subjects) {
		this.subjects = subjects;
	}

	public List<OaBrokerAuthor> getCreators() {
		return creators;
	}

	public void setCreators(final List<OaBrokerAuthor> creators) {
		this.creators = creators;
	}

	public String getPublicationdate() {
		return publicationdate;
	}

	public void setPublicationdate(final String publicationdate) {
		this.publicationdate = publicationdate;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(final String publisher) {
		this.publisher = publisher;
	}

	public String getEmbargoenddate() {
		return embargoenddate;
	}

	public void setEmbargoenddate(final String embargoenddate) {
		this.embargoenddate = embargoenddate;
	}

	public List<String> getContributor() {
		return contributor;
	}

	public void setContributor(final List<String> contributor) {
		this.contributor = contributor;
	}

	public OaBrokerJournal getJournal() {
		return journal;
	}

	public void setJournal(final OaBrokerJournal journal) {
		this.journal = journal;
	}

	public List<OaBrokerTypedValue> getPids() {
		return pids;
	}

	public void setPids(final List<OaBrokerTypedValue> pids) {
		this.pids = pids;
	}

	public List<OaBrokerInstance> getInstances() {
		return instances;
	}

	public void setInstances(final List<OaBrokerInstance> instances) {
		this.instances = instances;
	}

	public List<OaBrokerExternalReference> getExternalReferences() {
		return externalReferences;
	}

	public void setExternalReferences(final List<OaBrokerExternalReference> externalReferences) {
		this.externalReferences = externalReferences;
	}

	public List<OaBrokerProject> getProjects() {
		return projects;
	}

	public void setProjects(final List<OaBrokerProject> projects) {
		this.projects = projects;
	}

	public List<OaBrokerRelatedDataset> getDatasets() {
		return datasets;
	}

	public void setDatasets(final List<OaBrokerRelatedDataset> datasets) {
		this.datasets = datasets;
	}

	public List<OaBrokerRelatedPublication> getPublications() {
		return publications;
	}

	public void setPublications(final List<OaBrokerRelatedPublication> publications) {
		this.publications = publications;
	}

	public List<OaBrokerRelatedSoftware> getSoftwares() {
		return softwares;
	}

	public void setSoftwares(final List<OaBrokerRelatedSoftware> softwares) {
		this.softwares = softwares;
	}

	public List<OaBrokerRelatedDatasource> getDatasources() {
		return datasources;
	}

	public void setDatasources(final List<OaBrokerRelatedDatasource> datasources) {
		this.datasources = datasources;
	}

}
