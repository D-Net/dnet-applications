package eu.dnetlib.broker.objects;

import java.io.Serializable;

public class OaBrokerAuthor implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1295190133654199196L;

	private String fullname;
	private String orcid;

	public OaBrokerAuthor() {}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(final String fullname) {
		this.fullname = fullname;
	}

	public String getOrcid() {
		return orcid;
	}

	public void setOrcid(final String orcid) {
		this.orcid = orcid;
	}

	public OaBrokerAuthor(final String fullname, final String orcid) {
		this.fullname = fullname;
		this.orcid = orcid;
	}

}
