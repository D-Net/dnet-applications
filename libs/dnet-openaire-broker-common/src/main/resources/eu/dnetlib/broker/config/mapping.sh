#!/bin/bash

echo
echo "Fixing events index..."
curl -XPUT 'http://localhost:9200/events/_mapping/event' -d '
{
	"event" : {
		"properties" : {
			"map": {
				"type": "nested",
				"properties": {
					"target_datasource_id": {
						"type": "string",
						"index": "not_analyzed"
					},
					"target_datasource_name": {
						"type": "string",
						"index": "not_analyzed"
					}
				}
			}
		}
	}
}
'
echo
echo
echo "Fixing notifications index..."
curl -XPUT 'http://localhost:9200/notifications/_mapping/notification' -d '
{
	"notification" : {
		"properties" : {
			"map": {
				"type": "nested",
				"properties": {
					"target_datasource_id": {
						"type": "string",
						"index": "not_analyzed"
					},
					"target_datasource_name": {
						"type": "string",
						"index": "not_analyzed"
					}
				}
			}
		}
	}
}
'

echo
echo
echo "Registering topic type: ENRICH/<cond>/<field>..."
curl "http://localhost:8080/api/topic-types/add" -d "name=ENRICH&expression=ENRICH%2F%3Ccond%3E%2F%3Cfield%3E&producerId=OpenAIRE&mapKeys=target_datasource_name"
echo
echo
echo "Registering topic type: ENRICH/<cond>/<field>/<subfield>..."
curl "http://localhost:8080/api/topic-types/add" -d "name=ENRICH_WITH_SUBFIELD&expression=ENRICH%2F%3Ccond%3E%2F%3Cfield%3E%2F%3Csubfield%3E&producerId=OpenAIRE&mapKeys=target_datasource_name"
echo
echo
echo "Registering topic type: ADD/<field>..."
curl "http://localhost:8080/api/topic-types/add" -d "name=ADD&expression=ADD%2F%3Cfield%3E&producerId=OpenAIRE&mapKeys=target_datasource_name"
echo
echo
echo
