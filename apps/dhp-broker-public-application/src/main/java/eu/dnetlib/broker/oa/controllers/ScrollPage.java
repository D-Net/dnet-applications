package eu.dnetlib.broker.oa.controllers;

import java.util.List;

public class ScrollPage<T> {

	private String id;

	private boolean completed;

	private List<T> values;

	public ScrollPage() {}

	public ScrollPage(final String id, final boolean completed, final List<T> values) {
		this.id = id;
		this.completed = completed;
		this.values = values;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(final boolean completed) {
		this.completed = completed;
	}

	public List<T> getValues() {
		return values;
	}

	public void setValues(final List<T> values) {
		this.values = values;
	}

}
