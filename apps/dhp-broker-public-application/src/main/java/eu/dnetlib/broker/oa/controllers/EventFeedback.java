package eu.dnetlib.broker.oa.controllers;

import java.io.Serializable;

import eu.dnetlib.broker.common.feedbacks.FeedbackStatus;

public class EventFeedback implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6967719685282712195L;

	private String eventId;

	private FeedbackStatus status;

	public String getEventId() {
		return eventId;
	}

	public void setEventId(final String eventId) {
		this.eventId = eventId;
	}

	public FeedbackStatus getStatus() {
		return status;
	}

	public void setStatus(final FeedbackStatus status) {
		this.status = status;
	}

}
