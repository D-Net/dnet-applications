package eu.dnetlib.broker;

import java.util.Arrays;
import java.util.List;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import eu.dnetlib.common.app.AbstractDnetApp;
import io.swagger.v3.oas.models.tags.Tag;

@SpringBootApplication
public class BrokerPublicApplication extends AbstractDnetApp {

	public static final String OA_PUBLIC_APIS = "Openaire Broker Public API";

	public static void main(final String[] args) {
		SpringApplication.run(BrokerPublicApplication.class, args);
	}

	@Bean
	public GroupedOpenApi publicApi() {
		return GroupedOpenApi.builder()
			.group("Broker Public APIs")
			.pathsToMatch("/**")
			.build();
	}

	@Override
	protected String swaggerTitle() {
		return "OpenAIRE Public Broker API";
	}

	@Override
	protected List<Tag> swaggerTags() {
		return Arrays.asList(new Tag().name(OA_PUBLIC_APIS).description(OA_PUBLIC_APIS));
	}

}
