package eu.dnetlib.broker.oa.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ApiDocController {

	@GetMapping({ "/apidoc", "/api-doc", "/doc", "/swagger" })
	public String apiDoc() {
		return "redirect:swagger-ui/index.html";
	}
}
