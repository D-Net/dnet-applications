package eu.dnetlib.broker;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import eu.dnetlib.broker.common.elasticsearch.Event;
import eu.dnetlib.broker.common.elasticsearch.Notification;
import eu.dnetlib.broker.common.properties.ElasticSearchProperties;

@Configuration
@EnableCaching
@EnableScheduling
@EnableTransactionManagement
@EnableElasticsearchRepositories(basePackageClasses = {
	Event.class, Notification.class
})
@ComponentScan(basePackages = "eu.dnetlib")
public class BrokerConfiguration extends AbstractElasticsearchConfiguration {

	@Autowired
	private ElasticSearchProperties elasticSearchProperties;

	@Override
	@Bean
	public RestHighLevelClient elasticsearchClient() {
		final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
			.connectedTo(elasticSearchProperties.getClusterNodes().split(","))
			.withConnectTimeout(elasticSearchProperties.getConnectionTimeout())
			.withSocketTimeout(elasticSearchProperties.getSocketTimeout())
			.build();
		return RestClients.create(clientConfiguration).rest();
	}

}
