#!/usr/bin/perl

use strict;
use utf8;
use Net::RabbitMQ;

binmode STDOUT, ':utf8';

my $host   = 'broker1-dev-dnet.d4science.org';
my $user   = 'r_admin';
my $passwd = '9g8fed7gpohef9y84th98h';
my $queue  = 'events_queue';

my @files = @ARGV;

die "Missing input file" unless (@files);

foreach my $inputFile (@files) {

	die "File does not exist" unless (-e $inputFile);
	open(my $fh, '<:encoding(UTF-8)', $inputFile) or die "Could not open file '$inputFile' $!";
	
	print "Populating $queue from file $inputFile...\n";
	
	my $mq = Net::RabbitMQ->new();
	$mq->connect($host, { user => $user, password => $passwd });
	$mq->channel_open(1);

	my $count = 0;
	while (<$fh>) {
	    my $line = $_;
	    $mq->publish(1, $queue, $line) if ($line);
	    if ($count++ == 10000) {
		sleep 5;
		$count = 0;
	    }
	}
	close($fh);
		
	$mq->disconnect();

	print "Waiting 60 seconds...\n";
	sleep 60;
}

print "Done.\n\n";

1;
