#!/usr/bin/perl

use strict;
use utf8;
use Net::RabbitMQ;

binmode STDOUT, ':utf8';

my $host               = 'localhost';
my $user               = 'admin';
my $passwd             = 'driver';
my $queue              = 'test_queue';

my @files = @ARGV;

die "Missing input file" unless (@files);

foreach my $inputFile (@files) {

	die "File does not exist" unless (-e $inputFile);
	open(my $fh, '<:encoding(UTF-8)', $inputFile) or die "Could not open file '$inputFile' $!";
	
	print "Populating $queue from file $inputFile...\n";
	
	my $mq = Net::RabbitMQ->new();
	$mq->connect($host, { user => $user, password => $passwd });
	$mq->channel_open(1);
	
	foreach my $line (<$fh>) {
		$mq->publish(1, $queue, $line) if ($line);
	}
	close($fh);
		
	$mq->disconnect();
}

print "Done.\n\n";

1;
