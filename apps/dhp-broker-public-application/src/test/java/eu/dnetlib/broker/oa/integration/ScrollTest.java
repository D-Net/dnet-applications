package eu.dnetlib.broker.oa.integration;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import eu.dnetlib.broker.api.ShortEventMessage;
import eu.dnetlib.broker.oa.controllers.ScrollPage;

public class ScrollTest {

	private static final String baseUrl = "http://...";
	// private static final String baseUrl = "http://broker1-dev-dnet.d4science.org:8080"; // DEV
	// private static final String baseUrl = "http://lbs.openaire.eu:8080"; // PRODUCTION

	private static final String subscriptionId = "sub-c9767c84-3597-462b-803b-2d3e09de44c4";

	public class TestScrollPage extends ScrollPage<ShortEventMessage> {}

	@Test
	@Disabled
	public void testScroll() {

		int total = 0;

		TestScrollPage page = getPage(baseUrl + "/api/openaireBroker/scroll/notifications/start/ " + subscriptionId);
		total += page.getValues().size();

		while (!page.isCompleted()) {
			page = getPage(baseUrl + "/api/openaireBroker/scroll/notifications/ " + page.getId());
			total += page.getValues().size();
			for (final ShortEventMessage p : page.getValues()) {
				// DO SOMETHING
			}
		}

		System.out.println("\nTOTAL: " + total);
	}

	private TestScrollPage getPage(final String url) {
		System.out.println(url);
		final TestScrollPage p = new RestTemplate().getForObject(url, TestScrollPage.class);
		System.out.println("Page size: " + p.getValues().size());
		return p;
	}

}
