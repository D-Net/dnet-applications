#!/usr/bin/perl

use strict;
use utf8;

use threads;
use threads::shared;

binmode STDOUT, ':utf8';

my $nThreads = 4;

foreach my $i (1..$nThreads) {
	threads->create(\&populate);
}

sleep 10;

foreach my $t (threads->list()) {
	print "WAITING " . $t->tid() . "\n";
	$t->join();
}

print "Done.\n\n";

#end();

sub populate {
	my $i = threads->tid();
	
	print "Start $i\n";
	sleep 3 + $i;
	print "End $i\n";
	
	return 1;	

}

1;
