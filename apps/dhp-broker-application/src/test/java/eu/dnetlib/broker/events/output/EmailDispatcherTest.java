package eu.dnetlib.broker.events.output;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import eu.dnetlib.broker.common.elasticsearch.Event;
import eu.dnetlib.broker.common.subscriptions.Subscription;

public class EmailDispatcherTest {

	@Test
	void testGenerateMailContent() throws Exception {
		final EmailDispatcher dispatcher = new EmailDispatcher();
		dispatcher.setEmailTemplate(new ClassPathResource("/templates/dhp_openaire_mail.st"));
		dispatcher.setDashboardBaseUrl("https://provide.openaire.eu/content/notifications/");
		dispatcher.setHelpdesk("https://www.openaire.eu/support/helpdesk");
		dispatcher.setPublicApi("https://api.openaire.eu/broker");

		final Subscription s = new Subscription();
		s.setSubscriptionId("sub-00001");
		s.setTopic("ENRICH/MISSING/ABSTRACT");

		final Event events = new Event();

		final Map<String, Object> params = new HashMap<>();
		params.put("oa_notifications_total", 1023);
		params.put("oa_datasource", "ISTI OpenPortal");

		System.out.println(dispatcher.generateMailContent(s, params, events));
	}

}
