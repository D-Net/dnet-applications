package eu.dnetlib.broker.tests;

import java.util.Arrays;

import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.test.context.ContextConfiguration;

import eu.dnetlib.broker.common.elasticsearch.Event;
import eu.dnetlib.broker.objects.OaBrokerEventPayload;
import eu.dnetlib.broker.objects.OaBrokerMainEntity;
import eu.dnetlib.broker.openaire.AdvQueryObject;
import eu.dnetlib.broker.openaire.ElasticSearchQueryUtils;
import eu.dnetlib.broker.openaire.Range;

@Disabled
@SpringBootTest
@ContextConfiguration(locations = {
	"classpath:/applicationContext-test-queries.xml"
})
public class OpenaireQueriesTest {

	private static final String indexName = Event.class.getAnnotation(Document.class).indexName();

	@Autowired
	private ElasticsearchOperations esOperations;

	@Test
	@Disabled
	public void testAdvancedShowEvents() {

		final AdvQueryObject qObj = new AdvQueryObject();
		qObj.setDatasource("INRIA a CCSD electronic archive server");
		qObj.setTopic("ENRICH/MORE/PID");
		qObj.setTitles(Arrays.asList("Aerosol", "profiles"));
		qObj.setAuthors(Arrays.asList("Arnaud", "Kulmala", "Morten"));
		qObj.setSubjects(Arrays.asList("satellite", "cloud"));
		qObj.setTrust(new Range("0", "1"));

		final BoolQueryBuilder mapQuery = QueryBuilders.boolQuery();
		ElasticSearchQueryUtils.addMapCondition(mapQuery, "map.targetDatasourceName", qObj.getDatasource());
		ElasticSearchQueryUtils.addMapCondition(mapQuery, "map.targetResultTitle", qObj.getTitles());
		ElasticSearchQueryUtils.addMapCondition(mapQuery, "map.targetAuthors", qObj.getAuthors());
		ElasticSearchQueryUtils.addMapCondition(mapQuery, "map.targetSubjects", qObj.getSubjects());
		ElasticSearchQueryUtils.addMapConditionForTrust(mapQuery, "map.trust", qObj.getTrust());
		ElasticSearchQueryUtils.addMapConditionForDates(mapQuery, "map.targetDateofacceptance", qObj.getDates());

		final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
			.withQuery(QueryBuilders.boolQuery()
				.must(QueryBuilders.matchQuery("topic", qObj.getTopic()))
				.must(QueryBuilders.nestedQuery("map", mapQuery, ScoreMode.None)))
			.withSearchType(SearchType.DEFAULT)
			.withFields("payload")
			.withPageable(PageRequest.of(0, 10))
			.build();

		final SearchHits<Event> page = esOperations.search(searchQuery, Event.class, IndexCoordinates.of(indexName));

		page.stream()
			.map(SearchHit::getContent)
			.map(Event::getPayload)
			.map(OaBrokerEventPayload::fromJSON)
			.map(OaBrokerEventPayload::getResult)
			.map(OaBrokerMainEntity::getTitles)
			.forEach(System.out::println);

	}

}
