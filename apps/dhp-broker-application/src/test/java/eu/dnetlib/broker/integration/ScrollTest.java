package eu.dnetlib.broker.integration;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import eu.dnetlib.broker.objects.OaBrokerEventPayload;
import eu.dnetlib.broker.openaire.ScrollPage;

public class ScrollTest {

	private static final String baseUrl = "http://...";
	// private static final String baseUrl = "http://broker1-dev-dnet.d4science.org:8080"; // DEV
	// private static final String baseUrl = "http://lbs.openaire.eu:8080"; // PRODUCTION

	private static final String subscriptionId = "sub-c9767c84-3597-462b-803b-2d3e09de44c4";

	@Test
	@Disabled
	public void testScroll() {

		int total = 0;

		ScrollPage page = getPage(baseUrl + "/api/openaireBroker/scroll/notifications/start/ " + subscriptionId);
		total += page.getValues().size();

		while (!page.isCompleted()) {
			page = getPage(baseUrl + "/api/openaireBroker/scroll/notifications/ " + page.getId());
			total += page.getValues().size();
			for (final OaBrokerEventPayload p : page.getValues()) {
				// DO SOMETHING
			}
		}

		System.out.println("\nTOTAL: " + total);
	}

	private ScrollPage getPage(final String url) {
		System.out.println(url);
		final ScrollPage p = new RestTemplate().getForObject(url, ScrollPage.class);
		System.out.println("Page size: " + p.getValues().size());
		return p;
	}

}
