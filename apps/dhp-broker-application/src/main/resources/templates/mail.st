You have received this mail because you are owner of the following subscription:

<ul>
	<li><b>ID: </b>$sub.subscriptionId$</li>
	<li><b>Subscriber: </b>$sub.subscriber$</li>
	<li><b>Topic: </b>$sub.topic$</li>
	<li><b>Frequency: </b>$sub.frequency$</li>
	<li><b>Mode: </b>$sub.mode$</li>
	<li><b>Conditions: </b>$sub.conditions$</li>
</ul>

<hr />
<p>There are $total$ event(s) that may be of interest to you (The following list is limited to $max$ events):</p>

<ol>
$events:{e|
	<li>
		<ul>
			<li><b>ID: </b>$e.eventId$</li>
			<li><b>Producer: </b>$e.producerId$</li>
			<li><b>Topic: </b>$e.topic$</li>
			<li><b>Payload: </b>$e.payload$</li>
			<li><b>Event Creation Date: </b>$e.creationDate$</li>
			<li><b>Event Expiry Date: </b>$e.expiryDate$</li>
			<li><b>isInstantMessage: </b>$e.instantMessage$</li>
			<li><b>Indexed fields: </b><ul>$e.map.keys:{k|<li><b>$k$: </b>$e.map.(k)$</li>}$</ul></li>
		</ul>
	</li>
}$
</ol>

<hr />
This email message was auto-generated. Please do not respond.
