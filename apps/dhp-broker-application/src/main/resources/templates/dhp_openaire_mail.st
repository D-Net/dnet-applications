You have received this mail because you are owner of the following subscription:

<ul>
	<li><b>Datasource Name:  </b>$oa_datasource$</li>
	<li><b>Topic:            </b>$sub.topic$</li>
	<li><b>Number of events: </b>$oa_notifications_total$</li>
</ul>

<hr />

<p>
You can access the notified events at this address: 
<a href="$dashboardBaseUrl$$sub.subscriptionId$">$dashboardBaseUrl$$sub.subscriptionId$</a><br /> 
or using the <a href="$publicApi$"">Public Broker Service API</a> (the ID of the current subscription is <b>$sub.subscriptionId$</b>).
</p>

<hr />
This email message was auto-generated. Please do not respond. If you need support, please contact the 
<a href="$helpdesk$">OpenAIRE helpdesk</a>.
  
