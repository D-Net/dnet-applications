You have received this mail because you are owner of the following subscription:

<ul>
	<li><b>Topic: </b>$sub.topic$</li>
	<li><b>Frequency: </b>$sub.frequency$</li>
	<li><b>Mode: </b>$sub.mode$</li>
</ul>

<hr />
<p>
There are $total$ event(s) related to the following publications<br />
(The list is limited to $max$ events, see the Content Provider Dashboard for all):
</p>

<ol>
$events:{e|
	<li>
		<b>$e.map.targetResultTitle$</b><br/>
		<small style="color: grey">$e.map.targetAuthors$</small>
	</li>
}$
</ol>

<hr />
This email message was auto-generated. Please do not respond.
