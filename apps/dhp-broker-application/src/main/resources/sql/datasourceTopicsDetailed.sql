select
	topic     as topic,
	sum(size) as size 
from oa_datasource_stats 
where lower(name) = lower(?) 
group by topic 
order by sum(size) desc;
