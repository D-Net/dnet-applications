begin;
delete from oa_datasource_stats; 
insert into oa_datasource_stats (id, name, type, topic, size) select id, name, type, topic, size from oa_datasource_stats_temp;
drop table oa_datasource_stats_temp;
commit;
