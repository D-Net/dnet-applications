CREATE TABLE subscriptions (
    subscriptionid         text PRIMARY KEY,
    conditions             text,
    creation_date          timestamp without time zone DEFAULT '2018-01-01 00:00:00'::timestamp without time zone NOT NULL,
    frequency text         NOT NULL,
    last_notification_date timestamp without time zone,
    mode                   text NOT NULL,
    subscriber             text NOT NULL,
    topic                  text NOT NULL
);

CREATE TABLE topic_types (
    id          text PRIMARY KEY,
    name        text UNIQUE NOT NULL,
    expression  text UNIQUE NOT NULL,
    map_keys    text,
    producer_id text,
    regex       text UNIQUE NOT NULL
);

CREATE TABLE oa_datasource_stats (
	id    text NOT NULL,
	name  text NOT NULL,
	type  text NOT NULL,
	topic text NOT NULL,
	size  bigint NOT NULL,
	PRIMARY KEY (id, topic) 
);

INSERT INTO public.topic_types (id, name, expression, map_keys, producer_id, regex) VALUES ('tt-a739fa2b-fde0-4eb2-bcee-6e7f277347db', 'ENRICH',               'ENRICH/<cond>/<field>',            'targetDatasourceName', 'OpenAIRE', '^ENRICH\/[a-zA-Z0-9._-]+\/[a-zA-Z0-9._-]+$');
INSERT INTO public.topic_types (id, name, expression, map_keys, producer_id, regex) VALUES ('tt-93be0404-e7fb-43bb-9a0a-d317f418ed6d', 'ENRICH_WITH_SUBFIELD', 'ENRICH/<cond>/<field>/<subfield>', 'targetDatasourceName', 'OpenAIRE', '^ENRICH\/[a-zA-Z0-9._-]+\/[a-zA-Z0-9._-]+\/[a-zA-Z0-9._-]+$');
INSERT INTO public.topic_types (id, name, expression, map_keys, producer_id, regex) VALUES ('tt-80978da9-1859-47aa-9897-0a0c372365a1', 'ADD',                  'ADD/<field>',                      'targetDatasourceName', 'OpenAIRE', '^ADD\/[a-zA-Z0-9._-]+$');
INSERT INTO public.topic_types (id, name, expression, map_keys, producer_id, regex) VALUES ('tt-75e4b98f-17b8-4c2a-b3c6-d0ad7dfb4d51', '*',                    '*',                                'targetDatasourceName', 'OpenAIRE', '^\*$');

