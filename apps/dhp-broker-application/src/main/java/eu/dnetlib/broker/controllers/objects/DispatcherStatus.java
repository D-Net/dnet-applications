package eu.dnetlib.broker.controllers.objects;

public class DispatcherStatus implements Comparable<DispatcherStatus> {

	private final String name;
	private final long count;
	private final long countErrors;
	private final String lastError;

	public DispatcherStatus(final String name, final long count, final long countErrors, final String lastError) {
		this.name = name;
		this.count = count;
		this.countErrors = countErrors;
		this.lastError = lastError;
	}

	public String getName() {
		return name;
	}

	public long getCount() {
		return count;
	}

	public long getCountErrors() {
		return countErrors;
	}

	public String getLastError() {
		return lastError;
	}

	@Override
	public int compareTo(final DispatcherStatus o) {
		if (this.name == null) { return -1; }
		if (o.name == null) { return 1; }
		return this.name.compareTo(o.name);
	}

}
