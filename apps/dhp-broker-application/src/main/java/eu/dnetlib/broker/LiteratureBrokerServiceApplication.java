package eu.dnetlib.broker;

import java.util.ArrayList;
import java.util.List;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import eu.dnetlib.common.app.AbstractDnetApp;
import io.swagger.v3.oas.models.tags.Tag;

@SpringBootApplication
public class LiteratureBrokerServiceApplication extends AbstractDnetApp {

	public static final String TAG_EVENTS = "Events";
	public static final String TAG_SUBSCRIPTIONS = "Subscriptions";
	public static final String TAG_NOTIFICATIONS = "Notifications";
	public static final String TAG_TOPIC_TYPES = "Topic Types";
	public static final String TAG_OPENAIRE = "OpenAIRE";
	public static final String TAG_MATCHING = "Subscription-Event Matching";

	public static void main(final String[] args) {
		SpringApplication.run(LiteratureBrokerServiceApplication.class, args);
	}

	@Bean
	public GroupedOpenApi publicApi() {
		return GroupedOpenApi.builder()
			.group("Broker APIs")
			.pathsToMatch("/api/**")
			.build();
	}

	@Override
	protected String swaggerTitle() {
		return "OpenAIRE Broker API";
	}

	@Override
	protected List<Tag> swaggerTags() {
		final List<Tag> tags = new ArrayList<>();
		tags.add(new Tag().name(TAG_EVENTS).description("Events management"));
		tags.add(new Tag().name(TAG_SUBSCRIPTIONS).description("Subscriptions management"));
		tags.add(new Tag().name(TAG_NOTIFICATIONS).description("Notifications management"));
		tags.add(new Tag().name(TAG_TOPIC_TYPES).description("Topic types management"));
		tags.add(new Tag().name(TAG_OPENAIRE).description("OpenAIRE use case"));
		return tags;
	}

}
