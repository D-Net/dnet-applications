package eu.dnetlib.broker.events.output;

import java.util.Map;

import eu.dnetlib.broker.common.elasticsearch.Event;
import eu.dnetlib.broker.common.subscriptions.NotificationMode;
import eu.dnetlib.broker.common.subscriptions.Subscription;

public interface NotificationDispatcher {

	void sendNotification(Subscription subscription, Event... events);

	void resetCount();

	String getDispatcherName();

	long count();

	NotificationMode getMode();

	void sendNotification(Subscription subscription, Map<String, Object> params);

	long countErrors();

	String lastError();
}
