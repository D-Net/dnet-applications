package eu.dnetlib.broker.openaire;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/openaire")
	public String index_1() {
		return "redirect:/openaire/index.html";
	}

	@GetMapping("/openaireBroker")
	public String index_2() {
		return "redirect:/openaire/index.html";
	}

	@GetMapping("/openaireBroker/index.html")
	public String index_3() {
		return "redirect:/openaire/index.html";
	}

}
