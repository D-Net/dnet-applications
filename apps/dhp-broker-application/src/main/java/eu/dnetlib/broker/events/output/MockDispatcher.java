package eu.dnetlib.broker.events.output;

import java.io.StringWriter;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import eu.dnetlib.broker.common.elasticsearch.Event;
import eu.dnetlib.broker.common.subscriptions.NotificationMode;
import eu.dnetlib.broker.common.subscriptions.Subscription;

@Component
@Profile("dev")
public class MockDispatcher extends AbstractNotificationDispatcher<String> {

	private static final Log log = LogFactory.getLog(MockDispatcher.class);

	@Override
	protected String prepareAction(final Subscription subscription, final Event... events) throws Exception {
		final StringWriter sw = new StringWriter();
		sw.write("\n********************************");
		sw.write("\n* New notification");
		sw.write("\n*  -  Subscription : " + subscription);
		sw.write("\n*  -  N. events    : " + events.length);
		sw.write("\n********************************");
		return sw.toString();
	}

	@Override
	protected String prepareAction(final Subscription subscription, final Map<String, Object> params) throws Exception {
		final StringWriter sw = new StringWriter();
		sw.write("\n********************************");
		sw.write("\n* New notification");
		sw.write("\n*  -  Subscription : " + subscription);
		params.entrySet().forEach(e -> sw.write("\n*  -  " + e.getKey() + ": " + e.getValue()));
		sw.write("\n********************************");
		return sw.toString();
	}

	@Override
	protected void performAction(final String message) throws Exception {
		log.info(message);
	}

	@Override
	public NotificationMode getMode() {
		return NotificationMode.MOCK;
	}

}
