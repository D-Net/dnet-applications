package eu.dnetlib.broker.openaire;

import java.util.List;

import eu.dnetlib.broker.objects.OaBrokerEventPayload;

public class ScrollPage {

	private String id;

	private boolean completed;

	private List<OaBrokerEventPayload> values;

	public ScrollPage() {}

	public ScrollPage(final String id, final boolean completed, final List<OaBrokerEventPayload> values) {
		this.id = id;
		this.completed = completed;
		this.values = values;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(final boolean completed) {
		this.completed = completed;
	}

	public List<OaBrokerEventPayload> getValues() {
		return values;
	}

	public void setValues(final List<OaBrokerEventPayload> values) {
		this.values = values;
	}

}
