package eu.dnetlib.broker.events.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.dnetlib.broker.common.elasticsearch.Event;
import eu.dnetlib.broker.common.elasticsearch.EventRepository;
import eu.dnetlib.broker.common.elasticsearch.NotificationRepository;
import eu.dnetlib.broker.common.subscriptions.SubscriptionRepository;
import eu.dnetlib.broker.events.output.DispatcherManager;
import eu.dnetlib.broker.utils.LbsQueue;

@Component
public class EventManagerFactory {

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private NotificationRepository notificationRepository;

	@Autowired
	private SubscriptionRepository subscriptionRepo;

	@Autowired
	private DispatcherManager dispatcherManager;

	public EventManager newEventManager(final LbsQueue<String, Event> queue) {
		return new EventManager(this.eventRepository, this.notificationRepository, this.subscriptionRepo, this.dispatcherManager, queue);
	}

}
