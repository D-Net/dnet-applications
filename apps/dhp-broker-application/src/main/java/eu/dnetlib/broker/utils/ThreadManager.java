package eu.dnetlib.broker.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ThreadManager {

	private final List<Thread> threads = new ArrayList<>();

	public void newThread(final String name, final Runnable r) {
		final Thread thread = new Thread(r, name);
		this.threads.add(thread);
		thread.start();

	}

	public List<Thread> getThreads() {
		return this.threads;
	}

}
