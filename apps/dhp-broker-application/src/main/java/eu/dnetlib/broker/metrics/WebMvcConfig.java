package eu.dnetlib.broker.metrics;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	@Autowired
	private List<MetricInterceptor> interceptors;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		interceptors.stream()
				.filter(metricInterceptor -> metricInterceptor instanceof HandlerInterceptor)
				.map(metricInterceptor -> (HandlerInterceptor) metricInterceptor)
				.forEach(handlerInterceptor -> registry.addInterceptor(handlerInterceptor));
	}

}
