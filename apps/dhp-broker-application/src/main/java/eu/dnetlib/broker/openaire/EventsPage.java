package eu.dnetlib.broker.openaire;

import java.util.List;

import eu.dnetlib.broker.objects.OaBrokerEventPayload;

public class EventsPage {

	private final String datasource;
	private final String topic;
	private final long currPage;
	private final long totalPages;
	private final long total;
	private final List<OaBrokerEventPayload> values;

	public EventsPage(final String datasource, final String topic, final long currPage, final long totalPages, final long total,
		final List<OaBrokerEventPayload> values) {
		this.datasource = datasource;
		this.topic = topic;
		this.currPage = currPage;
		this.totalPages = totalPages;
		this.total = total;
		this.values = values;
	}

	public String getDatasource() {
		return this.datasource;
	}

	public String getTopic() {
		return this.topic;
	}

	public long getCurrPage() {
		return this.currPage;
	}

	public long getTotalPages() {
		return this.totalPages;
	}

	public long getTotal() {
		return this.total;
	}

	public List<OaBrokerEventPayload> getValues() {
		return this.values;
	}

}
