package eu.dnetlib.broker.controllers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.broker.LiteratureBrokerServiceApplication;
import eu.dnetlib.broker.common.topics.TopicType;
import eu.dnetlib.broker.common.topics.TopicTypeRepository;
import eu.dnetlib.common.controller.AbstractDnetController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/topic-types")
@Tag(name = LiteratureBrokerServiceApplication.TAG_TOPIC_TYPES)
public class TopicsController extends AbstractDnetController {

	@Autowired
	private TopicTypeRepository topicTypeRepo;

	private final Predicate<String> verifyExpression =
		Pattern.compile("^([a-zA-Z0-9._-]+|<[a-zA-Z0-9._-]+>)(\\/([a-zA-Z0-9._-]+|<[a-zA-Z0-9._-]+>))+$").asPredicate();

	@Operation(summary = "Return the list of topic types")
	@GetMapping("")
	public Iterable<TopicType> listTopicTypes() {
		return topicTypeRepo.findAll();
	}

	@Operation(summary = "Register a new topic type")
	@PostMapping("/add")
	public TopicType registerTopicType(@RequestParam final String name,
		@RequestParam final String expression,
		@RequestParam final String producerId,
		@RequestParam final String mapKeys) {

		if (StringUtils.isBlank(name)) { throw new IllegalArgumentException("name is empty"); }
		if (StringUtils.isBlank(expression)) { throw new IllegalArgumentException("expression is empty"); }
		if (!verifyExpression.test(expression)) { throw new IllegalArgumentException("Invalid expression: " + expression); }

		final String id = "tt-" + UUID.randomUUID();
		final TopicType type = new TopicType(id, name, expression, producerId, mapKeys);

		topicTypeRepo.save(type);

		return type;
	}

	@Operation(summary = "Return a topic type by ID")
	@GetMapping("/{id}")
	public TopicType getTopicType(@PathVariable final String id) {
		return topicTypeRepo.findById(id).get();
	}

	@Operation(summary = "Delete a topic type by ID")
	@DeleteMapping("/{id}")
	public List<String> deleteTopicType(@PathVariable final String id) {
		topicTypeRepo.deleteById(id);
		return Arrays.asList("Done.");
	}

	@Operation(summary = "Delete all topic types")
	@DeleteMapping("")
	public Map<String, Object> clearTopicTypes() {
		final Map<String, Object> res = new HashMap<>();
		topicTypeRepo.deleteAll();
		res.put("deleted", "all");
		return res;
	}
}
