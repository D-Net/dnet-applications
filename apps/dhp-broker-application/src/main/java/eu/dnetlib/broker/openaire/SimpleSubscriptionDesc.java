package eu.dnetlib.broker.openaire;

import java.util.Date;

public class SimpleSubscriptionDesc {

	private String id;
	private String datasource;
	private String topic;
	private Date creationDate;
	private Date lastNotificationDate;
	private long count;

	public SimpleSubscriptionDesc(final String id, final String datasource, final String topic, final Date creationDate, final Date lastNotificationDate,
			final long count) {
		this.id = id;
		this.datasource = datasource;
		this.topic = topic;
		this.creationDate = creationDate;
		this.setLastNotificationDate(lastNotificationDate);
		this.count = count;
	}

	public String getId() {
		return this.id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getDatasource() {
		return this.datasource;
	}

	public void setDatasource(final String datasource) {
		this.datasource = datasource;
	}

	public String getTopic() {
		return this.topic;
	}

	public void setTopic(final String topic) {
		this.topic = topic;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastNotificationDate() {
		return this.lastNotificationDate;
	}

	public void setLastNotificationDate(final Date lastNotificationDate) {
		this.lastNotificationDate = lastNotificationDate;
	}

	public long getCount() {
		return this.count;
	}

	public void setCount(final long count) {
		this.count = count;
	}

}
