package eu.dnetlib.broker.controllers.objects;

public class ThreadStatus implements Comparable<ThreadStatus> {

	private final String name;
	private final String state;
	private final boolean alive;

	public ThreadStatus(final String name, final String state, final boolean alive) {
		this.name = name;
		this.state = state;
		this.alive = alive;
	}

	public String getName() {
		return this.name;
	}

	public String getState() {
		return this.state;
	}

	public boolean isAlive() {
		return this.alive;
	}

	@Override
	public int compareTo(final ThreadStatus t) {
		if (this.name == null) { return -1; }
		if (t.name == null) { return 1; }
		return this.name.compareTo(t.name);
	}

}
