package eu.dnetlib.broker.controllers.objects;

public class BufferStatus implements Comparable<BufferStatus> {

	private final String name;
	private final long size;
	private final long lost;
	private final long skipped;
	private final long invalid;

	public BufferStatus(final String name, final long size, final long lost, final long skipped, final long invalid) {
		this.name = name;
		this.size = size;
		this.lost = lost;
		this.skipped = skipped;
		this.invalid = invalid;
	}

	public String getName() {
		return this.name;
	}

	public long getSize() {
		return this.size;
	}

	public long getLost() {
		return this.lost;
	}

	public long getSkipped() {
		return this.skipped;
	}

	public long getInvalid() {
		return this.invalid;
	}

	@Override
	public int compareTo(final BufferStatus o) {
		if (this.name == null) { return -1; }
		if (o.name == null) { return 1; }
		return this.name.compareTo(o.name);
	}

}
