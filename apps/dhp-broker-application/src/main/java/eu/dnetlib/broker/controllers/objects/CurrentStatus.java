package eu.dnetlib.broker.controllers.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.dnetlib.broker.common.properties.DatabaseProperties;
import eu.dnetlib.broker.common.properties.ElasticSearchProperties;
import eu.dnetlib.broker.properties.EmailProperties;

@Component
public class CurrentStatus {

	@Autowired
	private DatabaseProperties db;
	@Autowired
	private ElasticSearchProperties elasticSearch;

	@Autowired
	private EmailProperties emails;

	private List<ThreadStatus> threads;
	private List<BufferStatus> buffers = new ArrayList<>();
	private List<DispatcherStatus> dispatchers = new ArrayList<>();
	private final Map<String, Long> totals = new HashMap<>();

	public EmailProperties getEmails() {
		return this.emails;
	}

	public void setEmails(final EmailProperties emails) {
		this.emails = emails;
	}

	public List<ThreadStatus> getThreads() {
		return this.threads;
	}

	public void setThreads(final List<ThreadStatus> threads) {
		this.threads = threads;
	}

	public List<BufferStatus> getBuffers() {
		return this.buffers;
	}

	public void setBuffers(final List<BufferStatus> buffers) {
		this.buffers = buffers;
	}

	public List<DispatcherStatus> getDispatchers() {
		return this.dispatchers;
	}

	public void setDispatchers(final List<DispatcherStatus> dispatchers) {
		this.dispatchers = dispatchers;
	}

	public Map<String, Long> getTotals() {
		return this.totals;
	}

	public ElasticSearchProperties getElasticSearch() {
		return this.elasticSearch;
	}

	public void setElasticSearch(final ElasticSearchProperties elasticSearch) {
		this.elasticSearch = elasticSearch;
	}

	public DatabaseProperties getDb() {
		return this.db;
	}

	public void setDb(final DatabaseProperties db) {
		this.db = db;
	}

}
