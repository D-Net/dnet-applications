package eu.dnetlib.broker.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class QueueManager {

	final List<LbsQueue<?, ?>> lbsQueues = new ArrayList<>();

	@Value("${lbs.queues.maxReturnedValues}")
	private int maxReturnedValues;

	public <T, K> LbsQueue<T, K> newQueue(final String name, final Function<T, K> func, final Predicate<K> predicate) {
		final LbsQueue<T, K> queue = new LbsQueue<>(name, func, predicate, this.maxReturnedValues);
		this.lbsQueues.add(queue);
		return queue;
	}

	public <K> LbsQueue<K, K> newQueue(final String name, final Predicate<K> predicate) {
		return newQueue(name, s -> s, predicate);
	}

	public <K> LbsQueue<K, K> newQueue(final String name) {
		return newQueue(name, s -> s, Objects::nonNull);
	}

	public List<LbsQueue<?, ?>> getLbsQueues() {
		return this.lbsQueues;
	}

}
