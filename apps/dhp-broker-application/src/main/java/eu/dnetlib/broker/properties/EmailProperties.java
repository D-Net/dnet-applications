package eu.dnetlib.broker.properties;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "lbs.mail")
public class EmailProperties {

	@NotNull
	private String from;
	@NotNull
	private String fromName;

	private String cc;

	@NotNull
	private String smtpHost;
	@NotNull
	private int smtpPort;

	private String smtpUser;
	private String smtpPassword;

	public String getFrom() {
		return this.from;
	}

	public void setFrom(final String from) {
		this.from = from;
	}

	public String getFromName() {
		return this.fromName;
	}

	public void setFromName(final String fromName) {
		this.fromName = fromName;
	}

	public String getCc() {
		return this.cc;
	}

	public void setCc(final String cc) {
		this.cc = cc;
	}

	public String getSmtpHost() {
		return this.smtpHost;
	}

	public void setSmtpHost(final String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public int getSmtpPort() {
		return this.smtpPort;
	}

	public void setSmtpPort(final int smtpPort) {
		this.smtpPort = smtpPort;
	}

	public String getSmtpUser() {
		return this.smtpUser;
	}

	public void setSmtpUser(final String smtpUser) {
		this.smtpUser = smtpUser;
	}

	public String getSmtpPassword() {
		return this.smtpPassword;
	}

	public void setSmtpPassword(final String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}

}
