package eu.dnetlib.broker.openaire;

import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import eu.dnetlib.broker.common.subscriptions.NotificationFrequency;
import eu.dnetlib.broker.common.subscriptions.NotificationMode;
import eu.dnetlib.broker.common.subscriptions.Subscription;

public class OpenaireSubscription {

	private String subscriber;
	private NotificationFrequency frequency;
	private NotificationMode mode;
	private AdvQueryObject query;

	public OpenaireSubscription() {}

	public OpenaireSubscription(final String subscriber, final NotificationFrequency frequency, final NotificationMode mode,
		final AdvQueryObject query) {
		this.subscriber = subscriber;
		this.frequency = frequency;
		this.mode = mode;
		this.query = query;
	}

	public String getSubscriber() {
		return this.subscriber;
	}

	public void setSubscriber(final String subscriber) {
		this.subscriber = subscriber;
	}

	public NotificationFrequency getFrequency() {
		return this.frequency;
	}

	public void setFrequency(final NotificationFrequency frequency) {
		this.frequency = frequency;
	}

	public NotificationMode getMode() {
		return this.mode;
	}

	public void setMode(final NotificationMode mode) {
		this.mode = mode;
	}

	public AdvQueryObject getQuery() {
		return this.query;
	}

	public void setQuery(final AdvQueryObject query) {
		this.query = query;
	}

	public Subscription asSubscription() {
		if (StringUtils.isBlank(this.subscriber)) { throw new IllegalArgumentException("subscriber is empty"); }
		if (StringUtils.isBlank(this.query.getTopic())) { throw new IllegalArgumentException("topic is empty"); }

		final String subscriptionId = "sub-" + UUID.randomUUID();

		return new Subscription(subscriptionId, this.subscriber, this.query.getTopic(), this.frequency, this.mode, null, new Date(),
			this.query.asListOfConditions());
	}

}
