package eu.dnetlib.broker.controllers.objects;

public class Tool {

	private final String name;
	private final String url;

	public Tool(final String name, final String url) {
		this.name = name;
		this.url = url;
	}

	public String getName() {
		return this.name;
	}

	public String getUrl() {
		return this.url;
	}

}
