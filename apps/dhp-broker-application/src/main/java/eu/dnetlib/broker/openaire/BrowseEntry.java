package eu.dnetlib.broker.openaire;

public class BrowseEntry implements Comparable<BrowseEntry> {

	public final String value;
	public final long size;

	public BrowseEntry(final String value, final long size) {
		this.value = value;
		this.size = size;
	}

	public String getValue() {
		return this.value;
	}

	public long getSize() {
		return this.size;
	}

	@Override
	public int compareTo(final BrowseEntry bv) {
		return Long.compare(getSize(), bv.getSize());
	}

}
