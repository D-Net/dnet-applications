package eu.dnetlib.bioschemas.api.scraper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class ScrapingExecution {

	private String id;
	private Long dateStart;
	private Long dateEnd;
	private ScrapingStatus status = ScrapingStatus.NOT_YET_STARTED;
	private String message;

	private static final Log log = LogFactory.getLog(ScrapingExecution.class);

	public ScrapingExecution() {}

	public ScrapingExecution(final String id, final Long dateStart, final Long dateEnd, final ScrapingStatus status, final String message) {
		this.id = id;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.status = status;
		this.message = message;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public Long getDateStart() {
		return dateStart;
	}

	public void setDateStart(final Long dateStart) {
		this.dateStart = dateStart;
	}

	public Long getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(final Long dateEnd) {
		this.dateEnd = dateEnd;
	}

	public ScrapingStatus getStatus() {
		return status;
	}

	public void setStatus(final ScrapingStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public void startNew(final String message) {
		setId("scraping-" + UUID.randomUUID());
		setDateStart(System.currentTimeMillis());
		setDateEnd(null);
		setStatus(ScrapingStatus.RUNNING);
		setMessage(message);
		log.info(message);
	}

	public void complete() {
		setDateEnd(System.currentTimeMillis());
		setStatus(ScrapingStatus.SUCCESS);

		final long millis = getDateEnd() - getDateStart();
		setMessage(String
			.format("Scraping completed in %d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis) -
				TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));

		log.info(getMessage());

	}

	public void fail(final Throwable e) {
		setDateEnd(new Date().getTime());
		setStatus(ScrapingStatus.FAILED);
		setMessage(e.getMessage());
		log.error("Error scraping", e);
	}

}
