package eu.dnetlib.bioschemas.api.controller;

import eu.dnetlib.common.controller.AbstractDnetController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController extends AbstractDnetController {

	@GetMapping({
		"/doc", "/swagger"
	})
	public String apiDoc() {
		return "redirect:swagger-ui/index.html";
	}

}
