package eu.dnetlib.bioschemas.api;

import io.swagger.v3.oas.models.tags.Tag;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import eu.dnetlib.common.app.AbstractDnetApp;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@EnableCaching
@EnableScheduling
@ComponentScan(basePackages = "eu.dnetlib")
public class MainApplication extends AbstractDnetApp {

	public static final String BIOSCHEMAS_APIS = "D-Net Bioschemas Service APIs";

	public static void main(final String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}

	@Bean
	public GroupedOpenApi publicApi() {
		return GroupedOpenApi.builder()
			.group(BIOSCHEMAS_APIS)
			.pathsToMatch("/api/**")
			.build();
	}

	@Override
	protected String swaggerTitle() {
		return BIOSCHEMAS_APIS;
	}

	@Override
	protected List<Tag> swaggerTags() {
		return Arrays.asList(new Tag().name(BIOSCHEMAS_APIS).description(BIOSCHEMAS_APIS));
	}

	@Override
	protected String swaggerDesc() {
		return BIOSCHEMAS_APIS;
	}

}
