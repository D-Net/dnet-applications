package eu.dnetlib.bioschemas.api.utils;

/**
 * @author enrico.ottonello
 *
 */

public class BioschemasException extends Exception{

	public BioschemasException() {
	}

	public BioschemasException(final String message) {
		super(message);
	}

	public BioschemasException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public BioschemasException(final Throwable cause) {
		super(cause);
	}

	public BioschemasException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
