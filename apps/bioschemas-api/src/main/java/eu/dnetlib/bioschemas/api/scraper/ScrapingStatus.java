package eu.dnetlib.bioschemas.api.scraper;

public enum ScrapingStatus {
	SUCCESS,
	FAILED,
	RUNNING,
	NOT_LAUNCHED,
	NOT_YET_STARTED
}
