package eu.dnetlib.openaire.project.domain;

import java.io.IOException;

import eu.dnetlib.openaire.project.domain.db.ProjectDetails;
import org.junit.jupiter.api.Test;

/**
 * Created by claudio on 05/07/2017.
 */
public class ProjectDetailsTest {

	@Test
	public void testCSV() throws IOException {

		final ProjectDetails p = ProjectDetails.fromCSV(
				"arc_________::ANZCCART,,ANZCCART,{},\"[\"\"\\u003cfundingtree\\u003e\\n      \\u003cfunder\\u003e\\n         \\u003cid\\u003earc_________::ARC\\u003c/id\\u003e\\n         \\u003cshortname\\u003eARC\\u003c/shortname\\u003e\\n         \\u003cname\\u003eAustralian Research Council (ARC)\\u003c/name\\u003e\\n         \\u003cjurisdiction\\u003eAU\\u003c/jurisdiction\\u003e\\n      \\u003c/funder\\u003e\\n      \\u003cfunding_level_0\\u003e\\n         \\u003cid\\u003earc_________::ARC::Special Research initiative (Australian and New Zealand Council for the Care of Animals in Research and Teaching)\\u003c/id\\u003e\\n         \\u003cname\\u003eSpecial Research initiative (Australian and New Zealand Council for the Care of Animals in Research and Teaching)\\u003c/name\\u003e\\n         \\u003cdescription\\u003eSpecial Research initiative (Australian and New Zealand Council for the Care of Animals in Research and Teaching)\\u003c/description\\u003e\\n         \\u003cparent/\\u003e\\n         \\u003cclass\\u003earc:fundingStream\\u003c/class\\u003e\\n      \\u003c/funding_level_0\\u003e\\n   \\u003c/fundingtree\\u003e\"\"]\"");

		System.out.println(p.asJson());

		System.out.println(p.asCSV());


	}

}
