package eu.dnetlib.openaire.context;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.dnetlib.openaire.exporter.model.context.Context;

/**
 * Created by Alessia Bardi on 2019-04-04.
 *
 *
 * @author Alessia Bardi
 */
public class ContextMappingUtilsTest {

	@Test
	// @Disabled
	public void testParseContextProfile() throws IOException {
		final String communityProfile = IOUtils.toString(getClass().getResourceAsStream("community_test.xml"), Charset.defaultCharset());
		final Context context = ContextMappingUtils.parseContext(communityProfile, new LinkedBlockingQueue<>());

		assertNotNull(context);
		assertNotNull(context.getId());

		final Gson gson = new GsonBuilder().setPrettyPrinting().create();
		final String json = gson.toJson(context);
		assertNotNull(json);
		assertFalse(json.isEmpty());

		System.out.println(gson.toJson(gson.fromJson(json, Context.class)));

	}

}
