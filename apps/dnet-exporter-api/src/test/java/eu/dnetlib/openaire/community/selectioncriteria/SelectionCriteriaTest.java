package eu.dnetlib.openaire.community.selectioncriteria;

import com.google.gson.Gson;
import eu.dnetlib.data.bulktag.selectioncriteria.Selection;
import eu.dnetlib.openaire.exporter.model.community.selectioncriteria.Constraint;
import eu.dnetlib.openaire.exporter.model.community.selectioncriteria.Constraints;
import eu.dnetlib.openaire.exporter.model.community.selectioncriteria.SelectionCriteria;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;


public class SelectionCriteriaTest {

    private final String json = "{\"selectioncriteria\": " +
            "{ \"criteria\": [ { \"constraint\": " +
                "[ { \"field\": \"contributor\", \"value\": \"Neuroinformatics\", \"verb\": \"contains\" }  ] } ] }"+
            "}";


    @Test
    public void loadSelectionCriteria(){
        SelectionCriteria sc = new Gson().fromJson(json, SelectionCriteria.class);



    }

    @Test
    public void printSelectionCriteria(){
        SelectionCriteria sc = new SelectionCriteria();

        List<Constraints> list_constraints = new ArrayList<>();

        Constraints constraints = new Constraints();

        List<Constraint> list_constraint = new ArrayList<>();
        Constraint c = new Constraint();
        c.setField("fake");
        c.setValue("fake");
        c.setVerb("fake");
        list_constraint.add(c);
        c = new Constraint();
        c.setField("fake1");
        c.setValue("fake1");
        c.setVerb("fake1");
        list_constraint.add(c);
        constraints.setConstraint(list_constraint);
        list_constraints.add(constraints);

        constraints = new Constraints();
        list_constraint = new ArrayList<>();
        c = new Constraint();
        c.setField("fake");
        c.setValue("fake");
        c.setVerb("fake");
        list_constraint.add(c);
        c = new Constraint();
        c.setField("fake1");
        c.setValue("fake1");
        c.setVerb("fake1");
        list_constraint.add(c);
        constraints.setConstraint(list_constraint);


        list_constraints.add(constraints);
        sc.setCriteria(list_constraints);

        System.out.println(new Gson().toJson(sc));
    }

}
