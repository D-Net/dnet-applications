package eu.dnetlib.openaire.dsm.dao.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eu.dnetlib.openaire.dsm.domain.db.ApiDbEntry;
import eu.dnetlib.openaire.exporter.model.dsm.ApiDetails;

class DsmMappingUtilsTest {

	@BeforeEach
	void setUp() throws Exception {}

	@Test
	void testAsDetailsApiDbEntry() {
		final ApiDetails api = new ApiDetails();
		api.setId("id:123");
		final ApiDbEntry dbEntry = DsmMappingUtils.asDbEntry(api);
		assertEquals(api.getId(), dbEntry.getId());
	}

}
