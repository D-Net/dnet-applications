package eu.dnetlib.openaire.project;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProjectQueryParamsTest {

	private ProjectQueryParams queryParams;

	@BeforeEach
	public void setUp() throws Exception {
		queryParams = new ProjectQueryParams();

	}

	@Test
	public void testVerifyParamWhiteSpace() {
		queryParams.verifyParam("Discovery Projects");
	}

	@Test
	public void testVerifyParamPercentage() {
		queryParams.verifyParam("Discovery%20Projects");
	}

	@Test
	public void testVerifyDateParam() {
		final String correctDate = "2012-03-04";
		assertEquals(correctDate, queryParams.verifyDateParam(correctDate));

	}

	@Test
	public void testVerifyDateParamException() {
		final String wrongDate = "12-12-12";
		assertThrows(IllegalArgumentException.class, () -> queryParams.verifyDateParam(wrongDate));
	}
}
