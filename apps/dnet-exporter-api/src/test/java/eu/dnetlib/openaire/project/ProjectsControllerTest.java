package eu.dnetlib.openaire.project;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.antlr.stringtemplate.StringTemplate;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@Disabled
@SpringBootTest
public class ProjectsControllerTest {

	private static final Log log = LogFactory.getLog(ProjectsControllerTest.class);
	private final String queryTemplate = "/eu/dnetlib/openaire/sql/projects_fundings.sql.st";

	private final Resource expectedQueryTemplate = new ClassPathResource("/eu/dnetlib/openaire/sql/expected_projects_fundings.sql.st");

	private ProjectsController controller;
	private ProjectQueryParams params;

	@BeforeEach
	public void setup() {
		controller = new ProjectsController();
		final Resource template = new ClassPathResource(queryTemplate);

		// TODO reimplement bean injection for testing
		// controller.setProjectsFundingQueryTemplate(template);
		params = new ProjectQueryParams();
	}

	@Test
	public void testObtainFP7Query() throws IllegalArgumentException, IOException {
		params.setFundingProgramme("FP7");
		params.setFundingPath(null);
		final String res = controller.obtainQuery(params);
		final StringTemplate st = new StringTemplate(IOUtils.toString(expectedQueryTemplate.getInputStream(), ProjectsController.UTF8));
		st.setAttribute("fundingprefix", "ec__________::EC::FP7");
		log.debug(res);
		log.debug(st);
		assertEquals(st.toString(), res);
	}

	@Test
	public void testObtainFP7QuerySP1() throws IllegalArgumentException, IOException {
		params.setFundingProgramme("FP7");
		params.setFundingPath("SP1");
		final String res = controller.obtainQuery(params);
		final StringTemplate st = new StringTemplate(IOUtils.toString(expectedQueryTemplate.getInputStream(), ProjectsController.UTF8));
		st.setAttribute("fundingprefix", "ec__________::EC::FP7::SP1");
		log.debug(res);
		assertEquals(st.toString(), res);
	}

	@Test
	public void testObtainFP7QueryHealth() throws IllegalArgumentException, IOException {
		params.setFundingProgramme("FP7");
		params.setFundingPath("SP1::HEALTH");
		final String res = controller.obtainQuery(params);
		final StringTemplate st = new StringTemplate(IOUtils.toString(expectedQueryTemplate.getInputStream(), ProjectsController.UTF8));
		st.setAttribute("fundingprefix", "ec__________::EC::FP7::SP1::HEALTH");
		log.debug(res);
		assertEquals(st.toString(), res);
	}

	@Test
	public void testObtainFP7QueryHealth2() throws IllegalArgumentException, IOException {
		params.setFundingProgramme("FP7");
		params.setFundingPath("%::HEALTH");
		final String res = controller.obtainQuery(params);
		final StringTemplate st = new StringTemplate(IOUtils.toString(expectedQueryTemplate.getInputStream(), ProjectsController.UTF8));
		st.setAttribute("fundingprefix", "ec__________::EC::FP7::%::HEALTH");
		log.debug(res);
		assertEquals(st.toString(), res);
	}

	@Test
	public void testObtainWellcomeTrustQuery() throws IllegalArgumentException, IOException {
		params.setFundingProgramme("WT");
		params.setFundingPath(null);
		final String res = controller.obtainQuery(params);
		final StringTemplate st = new StringTemplate(IOUtils.toString(expectedQueryTemplate.getInputStream(), ProjectsController.UTF8));
		st.setAttribute("fundingprefix", "wt__________::WT");
		log.debug(res);
		assertEquals(st.toString(), res);
	}

	@Test
	public void testObtainFCTQuery() throws IllegalArgumentException, IOException {
		params.setFundingProgramme("FCT");
		params.setFundingPath(null);
		final String res = controller.obtainQuery(params);
		final StringTemplate st = new StringTemplate(IOUtils.toString(expectedQueryTemplate.getInputStream(), ProjectsController.UTF8));
		st.setAttribute("fundingprefix", "fct_________::FCT");
		log.debug(res);
		assertEquals(st.toString(), res);
	}

	@Test
	public void testQueryWithDateParams() throws IllegalArgumentException, IOException {
		params.setFundingProgramme("WT");
		params.setFundingPath(null);
		params.setStartFrom("2015-05-04");
		final String res = controller.obtainQuery(params);
		log.debug(res);
		final StringTemplate st = new StringTemplate(IOUtils.toString(expectedQueryTemplate.getInputStream(), ProjectsController.UTF8));
		st.setAttribute("fundingprefix", "wt__________::WT");
		final String q = st.toString() + " AND startdate >= '2015-05-04'";
		assertEquals(q, res);
	}

	@Test
	public void testObtainSNSFQuery() throws IllegalArgumentException, IOException {
		params.setFundingProgramme("SNSF");
		params.setFundingPath(null);
		final String res = controller.obtainQuery(params);
		final StringTemplate st = new StringTemplate(IOUtils.toString(expectedQueryTemplate.getInputStream(), ProjectsController.UTF8));
		st.setAttribute("fundingprefix", "snsf________::SNSF");
		log.debug(res);
		assertEquals(st.toString(), res);
	}

}
