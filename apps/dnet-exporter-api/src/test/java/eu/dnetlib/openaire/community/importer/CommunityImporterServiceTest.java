package eu.dnetlib.openaire.community.importer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;

import eu.dnetlib.openaire.community.CommunityService;
import eu.dnetlib.openaire.community.model.DbOrganization;
import eu.dnetlib.openaire.community.repository.DbOrganizationRepository;
import eu.dnetlib.openaire.context.ContextMappingUtils;
import eu.dnetlib.openaire.exporter.model.community.CommunityContentprovider;
import eu.dnetlib.openaire.exporter.model.community.CommunityDetails;
import eu.dnetlib.openaire.exporter.model.community.CommunityOrganization;
import eu.dnetlib.openaire.exporter.model.community.CommunityProject;
import eu.dnetlib.openaire.exporter.model.community.SubCommunity;
import eu.dnetlib.openaire.exporter.model.context.Context;

@ExtendWith(MockitoExtension.class)
class CommunityImporterServiceTest {

	// Class under test
	private CommunityImporterService importer;

	@Mock
	private DbOrganizationRepository dbOrganizationRepository;

	@Mock
	private CommunityService service;

	@Mock
	private JdbcTemplate jdbcTemplate;

	@BeforeEach
	public void setUp() {
		importer = new CommunityImporterService();
		importer.setDbOrganizationRepository(dbOrganizationRepository);
		importer.setService(service);
		importer.setJdbcTemplate(jdbcTemplate);
	}

	@Test
	public void testImportPropagationOrganizationsFromProfile() throws Exception {
		final String profile = IOUtils.toString(getClass().getResourceAsStream("old_provision_wf.xml"), StandardCharsets.UTF_8.toString());
		final List<DbOrganization> list = importer.importPropagationOrganizationsFromProfile(profile, true);
		// list.forEach(System.out::println);

		assertEquals(245, list.size());
		assertEquals(1, list.stream().filter(o -> "openorgs____::9dd5545aacd3d8019e00c3f837269746".equals(o.getOrgId())).count());
		assertEquals(2, list.stream().filter(o -> "openorgs____::d11f981828c485cd23d93f7f24f24db1".equals(o.getOrgId())).count());
		assertEquals(14, list.stream().filter(o -> "beopen".equals(o.getCommunity())).count());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testImportCommunity() throws Exception {
		final String profile = IOUtils.toString(getClass().getResourceAsStream("old_community_profile.xml"), StandardCharsets.UTF_8.toString());

		final Queue<Throwable> errors = new LinkedList<>();
		final Context context = ContextMappingUtils.parseContext(profile, errors);
		assertTrue(errors.isEmpty());

		Mockito.when(jdbcTemplate.queryForList(Mockito.anyString(), Mockito.any(Class.class), Mockito.anyString())).thenReturn(Arrays.asList("corda_______"));
		importer.importCommunity(context);

		final ArgumentCaptor<CommunityDetails> detailsCapture = ArgumentCaptor.forClass(CommunityDetails.class);
		final ArgumentCaptor<CommunityProject> projectsCapture = ArgumentCaptor.forClass(CommunityProject.class);
		final ArgumentCaptor<CommunityContentprovider> datasourcesCapture = ArgumentCaptor.forClass(CommunityContentprovider.class);
		final ArgumentCaptor<CommunityOrganization> orgsCapture = ArgumentCaptor.forClass(CommunityOrganization.class);
		final ArgumentCaptor<SubCommunity> subCommunitiesCapture = ArgumentCaptor.forClass(SubCommunity.class);

		Mockito.verify(service, Mockito.times(1)).saveCommunity(detailsCapture.capture());
		Mockito.verify(service, Mockito.times(1)).addCommunityProjects(Mockito.anyString(), projectsCapture.capture());
		Mockito.verify(service, Mockito.times(1)).addCommunityDatasources(Mockito.anyString(), datasourcesCapture.capture());
		Mockito.verify(service, Mockito.times(1)).addCommunityOrganizations(Mockito.anyString(), orgsCapture.capture());
		Mockito.verify(service, Mockito.times(688)).addSubCommunity(Mockito.anyString(), subCommunitiesCapture.capture());

		final CommunityDetails details = detailsCapture.getValue();
		assertEquals("egi", details.getId());
		// System.out.println(details);

		final List<CommunityProject> projects = projectsCapture.getAllValues();
		assertEquals(83, projects.size());
		// projects.forEach(System.out::println);

		final List<CommunityContentprovider> datasources = datasourcesCapture.getAllValues();
		assertEquals(1, datasources.size());
		// datasources.forEach(System.out::println);

		final List<CommunityOrganization> orgs = orgsCapture.getAllValues();
		assertEquals(1, orgs.size());
		// orgs.forEach(System.out::println);

		final List<SubCommunity> subs = subCommunitiesCapture.getAllValues();
		assertEquals(688, subs.size());
		// subs.forEach(System.out::println);

	}

	@Test
	public void testImportCommunityFetFp7() throws Exception {
		final String profile = IOUtils.toString(getClass().getResourceAsStream("old_community_profile_fet-fp7.xml"), StandardCharsets.UTF_8.toString());

		final Queue<Throwable> errors = new LinkedList<>();
		final Context context = ContextMappingUtils.parseContext(profile, errors);
		assertTrue(errors.isEmpty());

		// Mockito.when(jdbcTemplate.queryForList(Mockito.anyString(), Mockito.any(Class.class),
		// Mockito.anyString())).thenReturn(Arrays.asList("corda_______"));
		importer.importCommunity(context);

		final ArgumentCaptor<CommunityDetails> detailsCapture = ArgumentCaptor.forClass(CommunityDetails.class);
		final ArgumentCaptor<CommunityProject> projectsCapture = ArgumentCaptor.forClass(CommunityProject.class);
		final ArgumentCaptor<CommunityContentprovider> datasourcesCapture = ArgumentCaptor.forClass(CommunityContentprovider.class);
		final ArgumentCaptor<CommunityOrganization> orgsCapture = ArgumentCaptor.forClass(CommunityOrganization.class);
		final ArgumentCaptor<SubCommunity> subCommunitiesCapture = ArgumentCaptor.forClass(SubCommunity.class);

		Mockito.verify(service, Mockito.times(1)).saveCommunity(detailsCapture.capture());
		Mockito.verify(service, Mockito.times(1)).addCommunityProjects(Mockito.anyString(), projectsCapture.capture());
		Mockito.verify(service, Mockito.times(1)).addCommunityDatasources(Mockito.anyString(), datasourcesCapture.capture());
		Mockito.verify(service, Mockito.times(1)).addCommunityOrganizations(Mockito.anyString(), orgsCapture.capture());
		Mockito.verify(service, Mockito.times(151)).addSubCommunity(Mockito.anyString(), subCommunitiesCapture.capture());

		final CommunityDetails details = detailsCapture.getValue();
		assertEquals("fet-fp7", details.getId());
		// System.out.println(details);

		final List<CommunityProject> projects = projectsCapture.getAllValues();
		assertEquals(0, projects.size());
		// projects.forEach(System.out::println);

		final List<CommunityContentprovider> datasources = datasourcesCapture.getAllValues();
		assertEquals(0, datasources.size());
		// datasources.forEach(System.out::println);

		final List<CommunityOrganization> orgs = orgsCapture.getAllValues();
		assertEquals(0, orgs.size());
		// orgs.forEach(System.out::println);

		final List<SubCommunity> subs = subCommunitiesCapture.getAllValues();
		assertEquals(151, subs.size());
		subs.forEach(System.out::println);

	}

}
