package eu.dnetlib.openaire.funders;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.dnetlib.openaire.context.ContextMappingUtils;
import eu.dnetlib.openaire.exporter.model.context.Context;
import eu.dnetlib.openaire.exporter.model.funders.FunderDetails;

public class FunderContextClientTest {

	private FunderService fDao;

	@BeforeEach
	public void setUp() {
		fDao = new FunderService();
	}

	@Test
	public void testParseContextProfile() throws IOException {
		final String contextProfile = IOUtils.toString(getClass().getResourceAsStream("ec-fp7.xml"), Charset.defaultCharset());
		final Context context = ContextMappingUtils.parseContext(contextProfile, new LinkedBlockingQueue<>());

		assertNotNull(context);
		assertNotNull(context.getId());

		final Gson gson = new GsonBuilder().setPrettyPrinting().create();
		final String json = gson.toJson(context);
		assertNotNull(json);
		assertFalse(json.isEmpty());

		// System.out.println(gson.toJson(gson.fromJson(json, Context.class)));

		final FunderDetails funderDetails = ContextMappingUtils.asFunderDetails(context);
		// System.out.println(gson.toJson(funderDetails));

	}

}
