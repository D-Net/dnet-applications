package eu.dnetlib.openaire.community;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import eu.dnetlib.openaire.exporter.model.community.CommunitySummary;

/**
 * Created by Alessia Bardi on 2019-04-04.
 *
 * @author Alessia Bardi
 */
//@SpringBootTest
@WebMvcTest(CommunityApiController.class)
public class CommunityApiControllerTest {

	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
		MediaType.APPLICATION_JSON.getType(),
		MediaType.APPLICATION_JSON.getSubtype(),
		Charset.forName("utf8"));

	@Autowired
	private MockMvc mvc;

	@MockBean
	private CommunityApiController communityController;

	@Test
	public void testListCommunities() throws Exception {
		final CommunitySummary cs = new CommunitySummary();
		cs.setDescription("the description");
		cs.setId("id1");
		cs.setLastUpdateDate(LocalDateTime.now());
		cs.setName("X");
		cs.setShortName("x");
		final List<CommunitySummary> csList = singletonList(cs);
		given(communityController.listCommunities()).willReturn(csList);

		mvc.perform(get("/community/communities").contentType(APPLICATION_JSON_UTF8))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$[0].description", is(cs.getDescription())))
			.andExpect(jsonPath("$[0].id", is(cs.getId())))
			// TODO verify the lastUpdateDate format
			// .andExpect(jsonPath("$[0].lastUpdateDate", is(cs.getLastUpdateDate())))
			.andExpect(jsonPath("$[0].name", is(cs.getName())))
			.andExpect(jsonPath("$[0].shortName", is(cs.getShortName())));
	}

}
