select count(*) as count
from (
	select d.id
	from dsm_services d left outer join dsm_api a on (d.id = a.service) 
	group by d.id
	having min(a.first_collection_date) >= cast(? as date)
) as t
