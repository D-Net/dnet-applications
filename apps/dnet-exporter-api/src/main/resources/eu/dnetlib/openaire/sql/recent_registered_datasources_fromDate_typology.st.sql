select count(DISTINCT d.id) as count
from 
	dsm_services d 
	left outer join dsm_api a on (d.id = a.service) 
where 
	d.registrationdate >= cast(? as date)
	and d._typology_to_remove_ like ?
	and d.registrationdate < a.last_collection_date 
	and d.registeredby is not null
	and d.managed = true
	and a.last_collection_total > 0 
	and a.active = true;
