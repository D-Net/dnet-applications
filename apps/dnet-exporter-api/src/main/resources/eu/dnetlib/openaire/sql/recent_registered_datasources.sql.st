select 
	d.id                     as id, 
	d.officialname           as "officialName",
	d.englishname            as "englishName",
	o.legalname              as organization,
	d.eosc_datasource_type   as eosc_datasource_type, 
	d.registeredby           as "registeredBy", 
	d.registrationdate::text as "registrationDate", 
	a.compatibility          as compatibility, 
	a.last_collection_date   as "lastCollectionDate", 
	a.last_collection_total  as "lastCollectionTotal"
from 
	dsm_services d 
	left outer join dsm_api a on (d.id = a.service) 
	left outer join dsm_service_organization dso on (d.id = dso.service) 
	left outer join dsm_organizations o on (o.id = dso.organization) 
where 
	d.registrationdate is not null 
	and d.registrationdate < a.last_collection_date 
	and d.registeredby is not null
	and d.managed = true
	and a.last_collection_total > 0 
	and a.active = true
order by d.registrationdate desc 
limit ?;
