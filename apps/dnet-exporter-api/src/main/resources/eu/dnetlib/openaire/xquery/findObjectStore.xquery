for $x in collection('/db/DRIVER/RepositoryServiceResources/RepositoryServiceResourceType')
where $x[./RESOURCE_PROFILE/BODY/CONFIGURATION/DATASOURCE_ORIGINAL_ID = "%s"]
return $x/RESOURCE_PROFILE/BODY/CONFIGURATION/INTERFACES/INTERFACE[./@compliance="files"]/INTERFACE_EXTRA_FIELD[./@name="last_download_objId"]/text()
