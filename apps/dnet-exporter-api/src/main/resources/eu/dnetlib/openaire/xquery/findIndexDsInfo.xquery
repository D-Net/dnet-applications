distinct-values(
	let $format := collection('/db/DRIVER/ServiceResources/SearchServiceResourceType')//SERVICE_PROPERTIES[./PROPERTY[@key = 'infrastructure' and @value = 'public']]/PROPERTY[@key = "mdformat"]/@value/string()

	for $x in collection('/db/DRIVER/IndexDSResources/IndexDSResourceType')
	where
	    $x//METADATA_FORMAT = $format and
	            $x//METADATA_FORMAT_INTERPRETATION = 'openaire' and
	            $x//METADATA_FORMAT_LAYOUT = 'index'
	return
	    concat($x//RESOURCE_IDENTIFIER/@value/string(), ' @@@ ', $format, ' @@@ ', $format, '-index-openaire')
)
