BEGIN;

-- FIX ARRAY FIELDS
ALTER TABLE communities ALTER COLUMN subjects SET DEFAULT array[]::text[];
UPDATE communities SET subjects = DEFAULT where subjects IS NULL;
ALTER TABLE communities ALTER COLUMN subjects SET NOT NULL;

ALTER TABLE communities ALTER COLUMN fos SET DEFAULT array[]::text[];
UPDATE communities SET fos = DEFAULT where fos IS NULL;
ALTER TABLE communities ALTER COLUMN fos SET NOT NULL;

ALTER TABLE communities ALTER COLUMN sdg SET DEFAULT array[]::text[];
UPDATE communities SET sdg = DEFAULT where sdg IS NULL;
ALTER TABLE communities ALTER COLUMN sdg SET NOT NULL;

ALTER TABLE communities ALTER COLUMN other_zenodo_communities SET DEFAULT array[]::text[];
UPDATE communities SET other_zenodo_communities = DEFAULT where other_zenodo_communities IS NULL;
ALTER TABLE communities ALTER COLUMN other_zenodo_communities SET NOT NULL;

ALTER TABLE communities ALTER COLUMN suggested_acknowledgements SET DEFAULT array[]::text[];
UPDATE communities SET suggested_acknowledgements = DEFAULT where suggested_acknowledgements IS NULL;
ALTER TABLE communities ALTER COLUMN suggested_acknowledgements SET NOT NULL;

-- ADD FIELDS
ALTER TABLE communities ADD COLUMN sub_c_parent text REFERENCES communities(id) ON DELETE CASCADE;
ALTER TABLE communities ADD COLUMN sub_c_category text;
ALTER TABLE communities ADD COLUMN sub_c_claimable boolean;
ALTER TABLE communities ADD COLUMN sub_c_browsable boolean;
ALTER TABLE communities ADD COLUMN sub_c_params jsonb;

-- ADD ON DELETE CASCADE 
ALTER TABLE community_projects     DROP CONSTRAINT community_projects_community_fkey;
ALTER TABLE community_datasources  DROP CONSTRAINT community_datasources_community_fkey;
ALTER TABLE community_support_orgs DROP CONSTRAINT community_support_orgs_community_fkey;
ALTER TABLE community_orgs         DROP CONSTRAINT community_orgs_community_fkey;
ALTER TABLE community_projects     ADD CONSTRAINT community_projects_community_fkey     FOREIGN KEY (community) REFERENCES communities(id) ON DELETE CASCADE;
ALTER TABLE community_datasources  ADD CONSTRAINT community_datasources_community_fkey  FOREIGN KEY (community) REFERENCES communities(id) ON DELETE CASCADE;
ALTER TABLE community_support_orgs ADD CONSTRAINT community_support_orgs_community_fkey FOREIGN KEY (community) REFERENCES communities(id) ON DELETE CASCADE;
ALTER TABLE community_orgs         ADD CONSTRAINT community_orgs_community_fkey         FOREIGN KEY (community) REFERENCES communities(id) ON DELETE CASCADE;

-- NEW INDEX FOR PARENT
CREATE INDEX community_sub_c_parent ON communities(sub_c_parent);


-- COPY OLD sub-community DATA
INSERT INTO communities(
	id,
	sub_c_parent,
	name,
	shortname,
	type,
	sub_c_category,
	sub_c_claimable,
	sub_c_browsable,
	sub_c_params
) SELECT 
	sub_id,
	coalesce(parent, community),
	label,
	label,
	'subcommunity',
	category,
	claim,
	browsable,
	params
FROM community_subs;

DROP TABLE community_subs;

COMMIT;
