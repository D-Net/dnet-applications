\copy (
	SELECT 
		'netherlands'                         AS community,
		'nwo_________::'||md5(substr(id, 15)) AS project_id,
		code                                  AS project_code,
		title                                 AS project_name,
		acronym                               AS project_acronym,
		'NWO'                                 AS project_funder
	FROM projects
	WHERE id LIKE 'nwo\_\_\_\_\_\_\_\_\_::%'
) TO '/tmp/nwo_community_projects.csv' CSV HEADER;
