DROP TABLE IF EXISTS community_subs;
DROP TABLE IF EXISTS community_projects;
DROP TABLE IF EXISTS community_datasources;
DROP TABLE IF EXISTS community_support_orgs;
DROP TABLE IF EXISTS community_orgs;
DROP TABLE IF EXISTS communities;

CREATE TABLE communities (
	id                       text PRIMARY KEY,	
	name                     text NOT NULL,
	shortname                text NOT NULL, -- in the profile is label
	displayname              text,
	displayshortname         text,
	type                     text NOT NULL, -- community, ri, subcommunity
	description              text NOT NULL DEFAULT '',
	featured                 boolean,
	
	-- TODO how to manage this fields for sub-communityies ?? 
	status                   text NOT NULL DEFAULT 'hidden', -- all, manager, hidden, members
	membership               text NOT NULL DEFAULT 'by-invitation', -- open, by-invitation
	claim                    text, -- managers-only, members-only, all
	-- END TODO
	
	subjects                 text[] NOT NULL DEFAULT array[]::text[],
	fos                      text[] NOT NULL DEFAULT array[]::text[],
	sdg                      text[] NOT NULL DEFAULT array[]::text[],
	adv_constraints          jsonb,
	remove_constraints       jsonb,
	main_zenodo_community    text,
	other_zenodo_communities text[] NOT NULL DEFAULT array[]::text[],
	creation_date            timestamp NOT NULL DEFAULT now(),
	last_update              timestamp NOT NULL DEFAULT now(),
	logo_url                 text,
	suggested_acknowledgements text[] NOT NULL DEFAULT array[]::text[],
	plan                     text NOT NULL DEFAULT 'Default',
	
	-- extension for sub-communities
	sub_c_parent             text REFERENCES communities(id) ON DELETE CASCADE, -- NULL for the main community
	sub_c_category           text,
	sub_c_claimable          boolean,
	sub_c_browsable          boolean,
	sub_c_params             jsonb

);

CREATE TABLE community_projects (
	community       text NOT NULL REFERENCES communities(id) ON DELETE CASCADE,
	project_id      text NOT NULL,
	project_code    text NOT NULL,
	project_name    text NOT NULL,
	project_acronym text,
	project_funder  text NOT NULL,
	available_since date NOT NULL default now(),
	PRIMARY KEY (community, project_id)
);

CREATE TABLE community_datasources (
	community       text NOT NULL REFERENCES communities(id) ON DELETE CASCADE,
	ds_id           text NOT NULL,
	ds_name         text NOT NULL,
	ds_officialname text NOT NULL,
	enabled         boolean NOT NULL DEFAULT true,
	constraints     jsonb,
	deposit         boolean NOT NULL DEFAULT false,
	message         text,
	PRIMARY KEY (community, ds_id)
);

CREATE TABLE community_support_orgs (
	community     text NOT NULL REFERENCES communities(id) ON DELETE CASCADE,
	org_name      text NOT NULL,
	org_url       text NOT NULL,
	org_logourl   text NOT NULL,
	PRIMARY KEY (community, org_name)
);

CREATE TABLE community_orgs (
	community   text NOT NULL REFERENCES communities(id) ON DELETE CASCADE,
	org_id      text NOT NULL,
	PRIMARY KEY (community, org_id)
);

CREATE INDEX community_projects_community ON community_projects(community);
CREATE INDEX community_datasources_community ON community_datasources(community);
CREATE INDEX community_support_orgs_community ON community_support_orgs(community);
CREATE INDEX community_orgs_community ON community_orgs(community);
CREATE INDEX community_sub_c_parent ON communities(sub_c_parent);
