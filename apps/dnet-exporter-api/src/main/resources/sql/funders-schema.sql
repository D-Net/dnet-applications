ALTER TABLE dsm_organizations ADD COLUMN registered_funder boolean;


CREATE TABLE dsm_service_funder (
	_dnet_resource_identifier_ varchar(2048) DEFAULT 'temp_'||md5(clock_timestamp()::text)||'_'||md5(random()::text),
	service                    text NOT NULL REFERENCES dsm_services(id) ON DELETE CASCADE,
	funder                     text NOT NULL REFERENCES dsm_organizations(id) ON DELETE CASCADE,
	last_aggregation_date      date,
	PRIMARY KEY(funder, service)
);

INSERT INTO dsm_service_funder(_dnet_resource_identifier_, service, funder, last_aggregation_date)
SELECT
	o.id||'@@'||s.id AS _dnet_resource_identifier_,
	s.id AS service,
	o.id AS funder,
	max(a.last_aggregation_date::date) AS last_aggregation_date
FROM
	dsm_organizations o
	JOIN dsm_service_organization so ON (o.id = so.organization)
	JOIN dsm_services s ON (so.service = s.id)
	JOIN projects p ON p.collectedfrom = s.id
	LEFT OUTER JOIN dsm_api a ON (s.id = a.service)
GROUP BY s.id, o.id;

CREATE OR REPLACE VIEW funders_view AS SELECT
	o.id                AS id, 
	o.legalshortname    AS legalshortname,
	o.legalname         AS legalname,
	o.websiteurl        AS websiteurl, 
	o.logourl           AS logourl, 
	o.country           AS country,
	o.dateofcollection  AS registrationdate,
	o.registered_funder AS registered,
	array_remove(array_agg(DISTINCT s.id||' @=@ '||s.officialname||' @=@ '||s.eosc_datasource_type||' @=@ '||s.namespaceprefix), NULL)  AS datasources,
	array_remove(array_agg(DISTINCT sf.last_aggregation_date ORDER BY sf.last_aggregation_date DESC), NULL) AS aggregationdates,
	array_remove(array_agg(DISTINCT pids.issuertype||' @=@ '||pids.pid), NULL)                              AS pids
FROM 
	dsm_organizations o 
	JOIN dsm_service_funder sf                 ON (o.id = sf.funder) 
	JOIN dsm_services s                        ON (sf.service = s.id) 
	LEFT OUTER JOIN dsm_organizationpids opids ON (o.id = opids.organization)
	LEFT OUTER JOIN dsm_identities pids        ON (opids.pid = pids.pid)
GROUP BY o.id;

GRANT ALL ON dsm_service_funder TO dnetapi;
GRANT ALL ON funders_view TO dnetapi;
