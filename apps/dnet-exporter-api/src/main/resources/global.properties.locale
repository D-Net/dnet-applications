#services.is.host                    =   localhost
services.is.host                    =   dev-openaire.d4science.org
#services.is.port                    =   8280
services.is.port                    =   443
#services.is.protocol                =   http
services.is.protocol                =   https
#services.is.context                 =   app
services.is.context                 =   is
#services.is.baseurl                 =   ${services.is.protocol}://${services.is.host}:${services.is.port}/${services.is.context}/services
services.is.baseurl                 =   ${services.is.protocol}://${services.is.host}:${services.is.port}/${services.is.context}/services

openaire.exporter.isLookupUrl           =   ${services.is.baseurl}/isLookUp
openaire.exporter.objectStoreServiceUrl =   ${services.is.baseurl}/objectStore
openaire.exporter.isRegistryServiceUrl  =   ${services.is.baseurl}/isRegistry

openaire.exporter.requestWorkers    =   10
openaire.exporter.requestTimeout    =   10

openaire.exporter.cxfClientConnectTimeout    =   60000
openaire.exporter.cxfClientReceiveTimeout    =   120000

# JDBC
#openaire.exporter.jdbc.url          =   jdbc:postgresql://localhost:5432/dnet_openaire
openaire.exporter.jdbc.url          =   jdbc:postgresql://localhost:5432/dev_openaire_8280
openaire.exporter.jdbc.user         =   dnetapi
openaire.exporter.jdbc.pwd          =   dnetPwd
openaire.exporter.jdbc.minIdle      =   1
openaire.exporter.jdbc.maxIdle      =   20
openaire.exporter.jdbc.maxRows      =   100

# PROJECTS
openaire.exporter.project.dspaceHeadTemplate    =   classpath:/eu/dnetlib/openaire/st/projects_dspace_header.st
openaire.exporter.project.dspaceTemplate        =   classpath:/eu/dnetlib/openaire/st/projects_dspace_project.st
openaire.exporter.project.dspaceTailTemplate    =   classpath:/eu/dnetlib/openaire/st/projects_dspace_tail.st
openaire.exporter.project.eprintsTemplate       =   classpath:/eu/dnetlib/openaire/st/projects_eprints.st
openaire.exporter.project.tsvFields             =   Grant Agreement Number, Project Acronym, Project Title, Call ID, Start Date, End Date, OA Mandate on Publications, OA Mandate on Datasets, Discipline, Organization, Country, Role, Person Name, Person Second Names, Person Email
openaire.exporter.project.projectsFundingQueryTemplate=classpath:/eu/dnetlib/openaire/sql/projects_fundings.sql.st
openaire.exporter.project.flushSize             =   1000

# DATSOURCES
openaire.exporter.datasource.title		    =	Data Sources
openaire.exporter.datasource.mongoHost      =   localhost
openaire.exporter.datasource.mongoPort      =   27017
openaire.exporter.datasource.mongoConnectionsPerHost = 10
openaire.exporter.datasource.mongoCollectionName = wf_logs
openaire.exporter.datasource.mongoDbName    =   dnet_logs_prod
openaire.exporter.datasource.mongoQueryLimit=   100
openaire.exporter.findSolrIndexUrl          =   /eu/dnetlib/openaire/xquery/findSolrIndexUrl.xquery
openaire.exporter.findIndexDsInfo           =   /eu/dnetlib/openaire/xquery/findIndexDsInfo.xquery
openaire.exporter.findObjectStore           =   /eu/dnetlib/openaire/xquery/findObjectStore.xquery
openaire.exporter.findFunderContexts        =   /eu/dnetlib/openaire/xquery/findFunderContexts.xquery
openaire.exporter.findCommunityContexts     =   /eu/dnetlib/openaire/xquery/findCommunityContexts.xquery
openaire.exporter.findContextProfiles       =   /eu/dnetlib/openaire/xquery/findContextProfiles.xquery
openaire.exporter.findContextProfilesByType =   /eu/dnetlib/openaire/xquery/findContextProfilesByType.xquery
openaire.exporter.getRepoProfile            =   /eu/dnetlib/openaire/xquery/getRepoProfile.xquery

openaire.exporter.contentLoadQuery          =   { "$and" : [ { "system:profileName" : "Graph construction [PROD]" }, { "system:isCompletedSuccessfully" : "true" }, { "reuseContent" : "false" } ] }

# REST API CONFIGURATION
openaire.exporter.swaggerDsm.apiTitle          =   OpenAIRE aggregator REST API
openaire.exporter.swaggerDsm.apiDescription    =   The OpenAIRE data provision REST API allows developers to access the metadata information space of OpenAIRE programmatically.
openaire.exporter.swaggerDsm.apiLicense        =   LICENSED UNDER GNU AFFERO GENERAL PUBLIC LICENSE.
openaire.exporter.swaggerDsm.apiLicenseUrl     =   https://www.gnu.org/licenses/agpl-3.0.txt
openaire.exporter.swaggerDsm.apiContacName     =   D-Net team
openaire.exporter.swaggerDsm.apiContactUrl     =   http://www.openaire.eu
openaire.exporter.swaggerDsm.apiContactEmail   =   dnet-team@isti.cnr.it

openaire.exporter.swaggerProjects.apiTitle          =   OpenAIRE projects REST API
openaire.exporter.swaggerProjects.apiDescription    =   The OpenAIRE projects REST API allows programmatic access to funded research projects metadata.
openaire.exporter.swaggerProjects.apiLicense        =   ${openaire.exporter.swaggerDsm.apiLicense}
openaire.exporter.swaggerProjects.apiLicenseUrl     =   ${openaire.exporter.swaggerDsm.apiLicenseUrl}
openaire.exporter.swaggerProjects.apiContacName     =   ${openaire.exporter.swaggerDsm.apiContacName}
openaire.exporter.swaggerProjects.apiContactUrl     =   ${openaire.exporter.swaggerDsm.apiContactUrl}
openaire.exporter.swaggerProjects.apiContactEmail   =   ${openaire.exporter.swaggerDsm.apiContactEmail}

openaire.exporter.swaggerFunders.apiTitle          =   OpenAIRE funders REST API
openaire.exporter.swaggerFunders.apiDescription    =   The OpenAIRE funders REST API allows programmatic access to the funding agencies metadata in OpenAIRE.
openaire.exporter.swaggerFunders.apiLicense        =   ${openaire.exporter.swaggerDsm.apiLicense}
openaire.exporter.swaggerFunders.apiLicenseUrl     =   ${openaire.exporter.swaggerDsm.apiLicenseUrl}
openaire.exporter.swaggerFunders.apiContacName     =   ${openaire.exporter.swaggerDsm.apiContacName}
openaire.exporter.swaggerFunders.apiContactUrl     =   ${openaire.exporter.swaggerDsm.apiContactUrl}
openaire.exporter.swaggerFunders.apiContactEmail   =   ${openaire.exporter.swaggerDsm.apiContactEmail}

openaire.exporter.swaggerCommunities.apiTitle          =   OpenAIRE Communities REST API
openaire.exporter.swaggerCommunities.apiDescription    =   The OpenAIRE communities REST API allows programmatic access to the communities configurations in OpenAIRE.
openaire.exporter.swaggerCommunities.apiLicense        =   ${openaire.exporter.swaggerDsm.apiLicense}
openaire.exporter.swaggerCommunities.apiLicenseUrl     =   ${openaire.exporter.swaggerDsm.apiLicenseUrl}
openaire.exporter.swaggerCommunities.apiContacName     =   ${openaire.exporter.swaggerDsm.apiContacName}
openaire.exporter.swaggerCommunities.apiContactUrl     =   ${openaire.exporter.swaggerDsm.apiContactUrl}
openaire.exporter.swaggerCommunities.apiContactEmail   =   ${openaire.exporter.swaggerDsm.apiContactEmail}

openaire.exporter.swaggerContexts.apiTitle          =   OpenAIRE Contexts REST API
openaire.exporter.swaggerContexts.apiDescription    =   The OpenAIRE contexts REST API allows programmatic access to the context profiles in OpenAIRE.
openaire.exporter.swaggerContexts.apiLicense        =   ${openaire.exporter.swaggerDsm.apiLicense}
openaire.exporter.swaggerContexts.apiLicenseUrl     =   ${openaire.exporter.swaggerDsm.apiLicenseUrl}
openaire.exporter.swaggerContexts.apiContacName     =   ${openaire.exporter.swaggerDsm.apiContacName}
openaire.exporter.swaggerContexts.apiContactUrl     =   ${openaire.exporter.swaggerDsm.apiContactUrl}
openaire.exporter.swaggerContexts.apiContactEmail   =   ${openaire.exporter.swaggerDsm.apiContactEmail}

openaire.exporter.swaggerInfo.apiTitle          =   OpenAIRE Info REST API
openaire.exporter.swaggerInfo.apiDescription    =   The OpenAIRE Info REST API allows programmatic access to some interesting dates related to the content indexed in OpenAIRE.
openaire.exporter.swaggerInfo.apiLicense        =   ${openaire.exporter.swaggerDsm.apiLicense}
openaire.exporter.swaggerInfo.apiLicenseUrl     =   ${openaire.exporter.swaggerDsm.apiLicenseUrl}
openaire.exporter.swaggerInfo.apiContacName     =   ${openaire.exporter.swaggerDsm.apiContacName}
openaire.exporter.swaggerInfo.apiContactUrl     =   ${openaire.exporter.swaggerDsm.apiContactUrl}
openaire.exporter.swaggerInfo.apiContactEmail   =   ${openaire.exporter.swaggerDsm.apiContactEmail}

# VOCABULARIES
openaire.exporter.vocabularies.baseUrl                      = http://localhost:8980/provision/mvc/vocabularies
openaire.exporter.vocabularies.countriesEndpoint            = ${openaire.exporter.vocabularies.baseUrl}/dnet:countries.json
openaire.exporter.vocabularies.datasourceTypologiesEndpoint = ${openaire.exporter.vocabularies.baseUrl}/dnet:datasource_typologies.json
