package eu.dnetlib;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import eu.dnetlib.common.app.AbstractDnetApp;
import eu.dnetlib.openaire.community.CommunityApiController;
import eu.dnetlib.openaire.context.ContextApiController;
import eu.dnetlib.openaire.dsm.DsmApiController;
import eu.dnetlib.openaire.funders.FundersApiController;
import eu.dnetlib.openaire.info.InfoController;
import eu.dnetlib.openaire.project.ProjectsController;

@EnableCaching
@EnableScheduling
@SpringBootApplication
@EnableAutoConfiguration(exclude = {
	SolrAutoConfiguration.class
})
public class DNetOpenaireExporterApplication extends AbstractDnetApp {

	public static final String V1 = "1.0.0";

	public static void main(final String[] args) throws Exception {
		SpringApplication.run(DNetOpenaireExporterApplication.class, args);
	}

	@Override
	protected String swaggerTitle() {
		return "D-Net Exporter APIs";
	}

	@Override
	protected String swaggerVersion() {
		return V1;
	}

	@Bean
	@ConditionalOnProperty(value = "openaire.exporter.enable.dsm", havingValue = "true")
	public GroupedOpenApi dsm() {
		return newGroupedOpenApi("Datasource Manager", DsmApiController.class.getPackage().getName());
	}

	@Bean
	@ConditionalOnProperty(value = "openaire.exporter.enable.project", havingValue = "true")
	public GroupedOpenApi projects() {
		return newGroupedOpenApi("OpenAIRE Projects", ProjectsController.class.getPackage().getName());
	}

	@Bean
	@ConditionalOnProperty(value = "openaire.exporter.enable.funders", havingValue = "true")
	public GroupedOpenApi funders() {
		return newGroupedOpenApi("OpenAIRE Funders", FundersApiController.class.getPackage().getName());
	}

	@Bean
	@ConditionalOnProperty(value = "openaire.exporter.enable.community", havingValue = "true")
	public GroupedOpenApi communities() {
		return newGroupedOpenApi("OpenAIRE Communities", CommunityApiController.class.getPackage().getName());
	}

	@Bean
	@ConditionalOnProperty(value = "openaire.exporter.enable.context", havingValue = "true")
	public GroupedOpenApi contexts() {
		return newGroupedOpenApi("OpenAIRE Contexts", ContextApiController.class.getPackage().getName());
	}

	@Bean
	@ConditionalOnProperty(value = "openaire.exporter.enable.info", havingValue = "true")
	public GroupedOpenApi info() {
		return newGroupedOpenApi("OpenAIRE Info", InfoController.class.getPackage().getName());
	}

	private GroupedOpenApi newGroupedOpenApi(final String groupName, final String controllerPackage) {
		return GroupedOpenApi.builder()
			.group(groupName)
			.packagesToScan(controllerPackage)
			.build();
	}

}
