package eu.dnetlib;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * Created by Alessia Bardi on 31/03/17.
 *
 * @author Alessia Bardi, Claudio Atzori
 */
@Configuration
@PropertySource("classpath:global.properties")
@ConfigurationProperties(prefix = "openaire.exporter")
public class DnetOpenaireExporterProperties {

	// ISLOOKUP
	private ClassPathResource findSolrIndexUrl;
	private ClassPathResource findIndexDsInfo;
	private ClassPathResource findObjectStore;
	private ClassPathResource findFunderContexts;
	private ClassPathResource findCommunityContexts;
	private ClassPathResource findContextProfiles;
	private ClassPathResource findContextProfilesByType;
	private ClassPathResource getRepoProfile;

	private String isLookupUrl;
	private String objectStoreServiceUrl;
	private String isRegistryServiceUrl;

	private int requestWorkers = 100;
	private int requestTimeout = 10;

	private int cxfClientConnectTimeout = 120;
	private int cxfClientReceiveTimeout = 120;

	private Datasource datasource;
	private Project project;
	private Jdbc jdbc;

	private Vocabularies vocabularies;

	public static class Datasource {

		// MONGODB
		private String mongoHost;
		private int mongoPort;
		private String mongoCollectionName;
		private String mongoDbName;
		private int mongoConnectionsPerHost;
		private int mongoQueryLimit;

		public String getMongoHost() {
			return mongoHost;
		}

		public void setMongoHost(final String mongoHost) {
			this.mongoHost = mongoHost;
		}

		public int getMongoPort() {
			return mongoPort;
		}

		public void setMongoPort(final int mongoPort) {
			this.mongoPort = mongoPort;
		}

		public String getMongoCollectionName() {
			return mongoCollectionName;
		}

		public void setMongoCollectionName(final String mongoCollectionName) {
			this.mongoCollectionName = mongoCollectionName;
		}

		public String getMongoDbName() {
			return mongoDbName;
		}

		public void setMongoDbName(final String mongoDbName) {
			this.mongoDbName = mongoDbName;
		}

		public int getMongoConnectionsPerHost() {
			return mongoConnectionsPerHost;
		}

		public void setMongoConnectionsPerHost(final int mongoConnectionsPerHost) {
			this.mongoConnectionsPerHost = mongoConnectionsPerHost;
		}

		public int getMongoQueryLimit() {
			return mongoQueryLimit;
		}

		public void setMongoQueryLimit(final int mongoQueryLimit) {
			this.mongoQueryLimit = mongoQueryLimit;
		}
	}

	public static class Project {

		private int flushSize;
		private String tsvFields;
		private Resource projectsFundingQueryTemplate;
		private Resource dspaceTemplate;
		private Resource dspaceHeadTemplate;
		private Resource dspaceTailTemplate;
		private Resource eprintsTemplate;

		public int getFlushSize() {
			return flushSize;
		}

		public void setFlushSize(final int flushSize) {
			this.flushSize = flushSize;
		}

		public String getTsvFields() {
			return tsvFields;
		}

		public void setTsvFields(final String tsvFields) {
			this.tsvFields = tsvFields;
		}

		public Resource getProjectsFundingQueryTemplate() {
			return projectsFundingQueryTemplate;
		}

		public void setProjectsFundingQueryTemplate(final Resource projectsFundingQueryTemplate) {
			this.projectsFundingQueryTemplate = projectsFundingQueryTemplate;
		}

		public Resource getDspaceTemplate() {
			return dspaceTemplate;
		}

		public void setDspaceTemplate(final Resource dspaceTemplate) {
			this.dspaceTemplate = dspaceTemplate;
		}

		public Resource getDspaceHeadTemplate() {
			return dspaceHeadTemplate;
		}

		public void setDspaceHeadTemplate(final Resource dspaceHeadTemplate) {
			this.dspaceHeadTemplate = dspaceHeadTemplate;
		}

		public Resource getDspaceTailTemplate() {
			return dspaceTailTemplate;
		}

		public void setDspaceTailTemplate(final Resource dspaceTailTemplate) {
			this.dspaceTailTemplate = dspaceTailTemplate;
		}

		public Resource getEprintsTemplate() {
			return eprintsTemplate;
		}

		public void setEprintsTemplate(final Resource eprintsTemplate) {
			this.eprintsTemplate = eprintsTemplate;
		}
	}

	public static class Jdbc {

		// JDBC
		@Value("${spring.datasource.driverClassName}")
		private String driverClassName;

		private String url;
		private String user;
		private String pwd;
		private int minIdle;
		private int maxidle;
		private int maxRows;

		public String getDriverClassName() {
			return driverClassName;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(final String url) {
			this.url = url;
		}

		public String getUser() {
			return user;
		}

		public void setUser(final String user) {
			this.user = user;
		}

		public String getPwd() {
			return pwd;
		}

		public void setPwd(final String pwd) {
			this.pwd = pwd;
		}

		public int getMinIdle() {
			return minIdle;
		}

		public void setMinIdle(final int minIdle) {
			this.minIdle = minIdle;
		}

		public int getMaxidle() {
			return maxidle;
		}

		public void setMaxidle(final int maxidle) {
			this.maxidle = maxidle;
		}

		public int getMaxRows() {
			return maxRows;
		}

		public void setMaxRows(final int maxRows) {
			this.maxRows = maxRows;
		}
	}

	public static class Swagger {

		private String apiTitle;
		private String apiDescription;
		private String apiLicense;
		private String apiLicenseUrl;
		private String apiContactName;
		private String apiContactUrl;
		private String apiContactEmail;

		public String getApiTitle() {
			return apiTitle;
		}

		public void setApiTitle(final String apiTitle) {
			this.apiTitle = apiTitle;
		}

		public String getApiDescription() {
			return apiDescription;
		}

		public void setApiDescription(final String apiDescription) {
			this.apiDescription = apiDescription;
		}

		public String getApiLicense() {
			return apiLicense;
		}

		public void setApiLicense(final String apiLicense) {
			this.apiLicense = apiLicense;
		}

		public String getApiLicenseUrl() {
			return apiLicenseUrl;
		}

		public void setApiLicenseUrl(final String apiLicenseUrl) {
			this.apiLicenseUrl = apiLicenseUrl;
		}

		public String getApiContactName() {
			return apiContactName;
		}

		public void setApiContactName(final String apiContactName) {
			this.apiContactName = apiContactName;
		}

		public String getApiContactUrl() {
			return apiContactUrl;
		}

		public void setApiContactUrl(final String apiContactUrl) {
			this.apiContactUrl = apiContactUrl;
		}

		public String getApiContactEmail() {
			return apiContactEmail;
		}

		public void setApiContactEmail(final String apiContactEmail) {
			this.apiContactEmail = apiContactEmail;
		}
	}

	public static class Vocabularies {

		private String baseUrl;

		private String countriesEndpoint;

		private String datasourceTypologiesEndpoint;

		public String getCountriesEndpoint() {
			return countriesEndpoint;
		}

		public void setCountriesEndpoint(final String countriesEndpoint) {
			this.countriesEndpoint = countriesEndpoint;
		}

		public String getBaseUrl() {
			return baseUrl;
		}

		public void setBaseUrl(final String baseUrl) {
			this.baseUrl = baseUrl;
		}

		public String getDatasourceTypologiesEndpoint() {
			return datasourceTypologiesEndpoint;
		}

		public void setDatasourceTypologiesEndpoint(final String datasourceTypologiesEndpoint) {
			this.datasourceTypologiesEndpoint = datasourceTypologiesEndpoint;
		}
	}

	public ClassPathResource getFindSolrIndexUrl() {
		return findSolrIndexUrl;
	}

	public void setFindSolrIndexUrl(final ClassPathResource findSolrIndexUrl) {
		this.findSolrIndexUrl = findSolrIndexUrl;
	}

	public ClassPathResource getFindIndexDsInfo() {
		return findIndexDsInfo;
	}

	public ClassPathResource getFindObjectStore() {
		return findObjectStore;
	}

	public void setFindObjectStore(final ClassPathResource findObjectStore) {
		this.findObjectStore = findObjectStore;
	}

	public void setFindIndexDsInfo(final ClassPathResource findIndexDsInfo) {
		this.findIndexDsInfo = findIndexDsInfo;
	}

	public ClassPathResource getFindFunderContexts() {
		return findFunderContexts;
	}

	public void setFindFunderContexts(final ClassPathResource findFunderContexts) {
		this.findFunderContexts = findFunderContexts;
	}

	public ClassPathResource getFindCommunityContexts() {
		return findCommunityContexts;
	}

	public ClassPathResource getFindContextProfiles() {
		return findContextProfiles;
	}

	public ClassPathResource getFindContextProfilesByType() {
		return findContextProfilesByType;
	}

	public void setFindContextProfiles(final ClassPathResource findContextProfiles) {
		this.findContextProfiles = findContextProfiles;
	}

	public void setFindContextProfilesByType(final ClassPathResource findContextProfilesByType) {
		this.findContextProfilesByType = findContextProfilesByType;
	}

	public void setFindCommunityContexts(final ClassPathResource findCommunityContexts) {
		this.findCommunityContexts = findCommunityContexts;
	}

	public ClassPathResource getGetRepoProfile() {
		return getRepoProfile;
	}

	public void setGetRepoProfile(final ClassPathResource getRepoProfile) {
		this.getRepoProfile = getRepoProfile;
	}

	public String getIsLookupUrl() {
		return isLookupUrl;
	}

	public void setIsLookupUrl(final String isLookupUrl) {
		this.isLookupUrl = isLookupUrl;
	}

	public String getObjectStoreServiceUrl() {
		return objectStoreServiceUrl;
	}

	public void setObjectStoreServiceUrl(final String objectStoreServiceUrl) {
		this.objectStoreServiceUrl = objectStoreServiceUrl;
	}

	public String getIsRegistryServiceUrl() {
		return isRegistryServiceUrl;
	}

	public void setIsRegistryServiceUrl(final String isRegistryServiceUrl) {
		this.isRegistryServiceUrl = isRegistryServiceUrl;
	}

	public int getRequestWorkers() {
		return requestWorkers;
	}

	public void setRequestWorkers(final int requestWorkers) {
		this.requestWorkers = requestWorkers;
	}

	public int getRequestTimeout() {
		return requestTimeout;
	}

	public void setRequestTimeout(final int requestTimeout) {
		this.requestTimeout = requestTimeout;
	}

	public int getCxfClientConnectTimeout() {
		return cxfClientConnectTimeout;
	}

	public void setCxfClientConnectTimeout(final int cxfClientConnectTimeout) {
		this.cxfClientConnectTimeout = cxfClientConnectTimeout;
	}

	public int getCxfClientReceiveTimeout() {
		return cxfClientReceiveTimeout;
	}

	public void setCxfClientReceiveTimeout(final int cxfClientReceiveTimeout) {
		this.cxfClientReceiveTimeout = cxfClientReceiveTimeout;
	}

	public Datasource getDatasource() {
		return datasource;
	}

	public void setDatasource(final Datasource datasource) {
		this.datasource = datasource;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(final Project project) {
		this.project = project;
	}

	public Jdbc getJdbc() {
		return jdbc;
	}

	public void setJdbc(final Jdbc jdbc) {
		this.jdbc = jdbc;
	}

	public Vocabularies getVocabularies() {
		return vocabularies;
	}

	public void setVocabularies(final Vocabularies vocabularies) {
		this.vocabularies = vocabularies;
	}
}
