package eu.dnetlib.openaire.project;

import java.util.regex.Pattern;

public class ProjectQueryParams {

	private final Pattern patternFundingStream = Pattern.compile("(\\w*(::|%| )*)*");

	private final Pattern patternDate = Pattern.compile("\\d\\d\\d\\d-\\d\\d-\\d\\d");

	private String fundingProgramme = null;
	/** Whatever is following the fundingProgramme **/
	private String fundingPath = null;

	private String startFrom = null;
	private String startUntil = null;
	private String endFrom = null;
	private String endUntil = null;

	public String getFundingProgramme() {
		return fundingProgramme;
	}

	public void setFundingProgramme(final String fundingProgramme) {
		this.fundingProgramme = verifyParam(fundingProgramme);
	}

	public String getFundingPath() {
		return fundingPath;
	}

	public void setFundingPath(final String fundingPath) {
		this.fundingPath = verifyParam(fundingPath);
	}

	public String getStartFrom() {
		return startFrom;
	}

	public void setStartFrom(final String startFrom) {
		this.startFrom = verifyDateParam(startFrom);
	}

	public String getStartUntil() {
		return startUntil;
	}

	public void setStartUntil(final String startUntil) {
		this.startUntil = verifyDateParam(startUntil);
	}

	public String getEndFrom() {
		return endFrom;
	}

	public void setEndFrom(final String endFrom) {
		this.endFrom = verifyDateParam(endFrom);
	}

	public String getEndUntil() {
		return endUntil;
	}

	public void setEndUntil(final String endUntil) {
		this.endUntil = verifyDateParam(endUntil);
	}

	protected String verifyParam(final String p) {
		if (p != null && !patternFundingStream.matcher(p).matches()) {
			throw new IllegalArgumentException(String.format("Parameter '%s' contains an invalid character", p));
		}
		return p;
	}

	protected String verifyDateParam(final String date) {
		if (date != null && !patternDate.matcher(date).matches()) {
			throw new IllegalArgumentException(
				String.format("Parameter date '%s' contains an invalid character. Accepted pattern is %s", date, patternDate.toString()));
		}
		return date;
	}
}
