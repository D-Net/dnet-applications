package eu.dnetlib.openaire.info;

import java.time.LocalDate;

public interface JdbcInfoDao {

	enum DATE_INFO {
		claim_load_date,
		oaf_load_date,
		odf_load_date,
		db_load_date,
		inference_date,
		stats_update_date,
		crossref_update_date,
		orcid_update_date,
		unpaywall_update_date,
		mag_update_date
	}

	LocalDate getDate(final DATE_INFO dateInfo);

	void dropCache();

}
