package eu.dnetlib.openaire.community;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import eu.dnetlib.openaire.community.model.DbCommunity;
import eu.dnetlib.openaire.community.model.DbCommunityType;
import eu.dnetlib.openaire.community.model.DbDatasource;
import eu.dnetlib.openaire.community.model.DbDatasourcePK;
import eu.dnetlib.openaire.community.model.DbOrganization;
import eu.dnetlib.openaire.community.model.DbProject;
import eu.dnetlib.openaire.community.model.DbProjectPK;
import eu.dnetlib.openaire.community.model.DbSupportOrg;
import eu.dnetlib.openaire.community.model.DbSupportOrgPK;
import eu.dnetlib.openaire.community.repository.DbCommunityRepository;
import eu.dnetlib.openaire.community.repository.DbDatasourceRepository;
import eu.dnetlib.openaire.community.repository.DbOrganizationRepository;
import eu.dnetlib.openaire.community.repository.DbProjectRepository;
import eu.dnetlib.openaire.community.repository.DbSupportOrgRepository;
import eu.dnetlib.openaire.community.utils.CommunityMappingUtils;
import eu.dnetlib.openaire.exporter.exceptions.CommunityException;
import eu.dnetlib.openaire.exporter.exceptions.ResourceNotFoundException;
import eu.dnetlib.openaire.exporter.model.community.CommunityContentprovider;
import eu.dnetlib.openaire.exporter.model.community.CommunityDetails;
import eu.dnetlib.openaire.exporter.model.community.CommunityOrganization;
import eu.dnetlib.openaire.exporter.model.community.CommunityProject;
import eu.dnetlib.openaire.exporter.model.community.CommunityStatus;
import eu.dnetlib.openaire.exporter.model.community.CommunitySummary;
import eu.dnetlib.openaire.exporter.model.community.CommunityWritableProperties;
import eu.dnetlib.openaire.exporter.model.community.SubCommunity;
import eu.dnetlib.openaire.exporter.model.community.SubCommunityWritableProperties;
import eu.dnetlib.openaire.exporter.model.community.selectioncriteria.SelectionCriteria;
import eu.dnetlib.openaire.exporter.model.context.IISConfigurationEntry;

@Service
@ConditionalOnProperty(value = "openaire.exporter.enable.community", havingValue = "true")
public class CommunityService {

	@Autowired
	private DbCommunityRepository dbCommunityRepository;
	@Autowired
	private DbProjectRepository dbProjectRepository;
	@Autowired
	private DbDatasourceRepository dbDatasourceRepository;
	@Autowired
	private DbOrganizationRepository dbOrganizationRepository;
	@Autowired
	private DbSupportOrgRepository dbSupportOrgRepository;

	private static final Log log = LogFactory.getLog(CommunityService.class);

	public List<CommunitySummary> listCommunities() {
		return dbCommunityRepository.findByParentIsNull()
				.stream()
				.map(CommunityMappingUtils::toCommunitySummary)
				.collect(Collectors.toList());
	}

	@Transactional
	public CommunityDetails newCommunity(final CommunityDetails details) throws CommunityException {
		if (StringUtils.isBlank(details.getId())) { throw new CommunityException("Empty Id"); }
		if (dbCommunityRepository.existsById(details.getId())) { throw new CommunityException("Community already exists: " + details.getId()); }
		details.setCreationDate(LocalDateTime.now());
		return saveCommunity(details);

	}

	@Transactional
	public CommunityDetails saveCommunity(final CommunityDetails details) {
		details.setLastUpdateDate(LocalDateTime.now());
		dbCommunityRepository.save(CommunityMappingUtils.toCommunity(details));
		return getCommunity(details.getId());
	}

	@Transactional
	public CommunityDetails getCommunity(final String id) {
		final DbCommunity dbc = dbCommunityRepository.findById(id)
				.filter(c -> c.getType() != DbCommunityType.subcommunity)
				.filter(c -> c.getParent() == null)
				.orElseThrow(() -> new ResourceNotFoundException("Community not found: " + id));

		return CommunityMappingUtils.toCommunityDetails(dbc);
	}

	@Transactional
	public void updateCommunity(final String id, final CommunityWritableProperties details) {
		final DbCommunity dbc = dbCommunityRepository.findById(id)
				.filter(c -> c.getType() != DbCommunityType.subcommunity)
				.filter(c -> c.getParent() == null)
				.orElseThrow(() -> new ResourceNotFoundException("Community not found: " + id));

		CommunityMappingUtils.populateCommunity(dbc, details);
		dbc.setLastUpdateDate(LocalDateTime.now());

		dbCommunityRepository.save(dbc);
	}

	@Transactional
	public void removeCommunity(final String id) {
		final DbCommunity dbc = dbCommunityRepository.findById(id)
				.filter(c -> c.getType() != DbCommunityType.subcommunity)
				.filter(c -> c.getParent() == null)
				.orElseThrow(() -> new ResourceNotFoundException("Community not found: " + id));

		dbCommunityRepository.delete(dbc);
	}

	@Transactional
	public Page<CommunityProject> getCommunityProjects(final String id,
			final String funder,
			final String filter,
			final int page,
			final int size,
			final String orderBy) throws CommunityException {
		if (StringUtils.isBlank(id)) { throw new CommunityException("Empty ID"); }
		try {
			final Sort sort;
			if (StringUtils.isBlank(orderBy)) {
				sort = Sort.by("projectName");
			} else if ("funder".equalsIgnoreCase(orderBy)) {
				sort = Sort.by("projectFunder").and(Sort.by("projectName"));
			} else if ("grantId".equalsIgnoreCase(orderBy)) {
				sort = Sort.by("projectCode");
			} else if ("acronym".equalsIgnoreCase(orderBy)) {
				sort = Sort.by("projectAcronym");
			} else if ("openaireId".equalsIgnoreCase(orderBy)) {
				sort = Sort.by("projectId");
			} else {
				sort = Sort.by("projectName");
			}

			final PageRequest pageable = PageRequest.of(page, size, sort);
			if (StringUtils.isAllBlank(filter, funder)) {
				return dbProjectRepository.findByCommunity(id, pageable).map(CommunityMappingUtils::toCommunityProject);
			}
			final Specification<DbProject> projSpec = prepareProjectSpec(id, funder, filter);
			return dbProjectRepository.findAll(projSpec, pageable).map(CommunityMappingUtils::toCommunityProject);
		} catch (final Throwable e) {
			log.error(e);
			throw new CommunityException(e);
		}
	}

	private Specification<DbProject> prepareProjectSpec(final String community, final String funder, final String other) {
		return (project, query, cb) -> {

			final List<Predicate> andConds = new ArrayList<>();
			andConds.add(cb.equal(project.get("community"), community));

			if (StringUtils.isNotBlank(funder)) {
				andConds.add(cb.equal(project.get("projectFunder"), funder));
			}

			if (StringUtils.isNotBlank(other)) {
				final String s = other.toLowerCase().trim();

				final List<Predicate> orConds = new ArrayList<>();
				orConds.add(cb.equal(cb.lower(project.get("projectId")), s));
				orConds.add(cb.equal(cb.lower(project.get("projectCode")), s));
				orConds.add(cb.equal(cb.lower(project.get("projectAcronym")), s));
				orConds.add(cb.like(cb.lower(project.get("projectName")), "%" + s + "%"));
				if (StringUtils.isBlank(funder)) {
					orConds.add(cb.equal(cb.lower(project.get("projectFunder")), s));
				}

				andConds.add(cb.or(orConds.toArray(new Predicate[orConds.size()])));
			}

			return cb.and(andConds.toArray(new Predicate[andConds.size()]));
		};

	}

	@Transactional
	public CommunityProject addCommunityProject(final String id, final CommunityProject project) {
		final DbProject p = CommunityMappingUtils.toDbProject(id, project);
		dbProjectRepository.save(p);
		return project;
	}

	@Transactional
	public void addCommunityProjects(final String id, final CommunityProject... projects) throws CommunityException {
		try {
			final List<DbProject> list = Arrays.stream(projects)
					.map(p -> CommunityMappingUtils.toDbProject(id, p))
					.collect(Collectors.toList());

			dbProjectRepository.saveAll(list);
		} catch (final Throwable e) {
			log.error(e);
			throw new CommunityException(e);
		}
	}

	@Transactional
	public void removeCommunityProjects(final String id, final String... ids) {
		final List<DbProjectPK> list = Arrays.stream(ids)
				.map(projectId -> new DbProjectPK(id, projectId))
				.collect(Collectors.toList());
		dbProjectRepository.deleteAllById(list);
	}

	public List<CommunityContentprovider> getCommunityDatasources(final String id) {
		return dbDatasourceRepository.findByCommunity(id)
				.stream()
				.map(CommunityMappingUtils::toCommunityContentprovider)
				.collect(Collectors.toList());
	}

	public List<CommunityContentprovider> getCommunityDatasourcesWithDeposit(final String id, final boolean deposit) {
		return dbDatasourceRepository.findByCommunityAndDeposit(id, deposit)
				.stream()
				.map(CommunityMappingUtils::toCommunityContentprovider)
				.collect(Collectors.toList());
	}

	@Transactional
	public CommunityContentprovider updateCommunityDatasourcesDeposit(final String id, final String dsId, final Boolean deposit, final String message) {
		return dbDatasourceRepository.findById(new DbDatasourcePK(id, dsId))
				.map(ds -> {
					ds.setDeposit(deposit != null ? deposit : false);
					ds.setMessage(message);
					return ds;
				})
				.map(CommunityMappingUtils::toCommunityContentprovider)
				.orElseThrow(() -> new ResourceNotFoundException("Community and/or Datasource not found"));
	}

	@Transactional
	public void addCommunityDatasources(final String id, final CommunityContentprovider... contentproviders) {
		final List<DbDatasource> list = Arrays.stream(contentproviders)
				.map(cp -> CommunityMappingUtils.toDbDatasource(id, cp))
				.collect(Collectors.toList());

		dbDatasourceRepository.saveAll(list);
	}

	@Transactional
	public void removeCommunityDatasources(final String id, final String... ids) {
		final List<DbDatasourcePK> list = Arrays.stream(ids)
				.map(dsId -> new DbDatasourcePK(id, dsId))
				.collect(Collectors.toList());
		dbDatasourceRepository.deleteAllById(list);
	}

	@Transactional
	public void removeCommunityOrganizations(final String id, final String... orgNames) {
		final List<DbSupportOrgPK> list = Arrays.stream(orgNames)
				.map(name -> new DbSupportOrgPK(id, name))
				.collect(Collectors.toList());
		dbSupportOrgRepository.deleteAllById(list);
	}

	@Transactional
	public List<CommunityOrganization> getCommunityOrganizations(final String id) {
		return dbSupportOrgRepository.findByCommunity(id)
				.stream()
				.map(CommunityMappingUtils::toCommunityOrganization)
				.collect(Collectors.toList());
	}

	@Transactional
	public void addCommunityOrganizations(final String id, final CommunityOrganization... orgs) {
		final List<DbSupportOrg> list = Arrays.stream(orgs)
				.map(o -> CommunityMappingUtils.toDbSupportOrg(id, o))
				.collect(Collectors.toList());

		dbSupportOrgRepository.saveAll(list);
	}

	@Transactional
	public void addCommunitySubjects(final String id, final String... subjects) {
		modifyElementToArrayField(id, DbCommunity::getSubjects, DbCommunity::setSubjects, false, subjects);
	}

	public void removeCommunitySubjects(final String id, final String... subjects) {
		modifyElementToArrayField(id, DbCommunity::getSubjects, DbCommunity::setSubjects, true, subjects);
	}

	public void addCommunityFOS(final String id, final String... foss) {
		modifyElementToArrayField(id, DbCommunity::getFos, DbCommunity::setFos, false, foss);
	}

	public void removeCommunityFOS(final String id, final String... foss) {
		modifyElementToArrayField(id, DbCommunity::getFos, DbCommunity::setFos, true, foss);
	}

	public void addCommunitySDG(final String id, final String... sdgs) {
		modifyElementToArrayField(id, DbCommunity::getSdg, DbCommunity::setSdg, false, sdgs);
	}

	public void removeCommunitySDG(final String id, final String... sdgs) {
		modifyElementToArrayField(id, DbCommunity::getSdg, DbCommunity::setSdg, true, sdgs);
	}

	@Transactional
	public void addCommunityAdvancedConstraint(final String id, final SelectionCriteria advancedCosntraint) {
		final DbCommunity dbEntry = dbCommunityRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Community not found: " + id));
		dbEntry.setAdvancedConstraints(advancedCosntraint);
		dbEntry.setLastUpdateDate(LocalDateTime.now());
		dbCommunityRepository.save(dbEntry);
	}

	@Transactional
	public void removeCommunityAdvancedConstraint(final String id) {
		final DbCommunity dbEntry = dbCommunityRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Community not found: " + id));
		dbEntry.setAdvancedConstraints(null);
		dbEntry.setLastUpdateDate(LocalDateTime.now());
		dbCommunityRepository.save(dbEntry);
	}

	@Transactional
	public void addCommunityRemoveConstraint(final String id, final SelectionCriteria removeConstraint) {
		final DbCommunity dbEntry = dbCommunityRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Community not found: " + id));
		dbEntry.setRemoveConstraints(removeConstraint);
		dbEntry.setLastUpdateDate(LocalDateTime.now());
		dbCommunityRepository.save(dbEntry);
	}

	@Transactional
	public void removeCommunityRemoveConstraint(final String id) {
		final DbCommunity dbEntry = dbCommunityRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Community not found: " + id));
		dbEntry.setRemoveConstraints(null);
		dbEntry.setLastUpdateDate(LocalDateTime.now());
		dbCommunityRepository.save(dbEntry);
	}

	public void removeCommunityZenodoCommunity(final String id, final String zenodoCommunity, final boolean isMain) {
		if (isMain) {
			updateElementToSimpleField(id, DbCommunity::setMainZenodoCommunity, null);
		}
		modifyElementToArrayField(id, DbCommunity::getOtherZenodoCommunities, DbCommunity::setOtherZenodoCommunities, true, zenodoCommunity);
	}

	public void addCommunityZenodoCommunity(final String id, final String zenodoCommunity, final boolean isMain) {
		if (isMain) {
			updateElementToSimpleField(id, DbCommunity::setMainZenodoCommunity, zenodoCommunity);
		}
		modifyElementToArrayField(id, DbCommunity::getOtherZenodoCommunities, DbCommunity::setOtherZenodoCommunities, false, zenodoCommunity);
	}

	@Transactional
	private void updateElementToSimpleField(final String id,
			final BiConsumer<DbCommunity, String> setter,
			final String value) {
		final DbCommunity dbEntry = dbCommunityRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Community not found: " + id));
		setter.accept(dbEntry, value);
		dbEntry.setLastUpdateDate(LocalDateTime.now());
		dbCommunityRepository.save(dbEntry);
	}

	@Transactional
	private void modifyElementToArrayField(final String id,
			final Function<DbCommunity, String[]> getter,
			final BiConsumer<DbCommunity, String[]> setter,
			final boolean remove,
			final String... values) {

		final DbCommunity dbEntry = dbCommunityRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Community not found: " + id));

		final Set<String> tmpList = new LinkedHashSet<>();
		final String[] oldValues = getter.apply(dbEntry);
		if (oldValues != null) {
			Collections.addAll(tmpList, oldValues);
		}
		if (remove) {
			tmpList.removeAll(Arrays.asList(values));
		} else {
			tmpList.addAll(Arrays.asList(values));
		}

		setter.accept(dbEntry, tmpList.toArray(new String[tmpList.size()]));

		dbEntry.setLastUpdateDate(LocalDateTime.now());

		dbCommunityRepository.save(dbEntry);
	}

	@Transactional
	public List<String> getOpenAIRECommunitiesByZenodoId(final String zenodoId) {
		return dbCommunityRepository.findByZenodoId(zenodoId);
	}

	@Transactional
	public Map<String, Set<String>> getPropagationOrganizationCommunityMap() {

		final Set<String> valids = dbCommunityRepository.findAll()
				.stream()
				.filter(c -> c.getStatus() != CommunityStatus.hidden)
				.map(DbCommunity::getId)
				.collect(Collectors.toSet());

		return dbOrganizationRepository.findAll()
				.stream()
				.filter(o -> valids.contains(o.getCommunity()))
				.collect(Collectors.groupingBy(DbOrganization::getOrgId, Collectors.mapping(DbOrganization::getCommunity, Collectors.toSet())));
	}

	@Transactional
	public Map<String, Set<String>> getDatasourceCommunityMap() {

		final Set<String> valids = dbCommunityRepository.findAll()
				.stream()
				.filter(c -> c.getStatus() != CommunityStatus.hidden)
				.map(DbCommunity::getId)
				.collect(Collectors.toSet());

		return dbDatasourceRepository.findAll()
				.stream()
				.filter(o -> valids.contains(o.getCommunity()))
				.collect(Collectors.groupingBy(DbDatasource::getDsId, Collectors.mapping(DbDatasource::getCommunity, Collectors.toSet())));
	}

	@Transactional
	public Set<String> getPropagationOrganizationsForCommunity(final String communityId) {
		return dbOrganizationRepository.findByCommunity(communityId)
				.stream()
				.map(DbOrganization::getOrgId)
				.collect(Collectors.toSet());
	}

	@Transactional
	public Set<String> addPropagationOrganizationForCommunity(final String communityId, final String... organizationIds) {
		for (final String orgId : organizationIds) {
			final DbOrganization o = new DbOrganization(communityId.trim(), orgId.trim());
			dbOrganizationRepository.save(o);
		}
		return getPropagationOrganizationsForCommunity(communityId);
	}

	@Transactional
	public Set<String> removePropagationOrganizationForCommunity(final String communityId, final String... organizationIds) {
		for (final String orgId : organizationIds) {
			final DbOrganization o = new DbOrganization(communityId.trim(), orgId.trim());
			dbOrganizationRepository.delete(o);
		}
		return getPropagationOrganizationsForCommunity(communityId);
	}

	@Transactional
	public List<IISConfigurationEntry> getIISConfiguration(final String id) {
		final List<IISConfigurationEntry> res = new ArrayList<>();

		res.add(dbCommunityRepository.findById(id)
				.map(CommunityMappingUtils::asIISConfigurationEntry)
				.orElseThrow(() -> new ResourceNotFoundException("Community not found: " + id)));

		for (final DbCommunity subc : dbCommunityRepository.findByIdStartsWith(id + CommunityMappingUtils.COMMUNITY_ID_PARTS_SEPARATOR)) {
			res.add(CommunityMappingUtils.asIISConfigurationEntry(subc));
		}

		return res;
	}

	@Transactional
	public List<String> getCommunityFunders(final String id) {
		return dbProjectRepository.findFundersByCommunity(id);
	}

	// Sub-communities methods

	@Transactional
	public SubCommunity getSubCommunity(final String id, final String subCommunityId) {
		return dbCommunityRepository.findById(subCommunityId)
				.filter(c -> c.getId().startsWith(id + CommunityMappingUtils.COMMUNITY_ID_PARTS_SEPARATOR))
				.filter(c -> c.getType() == DbCommunityType.subcommunity)
				.filter(c -> c.getParent() != null)
				.map(CommunityMappingUtils::toSubCommunity)
				.orElseThrow(() -> new ResourceNotFoundException("Sub-Community not found: " + subCommunityId));

	}

	@Transactional
	public List<SubCommunity> getSubCommunitiesForCommunity(final String id) {
		return dbCommunityRepository.findByIdStartsWith(id + CommunityMappingUtils.COMMUNITY_ID_PARTS_SEPARATOR)
				.stream()
				.filter(c -> c.getType() == DbCommunityType.subcommunity)
				.filter(c -> c.getParent() != null)
				.map(CommunityMappingUtils::toSubCommunity)
				.collect(Collectors.toList());
	}

	@Transactional
	public void updateSubCommunity(final String subCommunityId, final SubCommunityWritableProperties details) {
		final DbCommunity dbc = dbCommunityRepository.findById(subCommunityId)
				.filter(c -> c.getType() == DbCommunityType.subcommunity)
				.orElseThrow(() -> new ResourceNotFoundException("Community not found: " + subCommunityId));

		CommunityMappingUtils.populateCommunity(dbc, details);

		dbc.setLastUpdateDate(LocalDateTime.now());

		dbCommunityRepository.save(dbc);
	}

	@Transactional
	public SubCommunity addSubCommunity(final String id, final SubCommunity subcommunity) throws CommunityException {

		if (!id.equals(CommunityMappingUtils.calculateMainCommunityId(subcommunity.getSubCommunityId()))) {
			throw new CommunityException("The sub-collection id does not start with " + id);
		}

		if (subcommunity.getParent() == null) {
			subcommunity.setParent(id);
		}
		final DbCommunity dbc = CommunityMappingUtils.toDbCommunity(subcommunity);

		dbCommunityRepository.save(dbc);

		return subcommunity;
	}

	@Transactional
	public void removeSubCommunity(final String communityId, final String subCommunityId) throws CommunityException {
		if (!communityId.equals(CommunityMappingUtils.calculateMainCommunityId(subCommunityId))) {
			throw new CommunityException("The sub-collection id does not start with " + communityId);
		}
		final DbCommunity dbc = dbCommunityRepository.findById(subCommunityId)
				.filter(c -> c.getType() == DbCommunityType.subcommunity)
				.filter(c -> c.getParent() != null)
				.orElseThrow(() -> new ResourceNotFoundException("SubCommunity not found: " + subCommunityId));

		dbCommunityRepository.delete(dbc);
	}

}
