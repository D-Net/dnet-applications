package eu.dnetlib.openaire.context;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;

import com.google.common.base.Functions;
import com.google.common.collect.Lists;

import eu.dnetlib.openaire.exporter.model.context.Category;
import eu.dnetlib.openaire.exporter.model.context.Concept;
import eu.dnetlib.openaire.exporter.model.context.Context;
import eu.dnetlib.openaire.exporter.model.context.Param;
import eu.dnetlib.openaire.exporter.model.funders.FunderDetails;

@Deprecated
public class ContextMappingUtils {

	private static final List<String> DATE_PATTERN = Lists.newArrayList("yyyy-MM-dd'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ssXXX", "yyyy-MM-dd'T'hh:mm:ss+00:00");

	private static final Log log = LogFactory.getLog(ContextMappingUtils.class);

	public static Context parseContext(final String s, final Queue<Throwable> errors) {
		try {
			final Document doc = DocumentHelper.parseText(s);
			final Element eContext = (Element) doc.selectSingleNode("/RESOURCE_PROFILE/BODY/CONFIGURATION/context");
			final String creationDate = eContext.valueOf("./param[./@name='creationdate']/text()");
			final String otherDate = doc.valueOf("/RESOURCE_PROFILE/HEADER/DATE_OF_CREATION/@value");
			final Context c = new Context()
				.setId(eContext.attributeValue("id"))
				.setLabel(eContext.attributeValue("label"))
				.setType(eContext.attributeValue("type"))
				.setLastUpdateDate(asDate(otherDate))
				.setParams(parseParams(eContext))
				.setCategories(parseCategories(eContext));
			// the creation date will be added in the param elements of the community profile. Funders may not have it, hence the check.
			if (StringUtils.isNotBlank(creationDate)) {
				c.setCreationDate(asDate(creationDate));
			} else {
				c.setCreationDate(asDate(otherDate));
			}
			return c;
		} catch (final DocumentException e) {
			errors.add(e);
			return new Context();
		}
	}

	private static Date asDate(final String s) {
		if (StringUtils.isBlank(s)) { return null; }

		for (final String pattern : DATE_PATTERN) {
			try {
				final Date res = DateUtils.parseDate(s, pattern);
				if (res != null) { return res; }
			} catch (final ParseException e) {}
		}
		log.warn("Invalid Date: " + s);
		return null;
	}

	private static Map<String, Category> parseCategories(final Element eContext) {
		final List<Node> eCategory = eContext.selectNodes("//category");
		return eCategory.stream()
			.map(n -> (Element) n)
			.map(eCat -> new Category()
				.setClaim(getClaim(eCat))
				.setId(eCat.attributeValue("id"))
				.setLabel(eCat.attributeValue("label"))
				.setParams(parseParams(eCat))
				.setConcepts(parseConcepts(eCat)))
			.collect(Collectors.toMap(Category::getId, Functions.identity()));
	}

	private static List<Concept> parseConcepts(final Element eCategory) {
		final List<Node> eConcepts = eCategory.selectNodes("./concept");
		return eConcepts.stream()
			.map(n -> (Element) n)
			.map(eCon -> new Concept()
				.setClaim(getClaim(eCon))
				.setId(eCon.attributeValue("id"))
				.setLabel(eCon.attributeValue("label"))
				.setParams(parseParams(eCon))
				.setConcepts(parseConcepts(eCon)))
			.collect(Collectors.toList());
	}

	private static Boolean getClaim(final Element eCon) {
		final String claim = eCon.attributeValue("claim");
		return BooleanUtils.toBooleanObject(StringUtils.isNotBlank(claim) ? claim : "false");
	}

	private static List<Param> parseParams(final Element e) {
		final List<Node> params = e.selectNodes("./param");
		return params.stream()
			.map(n -> (Element) n)
			.map(p -> new Param().setName(p.attributeValue("name")).setValue(p.getTextTrim()))
			.collect(Collectors.toList());
	}

	public static FunderDetails asFunderDetails(final Context c) {
		return new FunderDetails()
			.setId(c.getId())
			.setName(c.getLabel())
			.setShortname(c.getId());
	}

}
