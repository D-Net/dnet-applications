package eu.dnetlib.openaire.funders;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import eu.dnetlib.openaire.exporter.exceptions.FundersApiException;
import eu.dnetlib.openaire.funders.domain.db.FunderDatasource;
import eu.dnetlib.openaire.funders.domain.db.FunderDbEntry;
import eu.dnetlib.openaire.funders.domain.db.FunderDbUpdate;
import eu.dnetlib.openaire.funders.domain.db.FunderPid;

@Component
@ConditionalOnProperty(value = "openaire.exporter.enable.funders", havingValue = "true")
public class FunderService {

	private static final String SEPARATOR = "@=@";

	@Autowired
	private FunderRepository funderRepository;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<FunderDbEntry> getFunders() {
		return StreamSupport.stream(funderRepository.findAll().spliterator(), false)
				.map(this::patchFunder)
				.collect(Collectors.toList());

	}

	public FunderDbEntry getFunder(final String id) throws FundersApiException {
		return funderRepository.findById(id)
				.map(this::patchFunder)
				.orElseThrow(() -> new FundersApiException("Missing Funder: " + id));
	}

	public boolean isValidFunder(final String id) {
		return funderRepository.existsById(id);
	}

	private FunderDbEntry patchFunder(final FunderDbEntry funder) {
		// THIS PATCH IS NECESSARY FOR COMPATIBILITY WITH POSTGRES 9.3 (PARTIAL SUPPORT OF THE JSON LIBRARY)
		final List<FunderDatasource> datasources = Arrays.stream(funder.getDatasourcesPostgres())
				.filter(Objects::nonNull)
				.map(s -> s.split(SEPARATOR))
				.filter(arr -> arr.length == 4)
				.map(arr -> {
					final FunderDatasource ds = new FunderDatasource();
					ds.setId(arr[0].trim());
					ds.setName(arr[1].trim());
					ds.setType(arr[2].trim());
					ds.setNsPrefix(arr[3].trim());

					switch (ds.getNsPrefix()) {
					case "corda_______":
						ds.setFundingProgram("FP7");
						break;
					case "corda__h2020":
						ds.setFundingProgram("H2020");
						break;
					case "corda_____he":
						ds.setFundingProgram(null); // DEFAULT
						break;

					}

					return ds;
				})
				.filter(ds -> StringUtils.isNotBlank(ds.getId()))
				.collect(Collectors.toList());

		funder.setDatasources(datasources);

		final List<FunderPid> pids = Arrays.stream(funder.getPidsPostgres())
				.filter(Objects::nonNull)
				.map(s -> s.split(SEPARATOR))
				.filter(arr -> arr.length == 2)
				.map(arr -> {
					final FunderPid pid = new FunderPid();
					pid.setType(arr[0].trim());
					pid.setValue(arr[1].trim());
					return pid;
				})
				.filter(pid -> StringUtils.isNotBlank(pid.getValue()))
				.collect(Collectors.toList());

		funder.setPids(pids);

		return funder;
	}

	public void updateFunder(final String id, final FunderDbUpdate funderUpdate) {

		final String sql =
				"UPDATE dsm_organizations SET ("
						+ "  legalshortname,"
						+ "  legalname,"
						+ "  websiteurl,"
						+ "  logourl,"
						+ "  country,"
						+ "  registered_funder"
						+ ") = ("
						+ "  coalesce(?, legalshortname),"
						+ "  coalesce(?, legalname),"
						+ "  coalesce(?, websiteurl),"
						+ "  coalesce(?, logourl),"
						+ "  coalesce(?, country),"
						+ "  coalesce(?, registered_funder)"
						+ ") WHERE id = ?";

		jdbcTemplate.update(sql, funderUpdate.getLegalShortName(), funderUpdate.getLegalName(), funderUpdate.getWebsiteUrl(), funderUpdate
				.getLogoUrl(), funderUpdate.getCountry(), funderUpdate.getRegistered(), id);

		if (funderUpdate.getPids() != null) {
			funderUpdate.getPids().forEach(pid -> {
				// TODO: the first update should be deleted after the re-implementation of the pid-tables,
				// TODO: the field 'type' should also be moved in the second update

				if (jdbcTemplate.queryForObject("SELECT count(*) FROM dsm_identities WHERE issuertype = ? AND pid = ?", Integer.class, pid.getType(), pid
						.getValue()) == 0) {
					jdbcTemplate.update("INSERT INTO dsm_identities(issuertype, pid) VALUES (?, ?)", pid.getType(), pid.getValue());
				}

				if (jdbcTemplate.queryForObject("SELECT count(*) FROM dsm_organizationpids WHERE organization = ? AND pid = ?", Integer.class, id, pid
						.getValue()) == 0) {
					jdbcTemplate.update("INSERT INTO dsm_organizationpids(organization, pid) VALUES (?, ?)", id, pid.getValue());
				}
			});
		}

	}

}
