package eu.dnetlib.openaire.community.model;

import java.io.Serializable;
import java.util.Objects;

public class DbDatasourcePK implements Serializable {

	private static final long serialVersionUID = -8073510491611213955L;

	private String community;

	private String dsId;

	public DbDatasourcePK() {}

	public DbDatasourcePK(final String community, final String dsId) {
		this.community = community;
		this.dsId = dsId;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(final String community) {
		this.community = community;
	}

	public String getDsId() {
		return dsId;
	}

	public void setDsId(final String dsId) {
		this.dsId = dsId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(community, dsId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof DbDatasourcePK)) { return false; }
		final DbDatasourcePK other = (DbDatasourcePK) obj;
		return Objects.equals(community, other.community) && Objects.equals(dsId, other.dsId);
	}

	@Override
	public String toString() {
		return String.format("CommunityDatasourcePK [community=%s, dsId=%s]", community, dsId);
	}

}
