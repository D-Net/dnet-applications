package eu.dnetlib.openaire.project.dao;

import eu.dnetlib.openaire.project.domain.db.ProjectApi;
import eu.dnetlib.openaire.project.domain.db.ProjectTsv;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by claudio on 06/07/2017.
 */
@Repository
@ConditionalOnProperty(value = "openaire.exporter.enable.project", havingValue = "true")
public interface ProjectApiRepository extends PagingAndSortingRepository<ProjectApi, Integer> {

	Iterable<ProjectTsv> findByFundingpathidStartingWith(String fundingpathid);

}
