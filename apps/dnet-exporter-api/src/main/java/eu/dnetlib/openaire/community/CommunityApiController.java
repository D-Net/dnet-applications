package eu.dnetlib.openaire.community;

import static eu.dnetlib.openaire.common.ExporterConstants.C;
import static eu.dnetlib.openaire.common.ExporterConstants.C_CP;
import static eu.dnetlib.openaire.common.ExporterConstants.C_O;
import static eu.dnetlib.openaire.common.ExporterConstants.C_PJ;
import static eu.dnetlib.openaire.common.ExporterConstants.C_ZC;
import static eu.dnetlib.openaire.common.ExporterConstants.R;
import static eu.dnetlib.openaire.common.ExporterConstants.W;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.openaire.common.AbstractExporterController;
import eu.dnetlib.openaire.community.model.DepositionInfo;
import eu.dnetlib.openaire.community.utils.CommunityMappingUtils;
import eu.dnetlib.openaire.exporter.exceptions.CommunityException;
import eu.dnetlib.openaire.exporter.exceptions.ResourceNotFoundException;
import eu.dnetlib.openaire.exporter.model.community.CommunityContentprovider;
import eu.dnetlib.openaire.exporter.model.community.CommunityDetails;
import eu.dnetlib.openaire.exporter.model.community.CommunityOrganization;
import eu.dnetlib.openaire.exporter.model.community.CommunityProject;
import eu.dnetlib.openaire.exporter.model.community.CommunitySummary;
import eu.dnetlib.openaire.exporter.model.community.CommunityWritableProperties;
import eu.dnetlib.openaire.exporter.model.community.selectioncriteria.SelectionCriteria;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@CrossOrigin(origins = {
		"*"
})
@ConditionalOnProperty(value = "openaire.exporter.enable.community", havingValue = "true")
@Tag(name = "OpenAIRE Communities API", description = "the OpenAIRE Community API")
public class CommunityApiController extends AbstractExporterController {

	@Autowired
	private CommunityService communityService;

	@GetMapping("/community/communities")
	@Operation(summary = "get all community profiles", description = "get all community profiles", tags = {
			C, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public List<CommunitySummary> listCommunities() throws CommunityException {
		try {
			return communityService.listCommunities();
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/")
	@Operation(summary = "add a new community profile", description = "add a new community profile", tags = {
			C, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails addCommunity(@RequestBody final CommunityDetails details) throws CommunityException {
		try {
			return communityService.newCommunity(details);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@GetMapping("/community/{id}")
	@Operation(summary = "get community profile", description = "get community profile", tags = {
			C, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails getCommunity(@PathVariable final String id) throws CommunityException {
		try {
			return communityService.getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}")
	@Operation(summary = "update community details", description = "update community details", tags = {
			C, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails updateCommunity(
			@PathVariable final String id,
			@RequestBody final CommunityWritableProperties properties) throws CommunityException {
		try {
			communityService.updateCommunity(id, properties);

			return communityService.getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}")
	@Operation(summary = "delete a community", description = "delete a community", tags = {
			C, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void deleteCommunity(@PathVariable final String id)
			throws CommunityException {
		try {
			communityService.removeCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	// APIs to manage the sub communities

	// API to manage specific community fields

	@PostMapping("/community/{id}/subjects")
	@Operation(summary = "associate a subject to a community", description = "associate a subject to a community", tags = {
			C, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails addCommunitySubjects(
			@PathVariable final String id,
			@RequestBody final String[] subjects) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.addCommunitySubjects(id, subjects);

			return getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/subjects")
	@Operation(summary = "remove subjects from a community", description = "remove subjects from a community", tags = {
			C, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails removeCommunitySubjects(
			@PathVariable final String id,
			@RequestBody final String[] subjects) throws CommunityException {

		try {
			communityService.removeCommunitySubjects(id, subjects);

			return getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/fos")
	@Operation(summary = "associate a fos to a community", description = "associate a fos to a community", tags = {
			C, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails addCommunityFOS(
			@PathVariable final String id,
			@RequestBody final String[] subjects) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.addCommunityFOS(id, subjects);

			return getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/fos")
	@Operation(summary = "remove fos from a community", description = "remove fos from a community", tags = {
			C, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails removeCommunityFOS(
			@PathVariable final String id,
			@RequestBody final String[] subjects) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.removeCommunityFOS(id, subjects);

			return getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/sdg")
	@Operation(summary = "associate a sdg to a community", description = "associate a sdg to the community", tags = {
			C, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails addCommunitySDG(
			@PathVariable final String id,
			@RequestBody final String[] subjects) throws CommunityException {

		try {
			communityService.addCommunitySDG(id, subjects);

			return getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/sdg")
	@Operation(summary = "remove sdg from a community", description = "remove sdg from a community", tags = {
			C, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails removeCommunitySDG(
			@PathVariable final String id,
			@RequestBody final String[] subjects) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.removeCommunitySDG(id, subjects);

			return getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/advancedConstraint")
	@Operation(summary = "the set of constraints to be used to extend the association between result and community", description = "the set of constraints to be used to extend the association between result and community", tags = {
			C, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails addCommunityAdvancedConstraint(
			@PathVariable final String id,
			@RequestBody final SelectionCriteria advancedConstraint) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.addCommunityAdvancedConstraint(id, advancedConstraint);

			return getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/advancedConstraint")
	@Operation(summary = "remove the constraints to extend the association result community from a community", description = "remove the constraints to extend the association result community from a community", tags = {
			C, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails removeCommunityAdvancedConstraint(@PathVariable final String id) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.removeCommunityAdvancedConstraint(id);

			return getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/removeConstraint")
	@Operation(summary = "the set of constraints to be used to remove the association between result and community", description = "the set of constraints to be used to remove the association between result and community", tags = {
			C, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails addCommunityRemoveConstraint(
			@PathVariable final String id,
			@RequestBody final SelectionCriteria removeConstraint) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.addCommunityRemoveConstraint(id, removeConstraint);

			return getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/removeConstraint")
	@Operation(summary = "remove the constraints to remove the association beetween result and community", description = "remove the constraints to remove the association beetween result and community", tags = {
			C, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails removeCommunityRemoveConstraint(@PathVariable final String id)
			throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.removeCommunityRemoveConstraint(id);

			return getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/zenodocommunities")
	@Operation(summary = "associate a Zenodo community to a community", description = "associate a Zenodo community to the community", tags = {
			C_ZC, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails addCommunityZenodoCommunity(
			@PathVariable final String id,
			@RequestParam(required = false, defaultValue = "false") final boolean main,
			@RequestParam final String zenodocommunity) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.addCommunityZenodoCommunity(id, zenodocommunity, main);

			return getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}

	}

	@DeleteMapping("/community/{id}/zenodocommunities")
	@Operation(summary = "remove a Zenodo community from a community", description = "remove a Zenodo community from a community", tags = {
			C_ZC, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityDetails removeCommunityZenodoCommunity(
			@PathVariable final String id,
			@RequestParam(required = false, defaultValue = "false") final boolean main,
			@RequestParam final String zenodocommunity) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.removeCommunityZenodoCommunity(id, zenodocommunity, main);

			return getCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	// PROJECTS

	@GetMapping("/community/{id}/projects/{page}/{size}")
	@Operation(summary = "get community projects", description = "get community projects", tags = {
			C_PJ, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public Page<CommunityProject> getCommunityProjects(@PathVariable final String id,
			@PathVariable final Integer page,
			@PathVariable final Integer size,

			@RequestParam(required = false) final String funder,
			@RequestParam(required = false) final String searchFilter,
			@RequestParam(required = false) final String orderBy)
			throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			return communityService.getCommunityProjects(id, funder, searchFilter, page, size, orderBy);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/projects")
	@Operation(summary = "associate a project to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", description = "associate a project to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", tags = {
			C_PJ, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityProject addCommunityProject(
			@PathVariable final String id,

			@RequestBody final CommunityProject project) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			return communityService.addCommunityProject(id, project);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/projects")
	@Operation(summary = "remove a project from the community", description = "remove a project from the community", tags = {
			C_PJ, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void deleteCommunityProject(
			@PathVariable final String id,

			@RequestParam final String projectId) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.removeCommunityProjects(id, projectId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/projectList")
	@Operation(summary = "associate a list of project to the community", description = "associate a list of project to the community", tags = {
			C_PJ, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityProject[] addCommunityProjectList(
			@PathVariable final String id,

			@RequestBody final CommunityProject[] projects) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.addCommunityProjects(id, projects);
			return projects;
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/projectList")
	@Operation(summary = "remove a list of projects from the community", description = "remove a list of projects from the community", tags = {
			C_PJ, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void deleteCommunityProjectList(
			@PathVariable final String id,
			@RequestBody final String[] projectIdList) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.removeCommunityProjects(id, projectIdList);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@GetMapping("/community/{id}/funders")
	@Operation(summary = "get the funders of the projects of a community", description = "get the funders of the projects of a community", tags = {
			C_PJ, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public List<String> getCommunityFunders(@PathVariable final String id)
			throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			return communityService.getCommunityFunders(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	// Datasources

	@GetMapping("/community/{id}/datasources")
	@Operation(summary = "get the list of datasources associated to a given community", description = "get the list of content providers associated to a given community", tags = {
			C_CP, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public List<CommunityContentprovider> getCommunityDatasources(@PathVariable final String id,

			@RequestParam(required = false) final Boolean deposit)
			throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			return deposit == null ? communityService.getCommunityDatasources(id)
					: communityService.getCommunityDatasourcesWithDeposit(id, deposit);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/datasources")
	@Operation(summary = "associate a datasource to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", description = "associate a datasource to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", tags = {
			C_CP, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityContentprovider addCommunityDatasource(
			@PathVariable final String id,

			@RequestBody final CommunityContentprovider datasource) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.addCommunityDatasources(id, datasource);
			return datasource;
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/datasources/deposit")
	@Operation(summary = "update the deposit and message filelds of a datasource associated to the community", description = "update the deposit and message filelds of a datasource associated to the community", tags = {
			C_CP, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityContentprovider addCommunityDatasourceDeposit(
			@PathVariable final String id,

			@RequestBody final DepositionInfo info) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			return communityService.updateCommunityDatasourcesDeposit(id, info.getOpenaireId(), info
					.getDeposit(), info.getMessage());
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/datasources")
	@Operation(summary = "remove the association between a datasource and the community", description = "remove the association between a datasource and the community", tags = {
			C_CP, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void removeCommunityDatasource(
			@PathVariable final String id,

			@RequestParam final String dsId) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.removeCommunityDatasources(id, dsId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/datasourcesList")
	@Operation(summary = "associate a list of datasources to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", description = "associate a list of datasources to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", tags = {
			C_CP, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityContentprovider[] addCommunityDatasourcesList(
			@PathVariable final String id,

			@RequestBody final CommunityContentprovider[] dsList) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.addCommunityDatasources(id, dsList);
			return dsList;
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/datasourcesList")
	@Operation(summary = "remove a list of datasources from the community", description = "remove a list of datasources from the community", tags = {
			C_CP, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void deleteCommunityDatasourcesList(
			@PathVariable final String id,

			@RequestBody final String[] dsIdList) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.removeCommunityDatasources(id, dsIdList);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	// ORGANIZATIONS

	@GetMapping("/community/{id}/organizations")
	@Operation(summary = "get the list of organizations for a given community", description = "get the list of organizations for a given community", tags = {
			C_O, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public List<CommunityOrganization> getCommunityOrganizations(@PathVariable final String id)
			throws CommunityException {
		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			return communityService.getCommunityOrganizations(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/organizations")
	@Operation(summary = "associate an organization to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", description = "associate an organization to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", tags = {
			C_O, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityOrganization addCommunityOrganization(
			@PathVariable final String id,

			@RequestBody final CommunityOrganization organization) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.addCommunityOrganizations(id, organization);
			return organization;
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/organizationList")
	@Operation(summary = "associate a list of organizations to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", description = "associate a list of organizations to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", tags = {
			C_O, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityOrganization[] addCommunityOrganizationList(
			@PathVariable final String id,

			@RequestBody final CommunityOrganization[] orgs) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.addCommunityOrganizations(id, orgs);
			return orgs;
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/organizations")
	@Operation(summary = "remove the association between an organization and the community", description = "remove the association between an organization and the community", tags = {
			C_O, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void removeCommunityOrganization(
			@PathVariable final String id,

			@RequestParam final String organizationName) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.removeCommunityOrganizations(id, organizationName);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/organizationList")
	@Operation(summary = "remove a list of associations between some organizations and the community", description = "remove a list of associations between some organizations and the community", tags = {
			C_O, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void removeCommunityOrganizationList(
			@PathVariable final String id,

			@RequestBody final String[] orgNames) throws CommunityException {

		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		try {
			communityService.removeCommunityOrganizations(id, orgNames);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	// Propagation Organizations

	@GetMapping("/community/{id}/propagationOrganizations")
	@Operation(summary = "try { return the propagation organizations of a community", description = "try { return the propagation organizations of a community", tags = {
			C_O, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public Set<String> getPropagationOrganizationsForCommunity(@PathVariable final String id) throws CommunityException {
		try {
			return communityService.getPropagationOrganizationsForCommunity(id);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/propagationOrganizations")
	@Operation(summary = "add an organization to the propagationOrganizationCommunityMap", description = "add an organization to the propagationOrganizationCommunityMap", tags = {
			C_O, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public Set<String> addPropagationOrganizationForCommunity(@PathVariable final String id,
			@RequestParam final String organizationId) throws CommunityException {

		try {
			return communityService.addPropagationOrganizationForCommunity(id, organizationId.split(","));
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/propagationOrganizations")
	@Operation(summary = "delete an organization to the propagationOrganizationCommunityMap", description = "delete an organization to the propagationOrganizationCommunityMap", tags = {
			C_O, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public Set<String> removePropagationOrganizationForCommunity(@PathVariable final String id,
			@RequestParam final String organizationId) throws CommunityException {

		try {
			return communityService.removePropagationOrganizationForCommunity(id, organizationId.split(","));
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

}
