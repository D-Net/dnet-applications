package eu.dnetlib.openaire.dsm.dao;

import java.util.Queue;

import eu.dnetlib.openaire.dsm.dao.utils.IndexDsInfo;
import eu.dnetlib.openaire.dsm.dao.utils.IndexRecordsInfo;
import eu.dnetlib.openaire.exporter.exceptions.DsmApiException;

public interface DatasourceIndexClient {

	IndexRecordsInfo getIndexInfo(final String dsId, final IndexDsInfo info, final Queue<Throwable> errors) throws DsmApiException;

	String getLastIndexingDate(final IndexDsInfo info) throws DsmApiException;

}
