package eu.dnetlib.openaire.common;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.openaire.dsm.dao.utils.IndexDsInfo;
import eu.dnetlib.openaire.exporter.model.context.Context;

public interface ISClient {

	IndexDsInfo calculateCurrentIndexDsInfo() throws Exception;

	String getObjectStoreId(String dsId) throws Exception;

	@Deprecated
	Map<String, Context> getFunderContextMap() throws IOException;

	@Deprecated
	Map<String, Context> getCommunityContextMap() throws IOException;

	@Deprecated
	Map<String, Context> getContextMap(final List<String> type) throws IOException;

	@Deprecated
	void updateContextParam(String id, String name, String value, boolean toEscape);

	@Deprecated
	void updateContextAttribute(String id, String name, String value);

	@Deprecated
	void addConcept(String id, String categoryId, String data);

	@Deprecated
	void removeConcept(String id, String categoryId, String conceptId);

	void dropCache();

	/**
	 *
	 * @param id
	 *            id of the concept to be updated (i.e. ni::projects::2)
	 * @param name
	 *            name of the attribute to be updated
	 * @param value
	 *            new value for the attribute
	 */
	@Deprecated
	void updateConceptAttribute(String id, String name, String value);

	@Deprecated
	void updateConceptParam(String id, String name, String value);

	@Deprecated
	void updateConceptParamNoEscape(String id, String name, String value);

	String getProfile(String profileId) throws ISLookUpException;

}
