package eu.dnetlib.openaire.community.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.openaire.community.model.DbDatasource;
import eu.dnetlib.openaire.community.model.DbDatasourcePK;

@ConditionalOnProperty(value = "openaire.exporter.enable.community", havingValue = "true")
public interface DbDatasourceRepository extends JpaRepository<DbDatasource, DbDatasourcePK> {

	List<DbDatasource> findByCommunity(String community);

	List<DbDatasource> findByCommunityAndDeposit(String community, boolean deposit);

	void deleteByCommunity(String id);

}
