package eu.dnetlib.openaire.dsm.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import eu.dnetlib.openaire.exporter.model.dsm.DatasourceInfo;
import eu.dnetlib.openaire.exporter.model.dsm.Response;

@JsonAutoDetect
public class DatasourceSearchResponse extends Response {

	private List<DatasourceInfo> datasourceInfo;

	public DatasourceSearchResponse(final List<DatasourceInfo> datasourceInfo) {
		super();
		this.datasourceInfo = datasourceInfo;
	}

	public List<DatasourceInfo> getDatasourceInfo() {
		return datasourceInfo;
	}

	public void setDatasourceInfo(final List<DatasourceInfo> datasourceInfo) {
		this.datasourceInfo = datasourceInfo;
	}
}
