package eu.dnetlib.openaire.dsm.domain.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import eu.dnetlib.enabling.datasources.common.Datasource;

/**
 * Datasource
 */
@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "dsm_services")
public class DatasourceDbEntry extends Datasource<OrganizationDbEntry, IdentityDbEntry, PidSystemDbEntry> {

	public static final String DEFAULT_EOSC_TYPE = "Data Source";

	@Transient
	private String openaireId;

	@Deprecated
	@Column(name = "_typology_to_remove_")
	private String typology;

	@Column(name = "eosc_type")
	private String eoscType = DEFAULT_EOSC_TYPE;

	@Column(name = "dedup_main_service")
	private Boolean dedupMainService = true;

	public String getOpenaireId() {
		return openaireId;
	}

	public void setOpenaireId(final String openaireId) {
		this.openaireId = openaireId;
	}

	@Deprecated
	public String getTypology() {
		return typology;
	}

	@Deprecated
	public void setTypology(final String typology) {
		this.typology = typology;
	}

	public String getEoscType() {
		return eoscType;
	}

	public void setEoscType(final String eoscType) {
		this.eoscType = eoscType;
	}

	public Boolean getDedupMainService() {
		return dedupMainService;
	}

	public void setDedupMainService(final Boolean dedupMainService) {
		this.dedupMainService = dedupMainService;
	}
}
