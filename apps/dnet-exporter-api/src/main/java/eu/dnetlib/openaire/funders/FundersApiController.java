package eu.dnetlib.openaire.funders;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.openaire.common.AbstractExporterController;
import eu.dnetlib.openaire.exporter.exceptions.FundersApiException;
import eu.dnetlib.openaire.funders.domain.db.FunderDbEntry;
import eu.dnetlib.openaire.funders.domain.db.FunderDbUpdate;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@CrossOrigin(origins = {
		"*"
})
@ConditionalOnProperty(value = "openaire.exporter.enable.funders", havingValue = "true")
@Tag(name = "OpenAIRE funders API", description = "the OpenAIRE funders API")
public class FundersApiController extends AbstractExporterController {

	@Autowired
	private FunderService service;

	private static final Log log = LogFactory.getLog(FundersApiController.class);

	@RequestMapping(value = "/funders", produces = {
			"application/json"
	}, method = RequestMethod.GET)
	@Operation(summary = "get all funders", description = "get all funders")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public List<FunderDbEntry> getFunders() throws FundersApiException {
		try {
			return service.getFunders();
		} catch (final Throwable e) {
			log.error("Error getting funders", e);
			throw e;
		}
	}

	@RequestMapping(value = "/funders/{id}", produces = {
			"application/json"
	}, method = RequestMethod.GET)
	@Operation(summary = "get a funder by Id", description = "get a funder by Id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public FunderDbEntry getFunder(@PathVariable final String id) throws FundersApiException {
		try {
			return service.getFunder(id);
		} catch (final Throwable e) {
			log.error("Error getting funder: " + id, e);
			throw e;
		}
	}

	@RequestMapping(value = "/funders/{id}", produces = {
			"application/json"
	}, method = RequestMethod.POST)
	@Operation(summary = "update a funder by Id", description = "update a funder by Id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public FunderDbEntry updateFunder(@PathVariable final String id, @RequestBody final FunderDbUpdate funderUpdate) throws FundersApiException {
		if (service.isValidFunder(id)) {
			service.updateFunder(id, funderUpdate);
			return service.getFunder(id);
		}
		throw new FundersApiException("Invalid funder: " + id);
	}

}
