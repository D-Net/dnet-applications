package eu.dnetlib.openaire.community.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import eu.dnetlib.openaire.exporter.model.community.selectioncriteria.SelectionCriteria;

@Entity
@Table(name = "community_datasources")
@IdClass(DbDatasourcePK.class)
public class DbDatasource implements Serializable {

	private static final long serialVersionUID = -8782576185861694228L;

	@Id
	@Column(name = "community")
	private String community;

	@Id
	@Column(name = "ds_id")
	private String dsId;

	@Column(name = "ds_name")
	private String dsName;

	@Column(name = "ds_officialname")
	private String dsOfficialName;

	@Column(name = "enabled")
	private Boolean enabled;

	@Type(type = "jsonb")
	@Column(name = "constraints")
	private SelectionCriteria constraints;

	@Column(name = "deposit")
	private Boolean deposit;

	@Column(name = "message")
	private String message;

	public DbDatasource() {}

	public DbDatasource(final String community, final String dsId, final String dsName, final String dsOfficialName, final SelectionCriteria constraints) {
		this.community = community;
		this.dsId = dsId;
		this.dsName = dsName;
		this.dsOfficialName = dsOfficialName;
		this.constraints = constraints;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(final String community) {
		this.community = community;
	}

	public String getDsId() {
		return dsId;
	}

	public void setDsId(final String dsId) {
		this.dsId = dsId;
	}

	public String getDsName() {
		return dsName;
	}

	public void setDsName(final String dsName) {
		this.dsName = dsName;
	}

	public String getDsOfficialName() {
		return dsOfficialName;
	}

	public void setDsOfficialName(final String dsOfficialName) {
		this.dsOfficialName = dsOfficialName;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(final Boolean enabled) {
		this.enabled = enabled;
	}

	public SelectionCriteria getConstraints() {
		return constraints;
	}

	public void setConstraints(final SelectionCriteria constraints) {
		this.constraints = constraints;
	}

	public Boolean getDeposit() {
		return deposit;
	}

	public void setDeposit(final Boolean deposit) {
		this.deposit = deposit;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

}
