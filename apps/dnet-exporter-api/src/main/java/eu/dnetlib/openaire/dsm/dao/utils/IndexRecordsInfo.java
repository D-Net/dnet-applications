package eu.dnetlib.openaire.dsm.dao.utils;

/**
 * Created by claudio on 21/10/2016.
 */
public class IndexRecordsInfo {

	private long total;

	private long funded;

	private String date;

	public IndexRecordsInfo() {}

	public IndexRecordsInfo(final long total, final long funded, final String date) {
		this.total = total;
		this.funded = funded;
		this.date = date;
	}

	public long getTotal() {
		return total;
	}

	public IndexRecordsInfo setTotal(final long total) {
		this.total = total;
		return this;
	}

	public long getFunded() {
		return funded;
	}

	public IndexRecordsInfo setFunded(final long funded) {
		this.funded = funded;
		return this;
	}

	public String getDate() {
		return date;
	}

	public IndexRecordsInfo setDate(final String date) {
		this.date = date;
		return this;
	}

}
