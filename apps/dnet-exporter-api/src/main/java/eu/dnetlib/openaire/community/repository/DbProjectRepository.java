package eu.dnetlib.openaire.community.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import eu.dnetlib.openaire.community.model.DbProject;
import eu.dnetlib.openaire.community.model.DbProjectPK;

@ConditionalOnProperty(value = "openaire.exporter.enable.community", havingValue = "true")
public interface DbProjectRepository extends JpaRepository<DbProject, DbProjectPK>, JpaSpecificationExecutor<DbProject> {

	Page<DbProject> findByCommunity(String community, Pageable page);

	void deleteByCommunity(String id);

	@Query(value = "select distinct project_funder from community_projects where community = ?1 order by project_funder", nativeQuery = true)
	List<String> findFundersByCommunity(String id);

}
