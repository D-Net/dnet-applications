package eu.dnetlib.openaire.dsm.domain;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
@Schema(name = "Datasource model", description = "provides information about the datasource")
public class DatasourceSnippet {

	@NotBlank
	private String id;

	@NotBlank
	private String officialname;

	@NotBlank
	private String englishname;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getOfficialname() {
		return officialname;
	}

	public void setOfficialname(final String officialname) {
		this.officialname = officialname;
	}

	public String getEnglishname() {
		return englishname;
	}

	public void setEnglishname(final String englishname) {
		this.englishname = englishname;
	}
}
