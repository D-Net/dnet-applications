package eu.dnetlib.openaire.project;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.antlr.stringtemplate.StringTemplate;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.xml.XmlEscapers;

import eu.dnetlib.DnetOpenaireExporterProperties;
import eu.dnetlib.DnetOpenaireExporterProperties.Project;
import eu.dnetlib.openaire.common.AbstractExporterController;
import eu.dnetlib.openaire.common.ExporterConstants;
import eu.dnetlib.openaire.project.dao.JdbcApiDao;
import eu.dnetlib.openaire.project.dao.ValueCleaner;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Controller
@CrossOrigin(origins = {
	"*"
})
@ConditionalOnProperty(value = "openaire.exporter.enable.project", havingValue = "true")
@Tag(name = "OpenAIRE projects API", description = "the OpenAIRE projects API")
public class ProjectsController extends AbstractExporterController {

	private static final Log log = LogFactory.getLog(ProjectsController.class); // NOPMD by marko on 11/24/08 5:02 PM

	public final static String UTF8 = "UTF-8";

	@Autowired
	private DnetOpenaireExporterProperties config;

	@Autowired
	private JdbcApiDao dao;

	@Autowired
	private ProjectQueryParamsFactory projectQueryParamsFactory;

	@RequestMapping(value = "/export/**/project/dspace.do", method = RequestMethod.GET)
	@Operation(summary = "DSpace", description = "return project information in compatible with the OpenAIRE plugin for DSpace", tags = {
		ExporterConstants.DSPACE
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void processDspace(final HttpServletRequest request,
		final ServletResponse response,
		@RequestParam(value = "startFrom", required = false) final String startFrom,
		@RequestParam(value = "startUntil", required = false) final String startUntil,
		@RequestParam(value = "endFrom", required = false) final String endFrom,
		@RequestParam(value = "endUntil", required = false) final String endUntil) throws Exception {

		final Project conf = config.getProject();

		final ProjectQueryParams params = projectQueryParamsFactory.generateParams(request, startFrom, startUntil, endFrom, endUntil);
		final StringTemplate headSt = new StringTemplate(IOUtils.toString(conf.getDspaceHeadTemplate().getInputStream(), UTF8));

		headSt.setAttribute("fundingProgramme", params.getFundingProgramme());

		final StringTemplate tailSt = new StringTemplate(IOUtils.toString(conf.getDspaceTailTemplate().getInputStream(), UTF8));

		response.setContentType("text/xml");
		doProcess(response, params, headSt.toString(), conf.getDspaceTemplate(), tailSt.toString(), s -> XmlEscapers.xmlContentEscaper().escape(oneLiner(s)));
	}

	@RequestMapping(value = "/export/**/project/eprints.do", method = RequestMethod.GET)
	@Operation(summary = "EPrints", description = "return project information in compatible with the OpenAIRE plugin for Eprints", tags = {
		ExporterConstants.EPRINT
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void processEprints(final HttpServletRequest request,
		final ServletResponse response,
		@RequestParam(value = "startFrom", required = false) final String startFrom,
		@RequestParam(value = "startUntil", required = false) final String startUntil,
		@RequestParam(value = "endFrom", required = false) final String endFrom,
		@RequestParam(value = "endUntil", required = false) final String endUntil) throws Exception {

		final ProjectQueryParams params = projectQueryParamsFactory.generateParams(request, startFrom, startUntil, endFrom, endUntil);
		response.setContentType("text/html");
		doProcess(response, params, null, config.getProject().getEprintsTemplate(), null, this::oneLiner);
	}

	private String oneLiner(final String s) {
		return StringUtils.isNotBlank(s) ? s.replaceAll("\\n", " ").trim() : "";
	}

	private void doProcess(
		final ServletResponse response,
		final ProjectQueryParams params,
		final String head,
		final Resource projectTemplate,
		final String tail,
		final ValueCleaner cleaner) throws IOException, SQLException {

		final StringTemplate st = new StringTemplate(IOUtils.toString(projectTemplate.getInputStream(), UTF8));
		try (final OutputStream out = new BufferedOutputStream(response.getOutputStream())) {
			dao.streamProjects(obtainQuery(params), out, head, st, tail, cleaner);
		}
	}

	@RequestMapping(value = "/noads/project2tsv.do", method = RequestMethod.GET)
	@Operation(summary = "TSV", description = "download project information in TSV format", tags = {
		ExporterConstants.TSV
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void processTsv(final HttpServletResponse response,
		@RequestParam(value = "funding", required = true) final String funding,
		@RequestParam(value = "article293", required = false) final Boolean article293) throws Exception {

		final String fundingPrefix = getFundingPrefix(funding, null);

		final String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		final String filename = "projects_" + funding + "_" + date + ".tsv";
		response.setContentType("text/tab-separated-values");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + ".zip\"");
		try (final ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(response.getOutputStream()))) {
			dao.processTsvRequest(out, article293, fundingPrefix, filename);
		} catch (final Throwable e) {
			throw new RuntimeException("Error processing the request", e);
		}
	}

	@RequestMapping(value = "/export/streamProjectDetails.do", method = RequestMethod.GET)
	@Operation(summary = "Stream projects", description = "stream project information", tags = {
		ExporterConstants.STREAMING
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void streamProjectDetails(final HttpServletResponse response,
		@RequestParam(value = "format", required = true) final String format,
		@RequestParam(value = "compress", required = false) final Boolean compress) throws IOException, SQLException {

		if (compress != null && compress) {
			response.setHeader("Content-Encoding", "gzip");
		}
		switch (format) {
		case "csv":
			response.setContentType("text/csv");
			break;
		case "json":
			response.setContentType("text/plain");
			break;
		default:
			throw new IllegalArgumentException("unsupported format: " + format);
		}

		dao.processProjectDetails(response.getOutputStream(), format, compress);
	}

	/**
	 * Creates the query on the fundingProgramme specified in the given parameters.
	 *
	 * @param params
	 *            request parameters
	 * @return the query string
	 * @throws IllegalArgumentException
	 *             if the funding program is not recognized
	 * @throws IOException
	 *             if there are problem loading the query temlate
	 * @throws IllegalArgumentException
	 *             if the funding program is not recognized
	 */
	protected String obtainQuery(final ProjectQueryParams params) throws IllegalArgumentException, IOException {
		final String funding = params.getFundingProgramme();
		final String suffix = params.getFundingPath();

		final StringTemplate st = new StringTemplate(IOUtils.toString(config.getProject().getProjectsFundingQueryTemplate().getInputStream(), UTF8));
		st.setAttribute("fundingprefix", getFundingPrefix(funding, suffix));
		final String theQuery = setDateParameters(st.toString(), params);
		log.debug("Generated query: " + theQuery);
		return theQuery;
	}

	private String getFundingPrefix(final String funding, final String suffix) {
		final Map<String, String> fundingIds = dao.readFundingpathIds();
		if (!fundingIds.containsKey(funding.toUpperCase())) { throw new IllegalArgumentException("invalid funding " + funding); }
		final String fundingPrefix = fundingIds.get(funding.toUpperCase());
		return StringUtils.isBlank(suffix) ? fundingPrefix : fundingPrefix + "::" + suffix.toUpperCase();
	}

	private String setDateParameters(final String query, final ProjectQueryParams params) {
		String queryWithDates = query;
		if (params.getStartFrom() != null) {
			queryWithDates += " AND startdate >= '" + params.getStartFrom() + "'";
		}
		if (params.getStartUntil() != null) {
			queryWithDates += " AND startdate <= '" + params.getStartUntil() + "'";
		}
		if (params.getEndFrom() != null) {
			queryWithDates += " AND enddate >= '" + params.getEndFrom() + "'";
		}
		if (params.getEndUntil() != null) {
			queryWithDates += " AND enddate <= '" + params.getEndUntil() + "'";
		}
		return queryWithDates;
	}

}
