package eu.dnetlib.openaire.community.model;

import java.io.Serializable;
import java.util.Objects;

public class DbSupportOrgPK implements Serializable {

	private static final long serialVersionUID = -4117154543803798310L;

	private String community;

	private String orgName;

	public DbSupportOrgPK() {}

	public DbSupportOrgPK(final String community, final String orgName) {
		this.community = community;
		this.orgName = orgName;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(final String community) {
		this.community = community;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(final String orgName) {
		this.orgName = orgName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(community, orgName);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof DbSupportOrgPK)) { return false; }
		final DbSupportOrgPK other = (DbSupportOrgPK) obj;
		return Objects.equals(community, other.community) && Objects.equals(orgName, other.orgName);
	}

	@Override
	public String toString() {
		return String.format("CommunitySupportOrgPK [community=%s, orgName=%s]", community, orgName);
	}

}
