package eu.dnetlib.openaire.community.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "community_support_orgs")
@IdClass(DbSupportOrgPK.class)
public class DbSupportOrg implements Serializable {

	private static final long serialVersionUID = 1308759097276753411L;

	@Id
	@Column(name = "community")
	private String community;

	@Id
	@Column(name = "org_name")
	private String orgName;

	@Column(name = "org_url")
	private String orgUrl;

	@Column(name = "org_logourl")
	private String orgLogoUrl;

	public String getCommunity() {
		return community;
	}

	public void setCommunity(final String community) {
		this.community = community;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(final String orgName) {
		this.orgName = orgName;
	}

	public String getOrgUrl() {
		return orgUrl;
	}

	public void setOrgUrl(final String orgUrl) {
		this.orgUrl = orgUrl;
	}

	public String getOrgLogoUrl() {
		return orgLogoUrl;
	}

	public void setOrgLogoUrl(final String orgLogoUrl) {
		this.orgLogoUrl = orgLogoUrl;
	}

}
