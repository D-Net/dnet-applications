package eu.dnetlib.openaire.community.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name = "community_projects")
@IdClass(DbProjectPK.class)
public class DbProject implements Serializable {

	private static final long serialVersionUID = 1649065971750517925L;

	@Id
	@Column(name = "community")
	private String community;

	@Id
	@Column(name = "project_id")
	private String projectId;

	@Column(name = "project_code")
	private String projectCode;

	@Column(name = "project_name")
	private String projectName;

	@Column(name = "project_acronym")
	private String projectAcronym;

	@Column(name = "project_funder")
	private String projectFunder;

	@CreatedDate
	@Column(name = "available_since")
	private LocalDate availableSince;

	public DbProject() {}

	public DbProject(final String community, final String projectId, final String projectCode, final String projectName, final String projectAcronym,
		final String projectFunder, final LocalDate availableSince) {
		this.community = community;
		this.projectId = projectId;
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.projectAcronym = projectAcronym;
		this.projectFunder = projectFunder;
		this.availableSince = availableSince;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(final String community) {
		this.community = community;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(final String projectCode) {
		this.projectCode = projectCode;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(final String projectName) {
		this.projectName = projectName;
	}

	public String getProjectAcronym() {
		return projectAcronym;
	}

	public void setProjectAcronym(final String projectAcronym) {
		this.projectAcronym = projectAcronym;
	}

	public String getProjectFunder() {
		return projectFunder;
	}

	public void setProjectFunder(final String projectFunder) {
		this.projectFunder = projectFunder;
	}

	public LocalDate getAvailableSince() {
		return availableSince;
	}

	public void setAvailableSince(final LocalDate availableSince) {
		this.availableSince = availableSince;
	}
}
