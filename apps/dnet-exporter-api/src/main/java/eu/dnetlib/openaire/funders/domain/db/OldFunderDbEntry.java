package eu.dnetlib.openaire.funders.domain.db;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import eu.dnetlib.openaire.dsm.domain.db.IdentityDbEntry;

// @Entity
// @Table(name = "funders")
@Deprecated
public class OldFunderDbEntry {

	@Id
	private String id;
	private String name;
	private String shortname;
	private String jurisdiction;
	private String websiteurl;
	private String policy;
	private Date registrationdate;
	private Date lastupdatedate;

	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
	@JoinTable(name = "funder_identity", joinColumns = @JoinColumn(name = "funder"), inverseJoinColumns = @JoinColumn(name = "pid"))
	private Set<IdentityDbEntry> pids;

	@OneToMany(mappedBy = "funderid", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<OldFundingPathDbEntry> fundingpaths;

	public OldFunderDbEntry() {}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getShortname() {
		return shortname;
	}

	public void setShortname(final String shortname) {
		this.shortname = shortname;
	}

	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(final String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getWebsiteurl() {
		return websiteurl;
	}

	public void setWebsiteurl(final String websiteurl) {
		this.websiteurl = websiteurl;
	}

	public String getPolicy() {
		return policy;
	}

	public void setPolicy(final String policy) {
		this.policy = policy;
	}

	public Date getRegistrationdate() {
		return registrationdate;
	}

	public void setRegistrationdate(final Date registrationdate) {
		this.registrationdate = registrationdate;
	}

	public Date getLastupdatedate() {
		return lastupdatedate;
	}

	public void setLastupdatedate(final Date lastupdatedate) {
		this.lastupdatedate = lastupdatedate;
	}

	public Set<IdentityDbEntry> getPids() {
		return pids;
	}

	public void setPids(final Set<IdentityDbEntry> pids) {
		this.pids = pids;
	}

	public Set<OldFundingPathDbEntry> getFundingpaths() {
		return fundingpaths;
	}

	public void setFundingpaths(final Set<OldFundingPathDbEntry> fundingpaths) {
		this.fundingpaths = fundingpaths;
	}
}
