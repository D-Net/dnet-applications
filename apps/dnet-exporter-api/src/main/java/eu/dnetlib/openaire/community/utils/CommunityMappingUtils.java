package eu.dnetlib.openaire.community.utils;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.collect.Lists;

import eu.dnetlib.openaire.community.importer.CommunityImporterService;
import eu.dnetlib.openaire.community.model.DbCommunity;
import eu.dnetlib.openaire.community.model.DbCommunityType;
import eu.dnetlib.openaire.community.model.DbDatasource;
import eu.dnetlib.openaire.community.model.DbProject;
import eu.dnetlib.openaire.community.model.DbSupportOrg;
import eu.dnetlib.openaire.exporter.model.community.CommunityContentprovider;
import eu.dnetlib.openaire.exporter.model.community.CommunityDetails;
import eu.dnetlib.openaire.exporter.model.community.CommunityOrganization;
import eu.dnetlib.openaire.exporter.model.community.CommunityPlanType;
import eu.dnetlib.openaire.exporter.model.community.CommunityProject;
import eu.dnetlib.openaire.exporter.model.community.CommunitySummary;
import eu.dnetlib.openaire.exporter.model.community.CommunityType;
import eu.dnetlib.openaire.exporter.model.community.CommunityWritableProperties;
import eu.dnetlib.openaire.exporter.model.community.SubCommunity;
import eu.dnetlib.openaire.exporter.model.community.SubCommunityWritableProperties;
import eu.dnetlib.openaire.exporter.model.context.IISConfigurationEntry;

public class CommunityMappingUtils {

	public static final String COMMUNITY_ID_PARTS_SEPARATOR = "::";

	public final static String PIPE_SEPARATOR = "||";

	private static final List<String> DATE_PATTERN = Lists.newArrayList("yyyy-MM-dd'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ssXXX", "yyyy-MM-dd'T'hh:mm:ss+00:00");

	private static final Log log = LogFactory.getLog(CommunityMappingUtils.class);

	public static CommunitySummary toCommunitySummary(final DbCommunity c) {
		final CommunitySummary summary = new CommunitySummary();
		populateSummary(summary, c);
		return summary;
	}

	public static DbCommunity toCommunity(final CommunityDetails details) {
		final DbCommunity c = new DbCommunity();
		c.setId(details.getId());
		c.setName(details.getName());
		c.setShortName(details.getShortName());
		c.setDisplayName(details.getDisplayName());
		c.setDisplayShortName(details.getDisplayShortName());
		c.setDescription(details.getDescription());
		c.setStatus(details.getStatus());
		c.setLogoUrl(details.getLogoUrl());
		c.setMembership(details.getMembership());
		c.setType(toDbCommunityType(details.getType()));
		c.setClaim(details.getClaim());
		c.setSubjects(toStringArray(details.getSubjects()));
		c.setFos(toStringArray(details.getFos()));
		c.setSdg(toStringArray(details.getSdg()));
		c.setAdvancedConstraints(details.getAdvancedConstraints());
		c.setRemoveConstraints(details.getRemoveConstraints());
		c.setMainZenodoCommunity(details.getZenodoCommunity());
		c.setOtherZenodoCommunities(toStringArray(details.getOtherZenodoCommunities()));
		c.setSuggestedAcknowledgements(toStringArray(details.getSuggestedAcknowledgements()));
		c.setPlan(details.getPlan());
		c.setCreationDate(ObjectUtils.firstNonNull(details.getCreationDate(), LocalDateTime.now()));
		c.setLastUpdateDate(LocalDateTime.now());
		c.setFeatured(details.getFeatured());

		return c;
	}

	public static void populateCommunity(final DbCommunity c, final CommunityWritableProperties details) {
		if (StringUtils.isNotBlank(details.getName())) {
			c.setName(details.getName());
		}
		if (StringUtils.isNotBlank(details.getShortName())) {
			c.setShortName(details.getShortName());
		}
		if (StringUtils.isNotBlank(details.getDisplayName())) {
			c.setDisplayName(details.getDisplayName());
		}
		if (StringUtils.isNotBlank(details.getDisplayShortName())) {
			c.setDisplayShortName(details.getDisplayShortName());
		}
		if (StringUtils.isNotBlank(details.getDescription())) {
			c.setDescription(details.getDescription());
		}
		if (details.getStatus() != null) {
			c.setStatus(details.getStatus());
		}
		if (details.getMembership() != null) {
			c.setMembership(details.getMembership());
		}
		if (details.getType() != null) {
			c.setType(toDbCommunityType(details.getType()));
		}
		if (details.getClaim() != null) {
			c.setClaim(details.getClaim());
		}
		if (StringUtils.isNotBlank(details.getLogoUrl())) {
			c.setLogoUrl(details.getLogoUrl());
		}
		if (details.getFos() != null) {
			c.setFos(toStringArray(details.getFos()));
		}
		if (details.getSdg() != null) {
			c.setSdg(toStringArray(details.getSdg()));
		}
		if (details.getSubjects() != null) {
			c.setSubjects(toStringArray(details.getSubjects()));
		}
		if (details.getAdvancedConstraints() != null) {
			c.setAdvancedConstraints(details.getAdvancedConstraints());
		}
		if (details.getRemoveConstraints() != null) {
			c.setRemoveConstraints(details.getRemoveConstraints());
		}
		if (StringUtils.isNotBlank(details.getMainZenodoCommunity())) {
			c.setMainZenodoCommunity(details.getMainZenodoCommunity());
		}
		if (details.getOtherZenodoCommunities() != null) {
			c.setOtherZenodoCommunities(toStringArray(details.getOtherZenodoCommunities()));
		}
		if (details.getPlan() != null) {
			c.setPlan(details.getPlan());
		}
		if (details.getFeatured() != null) {
			c.setFeatured(details.getFeatured());
		}

		c.setLastUpdateDate(LocalDateTime.now());
	}

	public static void populateCommunity(final DbCommunity c, final SubCommunityWritableProperties details) {
		if (StringUtils.isNotBlank(details.getLabel())) {
			c.setName(details.getLabel());
			c.setShortName(details.getLabel());
		}

		if (StringUtils.isNotBlank(details.getCategory())) {
			c.setCategory(details.getCategory());
		}

		if (details.getParams() != null) {
			c.setParams(details.getParams());
		}

		if (details.getClaim() != null) {
			c.setClaimable(details.getClaim());
		}

		if (details.getBrowsable() != null) {
			c.setBrowsable(details.getBrowsable());
		}

		if (details.getFos() != null) {
			c.setFos(toStringArray(details.getFos()));
		}
		if (details.getSdg() != null) {
			c.setSdg(toStringArray(details.getSdg()));
		}
		if (details.getSubjects() != null) {
			c.setSubjects(toStringArray(details.getSubjects()));
		}
		if (details.getAdvancedConstraints() != null) {
			c.setAdvancedConstraints(details.getAdvancedConstraints());
		}
		if (details.getRemoveConstraints() != null) {
			c.setRemoveConstraints(details.getRemoveConstraints());
		}
		if (StringUtils.isNotBlank(details.getZenodoCommunity())) {
			c.setMainZenodoCommunity(details.getZenodoCommunity());
		}
		if (details.getOtherZenodoCommunities() != null) {
			c.setOtherZenodoCommunities(toStringArray(details.getOtherZenodoCommunities()));
		}
		if (details.getSuggestedAcknowledgements() != null) {
			c.setSuggestedAcknowledgements(toStringArray(details.getSuggestedAcknowledgements()));
		}

		c.setLastUpdateDate(LocalDateTime.now());

	}

	public static CommunityDetails toCommunityDetails(final DbCommunity c) {
		final CommunityDetails details = new CommunityDetails();
		populateSummary(details, c);
		details.setAdvancedConstraints(c.getAdvancedConstraints());
		details.setRemoveConstraints(c.getRemoveConstraints());
		details.setFos(Arrays.asList(c.getFos()));
		details.setSdg(Arrays.asList(c.getSdg()));
		details.setSubjects(Arrays.asList(c.getSubjects()));
		details.setOtherZenodoCommunities(Arrays.asList(c.getOtherZenodoCommunities()));
		details.setSuggestedAcknowledgements(Arrays.asList(c.getSuggestedAcknowledgements()));
		return details;
	}

	private static void populateSummary(final CommunitySummary summary, final DbCommunity c) {
		summary.setId(c.getId());
		summary.setName(c.getName());
		summary.setShortName(c.getShortName());
		summary.setDisplayName(c.getDisplayName());
		summary.setDisplayShortName(c.getDisplayShortName());
		summary.setLastUpdateDate(c.getLastUpdateDate());
		summary.setCreationDate(c.getCreationDate());
		summary.setQueryId(c.getId() + PIPE_SEPARATOR + c.getShortName());
		summary.setType(toCommunityType(c.getType()));
		summary.setDescription(c.getDescription());
		summary.setLogoUrl(c.getLogoUrl());
		summary.setStatus(c.getStatus());
		summary.setClaim(c.getClaim());
		summary.setMembership(c.getMembership());
		summary.setZenodoCommunity(c.getMainZenodoCommunity());
		summary.setPlan(c.getPlan());
		summary.setFeatured(c.getFeatured());
	}

	private static DbCommunityType toDbCommunityType(final CommunityType type) {
		if (type == CommunityType.community) { return DbCommunityType.community; }
		if (type == CommunityType.ri) { return DbCommunityType.ri; }
		return null;
	}

	private static CommunityType toCommunityType(final DbCommunityType type) {
		if (type == DbCommunityType.community) { return CommunityType.community; }
		if (type == DbCommunityType.ri) { return CommunityType.ri; }
		if (type == DbCommunityType.subcommunity) { throw new RuntimeException("Invalid type (subcommunity)"); }
		return null;
	}

	public static CommunityProject toCommunityProject(final DbProject dbEntry) {
		final CommunityProject cp = new CommunityProject();
		cp.setCommunityId(dbEntry.getCommunity());
		cp.setOpenaireId(dbEntry.getProjectId());
		cp.setName(dbEntry.getProjectName());
		cp.setAcronym(dbEntry.getProjectAcronym());
		cp.setFunder(dbEntry.getProjectFunder());
		cp.setGrantId(dbEntry.getProjectCode());
		cp.setAvailableSince(dbEntry.getAvailableSince());
		return cp;
	}

	public static DbProject toDbProject(final String id, final CommunityProject project) {
		final DbProject p = new DbProject();
		p.setCommunity(id);
		p.setProjectId(project.getOpenaireId());
		p.setProjectName(project.getName());
		p.setProjectAcronym(project.getAcronym());
		p.setProjectCode(project.getGrantId());
		p.setProjectFunder(project.getFunder());
		if (project.getAvailableSince() != null) {
			p.setAvailableSince(project.getAvailableSince());
		} else {
			p.setAvailableSince(LocalDate.now());
		}
		return p;
	}

	public static CommunityContentprovider toCommunityContentprovider(final DbDatasource dbEntry) {
		final CommunityContentprovider ccp = new CommunityContentprovider();
		ccp.setCommunityId(dbEntry.getCommunity());
		ccp.setOpenaireId(dbEntry.getDsId());
		ccp.setName(dbEntry.getDsName());
		ccp.setOfficialname(dbEntry.getDsOfficialName());
		ccp.setSelectioncriteria(dbEntry.getConstraints());
		ccp.setEnabled(dbEntry.getEnabled() != null ? dbEntry.getEnabled() : true);
		ccp.setDeposit(dbEntry.getDeposit() != null ? dbEntry.getDeposit() : false);
		ccp.setMessage(dbEntry.getMessage());
		return ccp;
	}

	public static DbDatasource toDbDatasource(final String id, final CommunityContentprovider provider) {
		final DbDatasource ds = new DbDatasource();
		ds.setCommunity(id);
		ds.setDsId(provider.getOpenaireId());
		ds.setDsName(provider.getName());
		ds.setDsOfficialName(provider.getOfficialname());
		ds.setConstraints(provider.getSelectioncriteria());
		ds.setEnabled(provider.isEnabled());
		ds.setDeposit(provider.getDeposit() != null ? provider.getDeposit() : false);
		ds.setMessage(provider.getMessage());
		return ds;
	}

	public static CommunityOrganization toCommunityOrganization(final DbSupportOrg dbEntry) {
		final CommunityOrganization co = new CommunityOrganization();
		co.setCommunityId(dbEntry.getCommunity());
		co.setName(dbEntry.getOrgName());
		co.setWebsite_url(dbEntry.getOrgUrl());
		co.setLogo_url(dbEntry.getOrgLogoUrl());
		return co;
	}

	public static DbSupportOrg toDbSupportOrg(final String id, final CommunityOrganization org) {
		final DbSupportOrg dbo = new DbSupportOrg();
		dbo.setCommunity(id);
		dbo.setOrgName(org.getName());
		dbo.setOrgUrl(org.getWebsite_url());
		dbo.setOrgLogoUrl(org.getLogo_url());
		return dbo;
	}

	public static DbCommunity toDbCommunity(final SubCommunity sub) {
		final DbCommunity dbsc = new DbCommunity();

		dbsc.setId(sub.getSubCommunityId());
		dbsc.setParent(sub.getParent());
		dbsc.setType(DbCommunityType.subcommunity);
		dbsc.setCategory(sub.getCategory());
		dbsc.setName(sub.getLabel());
		dbsc.setShortName(sub.getLabel());
		dbsc.setDescription("");
		dbsc.setPlan(CommunityPlanType.Default);
		dbsc.setParams(sub.getParams());
		dbsc.setClaimable(sub.isClaim());
		dbsc.setBrowsable(sub.isBrowsable());
		dbsc.setAdvancedConstraints(sub.getAdvancedConstraints());
		dbsc.setRemoveConstraints(sub.getRemoveConstraints());
		dbsc.setFos(toStringArray(sub.getFos()));
		dbsc.setSdg(toStringArray(sub.getSdg()));
		dbsc.setSubjects(toStringArray(sub.getSubjects()));
		dbsc.setMainZenodoCommunity(sub.getZenodoCommunity());
		dbsc.setOtherZenodoCommunities(toStringArray(sub.getOtherZenodoCommunities()));
		dbsc.setSuggestedAcknowledgements(toStringArray(sub.getSuggestedAcknowledgements()));
		dbsc.setCreationDate(LocalDateTime.now());
		dbsc.setLastUpdateDate(LocalDateTime.now());
		dbsc.setFeatured(null);

		return dbsc;
	}

	public static SubCommunity toSubCommunity(final DbCommunity sub) {
		final SubCommunity sc = new SubCommunity();

		sc.setSubCommunityId(sub.getId());
		sc.setCommunityId(calculateMainCommunityId(sub.getId()));
		sc.setParent(sub.getParent());
		sc.setCategory(sub.getCategory());
		sc.setLabel(sub.getName());
		sc.setParams(sub.getParams());
		sc.setClaim(ObjectUtils.firstNonNull(sub.getClaimable(), false));
		sc.setBrowsable(ObjectUtils.firstNonNull(sub.getBrowsable(), false));
		sc.setAdvancedConstraints(sub.getAdvancedConstraints());
		sc.setRemoveConstraints(sub.getRemoveConstraints());
		sc.setFos(Arrays.asList(sub.getFos()));
		sc.setSdg(Arrays.asList(sub.getSdg()));
		sc.setSubjects(Arrays.asList(sub.getSubjects()));
		sc.setZenodoCommunity(sub.getMainZenodoCommunity());
		sc.setOtherZenodoCommunities(Arrays.asList(sub.getOtherZenodoCommunities()));
		sc.setSuggestedAcknowledgements(Arrays.asList(sub.getSuggestedAcknowledgements()));

		return sc;
	}

	public static String calculateMainCommunityId(final String sub) {
		return StringUtils.substringBefore(sub, COMMUNITY_ID_PARTS_SEPARATOR);
	}

	public static LocalDateTime asLocalDateTime(final String s) {
		if (StringUtils.isBlank(s)) { return null; }

		for (final String pattern : DATE_PATTERN) {
			try {
				final Date res = DateUtils.parseDate(s, pattern);
				if (res != null) { return asLocalDateTime(res); }
			} catch (final ParseException e) {}
		}
		log.warn("Invalid Date: " + s);
		return null;
	}

	public static LocalDateTime asLocalDateTime(final Date date) {
		return date.toInstant()
				.atZone(ZoneId.systemDefault())
				.toLocalDateTime();
	}

	private static String[] toStringArray(final List<String> list) {
		return list != null ? list.toArray(new String[list.size()]) : new String[0];
	}

	public static IISConfigurationEntry asIISConfigurationEntry(final DbCommunity c) {
		final IISConfigurationEntry conf = new IISConfigurationEntry();
		conf.setId(c.getId());
		conf.setLabel(c.getName());

		if (c.getParent() == null) {
			conf.addParams(CommunityImporterService.CSUMMARY_DESCRIPTION, c.getDescription());
			conf.addParams(CommunityImporterService.CSUMMARY_LOGOURL, c.getLogoUrl());
			conf.addParams(CommunityImporterService.CSUMMARY_STATUS, c.getStatus().toString());
			conf.addParams(CommunityImporterService.CSUMMARY_NAME, c.getName());
			conf.addParams(CommunityImporterService.CSUMMARY_ZENODOC, c.getMainZenodoCommunity());
			conf.addParams(CommunityImporterService.CPROFILE_SUBJECT, c.getSubjects());
			conf.addParams(CommunityImporterService.CPROFILE_FOS, c.getFos());
			conf.addParams(CommunityImporterService.CPROFILE_SDG, c.getSdg());
			conf.addParams(CommunityImporterService.CPROFILE_ADVANCED_CONSTRAINT, c.getAdvancedConstraints() != null ? c.getAdvancedConstraints().toJson()
					: null);
			conf.addParams(CommunityImporterService.CPROFILE_REMOVE_CONSTRAINT, c.getRemoveConstraints() != null ? c.getRemoveConstraints().toJson() : null);
			conf.addParams(CommunityImporterService.CPROFILE_SUGGESTED_ACKNOWLEDGEMENT, c.getSuggestedAcknowledgements());
			conf.addParams(CommunityImporterService.CPROFILE_CREATIONDATE, c.getCreationDate() != null ? c.getCreationDate().toString() : null);
		} else if (c.getParams() != null) {
			conf.getParams().addAll(c.getParams());
		}

		return conf;
	}

	public static boolean isValidCommunityId(final String id) {
		return StringUtils.isNotBlank(id) && !StringUtils.contains(id, COMMUNITY_ID_PARTS_SEPARATOR);
	}

	public static boolean isValidSubCommunityId(final String subcommunityId) {
		return StringUtils.isNotBlank(subcommunityId) && StringUtils.split(subcommunityId, COMMUNITY_ID_PARTS_SEPARATOR).length > 1;
	}

}
