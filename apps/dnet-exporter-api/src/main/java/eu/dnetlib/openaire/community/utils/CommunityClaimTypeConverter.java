package eu.dnetlib.openaire.community.utils;

import javax.persistence.AttributeConverter;

import org.apache.commons.lang3.StringUtils;

import eu.dnetlib.openaire.exporter.model.community.CommunityClaimType;

public class CommunityClaimTypeConverter implements AttributeConverter<CommunityClaimType, String> {

	@Override
	public String convertToDatabaseColumn(final CommunityClaimType attribute) {
		if (attribute == null) {
			return null;
		} else {
			return attribute.getDescription();
		}
	}

	@Override
	public CommunityClaimType convertToEntityAttribute(final String dbData) {
		if (StringUtils.isBlank(dbData)) {
			return null;
		} else {
			return CommunityClaimType.fromDescription(dbData);
		}
	}

}
