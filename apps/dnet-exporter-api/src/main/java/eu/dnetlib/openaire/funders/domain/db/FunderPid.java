package eu.dnetlib.openaire.funders.domain.db;

import java.io.Serializable;

public class FunderPid implements Serializable {

	private static final long serialVersionUID = 2145493560459874509L;

	private String type;

	private String value;

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

}
