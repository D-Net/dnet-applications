package eu.dnetlib.openaire.dsm.dao;

import eu.dnetlib.openaire.exporter.exceptions.DsmApiException;
import eu.dnetlib.openaire.exporter.model.vocabularies.Vocabulary;

public interface VocabularyClient {

	Vocabulary getCountries() throws DsmApiException;

	Vocabulary getDatasourceTypologies() throws DsmApiException;

	void dropCache();

}
