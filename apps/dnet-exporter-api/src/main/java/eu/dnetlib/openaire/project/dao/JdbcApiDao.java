package eu.dnetlib.openaire.project.dao;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.Map;
import java.util.zip.ZipOutputStream;

import org.antlr.stringtemplate.StringTemplate;

public interface JdbcApiDao {

	Map<String, String> readFundingpathIds();

	void processProjectDetails(final OutputStream outputStream, String format, Boolean compress) throws IOException;

	void processTsvRequest(final ZipOutputStream out, final Boolean article293, final String fundingPrefix, final String filename) throws IOException;

	void streamProjects(
			final String sql,
			final OutputStream out,
			final String head,
			final StringTemplate projectTemplate,
			final String tail,
			final ValueCleaner cleaner) throws IOException, SQLException;

	void dropCache();

}
