package eu.dnetlib.openaire.funders.domain.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FunderDbUpdate implements Serializable {

	private static final long serialVersionUID = -9086478785780647627L;

	private String legalShortName;
	private String legalName;
	private String websiteUrl;
	private String logoUrl;
	private String country;
	private Boolean registered;
	private List<FunderPid> pids = new ArrayList<FunderPid>();

	public String getLegalShortName() {
		return legalShortName;
	}

	public void setLegalShortName(final String legalShortName) {
		this.legalShortName = legalShortName;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(final String legalName) {
		this.legalName = legalName;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(final String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(final String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	public Boolean getRegistered() {
		return registered;
	}

	public void setRegistered(final Boolean registered) {
		this.registered = registered;
	}

	public List<FunderPid> getPids() {
		return pids;
	}

	public void setPids(final List<FunderPid> pids) {
		this.pids = pids;
	}
}
