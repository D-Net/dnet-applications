package eu.dnetlib.openaire.dsm.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.google.common.collect.Lists;

import eu.dnetlib.openaire.exporter.model.dsm.Response;

@JsonAutoDetect
public class DatasourceResponse<T> extends Response {

	private List<T> datasourceInfo = Lists.newArrayList();

	public DatasourceResponse<T> addDatasourceInfo(final T datasourceInfo) {
		getDatasourceInfo().add(datasourceInfo);
		return this;
	}

	public List<T> getDatasourceInfo() {
		return datasourceInfo;
	}

	public DatasourceResponse<T> setDatasourceInfo(final List<T> datasourceInfo) {
		this.datasourceInfo = datasourceInfo;
		return this;
	}

}
