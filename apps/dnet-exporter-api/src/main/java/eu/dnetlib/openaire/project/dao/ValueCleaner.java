package eu.dnetlib.openaire.project.dao;

/**
 * Created by claudio on 23/09/2016.
 */
public interface ValueCleaner {

	String clean(String s);

}
