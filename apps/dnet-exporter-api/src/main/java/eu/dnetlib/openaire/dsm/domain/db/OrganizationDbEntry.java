package eu.dnetlib.openaire.dsm.domain.db;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.dnetlib.enabling.datasources.common.Organization;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 * Created by claudio on 13/04/2017.
 */
@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "dsm_organizations")
public class OrganizationDbEntry extends Organization<DatasourceDbEntry> {

	@Override
	@JsonIgnore
	public Set<DatasourceDbEntry> getDatasources() {
		return super.getDatasources();
	}

}
