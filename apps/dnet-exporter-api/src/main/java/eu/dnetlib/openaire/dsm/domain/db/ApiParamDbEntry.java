package eu.dnetlib.openaire.dsm.domain.db;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import eu.dnetlib.enabling.datasources.common.ApiParam;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Created by claudio on 13/04/2017.
 */
@Entity
@Table(name = "dsm_apiparams")
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "Datasource Api params model", description = "describes the datasource api params")
public class ApiParamDbEntry implements ApiParam {

	@EmbeddedId
	protected ApiParamKeyDbEntry id;

	protected String value;

	public ApiParamDbEntry() {}

	public ApiParam setId(final ApiParamKeyDbEntry id) {
		this.id = id;
		return this;
	}

	@JsonIgnore
	public ApiParamKeyDbEntry getId() {
		return id;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public void setValue(final String value) {
		this.value = value;
	}

	@Override
	public String getParam() {
		return id.getParam();
	}

	@Override
	public void setParam(final String param) {
		final ApiParamKeyDbEntry apk = new ApiParamKeyDbEntry();
		apk.setParam(param);
		setId(apk);
	}

}
