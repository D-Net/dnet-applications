package eu.dnetlib.openaire.community;

import static eu.dnetlib.openaire.common.ExporterConstants.C_CP;
import static eu.dnetlib.openaire.common.ExporterConstants.C_O;
import static eu.dnetlib.openaire.common.ExporterConstants.C_ZC;
import static eu.dnetlib.openaire.common.ExporterConstants.R;

import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.openaire.common.AbstractExporterController;
import eu.dnetlib.openaire.exporter.exceptions.CommunityException;
import eu.dnetlib.openaire.exporter.exceptions.ResourceNotFoundException;
import eu.dnetlib.openaire.exporter.model.community.CommunityOpenAIRECommunities;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@CrossOrigin(origins = {
		"*"
})
@ConditionalOnProperty(value = "openaire.exporter.enable.community", havingValue = "true")
@Tag(name = "OpenAIRE Communities API", description = "the OpenAIRE Community API")
public class OtherCommunityApiController extends AbstractExporterController {

	@Autowired
	private CommunityService communityService;

	// APIs to manage the propagationOrganizationCommunityMap

	@GetMapping("/propagationOrganizationCommunityMap")
	@Operation(summary = "Get the propagationOrganizationCommunityMap", description = "Get the propagationOrganizationCommunityMap", tags = {
			C_O, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public Map<String, Set<String>> getPropagationOrganizationCommunityMap() throws CommunityException {
		try {
			return communityService.getPropagationOrganizationCommunityMap();
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	// APIs to manage the datasourceCommunityMap

	@GetMapping("/datasourceCommunityMap")
	@Operation(summary = "Get the datasourceCommunityMap", description = "Get the datasourceCommunityMap", tags = {
			C_CP, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public Map<String, Set<String>> getDatasourceCommunityMap() throws CommunityException {
		try {
			return communityService.getPropagationOrganizationCommunityMap();
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	// Specific API for ZENODO communities
	@GetMapping("/community/{zenodoId}/openairecommunities")
	@Operation(summary = "get the list of OpenAIRE communities associated to a given Zenodo community", description = "get the list of OpenAIRE communities associated to a given Zenodo community", tags = {
			C_ZC, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityOpenAIRECommunities getOpenAireCommunities(@PathVariable final String zenodoId) throws CommunityException {
		try {
			final CommunityOpenAIRECommunities res = new CommunityOpenAIRECommunities();
			res.setZenodoid(zenodoId);
			res.setOpenAirecommunitylist(communityService.getOpenAIRECommunitiesByZenodoId(zenodoId));
			return res;
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}
}
