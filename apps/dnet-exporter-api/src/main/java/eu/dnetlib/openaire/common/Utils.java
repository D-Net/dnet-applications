package eu.dnetlib.openaire.common;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.google.common.escape.Escaper;
import org.apache.commons.lang3.StringUtils;

public class Utils {

	public static <T> Stream<T> stream(Iterator<T> iterator) {
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED), false);
	}

	public static String escape(final Escaper esc, final String value) {
		return StringUtils.isNotBlank(value) ? esc.escape(value) : "";
	}

}
