package eu.dnetlib.openaire.dsm.domain.db;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import eu.dnetlib.enabling.datasources.common.BrowseTerm;

/**
 * Created by claudio on 20/04/2017.
 */
@Entity
@Table(name = "browse_countries")
@JsonAutoDetect
public class CountryTerm implements Comparable<CountryTerm>, BrowseTerm {

	@Id
	private String term;
	private long total;

	@Override
	public String getTerm() {
		return term;
	}

	@Override
	public void setTerm(final String term) {
		this.term = term;
	}

	@Override
	public long getTotal() {
		return total;
	}

	@Override
	public void setTotal(final long total) {
		this.total = total;
	}

	@Override
	public int compareTo(final CountryTerm o) {
		return getTerm().compareTo(o.getTerm());
	}

}
