package eu.dnetlib.openaire.dsm.domain.db;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import eu.dnetlib.enabling.datasources.common.PidSystem;

@Embeddable
@JsonIgnoreProperties(ignoreUnknown = true)
public class PidSystemKeyDbEntry implements PidSystem, Serializable {

	private static final long serialVersionUID = 1L;

	private String service;
	private String type;
	private String scheme;

	public String getService() {
		return service;
	}

	public void setService(final String service) {
		this.service = service;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(final String type) {
		this.type = type;
	}

	@Override
	public String getScheme() {
		return scheme;
	}

	@Override
	public void setScheme(final String scheme) {
		this.scheme = scheme;
	}

	@Override
	public int hashCode() {
		return Objects.hash(scheme, service, type);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof PidSystemKeyDbEntry)) { return false; }
		final PidSystemKeyDbEntry other = (PidSystemKeyDbEntry) obj;
		return Objects.equals(scheme, other.scheme) && Objects.equals(service, other.service) && Objects.equals(type, other.type);
	}
}
