package eu.dnetlib.openaire.dsm.dao;

import java.util.List;
import java.util.Queue;

import com.google.common.collect.Lists;

import eu.dnetlib.openaire.dsm.domain.DatasourceSearchResponse;
import eu.dnetlib.openaire.exporter.model.dsm.ApiDetails;
import eu.dnetlib.openaire.exporter.model.dsm.ApiDetailsResponse;
import eu.dnetlib.openaire.exporter.model.dsm.DatasourceDetailResponse;
import eu.dnetlib.openaire.exporter.model.dsm.DatasourceDetails;
import eu.dnetlib.openaire.exporter.model.dsm.DatasourceInfo;
import eu.dnetlib.openaire.exporter.model.dsm.DatasourceSnippetExtended;
import eu.dnetlib.openaire.exporter.model.dsm.DatasourceSnippetResponse;
import eu.dnetlib.openaire.exporter.model.dsm.Header;
import eu.dnetlib.openaire.exporter.model.dsm.SimpleResponse;

public class ResponseUtils {

	public static ApiDetailsResponse apiResponse(final List<ApiDetails> api, final long total) {
		final ApiDetailsResponse rsp = new ApiDetailsResponse().setApi(api);
		rsp.setHeader(header(total));
		return rsp;
	}

	public static DatasourceSnippetResponse snippetResponse(final List<DatasourceSnippetExtended> snippets, final long total) {
		final DatasourceSnippetResponse rsp = new DatasourceSnippetResponse(snippets);
		rsp.setHeader(header(total));
		return rsp;
	}

	public static DatasourceDetailResponse detailsResponse(final List<DatasourceDetails> details, final long total) {
		final DatasourceDetailResponse rsp = new DatasourceDetailResponse(details);
		rsp.setHeader(header(total));
		return rsp;
	}

	public static DatasourceSearchResponse searchResponse(final List<DatasourceInfo> infos, final long total) {
		final DatasourceSearchResponse rsp = new DatasourceSearchResponse(infos);
		rsp.setHeader(header(total));
		return rsp;
	}

	public static Header header(final Queue<Throwable> errors, final long total) {
		return Header.newInsance()
			.setExceptions(errors)
			.setTotal(total);
	}

	public static Header header(final long total) {
		return header(Lists.newLinkedList(), total);
	}

	public static <T> SimpleResponse<T> simpleResponse(final List<T> list) {
		final SimpleResponse<T> rsp = new SimpleResponse<T>().setResponse(list);;
		rsp.setHeader(header(Lists.newLinkedList(), list.size()));
		return rsp;
	}

}
