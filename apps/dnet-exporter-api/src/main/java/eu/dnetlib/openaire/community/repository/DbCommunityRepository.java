package eu.dnetlib.openaire.community.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import eu.dnetlib.openaire.community.model.DbCommunity;

@ConditionalOnProperty(value = "openaire.exporter.enable.community", havingValue = "true")
public interface DbCommunityRepository extends JpaRepository<DbCommunity, String> {

	List<DbCommunity> findByParentIsNull();

	@Query(value = "select id from communities where ?1 = ANY(array_append(other_zenodo_communities, main_zenodo_community))", nativeQuery = true)
	List<String> findByZenodoId(String zenodoId);

	// TODO RICONTROLLARE QUERY

	List<DbCommunity> findByIdStartsWith(String prefix);

}
