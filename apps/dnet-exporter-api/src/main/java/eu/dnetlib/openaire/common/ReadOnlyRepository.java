package eu.dnetlib.openaire.common;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

@NoRepositoryBean
public interface ReadOnlyRepository<T, ID> extends Repository<T, ID> {

	Optional<T> findById(ID id);

	boolean existsById(ID id);

	Page<T> findAll(Pageable pageable);

	Iterable<T> findAll();

	long count();
}
