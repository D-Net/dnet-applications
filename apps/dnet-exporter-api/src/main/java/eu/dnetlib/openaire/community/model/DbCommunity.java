package eu.dnetlib.openaire.community.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

import eu.dnetlib.openaire.community.utils.CommunityClaimTypeConverter;
import eu.dnetlib.openaire.community.utils.CommunityMembershipTypeConverter;
import eu.dnetlib.openaire.exporter.model.community.CommunityClaimType;
import eu.dnetlib.openaire.exporter.model.community.CommunityMembershipType;
import eu.dnetlib.openaire.exporter.model.community.CommunityPlanType;
import eu.dnetlib.openaire.exporter.model.community.CommunityStatus;
import eu.dnetlib.openaire.exporter.model.community.selectioncriteria.SelectionCriteria;
import eu.dnetlib.openaire.exporter.model.context.Param;

@Entity
@Table(name = "communities")
@TypeDefs({
		@TypeDef(name = "string-array", typeClass = StringArrayType.class),
		@TypeDef(name = "json", typeClass = JsonStringType.class),
		@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class DbCommunity implements Serializable {

	private static final long serialVersionUID = 4315597783109726539L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "sub_c_parent")
	private String parent;

	@Column(name = "name")
	private String name;

	@Column(name = "shortname")
	private String shortName;

	@Column(name = "displayname")
	private String displayName;

	@Column(name = "displayshortname")
	private String displayShortName;

	@Column(name = "description")
	private String description;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private CommunityStatus status = CommunityStatus.hidden;

	@Column(name = "membership")
	@Convert(converter = CommunityMembershipTypeConverter.class)
	private CommunityMembershipType membership = CommunityMembershipType.byInvitation;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private DbCommunityType type;

	@Column(name = "claim")
	@Convert(converter = CommunityClaimTypeConverter.class)
	private CommunityClaimType claim;

	@Type(type = "string-array")
	@Column(name = "subjects", columnDefinition = "text[]")
	private String[] subjects;

	@Type(type = "string-array")
	@Column(name = "fos", columnDefinition = "text[]")
	private String[] fos;

	@Type(type = "string-array")
	@Column(name = "sdg", columnDefinition = "text[]")
	private String[] sdg;

	@Type(type = "jsonb")
	@Column(name = "adv_constraints")
	private SelectionCriteria advancedConstraints;

	@Type(type = "jsonb")
	@Column(name = "remove_constraints")
	private SelectionCriteria removeConstraints;

	@Column(name = "main_zenodo_community")
	private String mainZenodoCommunity;

	@Type(type = "string-array")
	@Column(name = "other_zenodo_communities", columnDefinition = "text[]")
	private String[] otherZenodoCommunities;

	@CreatedDate
	@Column(name = "creation_date")
	private LocalDateTime creationDate;

	@LastModifiedDate
	@Column(name = "last_update")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private LocalDateTime lastUpdateDate;

	@Column(name = "logo_url")
	private String logoUrl;

	@Type(type = "string-array")
	@Column(name = "suggested_acknowledgements", columnDefinition = "text[]")
	private String[] suggestedAcknowledgements;

	@Column(name = "plan")
	@Enumerated(EnumType.STRING)
	private CommunityPlanType plan;

	@Column(name = "featured")
	private Boolean featured;

	@Type(type = "jsonb")
	@Column(name = "sub_c_params")
	private List<Param> params = new ArrayList<>();

	@Column(name = "sub_c_category")
	private String category;

	@Column(name = "sub_c_claimable")
	private Boolean claimable;

	@Column(name = "sub_c_browsable")
	private Boolean browsable;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(final String parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayShortName() {
		return displayShortName;
	}

	public void setDisplayShortName(final String displayShortName) {
		this.displayShortName = displayShortName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public CommunityStatus getStatus() {
		return status;
	}

	public void setStatus(final CommunityStatus status) {
		this.status = status;
	}

	public CommunityMembershipType getMembership() {
		return membership;
	}

	public void setMembership(final CommunityMembershipType membership) {
		this.membership = membership;
	}

	public DbCommunityType getType() {
		return type;
	}

	public void setType(final DbCommunityType type) {
		this.type = type;
	}

	public CommunityClaimType getClaim() {
		return claim;
	}

	public void setClaim(final CommunityClaimType claim) {
		this.claim = claim;
	}

	public String[] getSubjects() {
		return subjects;
	}

	public void setSubjects(final String[] subjects) {
		this.subjects = subjects;
	}

	public String[] getFos() {
		return fos;
	}

	public void setFos(final String[] fos) {
		this.fos = fos;
	}

	public String[] getSdg() {
		return sdg;
	}

	public void setSdg(final String[] sdg) {
		this.sdg = sdg;
	}

	public SelectionCriteria getAdvancedConstraints() {
		return advancedConstraints;
	}

	public void setAdvancedConstraints(final SelectionCriteria advancedConstraints) {
		this.advancedConstraints = advancedConstraints;
	}

	public SelectionCriteria getRemoveConstraints() {
		return removeConstraints;
	}

	public void setRemoveConstraints(final SelectionCriteria removeConstraints) {
		this.removeConstraints = removeConstraints;
	}

	public String getMainZenodoCommunity() {
		return mainZenodoCommunity;
	}

	public void setMainZenodoCommunity(final String mainZenodoCommunity) {
		this.mainZenodoCommunity = mainZenodoCommunity;
	}

	public String[] getOtherZenodoCommunities() {
		return otherZenodoCommunities;
	}

	public void setOtherZenodoCommunities(final String[] otherZenodoCommunities) {
		this.otherZenodoCommunities = otherZenodoCommunities;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(final LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(final LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(final String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String[] getSuggestedAcknowledgements() {
		return suggestedAcknowledgements;
	}

	public void setSuggestedAcknowledgements(final String[] suggestedAcknowledgements) {
		this.suggestedAcknowledgements = suggestedAcknowledgements;
	}

	public CommunityPlanType getPlan() {
		return plan;
	}

	public void setPlan(final CommunityPlanType plan) {
		this.plan = plan;
	}

	public Boolean getFeatured() {
		return featured;
	}

	public void setFeatured(final Boolean featured) {
		this.featured = featured;
	}

	public List<Param> getParams() {
		return params;
	}

	public void setParams(final List<Param> params) {
		this.params = params;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(final String category) {
		this.category = category;
	}

	public Boolean getClaimable() {
		return claimable;
	}

	public void setClaimable(final Boolean claimable) {
		this.claimable = claimable;
	}

	public Boolean getBrowsable() {
		return browsable;
	}

	public void setBrowsable(final Boolean browsable) {
		this.browsable = browsable;
	}

}
