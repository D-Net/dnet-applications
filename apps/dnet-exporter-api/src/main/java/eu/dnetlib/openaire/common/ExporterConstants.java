package eu.dnetlib.openaire.common;

public class ExporterConstants {

	/*
	 * Tags used to group the operations on the swagger UI
	 */
	public final static String C = "Community";
	public final static String C_CP = "Community content providers";
	public final static String C_PJ = "Community projects";
	public final static String C_ZC = "Community Zenodo Communities";
	public final static String C_O = "Community Organizations";
	public final static String C_SUB = "Subcommunities";

	public final static String DS = "Datasource";
	public final static String API = "Interface";
	public final static String R = "Read";
	public final static String W = "Write";

	public final static String D = "Deprecated";
	public final static String M = "Management";

	public final static String DSPACE = "DSpace";
	public final static String EPRINT = "EPrints";
	public final static String TSV = "TSV";
	public final static String STREAMING = "Streaming";

	public static final String OAI = "oai";
	public static final String SET = "set";

}
