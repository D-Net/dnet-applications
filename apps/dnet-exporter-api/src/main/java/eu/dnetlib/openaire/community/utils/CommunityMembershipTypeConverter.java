package eu.dnetlib.openaire.community.utils;

import javax.persistence.AttributeConverter;

import org.apache.commons.lang3.StringUtils;

import eu.dnetlib.openaire.exporter.model.community.CommunityMembershipType;

public class CommunityMembershipTypeConverter implements AttributeConverter<CommunityMembershipType, String> {

	@Override
	public String convertToDatabaseColumn(final CommunityMembershipType attribute) {
		if (attribute == null) {
			return null;
		} else {
			return attribute.getDescription();
		}
	}

	@Override
	public CommunityMembershipType convertToEntityAttribute(final String dbData) {
		if (StringUtils.isBlank(dbData)) {
			return null;
		} else {
			return CommunityMembershipType.fromDescription(dbData);
		}
	}

}
