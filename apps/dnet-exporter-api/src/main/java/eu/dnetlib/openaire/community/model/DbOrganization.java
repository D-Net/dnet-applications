package eu.dnetlib.openaire.community.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "community_orgs")
@IdClass(DbOrganizationPK.class)
public class DbOrganization implements Serializable {

	private static final long serialVersionUID = -602114117980437763L;

	@Id
	@Column(name = "community")
	private String community;

	@Id
	@Column(name = "org_id")
	private String orgId;

	public DbOrganization() {}

	public DbOrganization(final String community, final String orgId) {
		this.community = community;
		this.orgId = orgId;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(final String community) {
		this.community = community;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	@Override
	public String toString() {
		return String.format("DbOrganization [community=%s, orgId=%s]", community, orgId);
	}

}
