package eu.dnetlib.openaire.community.importer;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Sets;

import eu.dnetlib.common.controller.AbstractDnetController;
import eu.dnetlib.openaire.common.ISClient;
import eu.dnetlib.openaire.community.model.DbOrganization;
import eu.dnetlib.openaire.exporter.exceptions.CommunityException;
import eu.dnetlib.openaire.exporter.model.context.Context;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@CrossOrigin(origins = {
	"*"
})
@ConditionalOnProperty(value = "openaire.exporter.enable.community.import", havingValue = "true")
@Tag(name = "OpenAIRE Communities: Migration API", description = "OpenAIRE Communities: Migration API")
public class CommunityImporterController extends AbstractDnetController {

	// public final static Set<String> communityBlackList = Sets.newHashSet("fet-fp7", "fet-h2020");
	public final static Set<String> communityBlackList = Sets.newHashSet();

	@Autowired
	private CommunityImporterService importer;

	@Autowired
	private ISClient isClient;

	private static final Log log = LogFactory.getLog(CommunityImporterController.class);

	@GetMapping("/community_importer/communities")
	public List<String> importProfiles() throws CommunityException {
		try {
			final Map<String, Context> contextMap = getContextMap();

			final List<String> list = contextMap.keySet()
				.stream()
				.filter(id -> !communityBlackList.contains(id))
				.collect(Collectors.toList());

			list.forEach(id -> {
				importer.importCommunity(contextMap.get(id));
			});

			return list;
		} catch (final Throwable e) {
			log.error("Error importing communities", e);
			throw new CommunityException(e.getMessage());
		}
	}

	@GetMapping("/community_importer/propagationOrgs")
	public List<DbOrganization> importPropagationOrgs(@RequestParam final String profileId,
		@RequestParam(required = false, defaultValue = "false") final boolean simulation) throws Exception {
		try {
			final String xml = isClient.getProfile(profileId);
			return importer.importPropagationOrganizationsFromProfile(xml, simulation);
		} catch (final Throwable e) {
			log.error("Error importing communities", e);
			throw new CommunityException(e.getMessage());
		}
	}

	private Map<String, Context> getContextMap() throws CommunityException {
		try {
			return isClient.getCommunityContextMap();
		} catch (final IOException e) {
			throw new CommunityException(e);
		}
	}

}
