package eu.dnetlib.openaire.dsm.domain.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import eu.dnetlib.enabling.datasources.common.Api;

/**
 * Api
 */
@Entity
@Table(name = "dsm_api")
public class ApiDbEntry extends Api<ApiParamDbEntry> {

	@Column(name = "compatibility_override")
	protected String compatibilityOverride;

	public String getCompatibilityOverride() {
		return compatibilityOverride;
	}

	public Api<ApiParamDbEntry> setCompatibilityOverride(final String compatibilityOverride) {
		this.compatibilityOverride = compatibilityOverride;
		return this;
	}

}
