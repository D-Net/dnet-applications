package eu.dnetlib.openaire.project.domain.db;

import java.sql.Date;
import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Created by claudio on 20/09/16.
 */
@Entity
@Table(name = "projects_api")
@Schema(name = "Project api model", description = "Project api model used by DSpace and Eprints exporter")
public class ProjectApi {

	public static final String INFO_EU_REPO_GRANT_AGREEMENT = "info:eu-repo/grantAgreement/";

	@Id
	@JsonIgnore
	private String id;

	private String code;
	private String acronym;
	private String title;
	private String funder;
	private String jurisdiction;
	private Date startdate;
	private Date enddate;
	private String fundingpathid;

	public ProjectApi() {}

	public String getIdnamespace() {
		String res = INFO_EU_REPO_GRANT_AGREEMENT + getFunder() + "/";
		final String fundingProgram = asFundingProgram(getFundingpathid());
		if (StringUtils.isNotBlank(fundingProgram)) {
			res += fundingProgram;
		}
		res += "/" + escapeCode(getCode());
		if (StringUtils.isNotBlank(getJurisdiction())) {
			res += "/" + getJurisdiction();
		}
		return res;
	}

	public String getListLabel() {
		return String.format("for:value:component:_%s_project_id", asFunder(getFunder()));
	}

	private String asFunder(final String legalshortname) {
		switch (legalshortname.toLowerCase()) {
		case "ec":
			return asFundingProgram(getFundingpathid()).toLowerCase();
		default:
			return legalshortname.toLowerCase();
		}
	}

	private String escapeCode(final String code) {
		return replaceSlash(code);
	}

	private String asFundingProgram(final String fundingpathid) {
		final ArrayList<String> strings = Lists.newArrayList(Splitter.on("::").split(fundingpathid));
		if (strings.size() <= 1) { throw new IllegalStateException("Unexpected funding id: " + fundingpathid); }
		if (strings.size() == 2) {
			return "";
		} else {
			return replaceSlash(strings.get(2));
		}
	}

	private String replaceSlash(final String s) {
		return s.replaceAll("/", "%2F");
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(final String acronym) {
		this.acronym = acronym;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getFunder() {
		return funder;
	}

	public void setFunder(final String funder) {
		this.funder = funder;
	}

	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(final String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(final Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(final Date enddate) {
		this.enddate = enddate;
	}

	public String getFundingpathid() {
		return fundingpathid;
	}

	public void setFundingpathid(final String fundingpathid) {
		this.fundingpathid = fundingpathid;
	}
}
