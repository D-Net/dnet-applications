package eu.dnetlib.openaire.community.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.openaire.community.model.DbOrganization;
import eu.dnetlib.openaire.community.model.DbOrganizationPK;

@ConditionalOnProperty(value = "openaire.exporter.enable.community", havingValue = "true")
public interface DbOrganizationRepository extends JpaRepository<DbOrganization, DbOrganizationPK> {

	List<DbOrganization> findByCommunity(String community);

	void deleteByCommunity(String id);

}
