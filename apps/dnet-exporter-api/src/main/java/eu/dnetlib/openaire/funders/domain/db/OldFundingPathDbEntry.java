package eu.dnetlib.openaire.funders.domain.db;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

// @Entity
// @Table(name = "fundingpaths")
@Deprecated
public class OldFundingPathDbEntry {

	@JsonIgnore
	@Transient
	@Column(name = "_dnet_resource_identifier_")
	private String dnetresourceidentifier;

	@Id
	private String id;
	private String path;

	@Column(name = "funder")
	@Transient
	@JsonIgnore
	private String orgId;
	private String jurisdiction;
	private String description;
	private String optional1;
	private String optional2;

	private String funderid;

	public OldFundingPathDbEntry() {}

	public String getDnetresourceidentifier() {
		return dnetresourceidentifier;
	}

	public void setDnetresourceidentifier(final String dnetresourceidentifier) {
		this.dnetresourceidentifier = dnetresourceidentifier;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(final String path) {
		this.path = path;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(final String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getOptional1() {
		return optional1;
	}

	public void setOptional1(final String optional1) {
		this.optional1 = optional1;
	}

	public String getOptional2() {
		return optional2;
	}

	public void setOptional2(final String optional2) {
		this.optional2 = optional2;
	}

	public String getFunderid() {
		return funderid;
	}

	public void setFunderid(final String funderid) {
		this.funderid = funderid;
	}
}
