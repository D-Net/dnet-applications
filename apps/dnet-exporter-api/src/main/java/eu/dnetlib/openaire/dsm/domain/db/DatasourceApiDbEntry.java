package eu.dnetlib.openaire.dsm.domain.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;

@Entity
@JsonAutoDetect
@Table(name = "dsm_datasource_api")
@Schema(name = "DatasourceApi model", description = "describes a joint view between datasources and their API (1:N)")
public class DatasourceApiDbEntry {

	@Id
	@JsonIgnore
	private Long rowid;

	private String id;
	private String officialname;
	private String englishname;
	private String websiteurl;
	private String contactemail;
	private String collectedfrom;
	private String registeredby;
	@Column(name = "eosc_datasource_type")
	private String eoscDatasourceType;
	private String platform;
	private Boolean managed;

	protected String protocol = null;
	protected String contentdescription = null;
	protected Boolean active = false;
	protected Boolean removable = false;

	protected String compatibility;
	private String baseurl;

	public DatasourceApiDbEntry() {}

	public Long getRowid() {
		return rowid;
	}

	public void setRowid(final Long rowid) {
		this.rowid = rowid;
	}

	public String getBaseurl() {
		return baseurl;
	}

	public void setBaseurl(final String baseurl) {
		this.baseurl = baseurl;
	}

	public String getContactemail() {
		return contactemail;
	}

	public void setContactemail(final String contactemail) {
		this.contactemail = contactemail;
	}

	public String getRegisteredby() {
		return registeredby;
	}

	public void setRegisteredby(final String registeredby) {
		this.registeredby = registeredby;
	}

	public String getOfficialname() {
		return officialname;
	}

	public void setOfficialname(final String officialname) {
		this.officialname = officialname;
	}

	public String getEnglishname() {
		return englishname;
	}

	public void setEnglishname(final String englishname) {
		this.englishname = englishname;
	}

	public String getWebsiteurl() {
		return websiteurl;
	}

	public void setWebsiteurl(final String websiteurl) {
		this.websiteurl = websiteurl;
	}

	public String getEoscDatasourceType() {
		return eoscDatasourceType;
	}

	public void setEoscDatasourceType(final String eoscDatasourceType) {
		this.eoscDatasourceType = eoscDatasourceType;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(final String platform) {
		this.platform = platform;
	}

	public Boolean getManaged() {
		return managed;
	}

	public void setManaged(final Boolean managed) {
		this.managed = managed;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(final String protocol) {
		this.protocol = protocol;
	}

	public String getContentdescription() {
		return contentdescription;
	}

	public void setContentdescription(final String contentdescription) {
		this.contentdescription = contentdescription;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(final Boolean active) {
		this.active = active;
	}

	public Boolean getRemovable() {
		return removable;
	}

	public void setRemovable(final Boolean removable) {
		this.removable = removable;
	}

	public String getCompatibility() {
		return compatibility;
	}

	public void setCompatibility(final String compatibility) {
		this.compatibility = compatibility;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getCollectedfrom() {
		return collectedfrom;
	}

	public void setCollectedfrom(final String collectedfrom) {
		this.collectedfrom = collectedfrom;
	}

}
