package eu.dnetlib.openaire.dsm.domain.db;

import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import eu.dnetlib.enabling.datasources.common.ApiParamKey;

/**
 * Created by claudio on 13/04/2017.
 */
@Embeddable
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiParamKeyDbEntry extends ApiParamKey<ApiDbEntry> {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Override
	@JsonIgnore
	public ApiDbEntry getApi() {
		return super.getApi();
	}

}
