package eu.dnetlib.openaire.project.dao;

import eu.dnetlib.openaire.project.domain.db.ProjectTsv;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by claudio on 04/07/2017.
 */
@Repository
@ConditionalOnProperty(value = "openaire.exporter.enable.project", havingValue = "true")
public interface ProjectTsvRepository extends PagingAndSortingRepository<ProjectTsv, String> {

	Iterable<ProjectTsv> findByFundingpathidStartingWithOrderByAcronym(String fundingpathid);

	Iterable<ProjectTsv> findByFundingpathidStartingWithAndOaMandateForDatasetsOrderByAcronym(String fundingpathid, boolean article293);

}
