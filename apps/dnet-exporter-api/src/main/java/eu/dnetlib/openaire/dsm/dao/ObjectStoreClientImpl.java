package eu.dnetlib.openaire.dsm.dao;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import eu.dnetlib.data.objectstore.rmi.ObjectStoreService;
import eu.dnetlib.data.objectstore.rmi.ObjectStoreServiceException;
import eu.dnetlib.openaire.exporter.exceptions.DsmApiException;

@Component
@ConditionalOnProperty(value = "openaire.exporter.enable.dsm", havingValue = "true")
public class ObjectStoreClientImpl implements ObjectStoreClient {

	private static final Log log = LogFactory.getLog(ObjectStoreClientImpl.class);

	@Autowired
	private ObjectStoreService objectStoreService;

	@Override
	public Long getObjectStoreSize(final String objectStoreId) throws DsmApiException {
		log.debug("get size for objectStore " + objectStoreId);
		if (StringUtils.isBlank(objectStoreId)) { return 0L; }
		try {
			final long size = objectStoreService.getSize(objectStoreId);
			log.debug("got objectStore size: " + size);
			return size;
		} catch (final ObjectStoreServiceException e) {
			throw new DsmApiException("unable to get size for objectStore " + objectStoreId);
		}
	}

}
