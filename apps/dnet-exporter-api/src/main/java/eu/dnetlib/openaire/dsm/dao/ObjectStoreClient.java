package eu.dnetlib.openaire.dsm.dao;

import eu.dnetlib.openaire.exporter.exceptions.DsmApiException;

public interface ObjectStoreClient {

	Long getObjectStoreSize(final String objectStoreId) throws DsmApiException;

}
