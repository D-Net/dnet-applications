package eu.dnetlib.openaire.dsm.dao;

import eu.dnetlib.openaire.dsm.domain.db.DatasourceApiDbEntry;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
@ConditionalOnProperty(value = "openaire.exporter.enable.dsm", havingValue = "true")
public interface DatasourceApiDbEntryRepository extends JpaRepository<DatasourceApiDbEntry, String>, JpaSpecificationExecutor<DatasourceApiDbEntry> {


}
