package eu.dnetlib.openaire.dsm;

import static eu.dnetlib.openaire.common.ExporterConstants.API;
import static eu.dnetlib.openaire.common.ExporterConstants.DS;
import static eu.dnetlib.openaire.common.ExporterConstants.M;
import static eu.dnetlib.openaire.common.ExporterConstants.R;
import static eu.dnetlib.openaire.common.ExporterConstants.W;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.openaire.common.AbstractExporterController;
import eu.dnetlib.openaire.common.OperationManager;
import eu.dnetlib.openaire.exporter.exceptions.DsmApiException;
import eu.dnetlib.openaire.exporter.model.dsm.AggregationHistoryResponseV1;
import eu.dnetlib.openaire.exporter.model.dsm.ApiDetails;
import eu.dnetlib.openaire.exporter.model.dsm.ApiDetailsResponse;
import eu.dnetlib.openaire.exporter.model.dsm.DatasourceDetailResponse;
import eu.dnetlib.openaire.exporter.model.dsm.DatasourceDetails;
import eu.dnetlib.openaire.exporter.model.dsm.DatasourceDetailsUpdate;
import eu.dnetlib.openaire.exporter.model.dsm.DatasourceDetailsWithApis;
import eu.dnetlib.openaire.exporter.model.dsm.DatasourceSnippetResponse;
import eu.dnetlib.openaire.exporter.model.dsm.RegisteredDatasourceInfo;
import eu.dnetlib.openaire.exporter.model.dsm.RequestFilter;
import eu.dnetlib.openaire.exporter.model.dsm.RequestSort;
import eu.dnetlib.openaire.exporter.model.dsm.RequestSortOrder;
import eu.dnetlib.openaire.exporter.model.dsm.SimpleResponse;
import eu.dnetlib.openaire.exporter.model.vocabularies.Country;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@CrossOrigin(origins = {
	"*"
})
@ConditionalOnProperty(value = "openaire.exporter.enable.dsm", havingValue = "true")
@Tag(name = "OpenAIRE DSM API", description = "the OpenAIRE Datasource Manager API")
public class DsmApiController extends AbstractExporterController {

	@Autowired
	private DsmCore dsmCore;

	@GetMapping(value = "/ds/countries", produces = {
		"application/json"
	})
	@Operation(summary = "list the datasource countries", description = "list the datasource countries", tags = {
		DS, R
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public List<Country> listCountries() throws DsmApiException {
		return dsmCore.listCountries();
	}

	@PostMapping(value = "/ds/searchdetails/{page}/{size}", produces = {
		"application/json"
	})
	@Operation(summary = "search datasources", description = "Returns list of Datasource details.", tags = {
		DS, R
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public DatasourceDetailResponse searchDsDetails(
		@RequestParam final RequestSort requestSortBy,
		@RequestParam final RequestSortOrder order,
		@RequestBody final RequestFilter requestFilter,
		@PathVariable final int page,
		@PathVariable final int size) throws DsmApiException {
		final StopWatch stop = StopWatch.createStarted();
		final DatasourceDetailResponse rsp = dsmCore.searchDsDetails(requestSortBy, order, requestFilter, page, size);
		return prepareResponse(page, size, stop, rsp);
	}

	@GetMapping(value = "/ds/aggregationhistory/{dsId}", produces = {
		"application/json"
	})
	@Operation(summary = "return aggregation hitory", description = "Returns the aggregation history of a datasource", tags = {
		DS, R
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public AggregationHistoryResponseV1 aggregationHistory(@PathVariable final String dsId) throws DsmApiException {
		final StopWatch stop = StopWatch.createStarted();
		final AggregationHistoryResponseV1 rsp = dsmCore.aggregationhistoryV1(dsId);
		return prepareResponse(0, rsp.getAggregationInfo().size(), stop, rsp);
	}

	@PostMapping(value = "/ds/searchsnippet/{page}/{size}", produces = {
		"application/json"
	})
	@Operation(summary = "search datasources", description = "Returns list of Datasource basic info.", tags = {
		DS, R
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public DatasourceSnippetResponse searchSnippet(
		@RequestParam final RequestSort requestSortBy,
		@RequestParam final RequestSortOrder order,
		@RequestBody final RequestFilter requestFilter,
		@PathVariable final int page,
		@PathVariable final int size) throws DsmApiException {
		final StopWatch stop = StopWatch.createStarted();
		final DatasourceSnippetResponse rsp = dsmCore.searchSnippet(requestSortBy, order, requestFilter, page, size);
		return prepareResponse(page, size, stop, rsp);
	}

	@PostMapping(value = "/ds/searchregistered/{page}/{size}", produces = {
		"application/json"
	})
	@Operation(summary = "search among registered datasources", description = "Returns list of Datasource basic info.", tags = {
		DS,
		R
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public DatasourceSnippetResponse searchRegistered(
		@RequestParam final RequestSort requestSortBy,
		@RequestParam final RequestSortOrder order,
		@RequestBody final RequestFilter requestFilter,
		@PathVariable final int page,
		@PathVariable final int size) throws DsmApiException {
		final StopWatch stop = StopWatch.createStarted();
		final DatasourceSnippetResponse rsp = dsmCore.searchRegistered(requestSortBy, order, requestFilter, page, size);
		return prepareResponse(page, size, stop, rsp);
	}

	@GetMapping(value = "/ds/recentregistered/{size}", produces = {
		"application/json"
	})
	@Operation(summary = "return the latest datasources that were registered through Provide", description = "Returns list of Datasource basic info.", tags = {
		DS,
		R
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SimpleResponse<RegisteredDatasourceInfo> recentRegistered(@PathVariable final int size) throws Throwable {
		final StopWatch stop = StopWatch.createStarted();
		final SimpleResponse<RegisteredDatasourceInfo> rsp = dsmCore.searchRecentRegistered(size);
		return prepareResponse(1, size, stop, rsp);
	}

	@GetMapping(value = "/ds/countregistered", produces = {
		"application/json"
	})
	@Operation(summary = "return the number of datasources registered after the given date", description = "Returns a number.", tags = {
		DS,
		R
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public Long countRegistered(@RequestParam final String fromDate,
		@RequestParam(required = false) final String typologyFilter) throws Throwable {
		return dsmCore.countRegisteredAfter(fromDate, typologyFilter);
	}

	@GetMapping(value = "/ds/api/{dsId}", produces = {
		"application/json"
	})
	@Operation(summary = "get the list of API for a given datasource", description = "Returns the list of API for a given datasource.", tags = {
		API,
		R
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public ApiDetailsResponse getApi(
		@PathVariable final String dsId) throws DsmApiException {

		final StopWatch stop = StopWatch.createStarted();
		final ApiDetailsResponse rsp = dsmCore.getApis(dsId);
		return prepareResponse(0, rsp.getApi().size(), stop, rsp);
	}

	@PostMapping(value = "/api/baseurl/{page}/{size}", produces = {
		"application/json"
	})
	@Operation(summary = "search for the list of base URLs of Datasource APIs managed by a user", description = "Returns the list of base URLs of Datasource APIs managed by a user", tags = {
		DS, API, R
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public List<String> searchBaseUrls(
		@RequestBody final RequestFilter requestFilter,
		@PathVariable final int page,
		@PathVariable final int size) throws DsmApiException {

		return dsmCore.findBaseURLs(requestFilter, page, size);
	}

	@DeleteMapping(value = "/ds/api/{apiId}")
	@Operation(summary = "delete an API", description = "delete an API, if removable", tags = {
		API, W
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "400", description = "Api not found"),
		@ApiResponse(responseCode = "403", description = "Api not removable"),
		@ApiResponse(responseCode = "500", description = "DSM Server error")
	})
	public void deleteApi(@PathVariable final String apiId) throws DsmApiException {
		dsmCore.deleteApi(apiId);
	}

	@PostMapping(value = "/ds/manage")
	@Operation(summary = "set the managed status for a given datasource", description = "set the managed status for a given datasource", tags = {
		DS, W
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void setManaged(
		@RequestParam final String id,
		@RequestParam final boolean managed) throws DsmApiException {

		dsmCore.setManaged(id, managed);
	}

	@GetMapping(value = "/ds/managed/{id}")
	@Operation(summary = "get the datasource managed status", description = "get the datasource managed status", tags = {
		DS, R
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public boolean isManaged(@PathVariable final String id) throws DsmApiException {
		return dsmCore.isManaged(id);
	}

	@PostMapping(value = "/ds/add")
	@Operation(summary = "add a new Datasource", description = "add a new Datasource", tags = {
		DS, W
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "400", description = "Malformed request"),
		@ApiResponse(responseCode = "500", description = "Unexpected error")
	})
	public void saveDs(@Valid @RequestBody final DatasourceDetails datasource) throws DsmApiException {

		if (dsmCore.exist(datasource)) { // TODO further check that the DS doesn't have any API
			throw new DsmApiException(HttpStatus.SC_CONFLICT, String.format("cannot register, datasource already defined '%s'", datasource.getId()));
		}
		dsmCore.save(datasource);
	}

	@PostMapping(value = "/ds/addWithApis")
	@Operation(summary = "add a new Datasource and its apis", description = "add a new Datasource and its apis", tags = {
		DS, W
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "400", description = "Malformed request"),
		@ApiResponse(responseCode = "500", description = "Unexpected error")
	})
	public void saveDsWithApis(@Valid @RequestBody final DatasourceDetailsWithApis d) throws DsmApiException {
		if (d.getDatasource() == null) { throw new DsmApiException(HttpStatus.SC_BAD_REQUEST, "Datasource field is null"); }
		if (dsmCore.exist(d.getDatasource())) { // TODO further check that the DS doesn't have any API
			throw new DsmApiException(HttpStatus.SC_CONFLICT, String.format("cannot register, datasource already defined '%s'", d.getDatasource().getId()));
		}
		dsmCore.save(d);
	}

	@PostMapping(value = "/ds/update")
	@Operation(summary = "update Datasource details", description = "update Datasource details", tags = {
		DS, W
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void updateDatasource(
		@RequestBody final DatasourceDetailsUpdate ds) throws DsmApiException {

		dsmCore.updateDatasource(ds);
	}

	@PostMapping(value = "/ds/api/baseurl")
	@Operation(summary = "update the base URL of a datasource interface", description = "update the base URL of a datasource interface", tags = {
		API, W
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void updateBaseUrl(
		@RequestParam final String dsId,
		@RequestParam final String apiId,
		@RequestParam final String baseUrl) throws DsmApiException {

		dsmCore.updateApiBaseurl(dsId, apiId, baseUrl);
	}

	@PostMapping(value = "/ds/api/compliance")
	@Operation(summary = "update the compatibility of a datasource interface", description = "update the compatibility of a datasource interface", tags = {
		API, W
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void updateCompliance(
		@RequestParam final String dsId,
		@RequestParam final String apiId,
		@RequestParam final String compliance,
		@RequestParam(required = false, defaultValue = "false") final boolean override) throws DsmApiException {

		dsmCore.updateApiCompatibility(dsId, apiId, compliance, override);
	}

	@PostMapping(value = "/ds/api/oaiset")
	@Operation(summary = "update the OAI set of a datasource interface", description = "update the OAI set of a datasource interface", tags = {
		API, W
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void updateOaiSetl(
		@RequestParam final String dsId,
		@RequestParam final String apiId,
		@RequestParam final String oaiSet) throws DsmApiException {

		dsmCore.updateApiOaiSet(dsId, apiId, oaiSet);
	}

	@PostMapping(value = "/ds/api/add")
	@Operation(summary = "adds a new Interface to one Datasource", description = "adds an Interface to one Datasource", tags = {
		API, W
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void addApi(@RequestBody final ApiDetails api) throws DsmApiException {
		if (StringUtils.isBlank(api.getDatasource())) { throw new DsmApiException(HttpStatus.SC_BAD_REQUEST, "missing datasource id"); }
		dsmCore.addApi(api);
	}

	// MANAGEMENT

	@Autowired
	private OperationManager operationManager;

	@GetMapping(value = "/dsm/ops")
	@Operation(summary = "get the number of pending operations", description = "get the number of pending operations", tags = {
		R, M
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public int getOps() throws DsmApiException {
		return operationManager.getOpSize();
	}

	@PostMapping(value = "/dsm/killops")
	@Operation(summary = "interrupts the pending operations", description = "return the number of interrupted operations", tags = {
		W, M
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public int killOps() throws DsmApiException {
		return operationManager.dropAll();
	}

	@PostMapping(value = "/dsm/dropcache")
	@Operation(summary = "drop the caches", description = "drop the internal caches", tags = {
		W, M
	})
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "OK"),
		@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void dropCache() throws DsmApiException {
		dsmCore.dropCaches();
	}

}
