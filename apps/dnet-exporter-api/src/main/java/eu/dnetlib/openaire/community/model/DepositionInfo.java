package eu.dnetlib.openaire.community.model;

import java.io.Serializable;

public class DepositionInfo implements Serializable {

	private static final long serialVersionUID = 7538663287451167904L;

	private String openaireId;

	private Boolean deposit;

	private String message;

	public String getOpenaireId() {
		return openaireId;
	}

	public void setOpenaireId(final String openaireId) {
		this.openaireId = openaireId;
	}

	public Boolean getDeposit() {
		return deposit;
	}

	public void setDeposit(final Boolean deposit) {
		this.deposit = deposit;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

}
