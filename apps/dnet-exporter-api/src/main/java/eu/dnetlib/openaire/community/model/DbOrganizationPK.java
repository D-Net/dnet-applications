package eu.dnetlib.openaire.community.model;

import java.io.Serializable;
import java.util.Objects;

public class DbOrganizationPK implements Serializable {

	private static final long serialVersionUID = -6720182815397534837L;

	private String community;

	private String orgId;

	public DbOrganizationPK() {}

	public DbOrganizationPK(final String community, final String orgId) {
		this.community = community;
		this.orgId = orgId;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(final String community) {
		this.community = community;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(community, orgId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof DbDatasourcePK)) { return false; }
		final DbOrganizationPK other = (DbOrganizationPK) obj;
		return Objects.equals(community, other.community) && Objects.equals(orgId, other.orgId);
	}

	@Override
	public String toString() {
		return String.format("CommunityOrgPK [community=%s, orgId=%s]", community, orgId);
	}
}
