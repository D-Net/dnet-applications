package eu.dnetlib.openaire.dsm.domain.db;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import eu.dnetlib.enabling.datasources.common.PidSystem;

/**
 * Created by claudio on 13/04/2017.
 */

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "dsm_pid_systems")
public class PidSystemDbEntry implements PidSystem {

	@EmbeddedId
	protected PidSystemKeyDbEntry id;

	public PidSystem setId(final PidSystemKeyDbEntry id) {
		this.id = id;
		return this;
	}

	@JsonIgnore
	public PidSystemKeyDbEntry getId() {
		return id;
	}

	@Override
	public String getType() {
		return id.getType();
	}

	@Override
	public void setType(final String type) {
		id.setType(type);
	}

	@Override
	public String getScheme() {
		return id.getScheme();
	}

	@Override
	public void setScheme(final String scheme) {
		id.setScheme(scheme);
	}

}
