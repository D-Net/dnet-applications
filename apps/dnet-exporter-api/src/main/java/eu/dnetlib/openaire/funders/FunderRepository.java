package eu.dnetlib.openaire.funders;

import org.springframework.stereotype.Repository;

import eu.dnetlib.openaire.common.ReadOnlyRepository;
import eu.dnetlib.openaire.funders.domain.db.FunderDbEntry;

@Repository
public interface FunderRepository extends ReadOnlyRepository<FunderDbEntry, String> {

}
