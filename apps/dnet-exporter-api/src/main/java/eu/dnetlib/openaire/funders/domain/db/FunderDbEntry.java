package eu.dnetlib.openaire.funders.domain.db;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vladmihalcea.hibernate.type.array.DateArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;

@Entity
@Table(name = "funders_view")
@TypeDefs({
		@TypeDef(name = "string-array", typeClass = StringArrayType.class),
		@TypeDef(name = "date-array", typeClass = DateArrayType.class),
})
public class FunderDbEntry implements Serializable {

	private static final long serialVersionUID = 1290088460508203016L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "legalshortname")
	private String legalShortName;

	@Column(name = "legalname")
	private String legalName;

	@Column(name = "websiteurl")
	private String websiteUrl;

	@Column(name = "logourl")
	private String logoUrl;

	@Column(name = "country")
	private String country;

	@Column(name = "registrationdate")
	private Date registrationDate;

	@Column(name = "registered")
	private Boolean registered;

	@JsonIgnore
	@Type(type = "string-array")
	@Column(name = "pids", columnDefinition = "text[]")
	private String[] pidsPostgres;

	@Transient
	private List<FunderPid> pids = new ArrayList<FunderPid>();

	@JsonIgnore
	@Type(type = "string-array")
	@Column(name = "datasources", columnDefinition = "text[]")
	private String[] datasourcesPostgres;

	@Transient
	private List<FunderDatasource> datasources = new ArrayList<FunderDatasource>();

	@Type(type = "date-array")
	@Column(name = "aggregationdates", columnDefinition = "date[]")
	private Date[] aggregationDates;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getLegalShortName() {
		return legalShortName;
	}

	public void setLegalShortName(final String legalShortName) {
		this.legalShortName = legalShortName;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(final String legalName) {
		this.legalName = legalName;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(final String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(final String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(final Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Boolean getRegistered() {
		return registered;
	}

	public void setRegistered(final Boolean registered) {
		this.registered = registered;
	}

	public String[] getPidsPostgres() {
		return pidsPostgres;
	}

	public void setPidsPostgres(final String[] pidsPostgres) {
		this.pidsPostgres = pidsPostgres;
	}

	public List<FunderPid> getPids() {
		return pids;
	}

	public void setPids(final List<FunderPid> pids) {
		this.pids = pids;
	}

	public String[] getDatasourcesPostgres() {
		return datasourcesPostgres;
	}

	public void setDatasourcesPostgres(final String[] datasourcesPostgres) {
		this.datasourcesPostgres = datasourcesPostgres;
	}

	public List<FunderDatasource> getDatasources() {
		return datasources;
	}

	public void setDatasources(final List<FunderDatasource> datasources) {
		this.datasources = datasources;
	}

	public Date[] getAggregationDates() {
		return aggregationDates;
	}

	public void setAggregationDates(final Date[] aggregationDates) {
		this.aggregationDates = aggregationDates;
	}

}
