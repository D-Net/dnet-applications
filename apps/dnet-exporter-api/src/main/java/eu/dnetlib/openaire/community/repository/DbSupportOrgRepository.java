package eu.dnetlib.openaire.community.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.openaire.community.model.DbSupportOrg;
import eu.dnetlib.openaire.community.model.DbSupportOrgPK;

@ConditionalOnProperty(value = "openaire.exporter.enable.community", havingValue = "true")
public interface DbSupportOrgRepository extends JpaRepository<DbSupportOrg, DbSupportOrgPK> {

	List<DbSupportOrg> findByCommunity(String community);

	void deleteByCommunity(String id);

}
