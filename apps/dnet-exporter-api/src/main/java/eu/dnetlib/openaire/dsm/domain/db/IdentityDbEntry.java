package eu.dnetlib.openaire.dsm.domain.db;

import javax.persistence.Entity;
import javax.persistence.Table;

import eu.dnetlib.enabling.datasources.common.Identity;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 * Created by claudio on 13/04/2017.
 */

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "dsm_identities")
public class IdentityDbEntry extends Identity {

}
