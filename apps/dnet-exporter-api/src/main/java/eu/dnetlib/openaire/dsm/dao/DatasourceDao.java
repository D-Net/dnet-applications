package eu.dnetlib.openaire.dsm.dao;

import java.util.List;

import org.springframework.data.domain.Page;

import eu.dnetlib.enabling.datasources.common.Api;
import eu.dnetlib.enabling.datasources.common.Datasource;
import eu.dnetlib.enabling.datasources.common.DatasourceManagerCommon;
import eu.dnetlib.enabling.datasources.common.DsmException;
import eu.dnetlib.openaire.exporter.model.dsm.RequestFilter;
import eu.dnetlib.openaire.exporter.model.dsm.RequestSort;
import eu.dnetlib.openaire.exporter.model.dsm.RequestSortOrder;

public interface DatasourceDao<DS extends Datasource<?, ?, ?>, API extends Api<?>> extends DatasourceManagerCommon<DS, API> {

	// DATASOURCE

	boolean existDs(final String dsId) throws DsmException;

	Page<DS> search(RequestSort requestSortBy, RequestSortOrder order, RequestFilter requestFilter, int page, int size) throws DsmException;

	Page<DS> searchRegistered(RequestSort requestSortBy, RequestSortOrder order, RequestFilter requestFilter, int page, int size) throws DsmException;

	void updateName(String dsId, String officialname, String englishname) throws DsmException;

	void updateLogoUrl(String dsId, String logourl) throws DsmException;

	void updateCoordinates(String dsId, Double latitude, Double longitude) throws DsmException;

	void updateTimezone(String dsId, String timezone) throws DsmException;

	void updateEoscDatasourceType(String dsId, String timezone) throws DsmException;

	void updateRegisteringUser(String dsId, String registeredBy) throws DsmException;

	void updatePlatform(String dsId, String platform) throws DsmException;

	// API

	List<String> findApiBaseURLs(RequestFilter requestFilter, int page, int size) throws DsmException;

	/**
	 * Insert the oai set in case it does not exists, updates it otherwise
	 *
	 * @param apiId
	 * @param oaiSet
	 * @return true in case of insert, false in case of update
	 * @throws DsmException
	 */
	boolean upsertApiOaiSet(String apiId, String oaiSet) throws DsmException;

	void updateApiBaseUrl(String apiId, String baseUrl) throws DsmException;

}
