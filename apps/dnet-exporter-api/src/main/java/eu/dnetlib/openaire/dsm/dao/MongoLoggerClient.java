package eu.dnetlib.openaire.dsm.dao;

import java.util.List;

import eu.dnetlib.openaire.exporter.exceptions.DsmApiException;
import eu.dnetlib.openaire.exporter.model.dsm.AggregationInfo;
import eu.dnetlib.openaire.exporter.model.dsm.AggregationInfoV1;

public interface MongoLoggerClient {

	List<AggregationInfoV1> getAggregationHistoryV1(final String dsId) throws DsmApiException;

	List<AggregationInfo> getAggregationHistoryV2(final String dsId) throws DsmApiException;

	void dropCache();

}
