package eu.dnetlib.openaire.community.model;

public enum DbCommunityType {
	community, ri, subcommunity
}
