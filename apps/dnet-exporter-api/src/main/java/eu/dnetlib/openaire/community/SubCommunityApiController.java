package eu.dnetlib.openaire.community;

import static eu.dnetlib.openaire.common.ExporterConstants.C_CP;
import static eu.dnetlib.openaire.common.ExporterConstants.C_O;
import static eu.dnetlib.openaire.common.ExporterConstants.C_PJ;
import static eu.dnetlib.openaire.common.ExporterConstants.C_SUB;
import static eu.dnetlib.openaire.common.ExporterConstants.C_ZC;
import static eu.dnetlib.openaire.common.ExporterConstants.R;
import static eu.dnetlib.openaire.common.ExporterConstants.W;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.openaire.common.AbstractExporterController;
import eu.dnetlib.openaire.community.model.DepositionInfo;
import eu.dnetlib.openaire.community.utils.CommunityMappingUtils;
import eu.dnetlib.openaire.exporter.exceptions.CommunityException;
import eu.dnetlib.openaire.exporter.exceptions.ResourceNotFoundException;
import eu.dnetlib.openaire.exporter.model.community.CommunityContentprovider;
import eu.dnetlib.openaire.exporter.model.community.CommunityProject;
import eu.dnetlib.openaire.exporter.model.community.SubCommunity;
import eu.dnetlib.openaire.exporter.model.community.SubCommunityWritableProperties;
import eu.dnetlib.openaire.exporter.model.community.selectioncriteria.SelectionCriteria;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@CrossOrigin(origins = {
		"*"
})
@ConditionalOnProperty(value = "openaire.exporter.enable.community", havingValue = "true")
@Tag(name = "OpenAIRE Communities API", description = "the OpenAIRE Community API")
public class SubCommunityApiController extends AbstractExporterController {

	@Autowired
	private CommunityService communityService;

	@GetMapping("/community/{id}/subcommunities")
	@Operation(summary = "get the list of subcommunities for a given community", description = "get the list of subcommunities for a given community", tags = {
			C_SUB, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public List<SubCommunity> getSubCommunities(@PathVariable final String id,
			@RequestParam(required = false) final String subCommunityId,
			@RequestParam(required = false, defaultValue = "false") final boolean all)
			throws CommunityException {
		try {
			return communityService.getSubCommunitiesForCommunity(id)
					.stream()
					.filter(sc -> subCommunityId != null ? subCommunityId.equals(sc.getSubCommunityId()) : all || sc.isBrowsable())
					.collect(Collectors.toList());
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunities")
	@Operation(summary = "associate a subcommunity to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", description = "associate a subcommunity to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity addSubCommunity(
			@PathVariable final String id,
			@RequestBody final SubCommunity subcommunity) throws CommunityException {
		try {
			return communityService.addSubCommunity(id, subcommunity);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunities/update")
	@Operation(summary = "update a subcommunity to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", description = "associate a subcommunity to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity updateSubCommunity(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final SubCommunityWritableProperties details) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.updateSubCommunity(subCommunityId, details);

			return communityService.getSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunitiesList")
	@Operation(summary = "associate a list of subcommunities to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", description = "associate a list of subcommunities to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public List<SubCommunity> addSubCommunityList(
			@PathVariable final String id,
			@RequestBody final SubCommunity[] subcommunities) throws CommunityException {

		for (final SubCommunity sub : subcommunities) {
			if (!id.equals(CommunityMappingUtils.calculateMainCommunityId(sub.getSubCommunityId()))) {
				throw new CommunityException("The sub-collection id does not start with " + id);
			}
		}
		try {
			final List<SubCommunity> res = new ArrayList<SubCommunity>();
			for (final SubCommunity subc : subcommunities) {
				res.add(communityService.addSubCommunity(id, subc));
			}
			return res;
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/subcommunities")
	@Operation(summary = "remove the association between a subcommunity and the community", description = "remove the association between a subcommunity and the community", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void removeSubCommunity(
			@PathVariable final String id,
			@RequestParam final String subCommunityId) throws CommunityException {
		try {
			communityService.removeSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/subcommunitiesList")
	@Operation(summary = "remove a list of associations between some subcommunities and the community", description = "remove a list of associations between some subcommunities and the community", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void removeSubcommunities(
			@PathVariable final String id,
			@RequestBody final String[] subCommunityIdList) throws CommunityException {
		try {
			for (final String subId : subCommunityIdList) {
				communityService.removeSubCommunity(id, subId);
			}
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	// API to manage specific sub-community fields

	@PostMapping("/community/{id}/subcommunities/subjects")
	@Operation(summary = "associate a subject to a sub-community", description = "associate a subject to a sub-community", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity addSubCommunitySubjects(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final String[] subjects) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.addCommunitySubjects(subCommunityId, subjects);

			return communityService.getSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/subcommunities/subjects")
	@Operation(summary = "remove subjects from a sub-community", description = "remove subjects from a sub-community", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity removeSubCommunitySubjects(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final String[] subjects) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.removeCommunitySubjects(subCommunityId, subjects);

			return communityService.getSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunities/fos")
	@Operation(summary = "associate a fos to a sub-community", description = "associate a fos to a sub-community", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity addSubCommunityFOS(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final String[] subjects) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.addCommunityFOS(subCommunityId, subjects);

			return communityService.getSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/subcommunities/fos")
	@Operation(summary = "remove fos from a sub-community", description = "remove fos from a sub-community", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity removeSubCommunityFOS(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final String[] subjects) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.removeCommunityFOS(subCommunityId, subjects);

			return communityService.getSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunities/sdg")
	@Operation(summary = "associate a sdg to a sub-community", description = "associate a sdg to the community", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity addSubCommunitySDG(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final String[] subjects) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.addCommunitySDG(subCommunityId, subjects);

			return communityService.getSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/subcommunities/sdg")
	@Operation(summary = "remove sdg from a sub-community", description = "remove sdg from a sub-community", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity removeSubCommunitySDG(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final String[] subjects) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.removeCommunitySDG(subCommunityId, subjects);

			return communityService.getSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunities/advancedConstraint")
	@Operation(summary = "the set of constraints to be used to extend the association between result and sub-community", description = "the set of constraints to be used to extend the association between result and sub-community", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity addASubCommunitydvancedConstraint(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final SelectionCriteria advancedConstraint) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.addCommunityAdvancedConstraint(subCommunityId, advancedConstraint);

			return communityService.getSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/subcommunities/advancedConstraint")
	@Operation(summary = "remove the constraints to extend the association result community from a community", description = "remove the constraints to extend the association result community from a community", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity removeSubCommunityAdvancedConstraint(@PathVariable final String id, @RequestParam final String subCommunityId)
			throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.removeCommunityAdvancedConstraint(subCommunityId);

			return communityService.getSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunities/removeConstraint")
	@Operation(summary = "the set of constraints to be used to remove the association between result and sub-community", description = "the set of constraints to be used to remove the association between result and sub-community", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity addSubCommunityRemoveConstraint(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final SelectionCriteria removeConstraint) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.addCommunityRemoveConstraint(subCommunityId, removeConstraint);

			return communityService.getSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/subcommunities/removeConstraint")
	@Operation(summary = "remove the constraints to remove the association beetween result and sub-community", description = "remove the constraints to remove the association beetween result and sub-community", tags = {
			C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity removeSubCommunityRemoveConstraint(@PathVariable final String id, @RequestParam final String subCommunityId)
			throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.removeCommunityRemoveConstraint(subCommunityId);

			return communityService.getSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunities/zenodocommunities")
	@Operation(summary = "associate a Zenodo community to a sub-community", description = "associate a Zenodo community to a sub-community", tags = {
			C_ZC, C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity addSubCommunityZenodoCommunity(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestParam(required = false, defaultValue = "false") final boolean main,
			@RequestParam final String zenodocommunity) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.addCommunityZenodoCommunity(subCommunityId, zenodocommunity, main);

			return communityService.getSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}

	}

	@DeleteMapping("/community/{id}/subcommunities/zenodocommunities")
	@Operation(summary = "remove a Zenodo community from a sub-community", description = "remove a Zenodo community from a sub-community", tags = {
			C_ZC, C_SUB, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public SubCommunity removeSubCommunityZenodoCommunity(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestParam(required = false, defaultValue = "false") final boolean main,
			@RequestParam final String zenodocommunity) throws CommunityException {
		verifyIdParameters(id, subCommunityId);

		try {
			communityService.removeCommunityZenodoCommunity(subCommunityId, zenodocommunity, main);

			return communityService.getSubCommunity(id, subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	// PROJECTS

	@GetMapping("/community/{id}/subcommunities/projects/{page}/{size}")
	@Operation(summary = "get community projects", description = "get community projects", tags = {
			C_SUB, C_PJ, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public Page<CommunityProject> getCommunityProjects(@PathVariable final String id,
			@PathVariable final Integer page,
			@PathVariable final Integer size,
			@RequestParam final String subCommunityId,
			@RequestParam(required = false) final String funder,
			@RequestParam(required = false) final String searchFilter,
			@RequestParam(required = false) final String orderBy)
			throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			return communityService.getCommunityProjects(subCommunityId, funder, searchFilter, page, size, orderBy);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunities/projects")
	@Operation(summary = "associate a project to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", description = "associate a project to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", tags = {
			C_SUB, C_PJ, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityProject addCommunityProject(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final CommunityProject project) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			return communityService.addCommunityProject(subCommunityId, project);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/subcommunities/projects")
	@Operation(summary = "remove a project from the community", description = "remove a project from the community", tags = {
			C_SUB, C_PJ, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void deleteCommunityProject(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestParam final String projectId) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.removeCommunityProjects(subCommunityId, projectId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunities/projectList")
	@Operation(summary = "associate a list of project to the community", description = "associate a list of project to the community", tags = {
			C_SUB, C_PJ, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityProject[] addCommunityProjectList(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final CommunityProject[] projects) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.addCommunityProjects(subCommunityId, projects);
			return projects;
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/subcommunities/projectList")
	@Operation(summary = "remove a list of projects from the community", description = "remove a list of projects from the community", tags = {
			C_SUB, C_PJ, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void deleteCommunityProjectList(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final String[] projectIdList) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.removeCommunityProjects(subCommunityId, projectIdList);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@GetMapping("/community/{id}/subcommunities/funders")
	@Operation(summary = "get the funders of the projects of a community", description = "get the funders of the projects of a community", tags = {
			C_SUB, C_PJ, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public List<String> getCommunityFunders(@PathVariable final String id, @RequestParam final String subCommunityId)
			throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			return communityService.getCommunityFunders(subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	// Datasources

	@GetMapping("/community/{id}/subcommunities/datasources")
	@Operation(summary = "get the list of datasources associated to a given community", description = "get the list of content providers associated to a given community", tags = {
			C_SUB, C_CP, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public List<CommunityContentprovider> getCommunityDatasources(@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestParam(required = false) final Boolean deposit)
			throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			return deposit == null ? communityService.getCommunityDatasources(subCommunityId)
					: communityService.getCommunityDatasourcesWithDeposit(subCommunityId, deposit);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunities/datasources")
	@Operation(summary = "associate a datasource to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", description = "associate a datasource to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", tags = {
			C_SUB, C_CP, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityContentprovider addCommunityDatasource(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final CommunityContentprovider datasource) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.addCommunityDatasources(subCommunityId, datasource);
			return datasource;
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunities/datasources/deposit")
	@Operation(summary = "update the deposit and message filelds of a datasource associated to the community", description = "update the deposit and message filelds of a datasource associated to the community", tags = {
			C_SUB, C_CP, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityContentprovider addCommunityDatasourceDeposit(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final DepositionInfo info) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			return communityService.updateCommunityDatasourcesDeposit(subCommunityId, info.getOpenaireId(), info
					.getDeposit(), info.getMessage());
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/subcommunities/datasources")
	@Operation(summary = "remove the association between a datasource and the community", description = "remove the association between a datasource and the community", tags = {
			C_SUB, C_CP, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void removeCommunityDatasource(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestParam final String dsId) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.removeCommunityDatasources(subCommunityId, dsId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunities/datasourcesList")
	@Operation(summary = "associate a list of datasources to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", description = "associate a list of datasources to the community, provide all the fields or the method will overwrite with nulls the fields that are missing", tags = {
			C_SUB, C_CP, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public CommunityContentprovider[] addCommunityDatasourcesList(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final CommunityContentprovider[] dsList) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.addCommunityDatasources(subCommunityId, dsList);
			return dsList;
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/subcommunities/datasourcesList")
	@Operation(summary = "remove a list of datasources from the community", description = "remove a list of datasources from the community", tags = {
			C_SUB, C_CP, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void deleteCommunityDatasourcesList(
			@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestBody final String[] dsIdList) throws CommunityException {

		verifyIdParameters(id, subCommunityId);

		try {
			communityService.removeCommunityDatasources(subCommunityId, dsIdList);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	// ORGANIZATIONS
	/*
	 * @GetMapping("/community/{id}/subcommunities/organizations")
	 *
	 * @Operation(summary = "get the list of organizations for a given community", description =
	 * "get the list of organizations for a given community", tags = { C_O, R })
	 *
	 * @ApiResponses(value = {
	 *
	 * @ApiResponse(responseCode = "200", description = "OK"),
	 *
	 * @ApiResponse(responseCode = "404", description = "not found"),
	 *
	 * @ApiResponse(responseCode = "500", description = "unexpected error") }) public List<CommunityOrganization>
	 * getCommunityOrganizations(@PathVariable final String id, @RequestParam final String subCommunityId) throws CommunityException {
	 * verifyIdParameters(id, subCommunityId);
	 *
	 * try { return communityService.getCommunityOrganizations(subCommunityId); } catch (final ResourceNotFoundException e) { throw e; }
	 * catch (final Throwable e) { throw new CommunityException(e); } }
	 *
	 * @PostMapping("/community/{id}/subcommunities/organizations")
	 *
	 * @Operation(summary =
	 * "associate an organization to the community, provide all the fields or the method will overwrite with nulls the fields that are missing"
	 * , description =
	 * "associate an organization to the community, provide all the fields or the method will overwrite with nulls the fields that are missing"
	 * , tags = { C_O, W })
	 *
	 * @ApiResponses(value = {
	 *
	 * @ApiResponse(responseCode = "200", description = "OK"),
	 *
	 * @ApiResponse(responseCode = "404", description = "not found"),
	 *
	 * @ApiResponse(responseCode = "500", description = "unexpected error") }) public CommunityOrganization addCommunityOrganization(
	 *
	 * @PathVariable final String id,
	 *
	 * @RequestParam final String subCommunityId,
	 *
	 * @RequestBody final CommunityOrganization organization) throws CommunityException {
	 *
	 * verifyIdParameters(id, subCommunityId);
	 *
	 * try { communityService.addCommunityOrganizations(subCommunityId, organization); return organization; } catch (final
	 * ResourceNotFoundException e) { throw e; } catch (final Throwable e) { throw new CommunityException(e); } }
	 *
	 * @PostMapping("/community/{id}/subcommunities/organizationList")
	 *
	 * @Operation(summary =
	 * "associate a list of organizations to the community, provide all the fields or the method will overwrite with nulls the fields that are missing"
	 * , description =
	 * "associate a list of organizations to the community, provide all the fields or the method will overwrite with nulls the fields that are missing"
	 * , tags = { C_O, W })
	 *
	 * @ApiResponses(value = {
	 *
	 * @ApiResponse(responseCode = "200", description = "OK"),
	 *
	 * @ApiResponse(responseCode = "404", description = "not found"),
	 *
	 * @ApiResponse(responseCode = "500", description = "unexpected error") }) public CommunityOrganization[] addCommunityOrganizationList(
	 *
	 * @PathVariable final String id,
	 *
	 * @RequestParam final String subCommunityId,
	 *
	 * @RequestBody final CommunityOrganization[] orgs) throws CommunityException {
	 *
	 * verifyIdParameters(id, subCommunityId);
	 *
	 * try { communityService.addCommunityOrganizations(subCommunityId, orgs); return orgs; } catch (final ResourceNotFoundException e) {
	 * throw e; } catch (final Throwable e) { throw new CommunityException(e); } }
	 *
	 * @DeleteMapping("/community/{id}/subcommunities/organizations")
	 *
	 * @Operation(summary = "remove the association between an organization and the community", description =
	 * "remove the association between an organization and the community", tags = { C_O, W })
	 *
	 * @ApiResponses(value = {
	 *
	 * @ApiResponse(responseCode = "200", description = "OK"),
	 *
	 * @ApiResponse(responseCode = "404", description = "not found"),
	 *
	 * @ApiResponse(responseCode = "500", description = "unexpected error") }) public void removeCommunityOrganization(
	 *
	 * @PathVariable final String id,
	 *
	 * @RequestParam final String subCommunityId,
	 *
	 * @RequestParam final String organizationName) throws CommunityException {
	 *
	 * verifyIdParameters(id, subCommunityId);
	 *
	 * try { communityService.removeCommunityOrganizations(subCommunityId, organizationName); } catch (final ResourceNotFoundException e) {
	 * throw e; } catch (final Throwable e) { throw new CommunityException(e); } }
	 *
	 * @DeleteMapping("/community/{id}/subcommunities/organizationList")
	 *
	 * @Operation(summary = "remove a list of associations between some organizations and the community", description =
	 * "remove a list of associations between some organizations and the community", tags = { C_O, W })
	 *
	 * @ApiResponses(value = {
	 *
	 * @ApiResponse(responseCode = "200", description = "OK"),
	 *
	 * @ApiResponse(responseCode = "404", description = "not found"),
	 *
	 * @ApiResponse(responseCode = "500", description = "unexpected error") }) public void removeCommunityOrganizationList(
	 *
	 * @PathVariable final String id,
	 *
	 * @RequestParam final String subCommunityId,
	 *
	 * @RequestBody final String[] orgNames) throws CommunityException {
	 *
	 * verifyIdParameters(id, subCommunityId);
	 *
	 * try { communityService.removeCommunityOrganizations(subCommunityId, orgNames); } catch (final ResourceNotFoundException e) { throw e;
	 * } catch (final Throwable e) { throw new CommunityException(e); } }
	 */

	// Propagation Organizations

	@GetMapping("/community/{id}/subcommunities/propagationOrganizations")
	@Operation(summary = "return the propagation organizations of a sub-community", description = "try { return the propagation organizations of a community", tags = {
			C_SUB, C_O, R
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public Set<String> getPropagationOrganizationsForCommunity(@PathVariable final String id, @RequestParam final String subCommunityId)
			throws CommunityException {
		try {
			return communityService.getPropagationOrganizationsForCommunity(subCommunityId);
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@PostMapping("/community/{id}/subcommunities/propagationOrganizations")
	@Operation(summary = "add an organization to the propagationOrganizationCommunityMap of a sub-community", description = "add an organization to the propagationOrganizationCommunityMap", tags = {
			C_SUB, C_O, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public Set<String> addPropagationOrganizationForCommunity(@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestParam final String organizationId) throws CommunityException {

		try {
			return communityService.addPropagationOrganizationForCommunity(subCommunityId, organizationId.split(","));
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	@DeleteMapping("/community/{id}/subcommunities/propagationOrganizations")
	@Operation(summary = "delete an organization to the propagationOrganizationCommunityMap", description = "delete an organization from the propagationOrganizationCommunityMap of a sub-community", tags = {
			C_SUB, C_O, W
	})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "404", description = "not found"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public Set<String> removePropagationOrganizationForCommunity(@PathVariable final String id,
			@RequestParam final String subCommunityId,
			@RequestParam final String organizationId) throws CommunityException {

		try {
			return communityService.removePropagationOrganizationForCommunity(subCommunityId, organizationId.split(","));
		} catch (final ResourceNotFoundException e) {
			throw e;
		} catch (final Throwable e) {
			throw new CommunityException(e);
		}
	}

	// Common methods

	private void verifyIdParameters(final String id, final String subCommunityId) throws CommunityException {
		if (!CommunityMappingUtils.isValidCommunityId(id)) { throw new CommunityException("Invalid community id: " + id); }

		if (!CommunityMappingUtils.isValidSubCommunityId(subCommunityId)) { throw new CommunityException("Invalid sub-collection id: " + subCommunityId); }

		if (!id.equals(CommunityMappingUtils.calculateMainCommunityId(subCommunityId))) {
			throw new CommunityException("The sub-collection id does not start with " + id);
		}
	}

}
