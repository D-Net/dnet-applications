package eu.dnetlib.openaire.funders.domain.db;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class FunderDatasource implements Serializable {

	private static final long serialVersionUID = 2145493560459874509L;

	private String id;

	private String name;

	private String type;

	private String nsPrefix;

	@JsonInclude(Include.NON_NULL)
	private String fundingProgram;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getNsPrefix() {
		return nsPrefix;
	}

	public void setNsPrefix(final String nsPrefix) {
		this.nsPrefix = nsPrefix;
	}

	public String getFundingProgram() {
		return fundingProgram;
	}

	public void setFundingProgram(final String fundingProgram) {
		this.fundingProgram = fundingProgram;
	}

}
