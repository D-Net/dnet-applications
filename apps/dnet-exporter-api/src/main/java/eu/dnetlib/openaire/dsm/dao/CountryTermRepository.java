package eu.dnetlib.openaire.dsm.dao;

import eu.dnetlib.openaire.dsm.domain.db.CountryTerm;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by claudio on 19/04/2017.
 */
@Repository
@ConditionalOnProperty(value = "openaire.exporter.enable.dsm", havingValue = "true")
public interface CountryTermRepository extends JpaRepository<CountryTerm, String> {

}
