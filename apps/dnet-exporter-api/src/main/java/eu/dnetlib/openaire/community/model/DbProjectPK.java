package eu.dnetlib.openaire.community.model;

import java.io.Serializable;
import java.util.Objects;

public class DbProjectPK implements Serializable {

	private static final long serialVersionUID = -4236577148534835803L;

	private String community;

	private String projectId;

	public DbProjectPK() {}

	public DbProjectPK(final String community, final String projectId) {
		this.community = community;
		this.projectId = projectId;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(final String community) {
		this.community = community;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(community, projectId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof DbProjectPK)) { return false; }
		final DbProjectPK other = (DbProjectPK) obj;
		return Objects.equals(community, other.community) && Objects.equals(projectId, other.projectId);
	}

	@Override
	public String toString() {
		return String.format("CommunityProjectPK [community=%s, projectId=%s]", community, projectId);
	}
}
