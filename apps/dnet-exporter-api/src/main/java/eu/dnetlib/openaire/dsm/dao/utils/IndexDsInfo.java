package eu.dnetlib.openaire.dsm.dao.utils;

/**
 * Created by claudio on 20/10/2016.
 */
public class IndexDsInfo {

	private final String indexBaseUrl;
	private final String indexDsId;
	private final String format;
	private final String coll;

	public IndexDsInfo(final String indexBaseUrl, final String indexDsId, final String format, final String coll) {
		this.indexBaseUrl = indexBaseUrl;
		this.indexDsId = indexDsId;
		this.format = format;
		this.coll = coll;
	}

	public String getIndexBaseUrl() {
		return indexBaseUrl;
	}

	public String getIndexDsId() {
		return indexDsId;
	}

	public String getFormat() {
		return format;
	}

	public String getColl() {
		return coll;
	}

}
