package eu.dnetlib.openaire.project.domain.db;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Created by claudio on 05/07/2017.
 */
@Entity
@Table(name = "projects_tsv")
@Schema(name = "Project TSV model", description = "project TSV model description")
public class ProjectTsv {

	@Id
	@JsonIgnore
	private long rowid;
	private String code;
	private String acronym;
	private String title;
	@Column(name = "call_identifier")
	private String callIdentifier;
	private Date startdate;
	private Date enddate;
	@Column(name = "ec_sc39")
	private Boolean ecSc39;
	@Column(name = "oa_mandate_for_publications")
	private Boolean oaMandateForPublications;
	@Column(name = "oa_mandate_for_datasets")
	private Boolean oaMandateForDatasets;
	@JsonIgnore
	private String fundingpathid;
	private String description;
	@Column(name = "legalname")
	private String orgLegalname;
	@Column(name = "country")
	private String orgCountry;
	@Column(name = "role")
	private String orgRole;
	private String contactfullname;
	private String contactemail;

	public ProjectTsv() {}

	public List<String> asList() {
		return Lists.newArrayList(clean(getCode()), clean(getAcronym()), clean(getTitle()), clean(getCallIdentifier()), clean(getStartdate() != null
			? getStartdate().toString()
			: ""), clean(getEnddate() != null ? getEnddate().toString() : ""), clean(String.valueOf(isOaMandateForPublications())), clean(String
				.valueOf(isOaMandateForDatasets())), clean(getDescription()), clean(getOrgLegalname()), clean(getOrgCountry()), clean(getOrgRole()), clean(getContactfullname()), clean(getContactemail()));
	}

	private String clean(final String s) {
		return StringUtils.isNotBlank(s) ? "\"" + s.replaceAll("\\n|\\t|\\s+", " ").replace("\"", "\"\"").trim() + "\"" : "";
	}

	public long getRowid() {
		return rowid;
	}

	public void setRowid(final long rowid) {
		this.rowid = rowid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(final String acronym) {
		this.acronym = acronym;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getCallIdentifier() {
		return callIdentifier;
	}

	public void setCallIdentifier(final String callIdentifier) {
		this.callIdentifier = callIdentifier;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(final Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(final Date enddate) {
		this.enddate = enddate;
	}

	public Boolean isEcSc39() {
		return ecSc39;
	}

	public void setEcSc39(final Boolean ecSc39) {
		this.ecSc39 = ecSc39;
	}

	public Boolean isOaMandateForPublications() {
		return oaMandateForPublications;
	}

	public void setOaMandateForPublications(final Boolean oaMandateForPublications) {
		this.oaMandateForPublications = oaMandateForPublications;
	}

	public Boolean isOaMandateForDatasets() {
		return oaMandateForDatasets;
	}

	public void setOaMandateForDatasets(final Boolean oaMandateForDatasets) {
		this.oaMandateForDatasets = oaMandateForDatasets;
	}

	public String getFundingpathid() {
		return fundingpathid;
	}

	public void setFundingpathid(final String fundingpathid) {
		this.fundingpathid = fundingpathid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getOrgLegalname() {
		return orgLegalname;
	}

	public void setOrgLegalname(final String orgLegalname) {
		this.orgLegalname = orgLegalname;
	}

	public String getOrgCountry() {
		return orgCountry;
	}

	public void setOrgCountry(final String orgCountry) {
		this.orgCountry = orgCountry;
	}

	public String getOrgRole() {
		return orgRole;
	}

	public void setOrgRole(final String orgRole) {
		this.orgRole = orgRole;
	}

	public String getContactfullname() {
		return contactfullname;
	}

	public void setContactfullname(final String contactfullname) {
		this.contactfullname = contactfullname;
	}

	public String getContactemail() {
		return contactemail;
	}

	public void setContactemail(final String contactemail) {
		this.contactemail = contactemail;
	}
}
