package eu.dnetlib;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;

import eu.dnetlib.DnetOpenaireExporterProperties.Jdbc;
import eu.dnetlib.data.objectstore.rmi.ObjectStoreService;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;

/**
 * Created by claudio on 07/07/2017.
 */
@Configuration
public class DNetOpenaireExporterConfiguration {

	private static final Log log = LogFactory.getLog(DNetOpenaireExporterConfiguration.class);

	@Autowired
	private DnetOpenaireExporterProperties props;

	@Bean
	public ISLookUpService getLookUpService() {
		return getServiceStub(ISLookUpService.class, props.getIsLookupUrl());
	}

	@Bean
	public ObjectStoreService getObjectStoreService() {
		return getServiceStub(ObjectStoreService.class, props.getObjectStoreServiceUrl());
	}

	@Bean
	public ISRegistryService getRegistryService() {
		return getServiceStub(ISRegistryService.class, props.getIsRegistryServiceUrl());
	}

	@SuppressWarnings("unchecked")
	private <T> T getServiceStub(final Class<T> clazz, final String endpoint) {
		log.info(String.format("Initializing service stub %s, endpoint %s", clazz.toString(), endpoint));
		final JaxWsProxyFactoryBean jaxWsProxyFactory = new JaxWsProxyFactoryBean();
		jaxWsProxyFactory.setServiceClass(clazz);
		jaxWsProxyFactory.setAddress(endpoint);

		final T service = (T) jaxWsProxyFactory.create();

		final Client client = ClientProxy.getClient(service);
		if (client != null) {
			final HTTPConduit conduit = (HTTPConduit) client.getConduit();
			final HTTPClientPolicy policy = new HTTPClientPolicy();

			log.info(String.format("setting connectTimeout to %s, receiveTimeout to %s for service %s", props.getCxfClientConnectTimeout(), props
				.getCxfClientReceiveTimeout(), clazz.getCanonicalName()));

			policy.setConnectionTimeout(props.getCxfClientConnectTimeout());
			policy.setReceiveTimeout(props.getCxfClientReceiveTimeout());
			conduit.setClient(policy);
		}

		return service;
	}

	@Bean
	public DataSource getSqlDataSource() {
		final Jdbc jdbc = props.getJdbc();
		return getDatasource(jdbc.getDriverClassName(), jdbc.getUrl(), jdbc.getUser(), jdbc.getPwd(), jdbc.getMinIdle(), jdbc.getMaxRows());
	}

	private BasicDataSource getDatasource(final String driverClassName,
		final String jdbcUrl,
		final String jdbcUser,
		final String jdbcPwd,
		final int jdbcMinIdle,
		final int jdbcMaxIdle) {
		final BasicDataSource d = new BasicDataSource();
		d.setDriverClassName(driverClassName);
		d.setUrl(jdbcUrl);
		d.setUsername(jdbcUser);
		d.setPassword(jdbcPwd);
		d.setMinIdle(jdbcMinIdle);
		d.setMaxIdle(jdbcMaxIdle);
		return d;
	}

	@Bean
	public MongoClient getMongoClient() {
		return new MongoClient(
			new ServerAddress(props.getDatasource().getMongoHost(), props.getDatasource().getMongoPort()),
			MongoClientOptions.builder().connectionsPerHost(props.getDatasource().getMongoConnectionsPerHost()).build());
	}

}
