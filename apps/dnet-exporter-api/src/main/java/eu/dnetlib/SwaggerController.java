package eu.dnetlib;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SwaggerController {

	@RequestMapping(value = {
		"/", "/docs", "swagger-ui.html", "swagger-ui/"
	})
	public String index() {
		return "redirect:swagger-ui/index.html";
	}

}
