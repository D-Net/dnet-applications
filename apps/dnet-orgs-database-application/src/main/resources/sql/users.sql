INSERT INTO users(email, valid, role) VALUES ('michele.artini@isti.cnr.it', true, 'ADMIN');
INSERT INTO users(email, valid, role) VALUES ('michele.debonis@isti.cnr.it', true, 'ADMIN');
INSERT INTO users(email, valid, role) VALUES ('andreas.czerniak@uni-bielefeld.de', true, 'ADMIN');
INSERT INTO users(email, valid, role) VALUES ('claudio.atzori@isti.cnr.it', true, 'ADMIN');
INSERT INTO users(email, valid, role) VALUES ('emma.lazzeri@isti.cnr.it', true, 'ADMIN');
INSERT INTO users(email, valid, role) VALUES ('gina.pavone@isti.cnr.it', true, 'ADMIN');
INSERT INTO users(email, valid, role) VALUES ('paolo.manghi@isti.cnr.it', true, 'ADMIN');
