CREATE TEMPORARY TABLE temp_ror_grid (
	ror_id text,
	grid_id text
);

COPY temp_ror_grid (ror_id, grid_id) FROM '/Users/michele/Develop/dnet45/dnet-applications/apps/dnet-orgs-database-application/data/ror_grid.tsv' DELIMITER E'\t';


INSERT INTO other_ids(id, otherid, type) (SELECT
	o.id       as id, 
	ror.ror_id as otherid,  
	'ROR'      as type 
FROM 
	organizations o
	JOIN other_ids oth ON (o.id = oth.id AND oth.type = 'GRID') 
	JOIN temp_ror_grid ror ON (oth.otherid = ror.grid_id)
) ON CONFLICT DO NOTHING;
