-- TO RESOLVE MANUALLY MANAGING CONFLICTS
select d.oa_original_id, array_agg(o.id), array_agg(o.name) from oa_duplicates d left outer join organizations o on (o.id = d.local_id) where d.reltype='is_similar' group by oa_original_id having count(reltype) > 1 and count (distinct o.status) = 1;


-- TO force the status of the orgs
-- select name from organizations WHERE country='IT' and type='UNKNOWN' and lower(reverse(split_part(reverse(trim(name)), ' ', 1))) in ('spa', 'snc', 'srl', 'scarl') and name not ilike '%universit%' and name not ilike '%ospedale%' and name not ilike '%hospital%';
