package eu.dnetlib.organizations.model.view;

import java.io.Serializable;
import java.util.Objects;

public class OtherIdentifier implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8290245353090885241L;

	private String id;
	private String type;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, type);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof OtherIdentifier)) { return false; }
		final OtherIdentifier other = (OtherIdentifier) obj;
		return Objects.equals(id, other.id) && Objects.equals(type, other.type);
	}

}
