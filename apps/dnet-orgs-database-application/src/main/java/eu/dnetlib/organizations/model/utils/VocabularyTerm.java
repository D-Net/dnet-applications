package eu.dnetlib.organizations.model.utils;

import java.io.Serializable;

public class VocabularyTerm implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -524482434889131310L;

	private String name;
	private String value;

	public VocabularyTerm() {}

	public VocabularyTerm(final String name, final String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}
}
