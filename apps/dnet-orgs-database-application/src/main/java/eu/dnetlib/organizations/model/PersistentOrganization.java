package eu.dnetlib.organizations.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "persistent_orgs")
public class PersistentOrganization implements Serializable {

	private static final long serialVersionUID = 7684478315366015099L;

	@Id
	@Column(name = "id")
	private String id;

	public PersistentOrganization() {}

	public PersistentOrganization(final String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

}
