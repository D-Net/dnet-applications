package eu.dnetlib.organizations.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "urls")
@IdClass(UrlPK.class)
public class Url implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5889020711423271565L;

	@Id
	@Column(name = "id")
	private String orgId;

	@Id
	@Column(name = "url")
	private String url;

	public Url() {}

	public Url(final String orgId, final String url) {
		this.orgId = orgId;
		this.url = url;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

}
