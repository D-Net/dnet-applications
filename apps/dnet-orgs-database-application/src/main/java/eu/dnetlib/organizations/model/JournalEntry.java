package eu.dnetlib.organizations.model;

import java.io.Serializable;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import eu.dnetlib.organizations.utils.JournalOperations;

@Entity
@Table(name = "journal")
public class JournalEntry implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6861395678624671324L;

	@Id
	@Column(name = "jid")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer journalEntryId;

	@Column(name = "id")
	private String orgId;

	@Column(name = "operation")
	private String operation;

	@Column(name = "description")
	private String description;

	@Column(name = "op_date")
	private OffsetDateTime date;

	@Column(name = "email")
	private String email;

	public JournalEntry() {}

	public JournalEntry(final String orgId, final JournalOperations operation, final String description, final String email) {
		this(orgId, operation.name(), description, email);
	}

	protected JournalEntry(final String orgId, final String operation, final String description, final String email) {
		this.orgId = orgId;
		this.operation = operation;
		this.description = description;
		this.date = OffsetDateTime.now();
		this.email = email;
	}

	public Integer getJournalEntryId() {
		return journalEntryId;
	}

	public void setJournalEntryId(final Integer journalEntryId) {
		this.journalEntryId = journalEntryId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(final String operation) {
		this.operation = operation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public OffsetDateTime getDate() {
		return date;
	}

	public void setDate(final OffsetDateTime date) {
		this.date = date;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

}
