package eu.dnetlib.organizations.model.utils;

import java.io.Serializable;

public class TempBrowseEntry implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2233409680550512318L;

	private String code;
	private String name;
	private String group;
	private Long count;

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(final String group) {
		this.group = group;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(final Long count) {
		this.count = count;
	}
}
