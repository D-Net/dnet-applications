package eu.dnetlib.organizations.repository.readonly;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import eu.dnetlib.organizations.model.view.DuplicateGroupView;

@Repository
public interface DuplicateGroupViewRepository extends ReadOnlyRepository<DuplicateGroupView, String> {

	List<DuplicateGroupView> findByCountry(String country, Pageable page);
}
