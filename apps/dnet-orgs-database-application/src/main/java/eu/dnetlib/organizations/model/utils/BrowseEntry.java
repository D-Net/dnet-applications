package eu.dnetlib.organizations.model.utils;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class BrowseEntry implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8854955977257064470L;

	private String code;
	private String name;
	private Map<String, Long> values = new LinkedHashMap<>();

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Map<String, Long> getValues() {
		return values;
	}

	public void setValues(final Map<String, Long> values) {
		this.values = values;
	}

}
