package eu.dnetlib.organizations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.organizations.model.Note;

public interface NoteRepository extends JpaRepository<Note, String> {

}
