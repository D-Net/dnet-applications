package eu.dnetlib.organizations.utils;

import eu.dnetlib.organizations.controller.UserRole;

public class OpenOrgsConstants {

	public static final String OPENORGS_PREFIX = "openorgs____::";
	public static final String OPENORGS_MESH_PREFIX = "openorgsmesh::";
	public static final String OPENORGS_PENDING_PREFIX = "pending_org_::";

	public static final String OPENORGS_ROLE_PREFIX = "OPENORGS_";

	public static final String[] VALID_ROLES = {
		OPENORGS_ROLE_PREFIX + UserRole.ADMIN,
		OPENORGS_ROLE_PREFIX + UserRole.NATIONAL_ADMIN,
		OPENORGS_ROLE_PREFIX + UserRole.USER
	};

	public static final String NOT_AUTORIZED_ROLE = OPENORGS_ROLE_PREFIX + UserRole.NOT_AUTHORIZED;

}
