package eu.dnetlib.organizations.model.view;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.vladmihalcea.hibernate.type.array.StringArrayType;

@Entity
@Table(name = "users_view")
@TypeDefs({
		@TypeDef(name = "string-array", typeClass = StringArrayType.class)
})
public class UserView implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3308680880727895075L;

	@Id
	@Column(name = "email")
	private String email;

	@Column(name = "fullname")
	private String fullname;

	@Column(name = "organization")
	private String organization;

	@Column(name = "reference_person")
	private String referencePerson;

	@Column(name = "request_message")
	private String requestMessage;

	@Column(name = "valid")
	private boolean valid;

	@Column(name = "role")
	private String role;

	@Column(name = "first_access")
	private OffsetDateTime firstAccess;

	@Column(name = "last_access")
	private OffsetDateTime lastAccess;

	@Type(type = "string-array")
	@Column(name = "countries", columnDefinition = "text[]")
	private String[] countries;

	@Transient
	private Map<String, String> countryMap = new LinkedHashMap<>();

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(final boolean valid) {
		this.valid = valid;
	}

	public String getRole() {
		return role;
	}

	public void setRole(final String role) {
		this.role = role;
	}

	public String[] getCountries() {
		return countries;
	}

	public void setCountries(final String[] countries) {
		this.countries = countries;
	}

	public OffsetDateTime getFirstAccess() {
		return firstAccess;
	}

	public void setFirstAccess(final OffsetDateTime firstAccess) {
		this.firstAccess = firstAccess;
	}

	public OffsetDateTime getLastAccess() {
		return lastAccess;
	}

	public void setLastAccess(final OffsetDateTime lastAccess) {
		this.lastAccess = lastAccess;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(final String fullname) {
		this.fullname = fullname;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(final String organization) {
		this.organization = organization;
	}

	public String getReferencePerson() {
		return referencePerson;
	}

	public void setReferencePerson(final String referencePerson) {
		this.referencePerson = referencePerson;
	}

	public String getRequestMessage() {
		return requestMessage;
	}

	public void setRequestMessage(final String requestMessage) {
		this.requestMessage = requestMessage;
	}

	public Map<String, String> getCountryMap() {
		return countryMap;
	}

	public void setCountryMap(final Map<String, String> countryMap) {
		this.countryMap = countryMap;
	}

}
