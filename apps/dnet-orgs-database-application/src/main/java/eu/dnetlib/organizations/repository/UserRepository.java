package eu.dnetlib.organizations.repository;

import java.time.OffsetDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import eu.dnetlib.organizations.model.User;

public interface UserRepository extends JpaRepository<User, String> {

	@Modifying
	@Query("update User set fullname = ?2, organization = ?3, last_access = ?4 where email = ?1")
	void updateDetails(final String email, final String fullname, final String organization, OffsetDateTime now);

}
