package eu.dnetlib.organizations.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "conflict_groups_view")
@IdClass(ConflictGroupViewPK.class)
public class ConflictGroupView implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6621501199394187765L;

	@Id
	@Column(name = "id_1")
	private String id1;

	@Id
	@Column(name = "id_2")
	private String id2;

	@Column(name = "name_1")
	private String name1;

	@Column(name = "name_2")
	private String name2;

	@Column(name = "type_1")
	private String type1;

	@Column(name = "type_2")
	private String type2;

	@Column(name = "city_1")
	private String city1;

	@Column(name = "city_2")
	private String city2;

	@Column(name = "country_1")
	private String country1;

	@Column(name = "country_2")
	private String country2;

	@Column(name = "idgroup")
	private String group;

	public String getId1() {
		return id1;
	}

	public void setId1(final String id1) {
		this.id1 = id1;
	}

	public String getId2() {
		return id2;
	}

	public void setId2(final String id2) {
		this.id2 = id2;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(final String name1) {
		this.name1 = name1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(final String name2) {
		this.name2 = name2;
	}

	public String getType1() {
		return type1;
	}

	public void setType1(final String type1) {
		this.type1 = type1;
	}

	public String getType2() {
		return type2;
	}

	public void setType2(final String type2) {
		this.type2 = type2;
	}

	public String getCity1() {
		return city1;
	}

	public void setCity1(final String city1) {
		this.city1 = city1;
	}

	public String getCity2() {
		return city2;
	}

	public void setCity2(final String city2) {
		this.city2 = city2;
	}

	public String getCountry1() {
		return country1;
	}

	public void setCountry1(final String country1) {
		this.country1 = country1;
	}

	public String getCountry2() {
		return country2;
	}

	public void setCountry2(final String country2) {
		this.country2 = country2;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(final String group) {
		this.group = group;
	}

}
