package eu.dnetlib.organizations.model;

import java.io.Serializable;
import java.util.Objects;

public class OtherNamePK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1225063182881256637L;

	private String orgId;

	private String name;

	private String lang;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(final String lang) {
		this.lang = lang;
	}

	@Override
	public int hashCode() {
		return Objects.hash(orgId, lang, name);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof OtherNamePK)) { return false; }
		final OtherNamePK other = (OtherNamePK) obj;
		return Objects.equals(orgId, other.orgId) && Objects.equals(lang, other.lang) && Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return String.format("OtherNamePK [orgId=%s, name=%s, lang=%s]", orgId, name, lang);
	}

}
