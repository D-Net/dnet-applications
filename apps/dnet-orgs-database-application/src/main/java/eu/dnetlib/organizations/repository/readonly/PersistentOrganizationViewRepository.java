package eu.dnetlib.organizations.repository.readonly;

import org.springframework.stereotype.Repository;

import eu.dnetlib.organizations.model.view.PersistentOrganizationView;

@Repository
public interface PersistentOrganizationViewRepository extends ReadOnlyRepository<PersistentOrganizationView, String> {

}
