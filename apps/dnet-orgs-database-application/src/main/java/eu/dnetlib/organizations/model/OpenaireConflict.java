package eu.dnetlib.organizations.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "oa_conflicts")
@IdClass(OpenaireConflictPK.class)
public class OpenaireConflict implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5143344657695218270L;

	@Id
	@Column(name = "id1")
	private String id1;

	@Id
	@Column(name = "id2")
	private String id2;

	@Column(name = "reltype")
	private String relType;

	@Column(name = "idgroup")
	private String group;

	public OpenaireConflict() {}

	public OpenaireConflict(final String id1, final String id2, final String relType, final String group) {
		this.id1 = id1;
		this.id2 = id2;
		this.relType = relType;
		this.group = group;
	}

	public String getId1() {
		return id1;
	}

	public void setId1(final String id1) {
		this.id1 = id1;
	}

	public String getId2() {
		return id2;
	}

	public void setId2(final String id2) {
		this.id2 = id2;
	}

	public String getRelType() {
		return relType;
	}

	public void setRelType(final String relType) {
		this.relType = relType;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(final String group) {
		this.group = group;
	}

}
