package eu.dnetlib.organizations.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "duplicate_groups_view")
public class DuplicateGroupView implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8387253981112795514L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "city")
	private String city;

	@Column(name = "country")
	private String country;

	@Column(name = "n_duplicates")
	private long numberOfDuplicates;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	public long getNumberOfDuplicates() {
		return numberOfDuplicates;
	}

	public void setNumberOfDuplicates(final long numberOfDuplicates) {
		this.numberOfDuplicates = numberOfDuplicates;
	}

}
