package eu.dnetlib.organizations.repository.readonly;

import java.util.List;

import org.springframework.stereotype.Repository;

import eu.dnetlib.organizations.model.view.ConflictGroupView;
import eu.dnetlib.organizations.model.view.ConflictGroupViewPK;

@Repository
public interface ConflictGroupViewRepository extends ReadOnlyRepository<ConflictGroupView, ConflictGroupViewPK> {

	List<ConflictGroupView> findByCountry1OrCountry2(String country1, String country2);

}
