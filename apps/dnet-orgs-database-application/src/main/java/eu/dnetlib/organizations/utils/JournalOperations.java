package eu.dnetlib.organizations.utils;

public enum JournalOperations {
	NEW_ORG,
	NEW_SUGG_ORG,
	APPROVE_SUGG_ORG,
	EDIT_ORG,
	EDIT_SUGG_ORG,
	DUPLICATES,
	FIX_CONFLICT,
	NO_CONFLICT,
	BACKUP_ORG,
	UNKNOWN
}
