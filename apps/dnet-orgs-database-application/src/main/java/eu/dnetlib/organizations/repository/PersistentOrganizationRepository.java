package eu.dnetlib.organizations.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.dnetlib.organizations.model.PersistentOrganization;

@Repository
public interface PersistentOrganizationRepository extends JpaRepository<PersistentOrganization, String> {

}
