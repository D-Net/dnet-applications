package eu.dnetlib.organizations.controller;

import java.util.ArrayList;
import java.util.List;

import eu.dnetlib.organizations.model.view.SuggestionInfoViewByCountry;

public class SuggestionInfo {

	public class SuggestionCounter {

		private String code;

		private String desc;

		private long nDuplicates = 0;

		private long nConflicts = 0;

		private long nPendingOrgs = 0;

		public SuggestionCounter() {}

		public SuggestionCounter(final SuggestionInfoViewByCountry infoCountry) {
			this.code = infoCountry.getCode();
			this.desc = infoCountry.getName();
			this.nDuplicates = infoCountry.getnDuplicates();
			this.nConflicts = infoCountry.getnConflicts();
			this.nPendingOrgs = infoCountry.getnPendingOrgs();
		}

		public String getCode() {
			return code;
		}

		public void setCode(final String code) {
			this.code = code;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(final String desc) {
			this.desc = desc;
		}

		public long getnDuplicates() {
			return nDuplicates;
		}

		public void setnDuplicates(final long nDuplicates) {
			this.nDuplicates = nDuplicates;
		}

		public long getnConflicts() {
			return nConflicts;
		}

		public void setnConflicts(final long nConflicts) {
			this.nConflicts = nConflicts;
		}

		public long getnPendingOrgs() {
			return nPendingOrgs;
		}

		public void setnPendingOrgs(final long nPendingOrgs) {
			this.nPendingOrgs = nPendingOrgs;
		}

		public void add(final SuggestionInfoViewByCountry infoCountry) {
			nDuplicates += infoCountry.getnDuplicates();
			nConflicts += infoCountry.getnConflicts();
			nPendingOrgs += infoCountry.getnPendingOrgs();
		}

	}

	public SuggestionCounter total = new SuggestionCounter();

	public List<SuggestionCounter> byCountry = new ArrayList<>();

	public void add(final SuggestionInfoViewByCountry infoCountry) {
		byCountry.add(new SuggestionCounter(infoCountry));
		total.add(infoCountry);
	}

}
