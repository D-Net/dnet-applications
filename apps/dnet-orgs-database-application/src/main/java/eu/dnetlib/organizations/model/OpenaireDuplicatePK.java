package eu.dnetlib.organizations.model;

import java.io.Serializable;
import java.util.Objects;

public class OpenaireDuplicatePK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2546257975404466712L;

	private String localId;
	private String oaOriginalId;

	public String getLocalId() {
		return localId;
	}

	public void setLocalId(final String localId) {
		this.localId = localId;
	}

	public String getOaOriginalId() {
		return oaOriginalId;
	}

	public void setOaOriginalId(final String oaOriginalId) {
		this.oaOriginalId = oaOriginalId;
	};

	@Override
	public int hashCode() {
		return Objects.hash(localId, oaOriginalId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof OpenaireDuplicatePK)) { return false; }
		final OpenaireDuplicatePK other = (OpenaireDuplicatePK) obj;
		return Objects.equals(localId, other.localId) && Objects.equals(oaOriginalId, other.oaOriginalId);
	}

	@Override
	public String toString() {
		return String.format("OpenaireDuplicatePK [localId=%s, oaOriginalId=%s]", localId, oaOriginalId);
	}

}
