package eu.dnetlib.organizations.repository.readonly;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import eu.dnetlib.organizations.model.view.OrganizationView;

@Repository
public interface OrganizationViewRepository extends ReadOnlyRepository<OrganizationView, String> {

	Optional<OrganizationView> findByOpenaireId(String openaireId);
}
