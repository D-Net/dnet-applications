package eu.dnetlib.organizations.model.view;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

import eu.dnetlib.organizations.model.utils.OpenaireGraphNode;

@Entity
@Table(name = "organizations_view")
@TypeDefs({
	@TypeDef(name = "json", typeClass = JsonStringType.class),
	@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class OrganizationView extends OpenaireGraphNode implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7864607073631041868L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "type")
	private String type;

	@Column(name = "lat")
	private Double lat;

	@Column(name = "lng")
	private Double lng;

	@Column(name = "city")
	private String city;

	@Column(name = "country")
	private String country;

	@Type(type = "jsonb")
	@Column(name = "other_ids", columnDefinition = "jsonb")
	private Set<OtherIdentifier> otherIdentifiers;

	@Type(type = "jsonb")
	@Column(name = "other_names", columnDefinition = "jsonb")
	private Set<OtherName> otherNames;

	@Type(type = "jsonb")
	@Column(name = "acronyms", columnDefinition = "jsonb")
	private Set<String> acronyms;

	@Type(type = "jsonb")
	@Column(name = "urls", columnDefinition = "jsonb")
	private Set<String> urls;

	@Type(type = "jsonb")
	@Column(name = "relations", columnDefinition = "jsonb")
	private Set<RelationByOrg> relations;

	@Column(name = "status")
	private String status;

	@Column(name = "ec_legalbody")
	private Boolean ecLegalBody;

	@Column(name = "ec_legalperson")
	private Boolean ecLegalPerson;

	@Column(name = "ec_nonprofit")
	private Boolean ecNonProfit;

	@Column(name = "ec_researchorganization")
	private Boolean ecResearchOrganization;

	@Column(name = "ec_highereducation")
	private Boolean ecHigherEducation;

	@Column(name = "ec_internationalorganizationeurinterests")
	private Boolean ecInternationalOrganizationEurInterests;

	@Column(name = "ec_internationalorganization")
	private Boolean ecInternationalOrganization;

	@Column(name = "ec_enterprise")
	private Boolean ecEnterprise;

	@Column(name = "ec_smevalidated")
	private Boolean ecSmeValidated;

	@Column(name = "ec_nutscode")
	private Boolean ecNutscode;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(final Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(final Double lng) {
		this.lng = lng;
	}

	public String getCity() {
		return city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	public Set<OtherIdentifier> getOtherIdentifiers() {
		return otherIdentifiers;
	}

	public void setOtherIdentifiers(final Set<OtherIdentifier> otherIdentifiers) {
		this.otherIdentifiers = otherIdentifiers;
	}

	public Set<OtherName> getOtherNames() {
		return otherNames;
	}

	public void setOtherNames(final Set<OtherName> otherNames) {
		this.otherNames = otherNames;
	}

	public Set<String> getAcronyms() {
		return acronyms;
	}

	public void setAcronyms(final Set<String> acronyms) {
		this.acronyms = acronyms;
	}

	public Set<String> getUrls() {
		return urls;
	}

	public void setUrls(final Set<String> urls) {
		this.urls = urls;
	}

	public Set<RelationByOrg> getRelations() {
		return relations;
	}

	public void setRelations(final Set<RelationByOrg> relations) {
		this.relations = relations;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public Boolean getEcLegalBody() {
		return ecLegalBody;
	}

	public void setEcLegalBody(final Boolean ecLegalBody) {
		this.ecLegalBody = ecLegalBody;
	}

	public Boolean getEcLegalPerson() {
		return ecLegalPerson;
	}

	public void setEcLegalPerson(final Boolean ecLegalPerson) {
		this.ecLegalPerson = ecLegalPerson;
	}

	public Boolean getEcNonProfit() {
		return ecNonProfit;
	}

	public void setEcNonProfit(final Boolean ecNonProfit) {
		this.ecNonProfit = ecNonProfit;
	}

	public Boolean getEcResearchOrganization() {
		return ecResearchOrganization;
	}

	public void setEcResearchOrganization(final Boolean ecResearchOrganization) {
		this.ecResearchOrganization = ecResearchOrganization;
	}

	public Boolean getEcHigherEducation() {
		return ecHigherEducation;
	}

	public void setEcHigherEducation(final Boolean ecHigherEducation) {
		this.ecHigherEducation = ecHigherEducation;
	}

	public Boolean getEcInternationalOrganizationEurInterests() {
		return ecInternationalOrganizationEurInterests;
	}

	public void setEcInternationalOrganizationEurInterests(final Boolean ecInternationalOrganizationEurInterests) {
		this.ecInternationalOrganizationEurInterests = ecInternationalOrganizationEurInterests;
	}

	public Boolean getEcInternationalOrganization() {
		return ecInternationalOrganization;
	}

	public void setEcInternationalOrganization(final Boolean ecInternationalOrganization) {
		this.ecInternationalOrganization = ecInternationalOrganization;
	}

	public Boolean getEcEnterprise() {
		return ecEnterprise;
	}

	public void setEcEnterprise(final Boolean ecEnterprise) {
		this.ecEnterprise = ecEnterprise;
	}

	public Boolean getEcSmeValidated() {
		return ecSmeValidated;
	}

	public void setEcSmeValidated(final Boolean ecSmeValidated) {
		this.ecSmeValidated = ecSmeValidated;
	}

	public Boolean getEcNutscode() {
		return ecNutscode;
	}

	public void setEcNutscode(final Boolean ecNutscode) {
		this.ecNutscode = ecNutscode;
	}

}
