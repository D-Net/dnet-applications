package eu.dnetlib.organizations.model.utils;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class OpenaireGraphNode {

	@Column(name = "openaire_id")
	private String openaireId;

	@Column(name = "openaire_persistent")
	private Boolean persistent = false;

	public String getOpenaireId() {
		return openaireId;
	}

	public void setOpenaireId(final String openaireId) {
		this.openaireId = openaireId;
	}

	public Boolean getPersistent() {
		return persistent;
	}

	public void setPersistent(final Boolean persistent) {
		this.persistent = persistent;
	}

}
