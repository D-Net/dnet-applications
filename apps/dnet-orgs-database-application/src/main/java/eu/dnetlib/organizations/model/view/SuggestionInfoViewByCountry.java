package eu.dnetlib.organizations.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "suggestions_info_by_country_view")
public class SuggestionInfoViewByCountry implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6814272980951063075L;

	@Id
	@Column(name = "code")
	private String code;

	@Column(name = "name")
	private String name;

	@Column(name = "n_duplicates")
	private long nDuplicates;

	@Column(name = "n_conflicts")
	private long nConflicts;

	@Column(name = "n_pending_orgs")
	private long nPendingOrgs;

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public long getnDuplicates() {
		return nDuplicates;
	}

	public void setnDuplicates(final long nDuplicates) {
		this.nDuplicates = nDuplicates;
	}

	public long getnConflicts() {
		return nConflicts;
	}

	public void setnConflicts(final long nConflicts) {
		this.nConflicts = nConflicts;
	}

	public long getnPendingOrgs() {
		return nPendingOrgs;
	}

	public void setnPendingOrgs(final long nPendingOrgs) {
		this.nPendingOrgs = nPendingOrgs;
	}

}
