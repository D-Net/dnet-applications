package eu.dnetlib.organizations.model.view;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import eu.dnetlib.organizations.model.OpenaireDuplicatePK;
import eu.dnetlib.organizations.model.utils.OpenaireGraphNode;

@Entity
@Table(name = "oa_duplicates_view")
@IdClass(OpenaireDuplicatePK.class)
public class OpenaireDuplicateView extends OpenaireGraphNode implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2673168234221945705L;

	@Id
	@Column(name = "local_id")
	private String localId;

	@Id
	@Column(name = "oa_original_id")
	private String oaOriginalId;

	@Column(name = "oa_name")
	private String oaName;

	@Column(name = "oa_acronym")
	private String oaAcronym;

	@Column(name = "oa_country")
	private String oaCountry;

	@Column(name = "oa_url")
	private String oaUrl;

	@Type(type = "jsonb")
	@Column(name = "oa_other_ids", columnDefinition = "jsonb")
	private Set<OtherIdentifier> otherIdentifiers;

	@Column(name = "oa_collectedfrom")
	private String oaCollectedFrom;

	@Column(name = "reltype")
	private String relType;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "ec_legalbody")
	private Boolean ecLegalBody;

	@Column(name = "ec_legalperson")
	private Boolean ecLegalPerson;

	@Column(name = "ec_nonprofit")
	private Boolean ecNonProfit;

	@Column(name = "ec_researchorganization")
	private Boolean ecResearchOrganization;

	@Column(name = "ec_highereducation")
	private Boolean ecHigherEducation;

	@Column(name = "ec_internationalorganizationeurinterests")
	private Boolean ecInternationalOrganizationEurInterests;

	@Column(name = "ec_internationalorganization")
	private Boolean ecInternationalOrganization;

	@Column(name = "ec_enterprise")
	private Boolean ecEnterprise;

	@Column(name = "ec_smevalidated")
	private Boolean ecSmeValidated;

	@Column(name = "ec_nutscode")
	private Boolean ecNutscode;

	public String getLocalId() {
		return localId;
	}

	public void setLocalId(final String localId) {
		this.localId = localId;
	}

	public String getOaOriginalId() {
		return oaOriginalId;
	}

	public void setOaOriginalId(final String oaOriginalId) {
		this.oaOriginalId = oaOriginalId;
	}

	public String getOaName() {
		return oaName;
	}

	public void setOaName(final String oaName) {
		this.oaName = oaName;
	}

	public String getOaAcronym() {
		return oaAcronym;
	}

	public void setOaAcronym(final String oaAcronym) {
		this.oaAcronym = oaAcronym;
	}

	public String getOaCountry() {
		return oaCountry;
	}

	public void setOaCountry(final String oaCountry) {
		this.oaCountry = oaCountry;
	}

	public String getOaUrl() {
		return oaUrl;
	}

	public void setOaUrl(final String oaUrl) {
		this.oaUrl = oaUrl;
	}

	public Set<OtherIdentifier> getOtherIdentifiers() {
		return otherIdentifiers;
	}

	public void setOtherIdentifiers(final Set<OtherIdentifier> otherIdentifiers) {
		this.otherIdentifiers = otherIdentifiers;
	}

	public String getOaCollectedFrom() {
		return oaCollectedFrom;
	}

	public void setOaCollectedFrom(final String oaCollectedFrom) {
		this.oaCollectedFrom = oaCollectedFrom;
	}

	public String getRelType() {
		return relType;
	}

	public void setRelType(final String relType) {
		this.relType = relType;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(final String createdBy) {
		this.createdBy = createdBy;
	}

	public Boolean getEcLegalBody() {
		return ecLegalBody;
	}

	public void setEcLegalBody(final Boolean ecLegalBody) {
		this.ecLegalBody = ecLegalBody;
	}

	public Boolean getEcLegalPerson() {
		return ecLegalPerson;
	}

	public void setEcLegalPerson(final Boolean ecLegalPerson) {
		this.ecLegalPerson = ecLegalPerson;
	}

	public Boolean getEcNonProfit() {
		return ecNonProfit;
	}

	public void setEcNonProfit(final Boolean ecNonProfit) {
		this.ecNonProfit = ecNonProfit;
	}

	public Boolean getEcResearchOrganization() {
		return ecResearchOrganization;
	}

	public void setEcResearchOrganization(final Boolean ecResearchOrganization) {
		this.ecResearchOrganization = ecResearchOrganization;
	}

	public Boolean getEcHigherEducation() {
		return ecHigherEducation;
	}

	public void setEcHigherEducation(final Boolean ecHigherEducation) {
		this.ecHigherEducation = ecHigherEducation;
	}

	public Boolean getEcInternationalOrganizationEurInterests() {
		return ecInternationalOrganizationEurInterests;
	}

	public void setEcInternationalOrganizationEurInterests(final Boolean ecInternationalOrganizationEurInterests) {
		this.ecInternationalOrganizationEurInterests = ecInternationalOrganizationEurInterests;
	}

	public Boolean getEcInternationalOrganization() {
		return ecInternationalOrganization;
	}

	public void setEcInternationalOrganization(final Boolean ecInternationalOrganization) {
		this.ecInternationalOrganization = ecInternationalOrganization;
	}

	public Boolean getEcEnterprise() {
		return ecEnterprise;
	}

	public void setEcEnterprise(final Boolean ecEnterprise) {
		this.ecEnterprise = ecEnterprise;
	}

	public Boolean getEcSmeValidated() {
		return ecSmeValidated;
	}

	public void setEcSmeValidated(final Boolean ecSmeValidated) {
		this.ecSmeValidated = ecSmeValidated;
	}

	public Boolean getEcNutscode() {
		return ecNutscode;
	}

	public void setEcNutscode(final Boolean ecNutscode) {
		this.ecNutscode = ecNutscode;
	}

}
