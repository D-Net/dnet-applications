package eu.dnetlib.organizations.repository.readonly;

import org.springframework.stereotype.Repository;

import eu.dnetlib.organizations.model.view.SuggestionInfoViewByCountry;

@Repository
public interface SuggestionInfoViewByCountryRepository extends ReadOnlyRepository<SuggestionInfoViewByCountry, String> {

}
