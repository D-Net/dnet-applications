package eu.dnetlib.organizations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.organizations.model.Url;
import eu.dnetlib.organizations.model.UrlPK;

public interface UrlRepository extends JpaRepository<Url, UrlPK> {

	void deleteByOrgId(String orgId);
}
