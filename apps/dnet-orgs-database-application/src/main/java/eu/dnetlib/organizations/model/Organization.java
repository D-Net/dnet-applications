package eu.dnetlib.organizations.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "organizations")
public class Organization implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5563256467964495301L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "type")
	private String type;

	@Column(name = "lat")
	private Double lat;

	@Column(name = "lng")
	private Double lng;

	@Column(name = "city")
	private String city;

	@Column(name = "country")
	private String country;

	@Column(name = "status")
	private String status;

	@Column(name = "ec_legalbody")
	private Boolean ecLegalBody;

	@Column(name = "ec_legalperson")
	private Boolean ecLegalPerson;

	@Column(name = "ec_nonprofit")
	private Boolean ecNonProfit;

	@Column(name = "ec_researchorganization")
	private Boolean ecResearchOrganization;

	@Column(name = "ec_highereducation")
	private Boolean ecHigherEducation;

	@Column(name = "ec_internationalorganizationeurinterests")
	private Boolean ecInternationalOrganizationEurInterests;

	@Column(name = "ec_internationalorganization")
	private Boolean ecInternationalOrganization;

	@Column(name = "ec_enterprise")
	private Boolean ecEnterprise;

	@Column(name = "ec_smevalidated")
	private Boolean ecSmeValidated;

	@Column(name = "ec_nutscode")
	private Boolean ecNutscode;

	public Organization() {}

	public Organization(final String id, final String name, final String type, final Double lat, final Double lng, final String city, final String country,
		final String status, final Boolean ecLegalBody,
		final Boolean ecLegalPerson, final Boolean ecNonProfit, final Boolean ecResearchOrganization, final Boolean ecHigherEducation,
		final Boolean ecInternationalOrganizationEurInterests,
		final Boolean ecInternationalOrganization, final Boolean ecEnterprise, final Boolean ecSmeValidated, final Boolean ecNutscode) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.lat = lat;
		this.lng = lng;
		this.city = city;
		this.country = country;
		this.status = status;
		this.ecLegalBody = ecLegalBody;
		this.ecLegalPerson = ecLegalPerson;
		this.ecNonProfit = ecNonProfit;
		this.ecResearchOrganization = ecResearchOrganization;
		this.ecHigherEducation = ecHigherEducation;
		this.ecInternationalOrganizationEurInterests = ecInternationalOrganizationEurInterests;
		this.ecInternationalOrganization = ecInternationalOrganization;
		this.ecEnterprise = ecEnterprise;
		this.ecSmeValidated = ecSmeValidated;
		this.ecNutscode = ecNutscode;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(final Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(final Double lng) {
		this.lng = lng;
	}

	public String getCity() {
		return city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public Boolean getEcNutscode() {
		return ecNutscode;
	}

	public void setEcNutscode(final Boolean ecNutscode) {
		this.ecNutscode = ecNutscode;
	}

}
