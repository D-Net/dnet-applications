package eu.dnetlib.organizations.repository.readonly;

import org.springframework.stereotype.Repository;

import eu.dnetlib.organizations.model.view.OrganizationInfoView;

@Repository
public interface OrganizationInfoViewRepository extends ReadOnlyRepository<OrganizationInfoView, String> {

}
