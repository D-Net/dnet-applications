package eu.dnetlib.organizations.metrics;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class OrganizationMetrics {

	private String name;

	private String imageUrl;

	private long approvedOrgs;

	private long suggestedOrgs;

	@JsonInclude(Include.NON_ABSENT)
	private Double percentageOfApproved;

	@JsonInclude(Include.NON_ABSENT)
	private Double percentageOfApprovedOnAllApproved;

	private long orgsWithSuggestedDuplicates;

	private long potentialConflicts;

	private long nationalCurators;

	@JsonInclude(Include.NON_ABSENT)
	private Long nationalCuratorCountries;

	private Map<String, Long> types = new HashMap<>();

	@JsonInclude(Include.NON_ABSENT)
	private Map<String, OrganizationMetrics> countries;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(final String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public long getApprovedOrgs() {
		return approvedOrgs;
	}

	public void setApprovedOrgs(final long approvedOrgs) {
		this.approvedOrgs = approvedOrgs;
	}

	public long getSuggestedOrgs() {
		return suggestedOrgs;
	}

	public void setSuggestedOrgs(final long suggestedOrgs) {
		this.suggestedOrgs = suggestedOrgs;
	}

	public Double getPercentageOfApproved() {
		return percentageOfApproved;
	}

	public void setPercentageOfApproved(final Double percentageOfApproved) {
		this.percentageOfApproved = percentageOfApproved;
	}

	public Double getPercentageOfApprovedOnAllApproved() {
		return percentageOfApprovedOnAllApproved;
	}

	public void setPercentageOfApprovedOnAllApproved(final Double percentageOfApprovedOnAllApproved) {
		this.percentageOfApprovedOnAllApproved = percentageOfApprovedOnAllApproved;
	}

	public long getOrgsWithSuggestedDuplicates() {
		return orgsWithSuggestedDuplicates;
	}

	public void setOrgsWithSuggestedDuplicates(final long orgsWithSuggestedDuplicates) {
		this.orgsWithSuggestedDuplicates = orgsWithSuggestedDuplicates;
	}

	public long getPotentialConflicts() {
		return potentialConflicts;
	}

	public void setPotentialConflicts(final long potentialConflicts) {
		this.potentialConflicts = potentialConflicts;
	}

	public long getNationalCurators() {
		return nationalCurators;
	}

	public void setNationalCurators(final long nationalCurators) {
		this.nationalCurators = nationalCurators;
	}

	public Long getNationalCuratorCountries() {
		return nationalCuratorCountries;
	}

	public void setNationalCuratorCountries(final Long nationalCuratorCountries) {
		this.nationalCuratorCountries = nationalCuratorCountries;
	}

	public Map<String, Long> getTypes() {
		return types;
	}

	public void setTypes(final Map<String, Long> types) {
		this.types = types;
	}

	public Map<String, OrganizationMetrics> getCountries() {
		return countries;
	}

	public void setCountries(final Map<String, OrganizationMetrics> countries) {
		this.countries = countries;
	}

}
