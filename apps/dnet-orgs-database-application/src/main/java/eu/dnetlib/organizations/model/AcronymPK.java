package eu.dnetlib.organizations.model;

import java.io.Serializable;
import java.util.Objects;

public class AcronymPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7771532658881429064L;

	private String orgId;

	private String acronym;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(final String acronym) {
		this.acronym = acronym;
	}

	@Override
	public int hashCode() {
		return Objects.hash(acronym, orgId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof AcronymPK)) { return false; }
		final AcronymPK other = (AcronymPK) obj;
		return Objects.equals(acronym, other.acronym) && Objects.equals(orgId, other.orgId);
	}

	@Override
	public String toString() {
		return String.format("AcronymPK [orgId=%s, acronym=%s]", orgId, acronym);
	}
}
