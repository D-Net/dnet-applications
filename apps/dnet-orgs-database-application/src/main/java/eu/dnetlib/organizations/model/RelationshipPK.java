package eu.dnetlib.organizations.model;

import java.io.Serializable;
import java.util.Objects;

public class RelationshipPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6090016904833186505L;

	private String id1;

	private String id2;

	private String relType;

	public String getId1() {
		return id1;
	}

	public void setId1(final String id1) {
		this.id1 = id1;
	}

	public String getId2() {
		return id2;
	}

	public void setId2(final String id2) {
		this.id2 = id2;
	}

	public String getRelType() {
		return relType;
	}

	public void setRelType(final String relType) {
		this.relType = relType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id1, id2, relType);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof RelationshipPK)) { return false; }
		final RelationshipPK other = (RelationshipPK) obj;
		return Objects.equals(id1, other.id1) && Objects.equals(id2, other.id2) && Objects.equals(relType, other.relType);
	}

	@Override
	public String toString() {
		return String.format("RelationshipPK [id1=%s, id2=%s, relType=%s]", id1, id2, relType);
	}
}
