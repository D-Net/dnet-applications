package eu.dnetlib.organizations.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "acronyms")
@IdClass(AcronymPK.class)
public class Acronym implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1521008848879976517L;

	@Id
	@Column(name = "id")
	private String orgId;

	@Id
	@Column(name = "acronym")
	private String acronym;

	public Acronym() {}

	public Acronym(final String orgId, final String acronym) {
		this.orgId = orgId;
		this.acronym = acronym;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(final String acronym) {
		this.acronym = acronym;
	}

}
