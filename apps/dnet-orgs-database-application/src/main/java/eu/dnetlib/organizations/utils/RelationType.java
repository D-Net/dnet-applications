package eu.dnetlib.organizations.utils;

public enum RelationType {

	IsChildOf,
	IsParentOf,
	Related,
	Other,
	Merged_In,
	Merges;

	public RelationType getInverse() {
		switch (this) {
		case IsChildOf:
			return IsParentOf;
		case IsParentOf:
			return IsChildOf;
		case Related:
			return Related;
		case Merged_In:
			return Merges;
		case Merges:
			return Merged_In;
		default:
			return Other;
		}
	}
}
