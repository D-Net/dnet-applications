package eu.dnetlib.organizations.utils;

public enum OrganizationStatus {
	suggested, // from user or dedup depends by created_by field
	approved,  // normal status of valid organizations
	hidden,    // hidden organizations after the fix of a conflict
	raw  // raw organizations (their id is not an openorgs id)
}
