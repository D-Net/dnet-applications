package eu.dnetlib.organizations.model.view;

import java.io.Serializable;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import eu.dnetlib.organizations.model.utils.OpenaireGraphNode;

@Entity
@Table(name = "organizations_info_view")
public class OrganizationInfoView extends OpenaireGraphNode implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6077637367923773598L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "creation_date")
	private OffsetDateTime creationDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Column(name = "modification_date")
	private OffsetDateTime modificationDate;

	@Column(name = "n_duplicates")
	private long nDuplicates;

	@Column(name = "n_conflicts")
	private long nConflicts;

	@Column(name = "note")
	private boolean note;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(final String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(final OffsetDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(final String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public OffsetDateTime getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(final OffsetDateTime modificationDate) {
		this.modificationDate = modificationDate;
	}

	public long getnDuplicates() {
		return nDuplicates;
	}

	public void setnDuplicates(final long nDuplicates) {
		this.nDuplicates = nDuplicates;
	}

	public long getnConflicts() {
		return nConflicts;
	}

	public void setnConflicts(final long nConflicts) {
		this.nConflicts = nConflicts;
	}

	public boolean isNote() {
		return note;
	}

	public void setNote(final boolean note) {
		this.note = note;
	}

}
