package eu.dnetlib.organizations.repository.readonly;

import org.springframework.stereotype.Repository;

import eu.dnetlib.organizations.model.view.UserView;

@Repository
public interface UserViewRepository extends ReadOnlyRepository<UserView, String> {

}
