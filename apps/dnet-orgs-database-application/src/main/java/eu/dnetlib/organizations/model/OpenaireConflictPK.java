package eu.dnetlib.organizations.model;

import java.io.Serializable;
import java.util.Objects;

public class OpenaireConflictPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6752695921988906962L;

	private String id1;

	private String id2;

	public OpenaireConflictPK() {}

	public OpenaireConflictPK(final String id1, final String id2) {
		this.id1 = id1;
		this.id2 = id2;
	}

	public String getId1() {
		return id1;
	}

	public void setId1(final String id1) {
		this.id1 = id1;
	}

	public String getId2() {
		return id2;
	}

	public void setId2(final String id2) {
		this.id2 = id2;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id1, id2);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof OpenaireConflictPK)) { return false; }
		final OpenaireConflictPK other = (OpenaireConflictPK) obj;
		return Objects.equals(id1, other.id1) && Objects.equals(id2, other.id2);
	}

	@Override
	public String toString() {
		return String.format("OpenaireConflictPK [id1=%s, id2=%s]", id1, id2);
	}

}
