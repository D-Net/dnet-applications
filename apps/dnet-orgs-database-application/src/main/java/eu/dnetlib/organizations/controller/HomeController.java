package eu.dnetlib.organizations.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.common.controller.AbstractDnetController;
import eu.dnetlib.organizations.importer.ImportExecutor;
import eu.dnetlib.organizations.importer.ImportStatus;
import eu.dnetlib.organizations.model.SystemConfiguration;
import eu.dnetlib.organizations.repository.SystemConfigurationRepository;

@Controller
public class HomeController extends AbstractDnetController {

	@Autowired
	private Environment env;

	@Autowired
	private ImportExecutor importExecutor;

	@Autowired
	private SystemConfigurationRepository systemConfigurationRepository;

	@Value("${openaire.explore.organization.baseurl}")
	private String openaireBaseUrl;

	@Value("${openorgs.support.pages}")
	private String supportPagesJson;

	private Map<String, String> supportPages;

	@GetMapping("/")
	public String home() {
		return env.acceptsProfiles(Profiles.of("dev")) ? "redirect:main" : "home";
	}

	@GetMapping("/redirect/oa/{orgId}")
	public String openaireUrl(@PathVariable final String orgId) {
		return "redirect:" + String.format(openaireBaseUrl, orgId);
	}

	@GetMapping("/main")
	public String main() {
		return "main";
	}

	@GetMapping("/login")
	public String login() {
		return "login";
	}

	@GetMapping("/authorizationRequest")
	public String authorizationRequest() {
		return "authorizationRequest";
	}

	@GetMapping("/alreadyRegistered")
	public String alreadyRegistered() {
		return "alreadyRegistered";
	}

	@ModelAttribute("email")
	public String getUserEmail(final Authentication authentication) {
		return authentication != null ? UserInfo.getEmail(authentication) : null;
	}

	@ModelAttribute("role")
	public UserRole getUserRole(final Authentication authentication) {
		return authentication != null ? UserInfo.findRole(authentication) : null;
	}

	@ModelAttribute("fullname")
	public String getUserFullname(final Authentication authentication) {
		return authentication != null ? UserInfo.getFullname(authentication) : "unknown";
	}

	@ModelAttribute("organization")
	public String getUserOrganization(final Authentication authentication) {
		return authentication != null ? UserInfo.getOrganization(authentication) : "unknown";
	}

	@ModelAttribute("sysconf")
	public SystemConfiguration getSysConf(final Authentication authentication) {
		return systemConfigurationRepository.findById(SystemConfiguration.DEFAULT_ID).get();
	}

	@ModelAttribute("isImportRunning")
	public boolean isImportRunning(final Authentication authentication) {
		return importExecutor.getLastImportExecution().getStatus() == ImportStatus.RUNNING;
	}

	@ModelAttribute("supportPages")
	public Map<String, String> supportPages(final Authentication authentication) throws JsonMappingException, JsonProcessingException {
		if (supportPages == null) {
			final ObjectMapper mapper = new ObjectMapper();
			final TypeReference<LinkedHashMap<String, String>> typeRef = new TypeReference<LinkedHashMap<String, String>>() {};
			supportPages = mapper.readValue(supportPagesJson, typeRef);
		}
		return supportPages;
	}

	@GetMapping({
			"apidoc", "api-doc", "/doc", "/swagger"
	})
	public String apiDoc() {
		return "redirect:swagger-ui/index.html";
	}

}
