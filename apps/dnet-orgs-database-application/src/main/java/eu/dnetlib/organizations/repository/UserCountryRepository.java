package eu.dnetlib.organizations.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import eu.dnetlib.organizations.model.UserCountry;
import eu.dnetlib.organizations.model.UserCountryPK;

public interface UserCountryRepository extends JpaRepository<UserCountry, UserCountryPK> {

	void deleteByEmail(String email);

	@Query(value = "select country from user_countries where email = ?1", nativeQuery = true)
	List<String> getCountriesForUser(String email);

	@Query(value = "select count(o.country) > 0 from organizations o left outer join user_countries uc on (o.country = uc.country) where o.id = ?1 and uc.email = ?2", nativeQuery = true)
	boolean verifyAuthorizationForId(String id, String user);

	@Query(value = "select count(country) > 0 from user_countries  where country = ?1 and email = ?2", nativeQuery = true)
	boolean verifyAuthorizationForCountry(String country, String user);

}
