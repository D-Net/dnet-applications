package eu.dnetlib.organizations.model.utils;

import java.io.Serializable;

public class CountryTypeCountEntry implements Serializable {

	private static final long serialVersionUID = 7191811800787531070L;
	private String country;
	private String type;
	private Long count;

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(final Long count) {
		this.count = count;
	}
}
