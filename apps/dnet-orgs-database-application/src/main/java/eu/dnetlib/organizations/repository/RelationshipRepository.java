package eu.dnetlib.organizations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.organizations.model.Relationship;
import eu.dnetlib.organizations.model.RelationshipPK;

public interface RelationshipRepository extends JpaRepository<Relationship, RelationshipPK> {

	void deleteById1(String id1);

	void deleteById2(String id2);
}
