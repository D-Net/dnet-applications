package eu.dnetlib.organizations.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "other_ids")
@IdClass(OtherIdentifierPK.class)
public class OtherIdentifier implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8290245353090885241L;

	@Id
	@Column(name = "id")
	private String orgId;

	@Id
	@Column(name = "otherid")
	private String otherId;

	@Id
	@Column(name = "type")
	private String type;

	public OtherIdentifier() {}

	public OtherIdentifier(final String orgId, final String otherId, final String type) {
		this.orgId = orgId;
		this.otherId = otherId;
		this.type = type;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getOtherId() {
		return otherId;
	}

	public void setOtherId(final String otherId) {
		this.otherId = otherId;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

}
