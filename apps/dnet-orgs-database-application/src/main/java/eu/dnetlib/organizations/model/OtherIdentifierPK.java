package eu.dnetlib.organizations.model;

import java.io.Serializable;
import java.util.Objects;

public class OtherIdentifierPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3525128185006173748L;

	private String orgId;
	private String otherId;
	private String type;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getOtherId() {
		return otherId;
	}

	public void setOtherId(final String otherId) {
		this.otherId = otherId;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		return Objects.hash(orgId, otherId, type);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof OtherIdentifierPK)) { return false; }
		final OtherIdentifierPK other = (OtherIdentifierPK) obj;
		return Objects.equals(orgId, other.orgId) && Objects.equals(otherId, other.otherId) && Objects.equals(type, other.type);
	}

	@Override
	public String toString() {
		return String.format("OtherIdentifierPK [orgId=%s, otherId=%s, type=%s]", orgId, otherId, type);
	}

}
