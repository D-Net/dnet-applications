package eu.dnetlib.organizations.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.organizations.model.JournalEntry;

public interface JournalEntryRepository extends JpaRepository<JournalEntry, Long> {

	public List<JournalEntry> findByOrgIdOrderByDateDesc(String orgId);

}
