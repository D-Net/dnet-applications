package eu.dnetlib.organizations.utils;

public enum SimilarityType {
	suggested,
	is_similar,
	is_different
}
