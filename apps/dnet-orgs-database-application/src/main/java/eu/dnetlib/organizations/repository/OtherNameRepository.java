package eu.dnetlib.organizations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.organizations.model.OtherName;
import eu.dnetlib.organizations.model.OtherNamePK;

public interface OtherNameRepository extends JpaRepository<OtherName, OtherNamePK> {

	void deleteByOrgId(String orgId);
}
