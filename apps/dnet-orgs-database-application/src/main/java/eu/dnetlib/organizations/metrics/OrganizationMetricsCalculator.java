package eu.dnetlib.organizations.metrics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import eu.dnetlib.common.metrics.MetricsCalculator;
import eu.dnetlib.organizations.model.view.SuggestionInfoViewByCountry;
import eu.dnetlib.organizations.model.view.UserView;
import eu.dnetlib.organizations.repository.readonly.SuggestionInfoViewByCountryRepository;
import eu.dnetlib.organizations.repository.readonly.UserViewRepository;
import eu.dnetlib.organizations.utils.DatabaseUtils;
import eu.dnetlib.organizations.utils.DatabaseUtils.VocabularyTable;
import io.prometheus.client.Collector.MetricFamilySamples;
import io.prometheus.client.GaugeMetricFamily;

@Component
public class OrganizationMetricsCalculator implements MetricsCalculator {

	@Value("${server.public_url}")
	private String publicUrl;

	@Autowired
	private UserViewRepository userViewRepository;

	@Autowired
	private DatabaseUtils dbUtils;

	@Autowired
	private SuggestionInfoViewByCountryRepository suggestionInfoViewByCountryRepository;

	private static final Log log = LogFactory.getLog(OrganizationMetricsCalculator.class);

	@Override
	public List<MetricFamilySamples> getMetrics() {

		final List<MetricFamilySamples> list = new ArrayList<>();

		final OrganizationMetrics m = prepareMetrics();

		list.add(new GaugeMetricFamily("openorgs_approved_organizations", "approved organisations", m.getApprovedOrgs()));
		list.add(new GaugeMetricFamily("openorgs_suggested_organizations", "suggested organisations", m.getSuggestedOrgs()));
		list.add(new GaugeMetricFamily("openorgs_with_duplicates_orgs", "orgs with new duplicates", m.getOrgsWithSuggestedDuplicates()));
		list.add(new GaugeMetricFamily("openorgs_potential_conflicts", "potential conflicts", m.getPotentialConflicts()));
		list.add(new GaugeMetricFamily("openorgs_national_curators", "Number of curators", m.getNationalCurators()));
		list.add(new GaugeMetricFamily("openorgs_national_curators_countries", "Number of collaborating countries", m.getNationalCuratorCountries()));

		m.getTypes()
				.forEach((k, v) -> list.add(new GaugeMetricFamily(
						"openorgs_approved_organizations_type_" + k.toLowerCase(),
						"Number of approved organisations by type (" + k + ")",
						v)));

		m.getCountries()
				.forEach((k, cm) -> list.add(new GaugeMetricFamily(
						"openorgs_approved_organizations_country_" + k.toLowerCase(),
						"Number of approved organisations by country (" + cm.getName() + ")",
						cm.getApprovedOrgs())));

		return list;

	}

	@Cacheable(value = "metrics", key = "'metrics'")
	public OrganizationMetrics prepareMetrics() {

		log.debug("Preparing metrics");

		long nUsers = 0;
		long nApproved = 0;
		long nSuggested = 0;
		long nDups = 0;
		long nConflicts = 0;
		final Map<String, Long> types = new LinkedHashMap<String, Long>();
		final Set<String> countriesWithUsers = new HashSet<>();
		final Map<String, OrganizationMetrics> countryMetrics = new LinkedHashMap<>();

		// Prepare the metrics by country
		dbUtils.listValuesOfVocabularyTable(VocabularyTable.countries).forEach(entry -> {
			final OrganizationMetrics m = new OrganizationMetrics();
			m.setName(entry.getName());
			m.setImageUrl(publicUrl + "/resources/images/flags/" + entry.getValue().toLowerCase() + ".svg");
			countryMetrics.put(entry.getValue(), m);
		});

		// Find approved and suggested by country
		dbUtils.browseCountries().forEach(entry -> {
			final OrganizationMetrics m = countryMetrics.get(entry.getCode());
			m.setApprovedOrgs(entry.getValues().getOrDefault("approved", 0L));
			m.setSuggestedOrgs(entry.getValues().getOrDefault("suggested", 0L));
		});

		// Find users by country
		for (final UserView u : userViewRepository.findAll()) {
			if (u.isValid()) {
				nUsers += 1;
			}
			for (final String country : u.getCountries()) {
				if (countryMetrics.containsKey(country)) {
					countryMetrics.get(country).setNationalCurators(countryMetrics.get(country).getNationalCurators() + 1);
				}
			}
		}

		// Find suggestions types by country
		for (final SuggestionInfoViewByCountry info : suggestionInfoViewByCountryRepository.findAll()) {
			final String country = info.getCode();
			countryMetrics.get(country).setSuggestedOrgs(info.getnPendingOrgs());
			countryMetrics.get(country).setOrgsWithSuggestedDuplicates(info.getnDuplicates());
			countryMetrics.get(country).setPotentialConflicts(info.getnConflicts());
		}

		// Find organization types by country
		for (final Map.Entry<String, Map<String, Long>> e : dbUtils.countValidOrgsByTypesAndCountry().entrySet()) {
			final String country = e.getKey();
			countryMetrics.get(country).setTypes(e.getValue());
		}

		// Calculate totals
		for (final OrganizationMetrics mc : countryMetrics.values()) {
			nApproved += mc.getApprovedOrgs();
			nSuggested += mc.getSuggestedOrgs();
			nDups += mc.getOrgsWithSuggestedDuplicates();
			nConflicts += mc.getPotentialConflicts();

			if (mc.getNationalCurators() > 0) {
				countriesWithUsers.add(mc.getName());
			}

			mc.getTypes().forEach((k, v) -> {
				if (types.containsKey(k)) {
					types.put(k, types.get(k) + v);
				} else {
					types.put(k, v);
				}
			});
		}

		// Update the percentages By country
		for (final OrganizationMetrics mc : countryMetrics.values()) {
			mc.setPercentageOfApproved(mc.getApprovedOrgs() * 100.0 / (mc.getApprovedOrgs() + mc.getSuggestedOrgs()));
			mc.setPercentageOfApprovedOnAllApproved(mc.getApprovedOrgs() * 100.0 / nApproved);
		}

		final OrganizationMetrics m = new OrganizationMetrics();
		m.setName("OpenOrgs Metrics");
		m.setImageUrl(publicUrl + "/resources/images/openorgs_logo.png");
		m.setApprovedOrgs(nApproved);
		m.setSuggestedOrgs(nSuggested);
		m.setPercentageOfApproved(nApproved * 100.0 / (nApproved + nSuggested));
		m.setOrgsWithSuggestedDuplicates(nDups);
		m.setPotentialConflicts(nConflicts);
		m.setNationalCurators(nUsers);
		m.setNationalCuratorCountries(Long.valueOf(countriesWithUsers.size()));
		m.setCountries(countryMetrics);
		m.setTypes(types);

		return m;
	}

	@CacheEvict(value = "metrics", allEntries = true)
	@Scheduled(fixedRate = 3600000) // One Hour
	public void clearCache() {
		log.debug("Metrics cache cleaned");
	}

}
