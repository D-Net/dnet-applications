package eu.dnetlib.organizations.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -9062674086098391604L;

	@Id
	@Column(name = "email")
	private String email;

	@Column(name = "fullname")
	private String fullname;

	@Column(name = "organization")
	private String organization;

	@Column(name = "reference_person")
	private String referencePerson;

	@Column(name = "request_message")
	private String requestMessage;

	@Column(name = "valid")
	private boolean valid;

	@Column(name = "role")
	private String role;

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(final String fullname) {
		this.fullname = fullname;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(final String organization) {
		this.organization = organization;
	}

	public String getReferencePerson() {
		return referencePerson;
	}

	public void setReferencePerson(final String referencePerson) {
		this.referencePerson = referencePerson;
	}

	public String getRequestMessage() {
		return requestMessage;
	}

	public void setRequestMessage(final String requestMessage) {
		this.requestMessage = requestMessage;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(final boolean valid) {
		this.valid = valid;
	}

	public String getRole() {
		return role;
	}

	public void setRole(final String role) {
		this.role = role;
	}
}
