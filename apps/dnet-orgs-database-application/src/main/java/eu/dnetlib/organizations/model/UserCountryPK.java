package eu.dnetlib.organizations.model;

import java.io.Serializable;
import java.util.Objects;

public class UserCountryPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2003686130380208901L;

	private String email;

	private String country;

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	@Override
	public int hashCode() {
		return Objects.hash(country, email);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof UserCountryPK)) { return false; }
		final UserCountryPK other = (UserCountryPK) obj;
		return Objects.equals(country, other.country) && Objects.equals(email, other.email);
	}

	@Override
	public String toString() {
		return String.format("UserCountryPK [email=%s, country=%s]", email, country);
	}

}
