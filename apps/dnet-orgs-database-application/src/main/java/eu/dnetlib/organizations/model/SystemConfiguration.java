package eu.dnetlib.organizations.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sysconf")
public class SystemConfiguration implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7935575713678578023L;

	public static final String DEFAULT_ID = "default";

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "title")
	private String title;

	@Column(name = "homepage_msg")
	private String homepageMessage;

	@Column(name = "homepage_msg_import")
	private String homepageMessageImport;

	@Column(name = "readonly")
	private Boolean readonly;

	@Column(name = "smtp_enabled")
	private Boolean smtpEnabled;

	@Column(name = "smtp_host")
	private String smtpHost;

	@Column(name = "smtp_port")
	private Integer smtpPort;

	@Column(name = "smtp_user")
	private String smtpUser;

	@Column(name = "smtp_password")
	private String smtpPassword;

	@Column(name = "smtp_from_mail")
	private String smtpFromMail;

	@Column(name = "smtp_from_name")
	private String smtpFromName;

	@Column(name = "smtp_to_mail_admin")
	private String smtpToMailAdmin;

	@Column(name = "smtp_new_user_message")
	private String smtpNewUserMessage;

	@Column(name = "smtp_update_user_message")
	private String smtpUpdateUserMessage;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getHomepageMessage() {
		return homepageMessage;
	}

	public void setHomepageMessage(final String homepageMessage) {
		this.homepageMessage = homepageMessage;
	}

	public String getHomepageMessageImport() {
		return homepageMessageImport;
	}

	public void setHomepageMessageImport(final String homepageMessageImport) {
		this.homepageMessageImport = homepageMessageImport;
	}

	public Boolean getReadonly() {
		return readonly;
	}

	public void setReadonly(final Boolean readonly) {
		this.readonly = readonly;
	}

	public Boolean getSmtpEnabled() {
		return smtpEnabled;
	}

	public void setSmtpEnabled(final Boolean smtpEnabled) {
		this.smtpEnabled = smtpEnabled;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(final String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public Integer getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(final Integer smtpPort) {
		this.smtpPort = smtpPort;
	}

	public String getSmtpUser() {
		return smtpUser;
	}

	public void setSmtpUser(final String smtpUser) {
		this.smtpUser = smtpUser;
	}

	public String getSmtpPassword() {
		return smtpPassword;
	}

	public void setSmtpPassword(final String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}

	public String getSmtpFromMail() {
		return smtpFromMail;
	}

	public void setSmtpFromMail(final String smtpFromMail) {
		this.smtpFromMail = smtpFromMail;
	}

	public String getSmtpFromName() {
		return smtpFromName;
	}

	public void setSmtpFromName(final String smtpFromName) {
		this.smtpFromName = smtpFromName;
	}

	public String getSmtpToMailAdmin() {
		return smtpToMailAdmin;
	}

	public void setSmtpToMailAdmin(final String smtpToMailAdmin) {
		this.smtpToMailAdmin = smtpToMailAdmin;
	}

	public String getSmtpNewUserMessage() {
		return smtpNewUserMessage;
	}

	public void setSmtpNewUserMessage(final String smtpNewUserMessage) {
		this.smtpNewUserMessage = smtpNewUserMessage;
	}

	public String getSmtpUpdateUserMessage() {
		return smtpUpdateUserMessage;
	}

	public void setSmtpUpdateUserMessage(final String smtpUpdateUserMessage) {
		this.smtpUpdateUserMessage = smtpUpdateUserMessage;
	}

}
