package eu.dnetlib.organizations.model;

import java.io.Serializable;
import java.util.Objects;

public class UrlPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6459755341474103359L;

	private String orgId;

	private String url;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	@Override
	public int hashCode() {
		return Objects.hash(orgId, url);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof UrlPK)) { return false; }
		final UrlPK other = (UrlPK) obj;
		return Objects.equals(orgId, other.orgId) && Objects.equals(url, other.url);
	}

	@Override
	public String toString() {
		return String.format("UrlPK [orgId=%s, urls=%s]", orgId, url);
	}
}
