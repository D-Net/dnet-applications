package eu.dnetlib.organizations.utils;

import java.io.StringWriter;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import eu.dnetlib.organizations.controller.UserRole;
import eu.dnetlib.organizations.model.Acronym;
import eu.dnetlib.organizations.model.JournalEntry;
import eu.dnetlib.organizations.model.OpenaireConflictPK;
import eu.dnetlib.organizations.model.OpenaireDuplicate;
import eu.dnetlib.organizations.model.Organization;
import eu.dnetlib.organizations.model.OtherIdentifier;
import eu.dnetlib.organizations.model.OtherName;
import eu.dnetlib.organizations.model.PersistentOrganization;
import eu.dnetlib.organizations.model.Relationship;
import eu.dnetlib.organizations.model.Url;
import eu.dnetlib.organizations.model.User;
import eu.dnetlib.organizations.model.UserCountry;
import eu.dnetlib.organizations.model.utils.BrowseEntry;
import eu.dnetlib.organizations.model.utils.CountryTypeCountEntry;
import eu.dnetlib.organizations.model.utils.OrganizationConflict;
import eu.dnetlib.organizations.model.utils.TempBrowseEntry;
import eu.dnetlib.organizations.model.utils.VocabularyTerm;
import eu.dnetlib.organizations.model.view.OrganizationView;
import eu.dnetlib.organizations.model.view.PersistentOrganizationView;
import eu.dnetlib.organizations.model.view.UserView;
import eu.dnetlib.organizations.repository.AcronymRepository;
import eu.dnetlib.organizations.repository.JournalEntryRepository;
import eu.dnetlib.organizations.repository.OpenaireConflictRepository;
import eu.dnetlib.organizations.repository.OpenaireDuplicateRepository;
import eu.dnetlib.organizations.repository.OrganizationRepository;
import eu.dnetlib.organizations.repository.OtherIdentifierRepository;
import eu.dnetlib.organizations.repository.OtherNameRepository;
import eu.dnetlib.organizations.repository.PersistentOrganizationRepository;
import eu.dnetlib.organizations.repository.RelationshipRepository;
import eu.dnetlib.organizations.repository.UrlRepository;
import eu.dnetlib.organizations.repository.UserCountryRepository;
import eu.dnetlib.organizations.repository.UserRepository;
import eu.dnetlib.organizations.repository.readonly.OrganizationViewRepository;
import eu.dnetlib.organizations.repository.readonly.PersistentOrganizationViewRepository;

@Component
public class DatabaseUtils {

	@Autowired
	private AcronymRepository acronymRepository;
	@Autowired
	private OrganizationRepository organizationRepository;
	@Autowired
	private OtherIdentifierRepository otherIdentifierRepository;
	@Autowired
	private OtherNameRepository otherNameRepository;
	@Autowired
	private UrlRepository urlRepository;
	@Autowired
	private RelationshipRepository relationshipRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserCountryRepository userCountryRepository;
	@Autowired
	private OpenaireConflictRepository openaireConflictRepository;
	@Autowired
	private OpenaireDuplicateRepository openaireDuplicateRepository;
	@Autowired
	private OrganizationViewRepository organizationViewRepository;
	@Autowired
	private JournalEntryRepository journalEntryRepository;
	@Autowired
	private PersistentOrganizationRepository persistentOrganizationRepository;
	@Autowired
	private PersistentOrganizationViewRepository persistentOrganizationViewRepository;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Log log = LogFactory.getLog(DatabaseUtils.class);

	public enum VocabularyTable {
		languages, countries, org_types, id_types, rel_types, simrel_types
	}

	@Transactional
	public String insertOrUpdateOrganization(final OrganizationView orgView, final String user, final boolean isSimpleUser) {

		final String oldId = StringUtils.isNotBlank(orgView.getId()) ? new String(orgView.getId()) : null;

		final String oldStatus = oldId != null ? organizationRepository.findById(oldId)
				.map(Organization::getStatus)
				.orElse(null) : null;

		final boolean alreadyApproved = StringUtils.equals(oldStatus, OrganizationStatus.approved.toString());

		final String newStatus;
		if (!isSimpleUser) { // IS ADMIN
			newStatus = OrganizationStatus.approved.toString();
		} else if (isSimpleUser && oldStatus == null) {
			newStatus = OrganizationStatus.suggested.toString();
		} else if (isSimpleUser && alreadyApproved) {
			newStatus = OrganizationStatus.approved.toString();
		} else {
			throw new RuntimeException("User not authorized");
		}

		if (oldId == null || !oldId.startsWith(OpenOrgsConstants.OPENORGS_PREFIX)) {
			if (isSimpleUser) {
				final String pendingId = OpenOrgsConstants.OPENORGS_PENDING_PREFIX + UUID.randomUUID();
				orgView.setId(pendingId);
				// to override the generation strategy of the ID
				organizationRepository.prepareOrgWithId(pendingId);
			} else {
				orgView.setId(null); // The ID is generated by the DB
			}
		}

		final Organization org = new Organization(orgView.getId(),
				orgView.getName(),
				orgView.getType(),
				orgView.getLat(), orgView.getLng(),
				orgView.getCity(), orgView.getCountry(),
				newStatus,
				orgView.getEcLegalBody(),
				orgView.getEcLegalPerson(), orgView.getEcNonProfit(), orgView.getEcResearchOrganization(), orgView.getEcHigherEducation(),
				orgView.getEcInternationalOrganizationEurInterests(), orgView.getEcInternationalOrganization(), orgView.getEcEnterprise(),
				orgView.getEcSmeValidated(), orgView.getEcNutscode());

		final String newId = organizationRepository.save(org).getId();

		final OffsetDateTime now = OffsetDateTime.now();

		if (StringUtils.equals(newId, oldId)) {
			makeRelations(newId, orgView, true);
		} else {
			organizationRepository.updateCreationDate(newId, user, now);
			makeRelations(newId, orgView, false);
			if (oldId != null) {

				final List<OpenaireDuplicate> dups = openaireDuplicateRepository.findByLocalId(oldId)
						.stream()
						.map(d -> prepareNewDuplicate(newId, oldId, d))
						.collect(Collectors.toList());

				openaireDuplicateRepository.saveAll(dups);

				dups.forEach(d -> {
					openaireDuplicateRepository.updateCreatedByIfMissing(d.getLocalId(), d.getOaOriginalId(), user);
					openaireDuplicateRepository.updateModificationDate(d.getLocalId(), d.getOaOriginalId(), user, now);
				});

				if (oldId.startsWith(OpenOrgsConstants.OPENORGS_PENDING_PREFIX)) {
					organizationRepository.deleteById(oldId);
				}
			}
		}

		organizationRepository.updateModificationDate(newId, user, now);

		JournalOperations op = JournalOperations.UNKNOWN;
		String message = "-";

		if (newStatus.equals(OrganizationStatus.suggested.toString())) {
			if (oldStatus == null) {
				op = JournalOperations.NEW_SUGG_ORG;
				message = "Created a new suggested org";
			} else {
				op = JournalOperations.EDIT_SUGG_ORG;
				message = "Metadata updated";
			}
		} else if (newStatus.equals(OrganizationStatus.approved.toString())) {
			if (oldStatus == null) {
				op = JournalOperations.NEW_ORG;
				message = "Created a new organization";
			} else if (oldStatus.equals(OrganizationStatus.suggested.toString())) {
				op = JournalOperations.APPROVE_SUGG_ORG;
				message = "Approved the suggested org: " + oldId;

			} else {
				op = JournalOperations.EDIT_ORG;
				message = "Metadata updated";
			}
		} else {
			// IMPOSSIBLE ???
		}

		journalEntryRepository.save(new JournalEntry(newId, op, message, user));

		return newId;
	}

	private OpenaireDuplicate prepareNewDuplicate(final String newId, final String oldId, final OpenaireDuplicate old) {
		final OpenaireDuplicate d = new OpenaireDuplicate();

		d.setLocalId(newId);
		d.setOaOriginalId(old.getOaOriginalId());
		d.setOaCollectedFrom(old.getOaCollectedFrom());

		if (oldId != null
				&& newId.startsWith(OpenOrgsConstants.OPENORGS_PREFIX)
				&& oldId.startsWith(OpenOrgsConstants.OPENORGS_PENDING_PREFIX)
				&& StringUtils.substringAfter(oldId, OpenOrgsConstants.OPENORGS_PENDING_PREFIX).equalsIgnoreCase(DigestUtils.md5Hex(d.getOaOriginalId()))) {
			d.setRelType(SimilarityType.is_similar.toString());
		} else {
			d.setRelType(SimilarityType.suggested.toString());
		}
		return d;

	}

	@Transactional
	public void saveDuplicates(final List<OpenaireDuplicate> simrels, final String user) {
		final OffsetDateTime now = OffsetDateTime.now();

		// Set is_different all the existing relations that refer to the new duplicates (suggested or is_similar)

		final List<OpenaireDuplicate> toSave = new ArrayList<>();
		toSave.addAll(simrels);

		final List<OpenaireDuplicate> toDelete = new ArrayList<>();

		for (final OpenaireDuplicate r1 : simrels) {
			if (!r1.getRelType().equals(SimilarityType.is_different.toString())) {
				for (final OpenaireDuplicate r2 : openaireDuplicateRepository.findByOaOriginalId(r1.getOaOriginalId())) {
					if (r2.getLocalId().startsWith(OpenOrgsConstants.OPENORGS_PENDING_PREFIX)) {
						toDelete.add(r2);
					} else if (!r1.getLocalId().equals(r2.getLocalId())) {
						r2.setRelType(SimilarityType.is_different.toString());
						toSave.add(r2);
					}
				}
			}
		}

		// Save the new rels
		openaireDuplicateRepository.saveAll(toSave).forEach(d -> {
			openaireDuplicateRepository.updateCreatedByIfMissing(d.getLocalId(), d.getOaOriginalId(), user);
			openaireDuplicateRepository.updateModificationDate(d.getLocalId(), d.getOaOriginalId(), user, now);
		});
		log.debug("Simrels saved (contains also the fixed rels): " + toSave.size());

		// delete rels to pending orgs
		openaireDuplicateRepository.deleteAll(toDelete);
		log.debug("Simrels related to a pending orgs deleted: " + toDelete.size());

		// Updating journal
		toSave.stream().collect(Collectors.groupingBy(OpenaireDuplicate::getLocalId)).forEach((id, list) -> {
			final long sim = list.stream()
					.filter(d -> d.getRelType().equals(SimilarityType.is_similar.toString()))
					.count();

			final long diff = list.stream()
					.filter(d -> d.getRelType().equals(SimilarityType.is_different.toString()))
					.count();

			final long sugg = list.stream()
					.filter(d -> d.getRelType().equals(SimilarityType.suggested.toString()))
					.count();

			final String message = String.format("Duplicates updated (%s similars, %s differents, %s suggested)", sim, diff, sugg);

			journalEntryRepository.save(new JournalEntry(id, JournalOperations.DUPLICATES, message, user));
		});;
	}

	@Scheduled(fixedRate = 300000)
	public void verifyConsistency() {

		log.debug("Verify consistency (START)");

		// delete invalid pending orgs (without simrels)
		final int n = jdbcTemplate
				.update("delete from organizations where id in (select o.id from organizations o left outer join oa_duplicates d on (o.id = d.local_id) where o.status = 'suggested' and o.created_by = 'dedupWf' group by o.id having count(d.local_id) = 0)");

		if (n > 0) {
			log.info("Invalid pending orgs deleted: " + n);
		}

		log.debug("Verify consistency (END)");

	}

	private void makeRelations(final String orgId, final OrganizationView orgView, final boolean update) {
		if (update) {
			acronymRepository.deleteByOrgId(orgId);
			otherNameRepository.deleteByOrgId(orgId);
			otherIdentifierRepository.deleteByOrgId(orgId);
			urlRepository.deleteByOrgId(orgId);
			relationshipRepository.deleteById1(orgId);
			relationshipRepository.deleteById2(orgId);
		}
		orgView.getAcronyms().forEach(s -> acronymRepository.save(new Acronym(orgId, s)));
		orgView.getOtherNames().forEach(n -> otherNameRepository.save(new OtherName(orgId, n.getName(), n.getLang())));
		orgView.getOtherIdentifiers().forEach(id -> otherIdentifierRepository.save(new OtherIdentifier(orgId, id.getId(), id.getType())));
		orgView.getUrls().forEach(u -> urlRepository.save(new Url(orgId, u)));
		orgView.getRelations().forEach(r -> makeRelation(orgId, r.getRelatedOrgId(), RelationType.valueOf(r.getType())));
	}

	@Cacheable("vocs")
	public List<VocabularyTerm> listValuesOfVocabularyTable(final VocabularyTable table) {
		final String sql = "select val as value, name as name from " + table + " order by name";
		return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(VocabularyTerm.class));
	}

	@Cacheable("countries_for_user")
	public List<VocabularyTerm> listCountriesForUser(final String name) {
		final String sql =
				"select uc.country as value, c.name as name from user_countries uc left outer join countries c on (c.val = uc.country) where uc.email = ? order by c.name";
		return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(VocabularyTerm.class), name);
	}

	@CacheEvict(value = {
			"vocs", "countries_for_user"
	}, allEntries = true)
	public void clearCache() {
		log.info("DB caches cleaned");
	}

	@Transactional
	public void updateUser(@RequestBody final UserView userView) {
		final User user = userRepository.findById(userView.getEmail()).orElseThrow(() -> new RuntimeException("User not found"));
		user.setFullname(userView.getFullname());
		user.setOrganization(userView.getOrganization());
		user.setReferencePerson(userView.getReferencePerson());
		user.setRequestMessage(userView.getRequestMessage());
		user.setRole(userView.getRole());
		user.setValid(userView.isValid());
		userRepository.save(user);
		userCountryRepository.deleteByEmail(userView.getEmail());
		if (userView.getCountries() != null) {
			userCountryRepository
					.saveAll(Arrays.stream(userView.getCountries()).map(c -> new UserCountry(userView.getEmail(), c)).collect(Collectors.toList()));
		}
	}

	@Transactional
	public void deleteUser(final String email) {
		userCountryRepository.deleteByEmail(email);
		userRepository.deleteById(email);
	}

	@Transactional
	public void newUser(final String email,
			final String fullname,
			final String organization,
			final String referencePerson,
			final String requestMessage,
			final List<String> countries) {

		final User user = new User();
		user.setEmail(email);
		user.setFullname(fullname);
		user.setOrganization(organization);
		user.setReferencePerson(referencePerson);
		user.setRequestMessage(requestMessage);
		user.setRole(UserRole.PENDING.name());
		user.setValid(false);

		userRepository.save(user);

		if (countries != null) {
			userCountryRepository.saveAll(countries.stream().map(c -> new UserCountry(email, c)).collect(Collectors.toList()));
		}
	}

	@Transactional
	public List<Relationship> makeRelation(final String id1, final String id2, final RelationType type) {
		final Relationship r1 = new Relationship(id1, id2, type.toString());
		final Relationship r2 = new Relationship(id2, id1, type.getInverse().toString());
		relationshipRepository.save(r1);
		relationshipRepository.save(r2);
		return Arrays.asList(r1, r2);
	}

	// BROWSE BY COUNTRY
	public List<BrowseEntry> browseCountries() {
		final String sql =
				"select o.country as code, c.name as name, o.status as group, count(o.status) as count from organizations o left outer join countries c on (o.country = c.val) group by o.country, c.name, o.status";
		return listBrowseEntries(sql);
	}

	// BROWSE BY COUNTRY FOR USER
	public List<BrowseEntry> browseCountriesForUser(final String email) {
		final String sql =
				"select o.country as code, c.name as name, o.status as group, count(o.status) as count from user_countries uc left outer join organizations o on (uc.country = o.country) left outer join countries c on (o.country = c.val) where uc.email=? group by o.country, c.name, o.status";
		return listBrowseEntries(sql, email);
	}

	// BROWSE BY ORG TYPE
	public List<BrowseEntry> browseTypes() {
		final String sql =
				"select type as code, type as name, status as group, count(status) as count from organizations group by type, status";
		return listBrowseEntries(sql);
	}

	// BROWSE BY ORG TYPE FOR USER
	public List<BrowseEntry> browseTypesForUser(final String email) {
		final String sql = "select o.type as code, o.type as name,"
				+ "o.status as group, count(o.status) as count "
				+ "from organizations o "
				+ "left outer join user_countries uc on (uc.country = o.country) "
				+ "where uc.email=? "
				+ "group by o.type, o.status";
		return listBrowseEntries(sql, email);
	}

	public Map<String, Map<String, Long>> countValidOrgsByTypesAndCountry() {
		final String sql = "select country, type, count(*) as count from organizations o group by country, type";

		final Map<String, Map<String, Long>> map = new HashMap<>();

		for (final CountryTypeCountEntry tcc : jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(CountryTypeCountEntry.class))) {
			if (!map.containsKey(tcc.getCountry())) {
				map.put(tcc.getCountry(), new HashMap<String, Long>());
			}
			map.get(tcc.getCountry()).put(tcc.getType(), tcc.getCount());
		}
		return map;

	}

	private List<BrowseEntry> listBrowseEntries(final String sql, final Object... params) {
		final Map<String, BrowseEntry> map = new HashMap<>();

		for (final TempBrowseEntry t : jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(TempBrowseEntry.class), params)) {
			if (StringUtils.isNotBlank(t.getCode())) {
				if (!map.containsKey(t.getCode())) {
					final BrowseEntry e = new BrowseEntry();
					e.setCode(t.getCode());
					e.setName(t.getName());
					map.put(t.getCode(), e);
				}
				map.get(t.getCode()).getValues().put(t.getGroup(), t.getCount());
			}
		}

		return map.values().stream().sorted((o1, o2) -> StringUtils.compare(o1.getName(), o2.getName())).collect(Collectors.toList());
	}

	public List<OrganizationConflict> listConflictsForId(final String id) {
		final String sql =
				"select o.id, o.name, o.type, o.city, o.country from oa_conflicts c left outer join organizations o on (c.id2 = o.id) where o.id is not null and c.id1 = ? and c.reltype = 'suggested'";
		return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(OrganizationConflict.class), id);
	}

	@Transactional
	public void importDedupEvents() throws Exception {
		jdbcTemplate.update("CALL import_dedup_events();");
		// verifyConflictGroups(true);
	}

	@Transactional
	public void updateFulltextIndex() {
		try {
			log.info("Updating Fulltext Index...");
			jdbcTemplate.queryForList("SELECT refresh_index_search()");
			log.info("...done");
		} catch (final Exception e) {
			log.error("Error updating Fulltext Index", e);
		}
	}

	@Transactional
	public String fixConflictSimilars(final List<String> similarIds, final String user) {

		final List<OrganizationView> views =
				similarIds.stream().map(organizationViewRepository::findById).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());

		final List<OrganizationView> persistents = views.stream().filter(OrganizationView::getPersistent).collect(Collectors.toList());

		final OrganizationView masterOrg = new OrganizationView();

		if (persistents.size() > 1) { throw new RuntimeException("Too many persintent organizations"); }
		if (persistents.size() == 1) {
			backupOrg(persistents.get(0), user);
			masterOrg.setId(persistents.get(0).getId());
			masterOrg.setStatus(OrganizationStatus.approved.toString());
		} else {
			masterOrg.setId(null);
			masterOrg.setStatus(null);
		}
		return fixConflicts(masterOrg, views, user);
	}

	private String backupOrg(final OrganizationView org, final String user) {
		final String origId = org.getId();
		final String backupId = origId + "::" + OffsetDateTime.now().toEpochSecond();

		organizationRepository.prepareOrgWithId(backupId);
		try {

			final OrganizationView backupOrg = (OrganizationView) BeanUtils.cloneBean(org);
			backupOrg.setId(backupId);

			insertOrUpdateOrganization(backupOrg, user, false);

			organizationRepository.updateStatus(backupId, OrganizationStatus.hidden.toString());

			journalEntryRepository
					.save(new JournalEntry(origId, JournalOperations.BACKUP_ORG, "Saved a backup copy: " + backupId, user));
			journalEntryRepository
					.save(new JournalEntry(backupId, JournalOperations.BACKUP_ORG, "Saved a backup copy of " + origId, user));

			return backupId;
		} catch (final Exception e) {
			log.error("Error performing the backup of " + origId, e);
			throw new RuntimeException("Error performing the backup of " + origId, e);
		}
	}

	private String fixConflicts(final OrganizationView masterOrg, final List<OrganizationView> orgs, final String user) {

		final OffsetDateTime now = OffsetDateTime.now();

		final String finalMessage = (masterOrg.getId() == null ? "New org created merging: " : "Merging in persistent org: ") +
				orgs.stream()
						.map(OrganizationView::getId)
						.collect(Collectors.joining(",  "));

		masterOrg.setName(findFirstString(orgs, OrganizationView::getName));
		masterOrg.setType(findFirstString(orgs, OrganizationView::getType));
		masterOrg.setLat(findFirstNumber(orgs, OrganizationView::getLat));
		masterOrg.setLng(findFirstNumber(orgs, OrganizationView::getLng));
		masterOrg.setCity(findFirstString(orgs, OrganizationView::getCity));
		masterOrg.setCountry(findFirstString(orgs, OrganizationView::getCountry));
		masterOrg.setOtherIdentifiers(findAll(orgs, OrganizationView::getOtherIdentifiers));
		masterOrg.setOtherNames(findAll(orgs, OrganizationView::getOtherNames));
		masterOrg.setAcronyms(findAll(orgs, OrganizationView::getAcronyms));
		masterOrg.setUrls(findAll(orgs, OrganizationView::getUrls));
		masterOrg.setRelations(findAll(orgs, OrganizationView::getRelations, r -> !r.getType().equals(RelationType.Merged_In.toString())
				&& !r.getType().equals(RelationType.Merged_In.toString())));

		masterOrg.getOtherNames()
				.addAll(orgs.stream()
						.map(OrganizationView::getName)
						.filter(StringUtils::isNotBlank)
						.filter(s -> StringUtils.equalsIgnoreCase(s, masterOrg.getName()))
						.map(s -> new eu.dnetlib.organizations.model.view.OtherName(s, "UNKNOWN"))
						.collect(Collectors.toList()));

		final String masterId = insertOrUpdateOrganization(masterOrg, user, false);

		// I hide the merged organizations
		orgs.stream()
				.map(OrganizationView::getId)
				.filter(id -> !id.equals(masterId))
				.forEach(id -> {
					hideConflictOrgs(masterId, id);
					journalEntryRepository
							.save(new JournalEntry(id, JournalOperations.FIX_CONFLICT, "The org has been hidded and merged in " + masterId, user));
				});

		// I reassign the duplicates to the new org
		final List<OpenaireDuplicate> newDuplicates = orgs.stream()
				.map(OrganizationView::getId)
				.map(openaireDuplicateRepository::findByLocalId)
				.flatMap(List::stream)
				.map(d -> new OpenaireDuplicate(masterId, d.getOaOriginalId(), d.getRelType(), d.getOaCollectedFrom()))
				.collect(Collectors.toList());

		openaireDuplicateRepository.saveAll(newDuplicates);
		newDuplicates.forEach(d -> {
			openaireDuplicateRepository.updateCreatedByIfMissing(d.getLocalId(), d.getOaOriginalId(), user);
			openaireDuplicateRepository.updateModificationDate(d.getLocalId(), d.getOaOriginalId(), user, now);
		});

		orgs.forEach(org -> {
			final String similarId = org.getId();
			openaireConflictRepository.updateMultipleStatusAndResetGroup(similarId, SimilarityType.is_different.toString(), user, now);
		});

		for (int i = 0; i < orgs.size(); i++) {
			for (int j = i + 1; j < orgs.size(); j++) {
				openaireConflictRepository
						.updateStatusAndResetGroup(orgs.get(i).getId(), orgs.get(j).getId(), SimilarityType.is_similar.toString(), user, now);
			}
		}

		journalEntryRepository
				.save(new JournalEntry(masterId, JournalOperations.FIX_CONFLICT, finalMessage, user));

		return masterId;
	}

	@Transactional
	public void fixConflictDifferents(final List<String> differentsIds, final String user) {

		final OffsetDateTime now = OffsetDateTime.now();

		final String message = "Mark the following orgs as different: " + StringUtils.join(differentsIds, ", ");

		for (int i = 0; i < differentsIds.size(); i++) {
			for (int j = i + 1; j < differentsIds.size(); j++) {
				openaireConflictRepository
						.updateStatusAndResetGroup(differentsIds.get(i), differentsIds.get(j), SimilarityType.is_different.toString(), user, now);
			}
			journalEntryRepository.save(new JournalEntry(differentsIds.get(i), JournalOperations.NO_CONFLICT, message, user));
		}
	}

	@Transactional
	public Optional<User> findUser(final String email) {
		return userRepository.findById(email);
	}

	@Transactional
	public void updateUserDetails(final String email, final String fullname, final String organization) {
		userRepository.updateDetails(email, fullname, organization, OffsetDateTime.now());
	}

	private String findFirstString(final List<OrganizationView> views, final Function<OrganizationView, String> mapper) {
		return views.stream().map(mapper).filter(StringUtils::isNotBlank).findFirst().orElse(null);
	}

	private Double findFirstNumber(final List<OrganizationView> views, final Function<OrganizationView, Double> mapper) {
		return views.stream().map(mapper).filter(Objects::nonNull).filter(n -> n != 0).findFirst().orElse(0.0);
	}

	private <T> Set<T> findAll(final List<OrganizationView> views, final Function<OrganizationView, Set<T>> mapper) {
		return views.stream().map(mapper).flatMap(Set::stream).collect(Collectors.toCollection(LinkedHashSet::new));
	}

	private <T> Set<T> findAll(final List<OrganizationView> views, final Function<OrganizationView, Set<T>> mapper, final Predicate<T> filter) {
		return views.stream().map(mapper).flatMap(Set::stream).filter(filter).collect(Collectors.toCollection(LinkedHashSet::new));
	}

	private List<Relationship> hideConflictOrgs(final String masterId, final String otherId) {
		organizationRepository.updateStatus(otherId, OrganizationStatus.hidden.toString());
		openaireConflictRepository.findById(new OpenaireConflictPK(masterId, otherId)).ifPresent(openaireConflictRepository::delete);
		openaireConflictRepository.findById(new OpenaireConflictPK(otherId, masterId)).ifPresent(openaireConflictRepository::delete);
		return makeRelation(masterId, otherId, RelationType.Merges);
	}

	public List<String> invalidCountriesInSuggestions() {
		final String sql =
				" select distinct t.oa_country from tmp_dedup_events t left outer join countries c on (t.oa_country = c.val) where c.val is null order by t.oa_country";
		return jdbcTemplate.queryForList(sql, String.class);
	}

	public Iterable<PersistentOrganizationView> listPersistentOrgs() {
		return persistentOrganizationViewRepository.findAll();
	}

	public String addPersistentOrgs(final String id) {

		final boolean valid;
		final String ooid;

		if (id.length() == 46) {
			final Optional<OrganizationView> orgView = organizationViewRepository.findByOpenaireId(id);
			valid = orgView.map(OrganizationView::getStatus)
					.filter(s -> s.equals(OrganizationStatus.approved.toString()))
					.isPresent();
			ooid = orgView.get().getId();
		} else {
			valid = organizationRepository.findById(id)
					.map(Organization::getStatus)
					.filter(s -> s.equals(OrganizationStatus.approved.toString()))
					.isPresent();
			ooid = id;
		}

		if (valid) {
			persistentOrganizationRepository.save(new PersistentOrganization(ooid));
			return ooid;
		}
		throw new RuntimeException("The ID does not refer to an approved Organization");
	}

	public void deletePersistentOrgs(final String id) {
		persistentOrganizationRepository.deleteById(id);
	}

	public List<String> obtainLogEntries(final String id) {
		final String query = "SELECT o.id, o.name, j.operation, j.description, date(j.op_date) as op_date "
				+ "FROM organizations o JOIN journal j ON o.id = j.id "
				+ "WHERE o.id=? "
				+ "ORDER BY date(j.op_date)";
		return jdbcTemplate.queryForList(query, id)
				.stream()
				.map(this::asLogEntry)
				.collect(Collectors.toList());
	}

	public List<String> obtainLogEntries(final int year, final int month, final String country) {
		final String query = "SELECT o.id, o.name, j.operation, j.description, date(j.op_date) as op_date "
				+ "FROM organizations o JOIN journal j ON o.id = j.id "
				+ "WHERE o.country=? AND extract(year FROM j.op_date)=? AND extract(month FROM j.op_date)=? "
				+ "ORDER BY date(j.op_date), o,id";
		return jdbcTemplate.queryForList(query, country, year, month)
				.stream()
				.map(this::asLogEntry)
				.collect(Collectors.toList());

	}

	private String asLogEntry(final Map<String, Object> map) {
		final JournalOperations op = JournalOperations.valueOf("" + map.get("operation"));

		final StringWriter sw = new StringWriter();
		sw.write("On %s a curator ");

		switch (op) {
		case APPROVE_SUGG_ORG:
			sw.write("approved the suggested organisation %s (ID: %s)");
			break;
		case NEW_ORG:
			sw.write("created the organisation %s (ID: %s)");
			break;
		case NEW_SUGG_ORG:
			sw.write("suggested the organisation %s (ID: %s)");
			break;
		case EDIT_ORG:
			sw.write("updated the metadata of %s (ID: %s)");
			break;
		case EDIT_SUGG_ORG:
			sw.write("updated the metadata of the suggested organisation %s (ID: %s)");
			break;
		case DUPLICATES:
			sw.write("updated the list of duplicates for %s (ID: %s)");
			break;
		case FIX_CONFLICT:
			sw.write("chose as master the organisation %s (id: %s), which has been involved in a conflict resolution");
			break;
		case NO_CONFLICT:
			sw.write("stated that the conflict where the organisation %s (id: %s) was involved was a false positive");
			break;
		case BACKUP_ORG:
			sw.write("resolved a conflict and the organisation %s (id: %s) involved has been hidden, because another organisation has been chosen as master");
			break;
		case UNKNOWN:
		default:
			sw.write("performed an unknown operation on %s (ID: %s)");
		}
		sw.write(". \"%s\"\n");

		return String.format(sw.toString(), map.get("op_date"), map.get("name"), map.get("id"), map.get("description"));
	}

}
