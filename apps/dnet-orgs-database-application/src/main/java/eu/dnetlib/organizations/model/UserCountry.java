package eu.dnetlib.organizations.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "user_countries")
@IdClass(UserCountryPK.class)
public class UserCountry implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3493897777214583701L;

	@Id
	@Column(name = "email")
	private String email;

	@Id
	@Column(name = "country")
	private String country;

	public UserCountry() {}

	public UserCountry(final String email, final String country) {
		this.email = email;
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

}
