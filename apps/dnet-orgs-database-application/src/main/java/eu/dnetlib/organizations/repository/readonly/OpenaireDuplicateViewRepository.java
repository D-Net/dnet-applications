package eu.dnetlib.organizations.repository.readonly;

import java.util.List;

import org.springframework.stereotype.Repository;

import eu.dnetlib.organizations.model.OpenaireDuplicatePK;
import eu.dnetlib.organizations.model.view.OpenaireDuplicateView;

@Repository
public interface OpenaireDuplicateViewRepository extends ReadOnlyRepository<OpenaireDuplicateView, OpenaireDuplicatePK> {

	List<OpenaireDuplicateView> findByLocalId(String id);

}
