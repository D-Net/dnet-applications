package eu.dnetlib.organizations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.organizations.model.Acronym;
import eu.dnetlib.organizations.model.AcronymPK;

public interface AcronymRepository extends JpaRepository<Acronym, AcronymPK> {

	void deleteByOrgId(String orgId);

}
