package eu.dnetlib.organizations.model.view;

import java.io.Serializable;
import java.util.Objects;

public class RelationByOrg implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -659747760287666406L;

	private String relatedOrgId;
	private String relatedOrgName;
	private String type;

	public String getRelatedOrgId() {
		return relatedOrgId;
	}

	public void setRelatedOrgId(final String relatedOrgId) {
		this.relatedOrgId = relatedOrgId;
	}

	public String getRelatedOrgName() {
		return relatedOrgName;
	}

	public void setRelatedOrgName(final String relatedOrgName) {
		this.relatedOrgName = relatedOrgName;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		return Objects.hash(relatedOrgId, relatedOrgName, type);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof RelationByOrg)) { return false; }
		final RelationByOrg other = (RelationByOrg) obj;
		return Objects.equals(relatedOrgId, other.relatedOrgId) && Objects.equals(relatedOrgName, other.relatedOrgName) && Objects.equals(type, other.type);
	}

}
