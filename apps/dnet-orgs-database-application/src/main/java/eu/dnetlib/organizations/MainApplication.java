package eu.dnetlib.organizations;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import eu.dnetlib.common.app.AbstractDnetApp;

@SpringBootApplication
@EnableCaching
@EnableScheduling
@ComponentScan(basePackages = "eu.dnetlib")
public class MainApplication extends AbstractDnetApp {

	public static void main(final String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}

	@Bean
	public GroupedOpenApi publicApi() {
		return GroupedOpenApi.builder()
				.group("D-Net Organizations Service APIs")
				.pathsToMatch("/api/**", "/oa_api/**", "/public-api/**")
				.build();
	}

	@Override
	protected String swaggerTitle() {
		return "D-Net Organizations Service APIs";
	}
}
