package eu.dnetlib.organizations.model;

import java.io.Serializable;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "notes")
public class Note implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8193034729860131949L;

	@Id
	@Column(name = "id")
	private String orgId;

	@Column(name = "note")
	private String note;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Column(name = "modification_date")
	private OffsetDateTime modificationDate;

	public Note() {}

	public Note(final String orgId, final String note, final String modifiedBy, final OffsetDateTime modificationDate) {
		this.orgId = orgId;
		this.note = note;
		this.modifiedBy = modifiedBy;
		this.modificationDate = modificationDate;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(final String note) {
		this.note = note;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(final String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public OffsetDateTime getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(final OffsetDateTime modificationDate) {
		this.modificationDate = modificationDate;
	}

}
