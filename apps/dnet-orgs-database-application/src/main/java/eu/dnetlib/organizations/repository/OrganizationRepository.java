package eu.dnetlib.organizations.repository;

import java.time.OffsetDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import eu.dnetlib.organizations.model.Organization;

public interface OrganizationRepository extends JpaRepository<Organization, String> {

	@Modifying
	@Query("update Organization set created_by = ?2, creation_date = ?3 where id = ?1")
	void updateCreationDate(String id, String user, OffsetDateTime now);

	@Modifying
	@Query("update Organization set modified_by = ?2, modification_date = ?3 where id = ?1")
	void updateModificationDate(String id, String user, OffsetDateTime now);

	@Modifying
	@Query("update Organization set status = ?2 where id = ?1")
	void updateStatus(String id, String status);

	// to override the generation strategy of the ID
	@Modifying
	@Query(value = "insert into organizations(id) values (?1)", nativeQuery = true)
	void prepareOrgWithId(String id);

	long countByStatus(String status);

	long countByStatusAndCountry(String status, String country);

}
