package eu.dnetlib.organizations.model.utils;

import java.io.Serializable;
import java.util.Objects;

public class OrganizationConflict implements Serializable, Comparable<OrganizationConflict> {

	/**
	 *
	 */
	private static final long serialVersionUID = 310162000309825019L;

	private String id;
	private String name;
	private String type;
	private String city;
	private String country;

	public OrganizationConflict() {}

	public OrganizationConflict(final String id, final String name, final String type, final String city, final String country) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.city = city;
		this.country = country;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getCity() {
		return city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	@Override
	public int compareTo(final OrganizationConflict o) {
		return id.compareTo(o.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(city, country, id, name, type);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof OrganizationConflict)) { return false; }
		final OrganizationConflict other = (OrganizationConflict) obj;
		return Objects.equals(city, other.city) && Objects.equals(country, other.country) && Objects.equals(id, other.id) && Objects.equals(name, other.name)
			&& Objects.equals(type, other.type);
	}
}
