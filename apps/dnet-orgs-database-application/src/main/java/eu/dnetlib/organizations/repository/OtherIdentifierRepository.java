package eu.dnetlib.organizations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.organizations.model.OtherIdentifier;
import eu.dnetlib.organizations.model.OtherIdentifierPK;

public interface OtherIdentifierRepository extends JpaRepository<OtherIdentifier, OtherIdentifierPK> {

	void deleteByOrgId(String orgId);
}
