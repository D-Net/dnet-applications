package eu.dnetlib.organizations.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "oa_duplicates")
@IdClass(OpenaireDuplicatePK.class)
public class OpenaireDuplicate implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5656204073481770618L;

	@Id
	@Column(name = "local_id")
	private String localId;

	@Id
	@Column(name = "oa_original_id")
	private String oaOriginalId;

	@Column(name = "reltype")
	private String relType;

	@Column(name = "oa_collectedfrom")
	private String oaCollectedFrom;

	public OpenaireDuplicate() {}

	public OpenaireDuplicate(final String localId, final String oaOriginalId, final String relType, final String oaCollectedFrom) {
		this.localId = localId;
		this.oaOriginalId = oaOriginalId;
		this.relType = relType;
		this.oaCollectedFrom = oaCollectedFrom;
	}

	public String getLocalId() {
		return localId;
	}

	public void setLocalId(final String localId) {
		this.localId = localId;
	}

	public String getOaOriginalId() {
		return oaOriginalId;
	}

	public void setOaOriginalId(final String oaOriginalId) {
		this.oaOriginalId = oaOriginalId;
	}

	public String getRelType() {
		return relType;
	}

	public void setRelType(final String relType) {
		this.relType = relType;
	}

	public String getOaCollectedFrom() {
		return oaCollectedFrom;
	}

	public void setOaCollectedFrom(final String oaCollectedFrom) {
		this.oaCollectedFrom = oaCollectedFrom;
	}

	@Override
	public String toString() {
		return String
			.format("OpenaireDuplicate [localId=%s, oaOriginalId=%s, relType=%s, oaCollectedFrom=%s]", localId, oaOriginalId, relType, oaCollectedFrom);
	}

}
