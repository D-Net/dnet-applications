package eu.dnetlib.organizations.model.view;

import java.io.Serializable;
import java.util.Objects;

public class OtherName implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5212012138309835912L;

	private String name;

	private String lang;

	public OtherName() {}

	public OtherName(final String name, final String lang) {
		this.name = name;
		this.lang = lang;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(final String lang) {
		this.lang = lang;
	}

	@Override
	public int hashCode() {
		return Objects.hash(lang, name);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof OtherName)) { return false; }
		final OtherName other = (OtherName) obj;
		return Objects.equals(lang, other.lang) && Objects.equals(name, other.name);
	}

}
