package eu.dnetlib.organizations.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "other_names")
@IdClass(OtherNamePK.class)
public class OtherName implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5212012138309835912L;

	@Id
	@Column(name = "id")
	private String orgId;

	@Id
	@Column(name = "name")
	private String name;

	@Id
	@Column(name = "lang")
	private String lang;

	public OtherName() {}

	public OtherName(final String orgId, final String name, final String lang) {
		this.orgId = orgId;
		this.name = name;
		this.lang = lang;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(final String lang) {
		this.lang = lang;
	}
}
