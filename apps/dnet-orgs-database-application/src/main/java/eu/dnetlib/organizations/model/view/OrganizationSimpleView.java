package eu.dnetlib.organizations.model.view;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.vladmihalcea.hibernate.type.array.StringArrayType;

@Entity
@Table(name = "organizations_simple_view")
@TypeDefs({
	@TypeDef(name = "string-array", typeClass = StringArrayType.class)
})
public class OrganizationSimpleView implements Serializable, Comparable<OrganizationSimpleView> {

	/**
	 *
	 */
	private static final long serialVersionUID = 417248897119259446L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "type")
	private String type;

	@Column(name = "city")
	private String city;

	@Column(name = "country")
	private String country;

	@Type(type = "string-array")
	@Column(name = "acronyms", columnDefinition = "text[]")
	private String[] acronyms;

	@Type(type = "string-array")
	@Column(name = "urls", columnDefinition = "text[]")
	private String[] urls;

	@Column(name = "status")
	private String status;

	@Column(name = "n_similar_dups")
	private Long nSimilarDups;

	@Column(name = "n_suggested_dups")
	private Long nSuggestedDups;

	@Column(name = "n_different_dups")
	private Long nDifferentDups;

	public OrganizationSimpleView() {}

	public OrganizationSimpleView(final String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getCity() {
		return city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	public String[] getAcronyms() {
		return acronyms;
	}

	public void setAcronyms(final String[] acronyms) {
		this.acronyms = acronyms;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String[] getUrls() {
		return urls;
	}

	public void setUrls(final String[] urls) {
		this.urls = urls;
	}

	public Long getnSimilarDups() {
		return nSimilarDups;
	}

	public void setnSimilarDups(final Long nSimilarDups) {
		this.nSimilarDups = nSimilarDups;
	}

	public Long getnSuggestedDups() {
		return nSuggestedDups;
	}

	public void setnSuggestedDups(final Long nSuggestedDups) {
		this.nSuggestedDups = nSuggestedDups;
	}

	public Long getnDifferentDups() {
		return nDifferentDups;
	}

	public void setnDifferentDups(final Long nDifferentDups) {
		this.nDifferentDups = nDifferentDups;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof OrganizationSimpleView)) { return false; }
		final OrganizationSimpleView other = (OrganizationSimpleView) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public int compareTo(final OrganizationSimpleView o) {
		return id.compareTo(o.getId());
	}

}
