package eu.dnetlib.organizations.importer;

public enum ImportStatus {
	SUCCESS,
	FAILED,
	RUNNING,
	NOT_LAUNCHED,
	NOT_YET_STARTED
}
