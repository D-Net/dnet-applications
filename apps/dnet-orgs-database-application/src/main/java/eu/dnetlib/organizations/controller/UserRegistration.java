package eu.dnetlib.organizations.controller;

import java.io.Serializable;
import java.util.List;

public class UserRegistration implements Serializable {

	private static final long serialVersionUID = 4872798305803491565L;

	private String referencePerson;

	private String requestMessage;

	private List<String> countries;

	public String getReferencePerson() {
		return referencePerson;
	}

	public void setReferencePerson(final String referencePerson) {
		this.referencePerson = referencePerson;
	}

	public String getRequestMessage() {
		return requestMessage;
	}

	public void setRequestMessage(final String requestMessage) {
		this.requestMessage = requestMessage;
	}

	public List<String> getCountries() {
		return countries;
	}

	public void setCountries(final List<String> countries) {
		this.countries = countries;
	}

}
