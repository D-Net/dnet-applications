package eu.dnetlib.organizations.controller;

public enum UserRole {
	USER, ADMIN, NATIONAL_ADMIN, PENDING, NOT_AUTHORIZED
}
