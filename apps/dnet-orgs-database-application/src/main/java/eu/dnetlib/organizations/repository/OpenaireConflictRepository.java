package eu.dnetlib.organizations.repository;

import java.time.OffsetDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import eu.dnetlib.organizations.model.OpenaireConflict;
import eu.dnetlib.organizations.model.OpenaireConflictPK;

@Repository
public interface OpenaireConflictRepository extends JpaRepository<OpenaireConflict, OpenaireConflictPK> {

	Iterable<OpenaireConflict> findById1AndGroupIsNull(String id);

	Iterable<OpenaireConflict> findById2AndGroupIsNull(String id);

	@Modifying
	@Query(value = "update oa_conflicts set idgroup = null", nativeQuery = true)
	void resetGroupIds();

	@Modifying
	@Query(value = "update oa_conflicts set idgroup = null, reltype = ?3, modified_by = ?4, modification_date = ?5 where (id1 = ?1 and id2 = ?2) or (id1 = ?2 and id2 = ?1)", nativeQuery = true)
	void updateStatusAndResetGroup(String id1, String id2, String status, String user, OffsetDateTime now);

	long countByGroupNull();

	@Modifying
	@Query(value = "update oa_conflicts set idgroup = null, reltype = ?2, modified_by = ?3, modification_date = ?4 where id1 = ?1 or id2 = ?1", nativeQuery = true)
	void updateMultipleStatusAndResetGroup(String id, String type, String user, OffsetDateTime now);

}
