#!/bin/bash

CACERT_DIR=/Users/michele/.jenv/versions/1.8/jre/lib/security/cacerts

echo "Importing HTTPS Cerificates in $CACERT_DIR" 

TMP_FILE=`mktemp /tmp/cert.XXXXXX`
echo -n | openssl s_client -connect openaire-dev.aai-dev.grnet.gr:443 | openssl x509 > $TMP_FILE
keytool -import -alias example -keystore $CACERT_DIR -file $TMP_FILE

echo
echo "Done."
echo
