package eu.dnetlib.data.mdstore.manager.utils.zeppelin;

import java.util.Map;

public class MapResponse {

	private String status;
	private String message;
	private Map<String, String> body;

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public Map<String, String> getBody() {
		return body;
	}

	public void setBody(final Map<String, String> body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return String.format("Response [status=%s, message=%s, body=%s]", status, message, body);
	}
}
