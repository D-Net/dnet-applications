package eu.dnetlib.data.mdstore.manager.utils.zeppelin;

public interface HasStatus {

	String getStatus();
}
