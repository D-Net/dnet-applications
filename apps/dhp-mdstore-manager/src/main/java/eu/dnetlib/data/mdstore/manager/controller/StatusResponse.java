package eu.dnetlib.data.mdstore.manager.controller;

public class StatusResponse {

	public static final StatusResponse DELETED = new StatusResponse("DELETED");
	public static final StatusResponse DELETING = new StatusResponse("DELETING...");
	public static final StatusResponse ABORTED = new StatusResponse("ABORTED");;

	private String status;

	public StatusResponse() {}

	public StatusResponse(final String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

}
