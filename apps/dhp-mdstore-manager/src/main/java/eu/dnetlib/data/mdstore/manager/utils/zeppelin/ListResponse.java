package eu.dnetlib.data.mdstore.manager.utils.zeppelin;

import java.util.List;
import java.util.Map;

public class ListResponse implements HasStatus {

	private String status;
	private String message;
	private List<Map<String, String>> body;

	@Override
	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public List<Map<String, String>> getBody() {
		return body;
	}

	public void setBody(final List<Map<String, String>> body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return String.format("Response [status=%s, message=%s, body=%s]", status, message, body);
	}
}
