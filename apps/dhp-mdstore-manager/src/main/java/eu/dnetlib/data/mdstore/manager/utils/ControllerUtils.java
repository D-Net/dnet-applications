package eu.dnetlib.data.mdstore.manager.utils;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

public class ControllerUtils {

	private static final Logger log = LoggerFactory.getLogger(ControllerUtils.class);

	public static ModelAndView errorPage(final String title, final Throwable e) {
		log.debug(e.getMessage(), e);
		final ModelAndView mv = new ModelAndView();
		mv.setViewName("error");
		mv.addObject("title", title);
		mv.addObject("error", e.getMessage());
		mv.addObject("stacktrace", ExceptionUtils.getStackTrace(e));
		return mv;

	}
}
