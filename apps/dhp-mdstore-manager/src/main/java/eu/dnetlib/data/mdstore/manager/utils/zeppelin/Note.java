package eu.dnetlib.data.mdstore.manager.utils.zeppelin;

public class Note {

	private String name;

	public Note() {}

	public Note(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

}
