package eu.dnetlib.data.mdstore.manager.utils.zeppelin;

import java.util.LinkedHashMap;
import java.util.Map;

public class Paragraph {

	private String title;
	private String text;
	private int index = 0;
	private Map<String, Object> config = new LinkedHashMap<>();

	public Paragraph() {}

	public Paragraph(final String title, final String text, final int index) {
		this.title = title;
		this.text = text;
		this.index = index;
		this.config.put("title", true);
		this.config.put("enabled", true);
		this.config.put("editorHide", true);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(final int index) {
		this.index = index;
	}

	public Map<String, Object> getConfig() {
		return config;
	}

	public void setConfig(final Map<String, Object> config) {
		this.config = config;
	}

}
