package eu.dnetlib.data.mdstore.manager.utils.zeppelin;

public class SimpleResponse implements HasStatus {

	private final String status;

	public SimpleResponse(final String status) {
		this.status = status;
	}

	@Override
	public String getStatus() {
		return status;
	}

}
