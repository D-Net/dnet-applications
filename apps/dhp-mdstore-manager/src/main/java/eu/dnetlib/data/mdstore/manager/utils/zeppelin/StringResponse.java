package eu.dnetlib.data.mdstore.manager.utils.zeppelin;

public class StringResponse implements HasStatus {

	private String status;
	private String message;
	private String body;

	@Override
	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public String getBody() {
		return body;
	}

	public void setBody(final String body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return String.format("Response [status=%s, message=%s, body=%s]", status, message, body);
	}
}
