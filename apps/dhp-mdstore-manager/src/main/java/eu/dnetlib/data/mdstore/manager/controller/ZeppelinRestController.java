package eu.dnetlib.data.mdstore.manager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.controller.AbstractDnetController;
import eu.dnetlib.data.mdstore.manager.exceptions.MDStoreManagerException;
import eu.dnetlib.data.mdstore.manager.utils.ZeppelinClient;

@RestController
public class ZeppelinRestController extends AbstractDnetController {

	@Autowired
	private ZeppelinClient zeppelinClient;

	@GetMapping("/zeppelin/templates")
	public List<String> getTemplates() throws MDStoreManagerException {
		try {
			// if (zeppelinClient.get)
			return zeppelinClient.listTemplates();
		} catch (final Throwable e) {
			throw new MDStoreManagerException("Zeppelin is unreachable", e);
		}
	}

}
