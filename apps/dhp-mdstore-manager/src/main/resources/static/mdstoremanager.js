var app = angular.module('mdstoreManagerApp', []);			

app.controller('mdstoreManagerController', function($scope, $http) {			
	$scope.mdstores = [];			
	$scope.versions = [];			
	$scope.openMdstore = '';			
	$scope.openCurrentVersion = ''			
	$scope.zeppelinTemplates = [];			
				
	$scope.forceVersionDelete = false;			
				
	$scope.reload = function() {			
		$http.get('./mdstores/?' + $.now()).then(function successCallback(res) {			
			$scope.mdstores = res.data;			
		}, function errorCallback(res) {			
			alert('ERROR: ' + res.data.message);			
		});			
	};
	
	$scope.obtainZeppelinTemplates = function() {			
		$http.get('./zeppelin/templates?' + $.now()).then(function successCallback(res) {			
			$scope.zeppelinTemplates = res.data;			
		}, function errorCallback(res) {			
			alert('ERROR: ' + res.data.message);			
		});			
	};
	
					
	$scope.newMdstore = function(format, layout, interpretation, dsName, dsId, apiId) {			
		var url = './mdstores/new/' + encodeURIComponent(format) + '/' + encodeURIComponent(layout) + '/' + encodeURIComponent(interpretation);			
		if (dsName || dsId || apiId) {			
			url += '?dsName=' + encodeURIComponent(dsName) + '&dsId=' + encodeURIComponent(dsId) + '&apiId=' + encodeURIComponent(apiId);			
		}			
		$http.get(url).then(function successCallback(res) {			
			$scope.reload();			
		}, function errorCallback(res) {			
			alert('ERROR: ' + res.data.message);			
		});			
	};			
	 					
	$scope.deleteMdstore = function(mdId) {			
		if (confirm("Are you sure ?")) {			
			$http.delete('./mdstores/mdstore/' + mdId).then(function successCallback(res) {			
				$scope.reload();			
			}, function errorCallback(res) {			
				alert('ERROR: ' + res.data.message);			
			});			
		}			
	};			

	$scope.prepareVersion = function(mdId, currentVersion) {			
		$scope.versions = [];			
		$http.get('./mdstores/mdstore/' + mdId + '/newVersion?' + $.now()).then(function successCallback(res) {			
			$scope.reload();			
			$scope.listVersions(mdId, currentVersion);			
		}, function errorCallback(res) {			
			alert('ERROR: ' + res.data.message);			
		});			
	};			

	$scope.commitVersion = function(versionId) {			
		var size = parseInt(prompt("New Size", "0"));			
		if (size >= 0) {			
			$http.get("./mdstores/version/" + versionId + "/commit/" + size + '?' + $.now()).then(function successCallback(res) {			
				$scope.reload();			
				$scope.openCurrentVersion = versionId;			
				$scope.refreshVersions();			
			}, function errorCallback(res) {			
				alert('ERROR: ' + res.data.message);			
			});			
		}			
	};			

	$scope.abortVersion = function(versionId) {			
		$http.get("./mdstores/version/" + versionId + "/abort?" + $.now()).then(function successCallback(res) {			
			$scope.reload();			
			$scope.refreshVersions();			
		}, function errorCallback(res) {			
			alert('ERROR: ' + res.data.message);			
		});			
	};			

	$scope.resetReading = function(versionId) {			
		$http.get("./mdstores/version/" + versionId + "/resetReading" + '?' + $.now()).then(function successCallback(res) {			
			$scope.reload();			
			$scope.refreshVersions();			
		}, function errorCallback(res) {			
			alert('ERROR: ' + res.data.message);			
		});			
	};			
				
	$scope.listVersions = function(mdId, current) {			
		$scope.openMdstore = mdId;			
		$scope.openCurrentVersion = current;			
		$scope.versions = [];			
		$scope.refreshVersions();			
	};			
				
	$scope.refreshVersions = function() {			
		$http.get('./mdstores/mdstore/' + $scope.openMdstore + '/versions?' + $.now()).then(function successCallback(res) {			
			angular.forEach(res.data, function(value, key) {			
				value.current = (value.id == $scope.openCurrentVersion);			
			});			
			$scope.versions = res.data;			
		}, function errorCallback(res) {			
			alert('ERROR: ' + res.data.message);			
		});			
	};			
				
	$scope.deleteVersion = function(versionId, force) {			
		if (confirm("Are you sure ?")) {			
			var url = './mdstores/version/' + versionId;			
			if (force) { url += '?force=true'; }			
									
			$http.delete(url).then(function successCallback(res) {			
				$scope.reload();			
				$scope.refreshVersions();			
			}, function errorCallback(res) {			
				alert('ERROR: ' + res.data.message);			
			});			
		}			
	};			
				
	$scope.reload();			
	$scope.obtainZeppelinTemplates();
});
