package eu.dnetlib.scholix.api.index;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * The type Elastic search properties.
 */
@Component("elasticSearchProperties")
@ConfigurationProperties(prefix = "scholix.elastic")
public class ElasticSearchProperties extends GenericObjectPoolConfig {

	@NotNull
	private String clusterNodes;
	@NotNull
	private String indexName;
	@NotNull
	private long connectionTimeout;
	@NotNull
	private long socketTimeout;

	/**
	 * Gets cluster nodes.
	 *
	 * @return the cluster nodes
	 */
	public String getClusterNodes() {
		return clusterNodes;
	}

	/**
	 * Sets cluster nodes.
	 *
	 * @param clusterNodes the cluster nodes
	 * @return the cluster nodes
	 */
	public ElasticSearchProperties setClusterNodes(String clusterNodes) {
		this.clusterNodes = clusterNodes;
		return this;
	}

	/**
	 * Gets index name.
	 *
	 * @return the index name
	 */
	public String getIndexName() {
		return indexName;
	}

	/**
	 * Sets index name.
	 *
	 * @param indexName the index name
	 * @return the index name
	 */
	public ElasticSearchProperties setIndexName(String indexName) {
		this.indexName = indexName;
		return this;
	}

	/**
	 * Gets connection timeout.
	 *
	 * @return the connection timeout
	 */
	public long getConnectionTimeout() {
		return connectionTimeout;
	}

	/**
	 * Sets connection timeout.
	 *
	 * @param connectionTimeout the connection timeout
	 * @return the connection timeout
	 */
	public ElasticSearchProperties setConnectionTimeout(long connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
		return this;
	}

	/**
	 * Gets socket timeout.
	 *
	 * @return the socket timeout
	 */
	public long getSocketTimeout() {
		return socketTimeout;
	}

	/**
	 * Sets socket timeout.
	 *
	 * @param socketTimeout the socket timeout
	 * @return the socket timeout
	 */
	public ElasticSearchProperties setSocketTimeout(long socketTimeout) {
		this.socketTimeout = socketTimeout;
		return this;
	}
}
