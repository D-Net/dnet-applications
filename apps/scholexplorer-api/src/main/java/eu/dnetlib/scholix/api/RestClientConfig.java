package eu.dnetlib.scholix.api;

import eu.dnetlib.scholix.api.index.ElasticSearchPool;
import eu.dnetlib.scholix.api.index.ElasticSearchProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RestClientConfig  {

    @Autowired
    private ElasticSearchProperties elasticSearchProperties;


    @Bean
    public ElasticSearchPool connectionPool() {

        elasticSearchProperties.setMaxIdle(5);
        elasticSearchProperties.setMaxTotal(10);
        ElasticSearchPool pool = new ElasticSearchPool(elasticSearchProperties);
        return pool;
    }


}
