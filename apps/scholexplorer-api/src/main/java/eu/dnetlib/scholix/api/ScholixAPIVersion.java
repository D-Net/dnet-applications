package eu.dnetlib.scholix.api;

public enum ScholixAPIVersion {

    V1,
    V2

}
