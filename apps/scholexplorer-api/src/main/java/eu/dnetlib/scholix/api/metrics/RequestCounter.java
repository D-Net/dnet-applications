package eu.dnetlib.scholix.api.metrics;

import eu.dnetlib.common.metrics.MetricInfo;
import org.springframework.stereotype.Component;


@Component("scholexplorer_test")
public class RequestCounter implements MetricInfo {
    @Override
    public double obtainValue() {
        return 0L;
    }
}
