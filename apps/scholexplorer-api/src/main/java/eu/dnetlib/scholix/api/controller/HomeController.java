package eu.dnetlib.scholix.api.controller;


import eu.dnetlib.common.controller.AbstractDnetController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController extends AbstractDnetController {

    @GetMapping({
            "/doc", "/swagger"
    })
    public String apiDoc() {
        return "redirect:swagger-ui/index.html";
    }

    @GetMapping({
            "/v1/ui"
    })
    public String v1Doc() {
        return "redirect:/swagger-ui/index.html?urls.primaryName=Scholexplorer%20API%20V1.0";
    }


    @GetMapping({
            "/v2/ui"
    })
    public String v2Doc() {
        return "redirect:/swagger-ui/index.html?urls.primaryName=Scholexplorer%20API%20V2.0";
    }





}
