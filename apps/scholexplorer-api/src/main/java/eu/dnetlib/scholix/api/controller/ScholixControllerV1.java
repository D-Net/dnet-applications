package eu.dnetlib.scholix.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.controller.AbstractDnetController;
import eu.dnetlib.dhp.schema.sx.api.model.v1.ScholixV1;
import eu.dnetlib.dhp.schema.sx.scholix.Scholix;
import eu.dnetlib.scholix.api.ScholixException;
import eu.dnetlib.scholix.api.index.ScholixIndexManager;
import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/v1")
@Tag(name = "Scholix")
public class ScholixControllerV1 extends AbstractDnetController {

	@Autowired
	ScholixIndexManager manager;

	@Operation(summary = "Get all Scholix relation collected from a publisher", description = "return a list of scholix object published from a specific publisher")
	@GetMapping("/linksFromPublisher")
	@Timed(value = "scholix.v1.linksFromPublisher", description = "Time taken to return links on Version 1.0 of Scholix collected from a publisher")
	public List<ScholixV1> linksFromPublisher(

			@Parameter(in = ParameterIn.QUERY, description = "Filter Scholix relationships collected from a publisher",  required = true) final String publisher,
			@Parameter(in = ParameterIn.QUERY, description = "The page number") @RequestParam(required = false) final Integer page) throws ScholixException {

		final int currentPage = page != null ? page : 0;

		final Pair<Long, List<Scholix>> scholixResult = manager.linksFromPid(null, null, null, publisher, null, null, null, null, null, currentPage);
		final List<Scholix> scholixData = scholixResult.getValue();
		if (scholixData == null) { return null; }
		return scholixData.stream().map(ScholixV1::fromScholix).collect(Collectors.toList());
	}

	@Operation(summary = "Get all Scholix relation collected from a datasource", description = "return a list of scholix object collected from a specific datasource")
	@GetMapping("/linksFromDatasource")
	@Timed(value = "scholix.v1.linksFromDatasource", description = "Time taken to return links on Version 1.0 of Scholix collected from a LinkProvider")
	public List<ScholixV1> linksFromDatasource(
		@Parameter(in = ParameterIn.QUERY, description = "Filter Scholix relationships collected from a LinkProvider") @NotNull final String datasource,
		@Parameter(in = ParameterIn.QUERY, description = "The page number") @RequestParam(required = false) final Integer page) throws ScholixException {

		final int currentPage = page != null ? page : 0;
		final Pair<Long, List<Scholix>> scholixResult = manager.linksFromPid(datasource, null, null, null, null, null, null, null, null, currentPage);
		final List<Scholix> scholixData = scholixResult.getValue();
		if (scholixData == null) { return null; }
		return scholixData.stream().map(ScholixV1::fromScholix).collect(Collectors.toList());
	}



	@Operation(summary = "Retrieve all scholix links from a persistent identifier", description = "The linksFromPid endpoint returns a list of scholix object related from a specific persistent identifier")
	@GetMapping("/linksFromPid")
	@Timed(value = "scholix.v1.linksFromPid", description = "Time taken to return links on Version 1.0 of Scholix related from a specific persistent identifier")
	public List<ScholixV1> linksFromPid(
		@Parameter(in = ParameterIn.QUERY, description = "persistent Identifier") @NotNull final String pid,
		@Parameter(in = ParameterIn.QUERY, description = "Persistent Identifier Type") @RequestParam(required = false) final String pidType,
		@Parameter(in = ParameterIn.QUERY, description = "typology target filter should be publication, dataset or unknown") @RequestParam(required = false) final String typologyTarget,
		@Parameter(in = ParameterIn.QUERY, description = "a datasource provenance filter of the target relation") @RequestParam(required = false) final String datasourceTarget,
		@Parameter(in = ParameterIn.QUERY, description = "The page number") @RequestParam(required = false) final Integer page) throws ScholixException {

		final int currentPage = page != null ? page : 0;
		final Pair<Long, List<Scholix>> scholixResult =
			manager.linksFromPid(datasourceTarget, null, null, null, typologyTarget, pid, pidType, null, null, currentPage);
		final List<Scholix> scholixData = scholixResult.getValue();
		if (scholixData == null) { return null; }
		return scholixData.stream().map(ScholixV1::fromScholix).collect(Collectors.toList());
	}

}
