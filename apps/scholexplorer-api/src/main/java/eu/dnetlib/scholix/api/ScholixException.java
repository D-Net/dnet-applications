package eu.dnetlib.scholix.api;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ScholixException extends Exception{

    private static final long serialVersionUID = -3414428892721711308L;


    public ScholixException() {
        super();
    }

    public ScholixException(String message, Throwable cause, boolean enableSuppression,
                                   boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ScholixException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScholixException(String message) {
        super(message);
    }

    public ScholixException(Throwable cause) {
        super(cause);
    }


}
