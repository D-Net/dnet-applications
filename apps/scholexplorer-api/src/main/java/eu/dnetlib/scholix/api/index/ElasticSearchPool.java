package eu.dnetlib.scholix.api.index;

import org.apache.commons.lang3.tuple.Pair;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;

/**
 * The type Elastic search pool.
 */
public class ElasticSearchPool extends Pool<Pair<RestHighLevelClient, ElasticsearchRestTemplate>> {

    private final ElasticSearchProperties elasticSearchProperties;

    /**
     * Instantiates a new Elastic search pool.
     *
     * @param elasticSearchProperties the elastic search properties
     */
    public ElasticSearchPool(ElasticSearchProperties elasticSearchProperties){
        super(elasticSearchProperties, new ElasticSearchClientFactory(elasticSearchProperties));
        this.elasticSearchProperties = elasticSearchProperties;
    }

    /**
     * Gets elastic search properties.
     *
     * @return the elastic search properties
     */
    public ElasticSearchProperties getElasticSearchProperties() {
        return elasticSearchProperties;
    }
}


