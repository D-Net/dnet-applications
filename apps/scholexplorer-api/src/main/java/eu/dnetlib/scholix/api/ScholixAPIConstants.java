package eu.dnetlib.scholix.api;

public class ScholixAPIConstants {

    public static final String API_V1_NAME = "Scholexplorer API V1.0";
    public static final String API_V2_NAME = "Scholexplorer API V2.0";

    public static String API_DESCRIPTION =" <p style=\"text-align:center;\"><img src=\"/logo.png\" alt=\"ScholeXplorer\"> </p>" +
            "The Scholix Swagger API allows clients to run REST queries over the Scholexplorer index in order to fetch links matching given criteria. In the current version, clients can search for:" +
            "<ul><li>Links whose source object has a given PID or PID type</li>" +
            "<li>Links whose source object has been published by a given data source (\"data source as publisher\")</li>" +
            "<li>Links that were collected from a given data source (\"data source as provider\").</li></ul>";


    public static  String SCHOLIX_MANAGER_COUNTER_NAME= "scholixLinkCounter";
    public static final String SCHOLIX_MANAGER_TAG_NAME = "links";

    public static String SCHOLIX_COUNTER_PREFIX = "scholix";




}
